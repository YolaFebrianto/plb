<?php 
include 'db-inc2.php';
$no_aju = $_POST['aju'];
$aksi = $_POST['aksi'];
$no_ajub = $_POST['no_ajub'];
$no_ajuc = $_POST['no_ajuc'];
$no_ajud = $_POST['no_ajud'];
$jenis_laporan = $_POST['jenis_laporan'];
$laporan = $_POST['laporan'];
$no_dok = $_POST['no_dok'];
$tgl_dok = $_POST['tgl_dok'];
$seri_barang = $_POST['seri_barang'];
$kd_brg = $_POST['kd_brg'];
$nm_brg = $_POST['nm_brg'];
$jml = $_POST['jml'];
$satuan = $_POST['satuan'];
$no_ajus = $_POST['no_aju'];
$tglan1 = $_POST['tglan1'];
$nopen = $_POST['nopen'];
$tglan2 = $_POST['tglan2'];
$no_npe = $_POST['no_npe'];
$tglan3 = $_POST['tglan3'];
$penerima = $_POST['penerima'];
$negara = $_POST['negara'];
$hs = $_POST['hs'];
$cif = $_POST['cif'];
$currency = $_POST['cur'];
$batch_no = $_POST['batch_no'];
$pallet = $_POST['pallet'];
$user = $_POST['user'];
$no_aju_brgu = $_POST['no_aju_brgu'];
$seri_barang_u = $_POST['seri_barang_u'];
$kode_barang_u = $_POST['kode_barang_u'];
$nama_barang_u = $_POST['nama_barang_u'];
$jml_u = $_POST['jml_u'];
$sat_u = $_POST['sat_u'];
$cif_u = $_POST['cif_u'];
$cur_u = $_POST['cur_u'];
$no_aju_dtlu = $_POST['no_aju_dtlu'];
$no_doc_dtlu = $_POST['no_doc_dtlu'];
$tgl_doc_u = $_POST['tgl_doc_u'];
$jenis_doc_u = $_POST['jenis_doc_u'];
$id_dtl_u = $_POST['id_dtl_u'];
$no_aju_dok_u = $_POST['no_aju_dok_u'];
$no_doc_dok_u = $_POST['no_doc_dok_u'];
$batch_no_dok_u = $_POST['batch_no_dok_u'];
$pallet_dok_u = $_POST['pallet_dok_u'];
$id_brgdtl_u = $_POST['id_brgdtl_u'];
$kode_rel = $_POST['kode_rel'];

//////////////////////////////////////////////////// MENAMPILKAN DATA REPORT.PLB_DOC_IN27_DTL ///////////////////////////////////
if ($aksi == 'tampil'){
?>
<table class="table">
	 <thead>
      <tr>
	    <th>No</th>
		<th>Jenis DOC</th>
		<th>Tanggal DOC</th>
		<th>No DOC</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$data = pg_query("select * from report.plb_doc_in27_dtl where no_aju ='$no_aju'");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	} $no = 0;
	while($d=pg_fetch_array($data)){
		$no += 1;
	?>
	<tr>
	    <td><?php echo $no; ?></td>
		<td><?php echo $d['jenis_doc']; ?></td>
		<td><?php echo $d['tgl_doc']; ?></td>
		<td><?php echo $d['no_doc']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-dtl" id="<?php echo $d['no_aju']."#".$d['jenis_doc']."#".$d['no_doc']."#".$d['tgl_doc']."#".$d['id_detail']; ?>" data-toggle="modal" data-target="#upd_dtl" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-dtl" id="<?php echo $d['no_aju']."-".$d['id_detail']; ?>" type="button">Hapus</button> </td>
	</tr>
	<?php } ?>
	</tbody>
</table>
<?php
}

///////////////////////////////////////////////// HAPUS DETIL BC 27 IN ////////////////////////////////////////////////////////
else if ($aksi == 'hapus'){
	$id = $_POST['kunci'];
    list($kd1,$kd2) = split ('[/.-]',$id);
	
	$query = pg_query("delete from report.plb_doc_in27_dtl where no_aju='$kd1' and id_detail='$kd2'");
	?>
	<table class="table">
	 <thead>
      <tr>
	    <th>No</th>
		<th>Jenis DOC</th>
		<th>Tanggal DOC</th>
		<th>No DOC</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php
    $data = pg_query("select * from report.plb_doc_in27_dtl where no_aju ='$kd1'");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	} $no = 0;
	while($d=pg_fetch_array($data)){
		$no += 1;
	?>
	<tr>
	    <td><?php echo $no; ?></td>
		<td><?php echo $d['jenis_doc']; ?></td>
		<td><?php echo $d['tgl_doc']; ?></td>
		<td><?php echo $d['no_doc']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-dtl" id="<?php echo $d['no_aju']."#".$d['jenis_doc']."#".$d['no_doc']."#".$d['tgl_doc']."#".$d['id_detail']; ?>" data-toggle="modal" data-target="#upd_dtl" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-dtl" id="<?php echo $d['no_aju']."-".$d['id_detail']; ?>" type="button">Hapus</button> </td>
	</tr>
	<?php } ?>
	</tbody>
</table>

<?php
}

///////////////////////////////////////////////////////////// SIMPAN DTL /////////////////////////////////////////////////////////
else if ($aksi == 'save-dtl'){
	
	$query = pg_query("INSERT INTO report.plb_doc_in27_dtl(
                       no_aju, jenis_doc, tgl_doc, no_doc, id_detail)
                       VALUES ('$no_ajub', '$jenis_laporan', '$tgl_dok', '$no_dok', RANDOM()*(500-1)+1)");
	?>
	<table class="table">
	 <thead>
      <tr>
	    <th>No</th>
		<th>Jenis DOC</th>
		<th>Tanggal DOC</th>
		<th>No DOC</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php
    $data = pg_query("select * from report.plb_doc_in27_dtl where no_aju ='$no_ajub'");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	} $no = 0;
	while($d=pg_fetch_array($data)){
		$no += 1;
	?>
	<tr>
	    <td><?php echo $no; ?></td>
		<td><?php echo $d['jenis_doc']; ?></td>
		<td><?php echo $d['tgl_doc']; ?></td>
		<td><?php echo $d['no_doc']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-dtl" id="<?php echo $d['no_aju']."#".$d['jenis_doc']."#".$d['no_doc']."#".$d['tgl_doc']."#".$d['id_detail']; ?>" data-toggle="modal" data-target="#upd_dtl" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-dtl" id="<?php echo $d['no_aju']."-".$d['id_detail']; ?>" type="button">Hapus</button> </td>
	</tr>
	<?php } ?>
	</tbody>
</table>

<?php
}

///////////////////////////////////////////////// UPDATE DETIL BC 27 IN ////////////////////////////////////////////////////////
else if ($aksi == 'upd-dtl'){
	
	$query = pg_query("UPDATE report.plb_doc_in27_dtl
                       SET jenis_doc='$jenis_doc_u', tgl_doc='$tgl_doc_u', no_doc='$no_doc_dtlu'
                       WHERE no_aju='$no_aju_dtlu' and id_detail ='$id_dtl_u'");
	?>
	<table class="table">
	 <thead>
      <tr>
	    <th>No</th>
		<th>Jenis DOC</th>
		<th>Tanggal DOC</th>
		<th>No DOC</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php
    $data = pg_query("select * from report.plb_doc_in27_dtl where no_aju ='$no_aju_dtlu'");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	} $no = 0;
	while($d=pg_fetch_array($data)){
		$no += 1;
	?>
	<tr>
	    <td><?php echo $no; ?></td>
		<td><?php echo $d['jenis_doc']; ?></td>
		<td><?php echo $d['tgl_doc']; ?></td>
		<td><?php echo $d['no_doc']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-dtl" id="<?php echo $d['no_aju']."#".$d['jenis_doc']."#".$d['no_doc']."#".$d['tgl_doc']."#".$d['id_detail']; ?>" data-toggle="modal" data-target="#upd_dtl" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-dtl" id="<?php echo $d['no_aju']."-".$d['id_detail']; ?>" type="button">Hapus</button> </td>
	</tr>
	<?php } ?>
	</tbody>
</table>

<?php
}

/////////////////////////////////// MENAMPILKAN DATA BARANG REPORT.PLB_DOC_BC27_BRG ///////////////////////////////////
else if ($aksi == 'tampil2'){
?>
<table class="table">
	 <thead>
      <tr>
		<th>Seri Barang</th>
		<th>Kode Barang</th>
		<th>Nama Barang</th>
		<th>Jumlah</th>
		<th>Satuan</th>
		<th>CIF</th>
		<th>Currency</t
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$data = pg_query("select * from report.plb_doc_bc27_brg where no_aju ='$no_aju' order by seri_barang ASC");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="6" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	}
	while($d=pg_fetch_array($data)){
	?>
	<tr>
		<td><?php echo $d['seri_barang']; ?></td>
		<td><?php echo $d['kode_barang']; ?></td>
		<td><?php echo $d['nama_barang']; ?></td>
		<td><?php echo $d['jumlah']; ?></td>
		<td><?php echo $d['satuan']; ?></td>
		<td><?php echo $d['cif']; ?></td>
		<td><?php echo $d['currency']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-brg" id="<?php echo $d['no_aju']."#".$d['seri_barang']."#".$d['kode_barang']."#".$d['nama_barang']."#".$d['jumlah']."#".$d['satuan']."#".$d['cif']."#".$d['currency']; ?>" data-toggle="modal" data-target="#upd_brg" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-brg" id="<?php echo $d['no_aju']."-".$d['seri_barang']; ?>" type="button">Hapus</button>
		</td>
	</tr>
	<?php } ?>
	</tbody>
</table>
<?php
}

/////////////////////////////////// UPDATE BARANG REPORT.PLB_DOC_BC27_BRG ///////////////////////////////////
else if ($aksi == 'upd-brg'){
	
	$query = pg_query("UPDATE report.plb_doc_bc27_brg
                       SET kode_barang='$kode_barang_u', nama_barang='$nama_barang_u', jumlah='$jml_u', 
                       satuan='$sat_u', currency='$cur_u', cif='$cif_u'
                       WHERE no_aju = '$no_aju_brgu' and seri_barang = '$seri_barang_u'");
					   
					   if($query){
	?>
	<table class="table">
	 <thead>
      <tr>
		<th>Seri Barang</th>
		<th>Kode Barang</th>
		<th>Nama Barang</th>
		<th>Jumlah</th>
		<th>Satuan</th>
		<th>CIF</th>
		<th>Currency</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php
    $data = pg_query("select * from report.plb_doc_bc27_brg where no_aju ='$no_aju_brgu' order by seri_barang ASC");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	}
	while($d=pg_fetch_array($data)){
	?>
	<tr>
		<td><?php echo $d['seri_barang']; ?></td>
		<td><?php echo $d['kode_barang']; ?></td>
		<td><?php echo $d['nama_barang']; ?></td>
		<td><?php echo $d['jumlah']; ?></td>
		<td><?php echo $d['satuan']; ?></td>
		<td><?php echo $d['cif']; ?></td>
		<td><?php echo $d['currency']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-brg" id="<?php echo $d['no_aju']."#".$d['seri_barang']."#".$d['kode_barang']."#".$d['nama_barang']."#".$d['jumlah']."#".$d['satuan']."#".$d['cif']."#".$d['currency']; ?>" data-toggle="modal" data-target="#upd_brg" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-brg" id="<?php echo $d['no_aju']."-".$d['seri_barang']; ?>" type="button">Hapus</button>
		</td>
	</tr>
	<?php } ?>
	</tbody>
</table>

<?php
		}else{
			echo '<script language="javascript">';
            echo 'alert("Update Gagal")';
            echo '</script>';
		}
}

///////////////////////////////////////////////// HAPUS BRG BC 27 IN ////////////////////////////////////////////////////////
else if ($aksi == 'hapus2'){
	$id = $_POST['kunci'];
    list($kd1,$kd2) = split ('[/.-]',$id);
	
	$query = pg_query("delete from report.plb_doc_bc27_brg where no_aju='$kd1' and seri_barang='$kd2'");
	?>
	<table class="table">
	 <thead>
      <tr>
		<th>Seri Barang</th>
		<th>Kode Barang</th>
		<th>Nama Barang</th>
		<th>Jumlah</th>
		<th>Satuan</th>
		<th>CIF</th>
		<th>Currency</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php
    $data = pg_query("select * from report.plb_doc_bc27_brg where no_aju ='$kd1' order by seri_barang ASC");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	}
	while($d=pg_fetch_array($data)){
	?>
	<tr>
		<td><?php echo $d['seri_barang']; ?></td>
		<td><?php echo $d['kode_barang']; ?></td>
		<td><?php echo $d['nama_barang']; ?></td>
		<td><?php echo $d['jumlah']; ?></td>
		<td><?php echo $d['satuan']; ?></td>
		<td><?php echo $d['cif']; ?></td>
		<td><?php echo $d['currency']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-brg" id="<?php echo $d['no_aju']."#".$d['seri_barang']."#".$d['kode_barang']."#".$d['nama_barang']."#".$d['jumlah']."#".$d['satuan']."#".$d['cif']."#".$d['currency']; ?>" data-toggle="modal" data-target="#upd_brg" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-brg" id="<?php echo $d['no_aju']."-".$d['seri_barang']; ?>" type="button">Hapus</button>
		</td>
	</tr>
	<?php } ?>
	</tbody>
</table>

<?php
}

///////////////////////////////////////////////////////////// SIMPAN BRG /////////////////////////////////////////////////////////
else if ($aksi == 'save-brg'){
	
	$query = pg_query("INSERT INTO report.plb_doc_bc27_brg(
                       no_aju, seri_barang, kode_barang, nama_barang, jumlah, satuan, cif, currency)
                       VALUES ('$no_ajuc', '$seri_barang', '$kd_brg', '$nm_brg', '$jml', '$satuan', '$cif', '$currency')");
	?>
	<table class="table">
	 <thead>
      <tr>
		<th>Seri Barang</th>
		<th>Kode Barang</th>
		<th>Nama Barang</th>
		<th>Jumlah</th>
		<th>Satuan</th>
		<th>CIF</th>
		<th>Currency</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php
    $data = pg_query("select * from report.plb_doc_bc27_brg where no_aju ='$no_ajuc' order by seri_barang ASC");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	}
	while($d=pg_fetch_array($data)){
	?>
	<tr>
		<td><?php echo $d['seri_barang']; ?></td>
		<td><?php echo $d['kode_barang']; ?></td>
		<td><?php echo $d['nama_barang']; ?></td>
		<td><?php echo $d['jumlah']; ?></td>
		<td><?php echo $d['satuan']; ?></td>
		<td><?php echo $d['cif']; ?></td>
		<td><?php echo $d['currency']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-brg" id="<?php echo $d['no_aju']."#".$d['seri_barang']."#".$d['kode_barang']."#".$d['nama_barang']."#".$d['jumlah']."#".$d['satuan']."#".$d['cif']."#".$d['currency']; ?>" data-toggle="modal" data-target="#upd_brg" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-brg" id="<?php echo $d['no_aju']."-".$d['seri_barang']; ?>" type="button">Hapus</button>
		</td>
	</tr>
	<?php } ?>
	</tbody>
</table>

<?php
}

///////////////////////////////////////////////////////////// SIMPAN AJU /////////////////////////////////////////////////////////
else if ($aksi == 'save-aju'){
	
	$data = pg_query("select no_aju from report.plb_doc_in27 where no_aju ='$no_ajus'");
	if (pg_num_rows($data) == 0){
		
	$query = pg_query("INSERT INTO report.plb_doc_in27(
                       no_aju, tgl_aju, kode_rel, cif, username, userdate, 
                       no_daftar, tgl_daftar, tarif_hs, nsurat, 
                       negara, currency_cif)
					   
                       VALUES ('$no_ajus', '$tglan1', '$kode_rel', '$cif', '$user',
                       current_date, '$nopen', '$tglan2', '$hs', 'BC 2.7',
                       '$negara', '$currency')");
					   
					   if ($query){
						   echo 1;
					   }else{
						   echo 0;
					   }
					   
	}else{
		echo 2;
	}
			
			}
			
//////////////////////////////////////////////////// MENAMPILKAN DATA REPORT.PLB_DOC_BC27_BRGDTL ///////////////////////////////////
if ($aksi == 'tampil3'){
?>
<table class="table">
	 <thead>
      <tr>
	    <th>No</th>
		<th>Packing List</th>
		<th>Pallet</th>
		<th>No Batch</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$data = pg_query("select * from report.plb_doc_bc27_brgdtl where no_aju_dok ='$no_aju'");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	} $no = 0;
	while($d=pg_fetch_array($data)){
		$no += 1;
	?>
	<tr>
	    <td><?php echo $no; ?></td>
		<td><?php echo $d['no_doc_dok']; ?></td>
		<td><?php echo $d['pallet_dok']; ?></td>
		<td><?php echo $d['batch_no_dok']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-brgdtl" id="<?php echo $d['no_aju_dok']."#".$d['no_doc_dok']."#".$d['batch_no_dok']."#".$d['pallet_dok']."#".$d['id_brgdtl']; ?>" data-toggle="modal" data-target="#upd_brgdtl" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-brgdtl" id="<?php echo $d['no_aju_dok']."-".$d['id_brgdtl']."-".$d['pallet_dok']; ?>" type="button">Hapus</button>
		</td>
	</tr>
	<?php } ?>
	</tbody>
</table>
<?php
}

///////////////////////////////////////////////////////////// SIMPAN BRGDTL /////////////////////////////////////////////////////////
else if ($aksi == 'save-brgdtl'){
	
	$query = pg_query("INSERT INTO report.plb_doc_bc27_brgdtl(
                       no_aju_dok, no_doc_dok, pallet_dok, batch_no_dok, id_brgdtl)
                       VALUES ('$no_ajud', '$laporan', '$pallet', '$batch_no', RANDOM()*(500-1)+1)");
	?>
	<table class="table">
	 <thead>
      <tr>
	    <th>No</th>
		<th>Packing List</th>
		<th>Pallet</th>
		<th>No Batch</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$data = pg_query("select * from report.plb_doc_bc27_brgdtl where no_aju_dok ='$no_ajud'");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	} $no = 0;
	while($d=pg_fetch_array($data)){
		$no += 1;
	?>
	<tr>
	    <td><?php echo $no; ?></td>
		<td><?php echo $d['no_doc_dok']; ?></td>
		<td><?php echo $d['pallet_dok']; ?></td>
		<td><?php echo $d['batch_no_dok']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-brgdtl" id="<?php echo $d['no_aju_dok']."#".$d['no_doc_dok']."#".$d['batch_no_dok']."#".$d['pallet_dok']."#".$d['id_brgdtl']; ?>" data-toggle="modal" data-target="#upd_brgdtl" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-brgdtl" id="<?php echo $d['no_aju_dok']."-".$d['id_brgdtl']."-".$d['pallet_dok']; ?>" type="button">Hapus</button> </td>
	</tr>
	<?php } ?>
	</tbody>
</table>

<?php
}

///////////////////////////////////////////////// HAPUS DETIL BC 27 BRGDTL ////////////////////////////////////////////////////////
else if ($aksi == 'hapus3'){
	$id = $_POST['kunci'];
    list($kd1,$kd2,$kd3) = split ('[/.-]',$id);
	
	$query = pg_query("delete from report.plb_doc_bc27_brgdtl where no_aju_dok='$kd1' and id_brgdtl='$kd2' and pallet_dok='$kd3'");
	?>
	<table class="table">
	 <thead>
      <tr>
	    <th>No</th>
		<th>Packing List</th>
		<th>Pallet</th>
		<th>No Batch</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php
    $data = pg_query("select * from report.plb_doc_bc27_brgdtl where no_aju_dok ='$kd1'");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	} $no = 0;
	while($d=pg_fetch_array($data)){
		$no += 1;
	?>
	<tr>
	    <td><?php echo $no; ?></td>
		<td><?php echo $d['no_doc_dok']; ?></td>
		<td><?php echo $d['pallet_dok']; ?></td>
		<td><?php echo $d['batch_no_dok']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-brgdtl" id="<?php echo $d['no_aju_dok']."#".$d['no_doc_dok']."#".$d['batch_no_dok']."#".$d['pallet_dok']."#".$d['id_brgdtl']; ?>" data-toggle="modal" data-target="#upd_brgdtl" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-brgdtl" id="<?php echo $d['no_aju_dok']."-".$d['id_brgdtl']."-".$d['pallet_dok']; ?>" type="button">Hapus</button> </td>
	</tr>
	<?php } ?>
	</tbody>
</table>

<?php
}

///////////////////////////////////////////////// UPDATE DETIL BC 27 BRGDTL ////////////////////////////////////////////////////////
else if ($aksi == 'upd-brgdtl'){
	
	$query = pg_query("UPDATE report.plb_doc_bc27_brgdtl
                       SET no_doc_dok='$no_doc_dok_u', batch_no_dok='$batch_no_dok_u', pallet_dok='$pallet_dok_u'
                       WHERE no_aju_dok='$no_aju_dok_u' and id_brgdtl='$id_brgdtl_u'");
	?>
	<table class="table">
	 <thead>
      <tr>
	    <th>No</th>
		<th>Packing List</th>
		<th>Pallet</th>
		<th>No Batch</th>
		<th>Aksi</th>
	</tr>
	</thead>
	<tbody>
	<?php
    $data = pg_query("select * from report.plb_doc_bc27_brgdtl where no_aju_dok ='$no_aju_dok_u'");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	} $no = 0;
	while($d=pg_fetch_array($data)){
		$no += 1;
	?>
	<tr>
	    <td><?php echo $no; ?></td>
		<td><?php echo $d['no_doc_dok']; ?></td>
		<td><?php echo $d['pallet_dok']; ?></td>
		<td><?php echo $d['batch_no_dok']; ?></td>
		<td>
		<button class="btn btn-default btn-xs tombol-upd-brgdtl" id="<?php echo $d['no_aju_dok']."#".$d['no_doc_dok']."#".$d['batch_no_dok']."#".$d['pallet_dok']."#".$d['id_brgdtl']; ?>" data-toggle="modal" data-target="#upd_brgdtl" type="button">Edit</button>
		<button class="btn btn-danger btn-xs tombol-hapus-brgdtl" id="<?php echo $d['no_aju_dok']."-".$d['id_brgdtl']."-".$d['pallet_dok']; ?>" type="button">Hapus</button> </td>
	</tr>
	<?php } ?>
	</tbody>
</table>

<?php
}


//////////////////////////////////////////////////// CEK KOMODITAS ///////////////////////////////////
else if ($aksi == 'cek-komoditi'){
?>
<table class="table">
	 <thead>
      <tr>
	    <th>No</th>
		<th>Id Booking</th>
		<th>Nama Item</th>
		<th>Jumlah</th>
		<th>Satuan</th>
		<th>Batch No</th>
		<th>Pallet</th>
		<th>Packing List</th>
	</tr>
	</thead>
	<tbody>
	<?php 
	$data = pg_query("select A.id_flowbrg,A.jumlah0,A.satuan,A.batch_no,A.exim,A.palet,B.itemname from wh_flowbrg A
                      join wh_cargo B on A.itemcode = B.itemcode
					  where A.exim ='$laporan'
					  and A.id_flowbrg_in is null
	                  order by id_flowbrg ASC");
	if (pg_num_rows($data) == 0){
		?>
	<tr>
		<td colspan="4" align="center">Kosong</td>
	</tr>
	</tbody>
		<?php
	} $no = 0;
	  $jum_tot = 0;
	while($d=pg_fetch_array($data)){
		$no += 1;
		$jum_tot = $jum_tot + $d['jumlah0'];
	?>
	<tr>
	    <td><?php echo $no; ?></td>
		<td><?php echo $d['id_flowbrg']; ?></td>
		<td><?php echo $d['itemname']; ?></td>
		<td><?php echo $d['jumlah0']; ?></td>
		<td><?php echo $d['satuan']; ?></td>
		<td><?php echo $d['batch_no']; ?></td>
		<td><?php echo $d['palet']; ?></td>
		<td><?php echo $d['exim']; ?></td>
	</tr>
	<?php } ?>
	<tr>
	<td colspan="3"><b>Jumlah</b></td>
	<td><b><?php echo $jum_tot; ?></b></td>
	</tr>
	</tbody>
</table>
<?php
}


//////////////////////////////////////////////////// SHOW-DOCUMENT ///////////////////////////////////
else if ($aksi == 'show-document'){

	$data = pg_query("select no_doc from report.plb_doc_in27_dtl where no_aju ='$no_aju'");
		?>
	<option value=''>Pilih Dokumen</option>
		<?php

	while($d=pg_fetch_array($data)){
		echo "<option value='".$d['no_doc']."'>".$d['no_doc']."</option>";
	} 
}

//////////////////////////////////////////////////// PILIH-AJU ///////////////////////////////////
else if ($aksi == 'pilih-aju'){

	$data = pg_query("select distinct no_aju, tgl_aju from report.plb_doc_in27
                      where tgl_aju between current_date-14 and current_date and nsurat = 'BC 2.7' order by tgl_aju ASC");
		?>
	<option value=''>Pilih aju</option>
		<?php

	while($d=pg_fetch_array($data)){
		echo "<option value='".$d['no_aju']."'>".$d['no_aju']."</option>";
	} 
}



	?>
	