<?php
session_start();
$_SESSION['MENU'] = 'DOCUMENT';
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}
$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

include 'path.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title><?= TITLE_APP ?></title>
    <link rel="shortcut icon" href="<?= IMAGES_PATH ?>/icons/logoijs.jpg">

    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/DataTables/media/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/DataTables/media/css/dataTables.bootstrap.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap-datetimepicker.min.css" media="screen">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap.min.css">

    <!-- start:bootstrap v3.2.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.min.css">
    <!-- start:font awesome v4.1.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/font-awesome.min.css">
    <!-- start:bootstrap reset -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap-reset.css">
    <!-- start:style arjuna -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/arjuna.css">

    <script src="<?= ASSETS_PATH ?>/js/jquery-3.1.1.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= ASSETS_PATH ?>/DataTables/media/js/jquerydt.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/datatable.js"></script>

    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/style-view.css">
    <script src="bootcode.js"></script>


</head>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $jenis = $_POST['jenis_act'];
    $tgl1 = $_POST['tglan1'];
    $tgl2 = $_POST['tglan2'];
    $kode_rel = TRIM(isset($_POST['cust']));
}
?>

<body class="cl-default fixed">
    <!-- start:navbar top -->
    <header class="header">
        <!-- <a href="home.php" class="logo">Indra Jaya Swastika</a> -->
        <header class="header"><?php include("top_nav.php") ?></header>
    </header>
    <!-- end:navbar top -->
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas"><?php include("left_menu.php") ?></aside>

        <aside class="right-side">
            <section class="content">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="#"> Laporan</a></li>
                    <li><a href="#"> Laporan Dokumen TPB</a></li>
                </ol>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading hidden-print"><span class="glyphicon glyphicon-tasks"
                                    aria-hidden="true"></span> DOKUMEN TPB</div>
                            <div class="box">
                                <hr>
                                <div class="panel-body" id="pnl-filter">
                                    <form class="form-horizontal" role="form" method="POST" action="#">
                                        <input type="hidden" id="username" name="username"
                                            value="<?php echo $username ?>">
                                        <input type="hidden" id="logincat" name="logincat"
                                            value="<?php echo $logincat ?>">

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Jenis Laporan</label>
                                            <div class="col-md-4 col-xs-6">
                                                <select class="form-control input-sm" name="jenis_act" id="jenis_act">
                                                    <?php
                                                    if (isset($jenis) && $jenis == "BC 1.6") {
                                                        echo "<option value='BC 1.6'>Dokumen BC 1.6</option>";
                                                        echo "<option value='BC 2.7'>Dokumen BC 2.7</option>";
                                                        echo "<option value='BC 2.8'>Dokumen BC 2.8</option>";
                                                        echo "<option value='BC 4.0'>Dokumen BC 4.0</option>";
                                                        echo "<option value='BC 4.1'>Dokumen BC 4.1</option>";
                                                        echo "<p></p>";
                                                    } else if (isset($jenis) && $jenis == "BC 2.7") {
                                                        echo "<option value='BC 2.7'>Dokumen BC 2.7</option>";
                                                        echo "<option value='BC 1.6'>Dokumen BC 1.6</option>";
                                                        echo "<option value='BC 2.8'>Dokumen BC 2.8</option>";
                                                        echo "<option value='BC 4.0'>Dokumen BC 4.0</option>";
                                                        echo "<option value='BC 4.1'>Dokumen BC 4.1</option>";
                                                        echo "<p></p>";
                                                    } else if (isset($jenis) && $jenis == "BC 2.8") {
                                                        echo "<option value='BC 2.8'>Dokumen BC 2.8</option>";
                                                        echo "<option value='BC 1.6'>Dokumen BC 1.6</option>";
                                                        echo "<option value='BC 2.7'>Dokumen BC 2.7</option>";
                                                        echo "<option value='BC 4.0'>Dokumen BC 4.0</option>";
                                                        echo "<option value='BC 4.1'>Dokumen BC 4.1</option>";
                                                        echo "<p></p>";
                                                    } else if (isset($jenis) && $jenis == "BC 4.0") {
                                                        echo "<option value='BC 4.0'>Dokumen BC 4.0</option>";
                                                        echo "<option value='BC 1.6'>Dokumen BC 1.6</option>";
                                                        echo "<option value='BC 2.7'>Dokumen BC 2.7</option>";
                                                        echo "<option value='BC 2.8'>Dokumen BC 2.8</option>";
                                                        echo "<option value='BC 4.1'>Dokumen BC 4.1</option>";
                                                        echo "<p></p>";
                                                    } else if (isset($jenis) && $jenis == "BC 4.1") {
                                                        echo "<option value='BC 4.1'>Dokumen BC 4.1</option>";
                                                        echo "<option value='BC 1.6'>Dokumen BC 1.6</option>";
                                                        echo "<option value='BC 2.7'>Dokumen BC 2.7</option>";
                                                        echo "<option value='BC 2.8'>Dokumen BC 2.8</option>";
                                                        echo "<option value='BC 4.0'>Dokumen BC 4.0</option>";
                                                        echo "<p></p>";
                                                    } else {
                                                    ?>
                                                    <option value='BC 1.6'>Dokumen BC 1.6</option>
                                                    <option value='BC 2.7'>Dokumen BC 2.7</option>
                                                    <option value='BC 2.8'>Dokumen BC 2.8</option>
                                                    <option value='BC 4.0'>Dokumen BC 4.0</option>
                                                    <option value='BC 4.1'>Dokumen BC 4.1</option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Tanggal</label>
                                            <div class="col-md-4 col-xs-6">
                                                <input type="text" autocomplete="off" class="form-control" id="tglan1"
                                                    name="tglan1" value="<?php echo $tgl1; ?>">
                                            </div>
                                        </div>
                                        <div id="periode2" class="form-group">
                                            <label class="control-label col-xs-4">Sampai Tanggal</label>
                                            <div class="col-md-4 col-xs-6">
                                                <input type="text" autocomplete="off" class="form-control" id="tglan2"
                                                    name="tglan2" value="<?php echo $tgl2; ?>">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-6 col-xs-offset-4">
                                                <button class="form-control btn-primary" id="submit"
                                                    type="submit">Submit</button>
                                            </div>
                                        </div>

                                    </form>

                                    <?php
                                    if (isset($jenis)) {
                                    ?>
                                    <form class="form-horizontal" role="form" method="POST"
                                        action="model/cetak_dokumen.php">
                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-6 col-xs-offset-4">
                                                <input type="hidden" class="form-control" id="tglc1" name="tglc1"
                                                    value="<?php echo $tgl1; ?>">
                                                <input type="hidden" class="form-control" id="tglc2" name="tglc2"
                                                    value="<?php echo $tgl2; ?>">
                                                <input type="hidden" class="form-control" id="jenisc" name="jenisc"
                                                    value="<?php echo $jenis; ?>">
                                                <input type="hidden" class="form-control" id="custc" name="custc"
                                                    value="<?php echo $kode_rel; ?>">
                                                <button class="form-control btn-warning" id="submit"
                                                    type="submit">Export</button>
                                            </div>
                                        </div>

                                    </form>
                                    <?php
                                    } else {
                                    }
                                    ?>

                                </div>
                                <div class="form-group">
                                    <div class="col-xs-9 col-xs-offset-3" id="form-msg"></div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-5 col-xs-9" id="judul">
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="row">
                                <div class="col-sm"></div>
                            </div>
                            <div class="table-responsive" id="result">
                                <div id="view"><?php include "dokumen_view.php"; ?></div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </aside>
    </div>

    <script src="<?= ASSETS_PATH ?>/js/jquery.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/arjuna.js"></script>
    <script src="<?= ASSETS_PATH ?>/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="<?= JS_PATH ?>/bootstrap-datetimepicker.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/tanggal.js"></script>


</body>

</html>