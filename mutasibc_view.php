<?php
if (isset($_POST['jenis_act'])) {

    require_once('db-inc2.php');
    require_once('insert_log_activity.php');

    $jenis = $_POST['jenis_act'];
    $tgl1 = $_POST['tglan1'];
    $tgl2 = $_POST['tglan2'];
    $kode_rel = TRIM($_POST['cust']);
    if ($kode_rel == "ALL") {
        $kode_rel = '50%';
    }

    // insert_log($username,$kategori,$address ,$status,$remark ) 
    $log_remark = "Open REPORT MUTASI BARANG Tanggal " . date('d-m-Y', strtotime($tgl1)) . " s/d " . date('d-m-Y', strtotime($tgl2));
    insert_log($username, $kategori, $address, "REPORT", $log_remark);

    if ($jenis == "MUTASI") {
        /*	 $sqltext= "select A.id_stok_in,A.id_stok_out,case when kendaraan = 'CONTAINER' then A.no_unit else A.nopol end as jns_kend,
				A.tgl_awal,A.batch_no as invoice,A.nama,A.itemcode,A.doc_out,A.no_doc_out,A.tgl_doc_out, A.supplier,A.no_bl,A.tgl_bl,A.no_bc_16,A.tgl_bc_16,
				trim(split_part(itemname,':',1)) as cbarang, trim(split_part(itemname,':',2)) as nbarang, A.satuan , sum(qty) as jumlah,sum(qty_out) as jumlah_out
				from report.v_plb_mutasi  A 
				join wh_book bo on bo.no_book = split_part(A.id_stok_in, '-',1) 
				where case when tgl_in_asal isnull then tgl_awal between  '$tgl1' and '$tgl2'
				else tgl_in_asal BETWEEN  '$tgl1' and '$tgl2' end
				and A.kode_rel  LIKE '$kode_rel'
				and BO.id_aktifitas IN(1,2,14,16)
				group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
				order by nama,id_stok_in,invoice,A.itemcode,id_stok_out nulls first " ;
*/

        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN MUTASI PER BULAN <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<hr>";
        echo "<br>";
        echo "<br>";

        $sqltext = "select A.id_stok_in,A.id_stok_out,A.no_aju_out,
	                case when kendaraan = 'CONTAINER' then A.no_unit else A.nopol end as jns_kend,
                    B.kode_barang,
				    B.uraian_barang,
					A.tgl_awal,case when kode_rel <> '501600' then A.batch_no else A.exim end as invoice,A.nama,A.doc_out,A.no_doc_out,A.tgl_doc_out, A.supplier,A.no_bl,A.tgl_bl,A.no_bc_16,A.tgl_bc_16,
			        A.satuan,A.kode_rel, B.jenis_dokumen,
					sum_varchar (distinct A.keterangan ||'<br />') as keterangan,
					sum(qty) as jumlah,sum(qty_out) as jumlah_out
                    from report.v_plb_mutasi  A 
                    
                    left join (select distinct nomor_dokumen,substring(nomor_aju,15) as no_aju, jenis_dokumen,
                    sum_varchar(distinct(kode_barang)||'<br />') as kode_barang,
                                    sum_varchar(distinct(uraian_barang)||'<br />') as uraian_barang
                    from report.plb_documents_in
                    where uraian_dokumen = 'INVOICE'
                    group by 1,2,3
                    union
                    select distinct nomor_dokumen,substring(nomor_aju,15) as no_aju, 'BC 4.0' as jenis_dokumen,
                    sum_varchar(distinct(kode_barang)||'<br />') as kode_barang,
                                    sum_varchar(distinct(uraian_barang)||'<br />') as uraian_barang
                    from report.plb_documents_in40
                    where uraian_dokumen in ('SURAT JALAN','PACKING LIST','LAINNYA')
                    group by 1,2,3
                    union
                    select distinct nomor_dokumen,substring(no_aju,15) as no_aju, '' as jenis_dokumen,
                    sum_varchar(distinct(kode_barang)||'<br />') as kode_barang,
                                    sum_varchar(distinct(uraian_barang)||'<br />') as uraian_barang
                    from report.plb_documents_outbc27
                    where uraian_dokumen = 'INVOICE'
                    group by 1,2,3
                    union
                    select distinct nomor_dokumen,substring(no_aju,15) as no_aju, '' as jenis_dokumen,
                    sum_varchar(distinct(kode_barang)||'<br />') as kode_barang,
                                    sum_varchar(distinct(uraian_barang)||'<br />') as uraian_barang
                    from report.plb_documents_out
                    where uraian_dokumen = 'INVOICE'
                    group by 1,2,3
                    union
                    select distinct nomor_dokumen,substring(no_aju,15) as no_aju, '' as jenis_dokumen,
                    sum_varchar(distinct(kode_barang)||'<br />') as kode_barang,
                                    sum_varchar(distinct(uraian_barang)||'<br />') as uraian_barang
                    from report.plb_documents_outbc41
                    where uraian_dokumen in ('PACKING LIST','SURAT JALAN','LAINNYA')
                    group by 1,2,3) B on case when A.id_flowbrg_out is null then
                                            case when A.kode_rel <> '501600' then
                                            A.batch_no = B.nomor_dokumen and A.no_aju = B.no_aju
                                            else
                                            A.exim = B.nomor_dokumen and A.no_aju = B.no_aju
                                            end
                                                else
                                            case when A.kode_rel <> '501600' then
                                            A.batch_no = B.nomor_dokumen and A.no_aju_out = B.no_aju
                                            else
                                            A.exim = B.nomor_dokumen and A.no_aju_out = B.no_aju
                                            end
                                            end
                    
                    where A.id_stok_in || A.itemcode in (
					select distinct A.id_stok_in || A.itemcode
					from report.v_plb_mutasi  A 
					join wh_book bo on bo.no_book = split_part(A.id_stok_in, '-',1) 
					where tgl_awal between  '$tgl1' and '$tgl2'
					and A.kode_rel  LIKE '$kode_rel'
					and BO.id_aktifitas IN(1,2,14,16)
                    )
                    group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20
                    order by nama,id_stok_in,id_stok_out nulls first ";
        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = 0;
        if ($baris > 0) {
            echo "<a id=dlink  style=display:none;> </a> <br>";
            echo "<table width='75%' height=15 border= 1 font=8 bgcolor=#0000CC id=data_table >";
            echo "    <tr style=background:#0099FF;> ";
            echo "    <th><label class='style5'>NO</label></th>";
            echo "    <th><label class='style5'>Container</label></th>";
            echo "    <th><label class='style5'>No Bukti Pemasukan</label></th>";
            echo "    <th><label class='style5'>No Bukti Pengeluaran</label></th>";
            echo "    <th><label class='style5'>Jenis Dokumen</label></th>";
            echo "    <th><label class='style5'>Tanggal Aktifitas</label></th>";
            echo "    <th><label class='style5'>Invoice</label></th>";
            echo "    <th><label class='style5'>Pemilik Barang</label></th>";
            echo "    <th><label class='style5'>Lokasi</label></th>";
            echo "    <th width='100px'><label class='style5'>Kode Barang</label></th>";
            echo "    <th><label class='style5'>Nama Barang</label></th>";
            echo "    <th><label class='style5'>Jumlah IN</label></th>";
            echo "    <th><label class='style5'>Jumlah OUT</label></th>";
            echo "    <th><label class='style5'>Satuan</label></th>";
            echo "    <th><label class='style5'>Keterangan</label></th>";
            echo "  </tr>";
        }
        $id0 = "";
        $satan = "";
        $jumlahx = 0;
        while ($row = pg_fetch_assoc($result)) {
            if ($id0 != $row['id_stok_in']) {
                if ($id0 != "") {
                    echo "  <tr style=background:#E0E0E0; height=30> ";
                    echo "	<td colspan=11 align=right><label class='style4'>STOK AKHIR&nbsp;</label></td>";
                    echo "	<td colspan=2 align =center ><label class='style4'>$jumlahx</label></td>";
                    echo "	<td ><label class='style4'>" . $satuan . "</label></td>";
                    echo "  </tr>";
                }
                $jumlahx = 0;
                $id0 = $row['id_stok_in'];
                $rel = $row['kode_rel'];
            }

            $number = $number + 1;
            if (($number % 2) == 1) {
                echo "    <tr style=background:#FFFFCC;> ";
            } else {
                echo "    <tr style=background:#99CCFF;> ";
            }
            echo "	<td ><label class='style4'>$number</label></td>";
            echo "	<td ><label class='style4'>" . $row['jns_kend'] . "</label></td>";
            echo "	<td ><label class='style4'>" . $row['id_stok_in'] . "</label></td>";
            echo "	<td ><label class='style4'>" . $row['id_stok_out'] . "</label></td>";
            /*
				if(($row['id_stok_out'] =='') && ($row['kode_rel'] != '501600')){
				echo "	<td ><label class='style4'>BC 1.6</label></td>";
				}else if(($row['id_stok_out'] =='') && ($row['kode_rel'] == '501600')){
				echo "	<td ><label class='style4'>BC 1.6</label></td>";
				}else{
				echo "	<td ><label class='style4'>".$row['doc_out']."</label></td>";
				}
				*/
            if ($row['id_stok_out'] == '') {
                echo "	<td ><label class='style4'>" . $row['jenis_dokumen'] . "</label></td>";
            } else {
                echo "	<td ><label class='style4'>" . $row['doc_out'] . "</label></td>";
            }
            echo "	<td ><label class='style4'>" . $row['tgl_awal'] . "</label></td>";
            echo "	<td ><label class='style4'>" . $row['invoice'] . "</label></td>";
            echo "	<td ><label class='style4'>" . $row['nama'] . "</label></td>";
            if (($rel == '500200') || ($rel == '500300') || ($rel == '501500')) {
                echo            "<td><label class='style4'>WH.03</label></td>";
            } else if (($rel == '501600') || ($rel == '500100')) {
                echo            "<td><label class='style4'>WH.04</label></td>";
            } else {
                echo            "<td><label class='style4'>WH.LAP</label></td>";
            }
            echo "	<td ><label class='style4'>" . $row['kode_barang'] . "</label></td>";
            echo "	<td ><label class='style4'>" . $row['uraian_barang'] . "</label></td>";
            echo "	<td align =center ><label class='style4'>" . $row['jumlah'] . "</label></td>";
            echo "	<td align =center><label class='style4'>" . $row['jumlah_out'] . "</label></td>";
            echo "	<td ><label class='style4'>" . $row['satuan'] . "</label></td>";
            echo "	<td ><label class='style4'>" . $row['keterangan'] . "</label></td>";
            echo "  </tr>";
            $jumlahx = $jumlahx + $row['jumlah'] - $row['jumlah_out'];
            $satuan = $row['satuan'];
        }
        echo "  <tr style=background:#E0E0E0;> ";
        echo "	<td colspan=11 align=right><label class='style4'>STOK AKHIR&nbsp;</label></td>";
        echo "	<td colspan=2 align =center ><label class='style4'>$jumlahx</label></td>";
        echo "	<td ><label class='style4'>" . $satuan . "</label></td>";
        echo "  </tr>";
        echo "</table>";
        pg_free_result($result);
    }
}