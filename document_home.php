<?php
	session_start();
	if (!isset($_SESSION['username'])) {
		header('Location: index.php');
	}
	
	$username = $_SESSION['username'];
	
	require_once('db-inc.php');
	$query ="select perusahaan,kategori from users_web where login = '$username' ";
	$result = pg_query($db_, $query);
	$cust = pg_fetch_row($result);
	$relasi = $cust[0];	
	$logincat = $cust[1];	
	
?>
<script src="bootcode.js"></script>
<script language="javascript" >
var ajaxRequest;
function getAjax() //fungsi untuk mengecek AJAX pada browser
{
	try
	{
		ajaxRequest = new XMLHttpRequest();
	}
	catch (e)
	{
		try
		{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e) 
		{
			try
			{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e)
			{
				alert("Your browser broke!");
				return false;
			}
		}
	}
}

function showData() {
		
		var no_aju = document.getElementById("no_aju").value;
		
		var select = document.getElementById("jenis");
		var tipe = select.options[select.selectedIndex].value;
		
		var select = document.getElementById("data");
		var data = select.options[select.selectedIndex].value;
		 
		logincat = document.getElementById("logincat").value;  
		
		document.getElementById("form-msg").innerHTML = "<img src='progress.gif'>";
		document.getElementById("judul").style.display = 'none';
		document.getElementById("result").style.display = 'none';
	    getAjax();
		
		 ajaxRequest.open("GET","get_document.php?no_aju="+no_aju+"&tipe="+tipe+"&data="+data);
 
				ajaxRequest.onreadystatechange = function(){
					
				document.getElementById("result").style.display = '';
				document.getElementById("result").innerHTML  = ajaxRequest.responseText;
				var gagal = ajaxRequest.responseText
				 if (ajaxRequest) {
					 document.getElementById("form-msg").innerHTML = "";
				 			
				 }
				
               }
                
				ajaxRequest.send(null);	
				
				
		
}


function jenisLap(){
	
	var select = document.getElementById("jenis-laporan");
	var status = select.options[select.selectedIndex].value;
	
	//document.getElementById("jenis-laporan").value = document.getElementById("data-laporan").value ;
	
	if (status == 'OUT'){
		document.getElementById("pengangkut").style.display = 'none';
        document.getElementById("voy-flight").style.display = 'none';
		document.getElementById("tanggal-tiba").style.display = 'none';
		document.getElementById("negara").style.display = 'none';
		document.getElementById("suplier").style.display = 'none';
		document.getElementById("harga").style.display = 'none';
		document.getElementById("curency").style.display = 'none';
		document.getElementById("kemasan").style.display = '';
	}else if (status == 'OUT27' || status == 'OUT41'){
		document.getElementById("pengangkut").style.display = 'none';
        document.getElementById("voy-flight").style.display = 'none';
		document.getElementById("tanggal-tiba").style.display = 'none';
		document.getElementById("negara").style.display = 'none';
		document.getElementById("suplier").style.display = 'none';
		document.getElementById("harga").style.display = 'none';
		document.getElementById("curency").style.display = 'none';
		document.getElementById("kemasan").style.display = '';
	}else{
		document.getElementById("pengangkut").style.display = '';
        document.getElementById("voy-flight").style.display = '';
		document.getElementById("tanggal-tiba").style.display = '';
		document.getElementById("negara").style.display = '';
		document.getElementById("suplier").style.display = '';
		document.getElementById("harga").style.display = '';
		document.getElementById("curency").style.display = '';
		document.getElementById("kemasan").style.display = '';
		document.getElementById("cif").style.display = 'none';
		document.getElementById("cif-rupiah").style.display = 'none';
		
	}
}


</script>


<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">

    <title>PLB | PT. INDRA JAYA SWASTIKA</title>
	
		  <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
      <link rel="stylesheet" href="/resources/demos/style.css">
	   <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
      <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
      <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	<style>
		a:hover { text-decoration : none; background-color: #F2B968; }
	.kecil {font-size: 9px}
	.sedang_kecil {font-size: 10px}
	.sedang {font-size: 11px}
	.sedang_besar {font-size: 12px}
	.style4 {font-size: 10px}
	.style5 {font-size: 11px}
	.style6 {font-size: 12px}
	.style7 {font-size: 12px}
-->

    </style>
  </head>

  <body>
    <div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<h2 class="text-center">PUSAT LOGISTIK BERIKAT</h2>
				<h3 class="text-center">PT. INDRA JAYA SWASTIKA</h3>
				<p class="text-right hidden-print"><button class="btn btn-xs btn-danger" id="logout" onClick="logout();" type="button">Logout</button></p>
			</div>
			<div class="col-md-10">
			<p class="text-right hidden-print">
			<a href="plb_document.php"><button class="btn btn-xs btn-info" id="" onClick="" type="button">Edit Dokumen</button></a>&nbsp; &nbsp;
			<a href="home.php"><button class="btn btn-xs btn-success" id="" onClick="" type="button">Home</button></a>
			</p>
			</div>
	    </div>

		
		<div class="row hidden-print">
			<div class="col-md-8 col-md-offset-2 col-xs-12">
				<div class="panel panel-primary">
					<div class="panel-heading hidden-print"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> PLB DOCUMENT</div>
					<div class="panel-body" id="pnl-filter">
						<form class="form-horizontal" role="form" method="POST" enctype="multipart/form-data">
							<input type="hidden" id="username" name="username" value="<?php echo $username ?>">
							<input type="hidden" id="logincat" name="logincat" value="<?php echo $logincat ?>">
							
							<div class="form-group">
								<label class="col-xs-3 control-label">Jenis Laporan</label>
								<div class="col-md-5 col-xs-9">
								<select class="form-control input-sm" name="jenis_act" id="jenis_act"  onChange="show_change();" >
									<option value='BC 1.6'>Laporan BC 1.6/BC 4.0</option>
                                    <option value='BC 2.7IN'>Laporan BC 2.7 (IN)</option>									
									<option value='BC 2.7OT'>Laporan BC 2.7 (OUT)</option>		
								    <option value='BC 2.8'>Laporan BC 2.8</option>
									<option value='BC 3.0'>Laporan BC 3.0</option>
									<option value='BC 4.1'>Laporan BC 4.1</option>
									
								</select>
								</div>
							</div>

							<?php
							/*
							<div class="form-group">
								<label class="col-xs-3 control-label">Nama PDPLB</label>
								<div class="col-md-7 col-xs-9">
								<select class="form-control input-sm" name="pdplb" id="pdplb">
									<option value="PILIH" selected>--- Pilih salah satu ---</option>
								</select>
								</div>
							</div>
							*/
							?>

							<?php
							$day = date("d");
							$month = date("m");
							$year = date("Y");
							$bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
							$tahun = array($year-5, $year-4, $year-3, $year-2, $year-1, $year);
							?>
							
							<div class="form-group">
								<label class="col-xs-3 control-label">Tanggal</label>
								<div class="col-md-2 col-xs-3">	
									<select name="tgl1" id="tgl1" class="form-control input-sm">
									<?php
									for ($i = 1; $i <= 31; $i++) {
										if ($i == 1) {
									?>		
										<option value=<?php echo $i ?> selected><?php echo $i ?></option>
									<?php	
										}
										else {
									?>		
										<option value=<?php echo $i ?>><?php echo $i ?></option>
									<?php	
										}
									}
									?>
									</select>
								</div>
								<div class="col-md-3 col-xs-3">
									<select name="bln1" id="bln1" class="form-control input-sm">
									<?php
									for ($i = 0; $i <= 11; $i++) {
										if ($month == ($i+1)) {
									?>		
										<option value=<?php echo $i+1 ?> selected><?php echo $bulan[$i] ?></option>
									<?php	
										}
										else {
									?>		
										<option value=<?php echo $i+1 ?>><?php echo $bulan[$i] ?></option>
									<?php	
										}
									}
									?>
									</select>
								</div>
								<div class="col-md-2 col-xs-3">	
									<select name="thn1" id="thn1" class="form-control input-sm">
									<?php
									for ($i = 0; $i <= count($tahun)-1; $i++) {
										if ($year == $tahun[$i]) {
									?>		
										<option value=<?php echo $tahun[$i] ?> selected><?php echo $tahun[$i] ?></option>
									<?php	
										}
										else {
									?>		
										<option value=<?php echo $tahun[$i] ?>><?php echo $tahun[$i] ?></option>
									<?php	
										}
									}
									?>
									</select>
								</div>
							</div>
							<div id="periode2" class="form-group">
								<label class="col-xs-3 control-label">Sampai Tanggal</label>
								<div class="col-md-2 col-xs-3">	
									<select name="tgl2" id="tgl2" class="form-control input-sm">
									<?php
									for ($i = 1; $i <= 31; $i++) {
										if ($i == $day) {
									?>		
										<option value=<?php echo $i ?> selected><?php echo $i ?></option>
									<?php	
										}
										else {
									?>		
										<option value=<?php echo $i ?>><?php echo $i ?></option>
									<?php	
										}
									}
									?>
									</select>
								</div>
								<div class="col-md-3 col-xs-3">
									<select name="bln2" id="bln2" class="form-control input-sm">
									<?php
									for ($i = 0; $i <= 11; $i++) {
										if ($month == ($i+1)) {
									?>		
										<option value=<?php echo $i+1 ?> selected><?php echo $bulan[$i] ?></option>
									<?php	
										}
										else {
									?>		
										<option value=<?php echo $i+1 ?>><?php echo $bulan[$i] ?></option>
									<?php	
										}
									}
									?>
									</select>
								</div>
								<div class="col-md-2 col-xs-3">	
									<select name="thn2" id="thn2" class="form-control input-sm">
									<?php
									for ($i = 0; $i <= count($tahun)-1; $i++) {
										if ($year == $tahun[$i]) {
									?>		
										<option value=<?php echo $tahun[$i] ?> selected><?php echo $tahun[$i] ?></option>
									<?php	
										}
										else {
									?>		
										<option value=<?php echo $tahun[$i] ?>><?php echo $tahun[$i] ?></option>
									<?php	
										}
									}
									?>
									</select>
								</div>

						</div>		
						
							<div class="form-group">
								<div class="col-md-5 col-xs-6 col-xs-offset-3">
								<button class="btn btn-block btn-primary" id="submit" type="button" onClick="showData();">Submit</button>
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-9 col-xs-offset-3" id="form-msg"></div>
							</div>
						</form>
								</div>
							</div>

							<div class="form-group">
								<div class="col-xs-9 col-xs-offset-3" id="form-msg"></div>
							</div>
						

					</div>
				</div>

			</div>
		</div>
		<div class="row">
  		<div class="col-md-5 col-xs-9" id="judul">
        </div>
    	</div>
     <!-- /container -->
<div class="col-md-15" id="result">
</div>
<label class="sedang_kecil"> Copy right @ 2017</label>

  </body>
</html>
<script src="js/jquery.js"></script>
<script src="js/tableToExcel.js"></script>
<script src="js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/jquery-1.8.3.min.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>

<script language="javascript">
function logout(){
  tanya = confirm('Anda ingin keluar Aplikasi?');
        if (tanya == true) {
    var xhr = new XMLHttpRequest();
    xhr.onload = function() {
        document.location = 'home.php';
    }
    xhr.open('GET', 'logout.php', true);
    xhr.send();
	}

}

$(function Datepicker() {
                $('#tanggal-dokumen').datetimepicker({
				//language:  'fr',
				    format: 'yyyy-mm-dd',
                    weekStart: 1,
                    todayBtn:  1,
		            autoclose: 1,
		            todayHighlight: 1,
		            startView: 2,
		            minView: 2,
		            forceParse: 0
				});
				
				 $('#tanggal-daftar').datetimepicker({
				//language:  'fr',
				    format: 'yyyy-mm-dd',
                    weekStart: 1,
                    todayBtn:  1,
		            autoclose: 1,
		            todayHighlight: 1,
		            startView: 2,
		            minView: 2,
		            forceParse: 0
				});
				
				$('#tanggal-aju').datetimepicker({
				//language:  'fr',
				    format: 'yyyy-mm-dd',
                    weekStart: 1,
                    todayBtn:  1,
		            autoclose: 1,
		            todayHighlight: 1,
		            startView: 2,
		            minView: 2,
		            forceParse: 0
				});
				
				$('#tanggal-tiba').datetimepicker({
				//language:  'fr',
				    format: 'yyyy-mm-dd',
                    weekStart: 1,
                    todayBtn:  1,
		            autoclose: 1,
		            todayHighlight: 1,
		            startView: 2,
		            minView: 2,
		            forceParse: 0
				});
            });


</script>
