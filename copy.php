<?php
session_start();
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}

$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

?>
<!DOCTYPE html>
<html>

<head>
    <title>Input BC27</title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha256-k2/8zcNbxVIh5mnQ52A0r3a6jAgMGxFJFE2707UxGCk= sha512-ZV9KawG2Legkwp3nAlxLIVFudTauWuBpC10uEafMHYL0Sarrz5A7G79kXh5+5+woxQ5HM559XX2UZjMJ36Wplg=="
        crossorigin="anonymous">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!------ Include the above in your HEAD tag ---------->
</head>

<body style="font-family: 'Open Sans', sans-serif;">
    <div class="container">
        <div class="row">
            <div class="col-sm-4">
                <div class="centered title">
                    <h1>Input BC 27.</h1>
                </div>
            </div>
            <div class="col-sm-4">
            </div>
            <div class="col-sm-4">
                <p>
                <p>
                    <button type="button" data-toggle="modal" data-target="#impor_brgdtl"
                        class="btn btn-default btn-xs">Impor Data</button>
                    &nbsp;
                    <a target="_blank" href="cek_komoditi.php"><button type="button" class="btn btn-success btn-xs">Cek
                            Komoditi</button></a>
            </div>
        </div>


        <!---------------------------------------------- MODAL DETIL ------------------------------------------------>
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
            <div class="modal-dialog " role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalTitle">Tambah Dokumen</h4>
                        <form class="form-horizontal frm-dtl" method="post">
                    </div>

                    <div class="modal-body" id="myModalBody">

                        <!-- Isi Modal -->

                        <div class="container">

                            <div class="row">
                                <div class="col-md-6">
                                    <table class="table table-striped">

                                        <tbody>
                                            <tr>

                                                <td>

                                                    <div class="form-group">
                                                        <div class="col-md-6 col-xs-8">
                                                            <select class="form-control input-sm" name="jenis_laporan"
                                                                id="jenis_laporan">
                                                                <option value='INVOICE'>INVOICE</option>
                                                                <option value='PACKING LIST'>PACKING LIST</option>
                                                                <option value='SURAT JALAN'>SURAT JALAN</option>
                                                                <option value='LAINNYA'>LAINNYA</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <input type="hidden" class="form-control" name="no_ajub"
                                                        id="no_ajub">
                                                    <input type="hidden" class="form-control" name="aksi" id="aksi"
                                                        value="save-dtl">
                                                </td>
                                            </tr>

                                            <tr>

                                                <td>
                                                    <div class="col-xs-12">
                                                        <input type="text" class="form-control"
                                                            placeholder="Nomor Dokumen" id="no_dok" name="no_dok">
                                                    </div>
                                                </td>
                                            </tr>

                                            <tr>

                                                <td>
                                                    <div class="col-xs-12">
                                                        <div class="controls input-append date form_date" data-date=""
                                                            data-date-format="dd MM yyyy" data-link-field="dtp_input2"
                                                            data-link-format="yyyy-mm-dd">
                                                            <input type="text" class="form-control" id="tgl_dok"
                                                                placeholder="Tanggal Dokumen" name="tgl_dok">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-info simpan-dtl" id="submit"
                                name="simpan">Simpan</button>
                            </form>
                            <button type="button" class="btn btn-default" id="submit"
                                data-dismiss="modal">Close</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <!------------------ END MODAL DETIL --------------------------->

    <!---------------------------------------------- MODAL UPD DETIL ------------------------------------------------>
    <div class="modal fade" id="upd_dtl" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalTitle">Ubah Dokumen</h4>
                    <form class="form-horizontal frm-upd-dtl" method="post">
                </div>

                <div class="modal-body" id="myModalBody">

                    <!-- Isi Modal -->

                    <div class="container">

                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-striped">

                                    <tbody>
                                        <tr>

                                            <td>

                                                <div class="form-group">
                                                    <div class="col-md-6 col-xs-8">
                                                        <select class="form-control input-sm" name="jenis_doc_u"
                                                            id="jenis_doc_u">
                                                            <option value='INVOICE'>INVOICE</option>
                                                            <option value='PACKING LIST'>PACKING LIST</option>
                                                            <option value='SURAT JALAN'>SURAT JALAN</option>
                                                            <option value='LAINNYA'>LAINNYA</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="hidden" class="form-control" name="no_aju_dtlu"
                                                    id="no_aju_dtlu">
                                                <input type="hidden" class="form-control" name="id_dtl_u" id="id_dtl_u">
                                                <input type="hidden" class="form-control" name="aksi" id="aksi"
                                                    value="upd-dtl">
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Nomor Dokumen"
                                                        id="no_doc_dtlu" name="no_doc_dtlu">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <div class="controls input-append date form_date" data-date=""
                                                        data-date-format="dd MM yyyy" data-link-field="dtp_input2"
                                                        data-link-format="yyyy-mm-dd">
                                                        <input type="text" class="form-control" id="tgl_doc_u"
                                                            placeholder="Tanggal Dokumen" name="tgl_doc_u">
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info upd-dtl" id="submit" name="simpan">Simpan</button>
                        </form>
                        <button type="button" class="btn btn-default" id="submit" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    <!------------------ END UPD MODAL DETIL --------------------------->


    <!---------------------------------------------- MODAL BARANG DETIL ------------------------------------------------>
    <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalTitle">Tambah Detil Barang</h4>
                    <form class="form-horizontal frm-brgdtl" method="post">
                </div>

                <div class="modal-body" id="myModalBody">

                    <!-- Isi Modal -->

                    <div class="container">

                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-striped">

                                    <tbody>
                                        <tr>

                                            <td>

                                                <div class="form-group">
                                                    <div class="col-md-6 col-xs-8">
                                                        <select class="form-control input-sm" name="laporan"
                                                            id="laporan">
                                                            <option value=''>Pilih Dokumen</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <input type="hidden" class="form-control" name="no_ajud" id="no_ajud">
                                                <input type="hidden" class="form-control" name="aksi" id="aksi"
                                                    value="save-brgdtl">
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Pallet"
                                                        id="pallet" name="pallet">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="No Batch / Lot"
                                                        id="batch_no" name="batch_no">
                                                </div>
                            </div>
                            </td>
                            </tr>
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info simpan-brgdtl" id="submit" name="simpan">Simpan</button>
                    </form>
                    <button type="button" class="btn btn-default" id="submit" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    </div>
    </div>
    <!------------------ END MODAL BARANG DETIL --------------------------->

    <!---------------------------------------------- MODAL UPDATE BARANG DETIL ------------------------------------------------>
    <div class="modal fade" id="upd_brgdtl" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalTitle">Ubah Detil Barang</h4>
                    <form class="form-horizontal frm-upd-brgdtl" method="post">
                </div>

                <div class="modal-body" id="myModalBody">

                    <!-- Isi Modal -->

                    <div class="container">

                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-striped">

                                    <tbody>
                                        <tr>

                                            <td>

                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="No Dokumen"
                                                        id="no_doc_dok_u" name="no_doc_dok_u">
                                                </div>
                                                <input type="hidden" class="form-control" name="no_aju_dok_u"
                                                    id="no_aju_dok_u">
                                                <input type="hidden" class="form-control" name="id_brgdtl_u"
                                                    id="id_brgdtl_u">
                                                <input type="hidden" class="form-control" name="aksi" id="aksi"
                                                    value="upd-brgdtl">
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Pallet"
                                                        id="pallet_dok_u" name="pallet_dok_u">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="No Batch / Lot"
                                                        id="batch_no_dok_u" name="batch_no_dok_u">
                                                </div>
                            </div>
                            </td>
                            </tr>
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-info upd-brgdtl" id="submit" name="simpan">Simpan</button>
                    </form>
                    <button type="button" class="btn btn-default" id="submit" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    </div>
    </div>
    <!------------------ END MODAL UPDATE BARANG DETIL --------------------------->


    <!---------------------------------------------- MODAL BRG ------------------------------------------------>
    <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalTitle">Tambah Barang</h4>
                    <form class="form-horizontal frm-brg" method="post">
                </div>

                <div class="modal-body" id="myModalBody">

                    <!-- Isi Modal -->

                    <div class="container">

                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-striped">

                                    <tbody>
                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Seri Barang"
                                                        id="seri_barang" name="seri_barang">
                                                    <input type="hidden" class="form-control" name="no_ajuc"
                                                        id="no_ajuc">
                                                    <input type="hidden" class="form-control" name="aksi" id="aksi"
                                                        value="save-brg">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Kode Barang"
                                                        id="kd_brg" name="kd_brg">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Nama Barang"
                                                        id="nm_brg" name="nm_brg">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Jumlah"
                                                        id="jml" name="jml">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Satuan"
                                                        id="satuan" name="satuan">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="CIF" id="cif"
                                                        name="cif">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Currency"
                                                        id="cur" onkeyup="Kapital()" name="cur">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info simpan-brg" id="submit" name="simpan">Simpan</button>
                        </form>
                        <button type="button" class="btn btn-default" id="submit" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    <!------------------ END MODAL BRG --------------------------->

    <!---------------------------------------------- UPD MODAL BRG ------------------------------------------------>
    <div class="modal fade" id="upd_brg" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalTitle">Update Barang</h4>
                    <form class="form-horizontal frm-updbrg" method="post">
                </div>

                <div class="modal-body" id="myModalBody">

                    <!-- Isi Modal -->

                    <div class="container">

                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-striped">

                                    <tbody>
                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Seri Barang"
                                                        id="seri_barang_ut" name="seri_barang_ut" disabled>
                                                    <input type="hidden" class="form-control" name="seri_barang_u"
                                                        id="seri_barang_u">
                                                    <input type="hidden" class="form-control" name="no_aju_brgu"
                                                        id="no_aju_brgu">
                                                    <input type="hidden" class="form-control" name="aksi" id="aksi"
                                                        value="upd-brg">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Kode Barang"
                                                        id="kode_barang_u" name="kode_barang_u">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Nama Barang"
                                                        id="nama_barang_u" name="nama_barang_u">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Jumlah"
                                                        id="jml_u" name="jml_u">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Satuan"
                                                        id="sat_u" name="sat_u">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="CIF" id="cif_u"
                                                        name="cif_u">
                                                </div>
                                            </td>
                                        </tr>

                                        <tr>

                                            <td>
                                                <div class="col-xs-12">
                                                    <input type="text" class="form-control" placeholder="Currency"
                                                        id="cur_u" onkeyup="Kapital()" name="cur_u">
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default upd-brg" id="submit" name="update">Update</button>
                        </form>
                        <button type="button" class="btn btn-default" id="submit" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    <!------------------ END UPD MODAL BRG --------------------------->

    <!---------------------------------------------- MODAL PILIH ------------------------------------------------>
    <div class="modal fade" id="Modal-pilih" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalTitle">Pilih Aju</h4>
                    <form class="form-horizontal" method="post">
                </div>

                <div class="modal-body" id="myModalBody">

                    <!-- Isi Modal -->

                    <div class="container">

                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-striped">

                                    <tbody>
                                        <tr>

                                            <td>

                                                <div class="form-group">
                                                    <div class="col-md-8 col-xs-8">
                                                        <select class="form-control input-sm" name="laporan-pilih"
                                                            id="laporan-pilih">
                                                            <option value=''>Pilih Aju</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-info pilih-aju" id="submit" name="simpan">Pilih</button>
                        </form>
                        <button type="button" class="btn btn-default" id="tutup-modal"
                            data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    <!------------------ END MODAL PILIH --------------------------->
    <!---------------------------------------------- MODAL IMPOR BRG DTL ------------------------------------------------>
    <div class="modal fade" id="impor_brgdtl" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalTitle">Impor Data</h4>
                </div>

                <div class="modal-body" id="myModalBody">

                    <!-- Isi Modal -->

                    <div class="container">

                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-striped">

                                    <tbody>
                                        <tr>

                                            <td>

                                                <div class="form-group">
                                                    <div class="col-md-8 col-xs-8">
                                                        <form action="" method="post" enctype="multipart/form-data"
                                                            name="ContohUploadCSV" id="ContohUploadCSV">
                                                            <input type="file" name="filecsv" id="filecsv" />
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-info" id="submit" name="simpan">Impor</button>
                        </form>
                        <button type="button" class="btn btn-default" id="tutup-modal"
                            data-dismiss="modal">Close</button>
                    </div>

                </div>
            </div>
        </div>
    </div>
    </div>
    <!------------------ END MODAL PILIH --------------------------->
    </div>



    <hr class="featurette-divider">
    </hr>
    <div class="container">
        <div class="row">
            <div class="col-sm-6">
                <div class="tab-content">
                    <div id="stripe" class="tab-pane fade in active">
                        <form class="form-aju" method="POST">
                            <div class='form-row'>
                                <div class='form-group required'>
                                    <label class='control-label'>Nomor Aju</label> &nbsp;
                                    <button class='btn btn-info btn-xs aju-pilih' id='aju-pilih' data-toggle="modal"
                                        data-target="#Modal-pilih" type='button'>Click Me</button>
                                    <input class='form-control' id='no_aju' name='no_aju' type='text'>
                                    <input type="hidden" class="form-control" name="aksi" id="aksi" value="save-aju">
                                    <input type="hidden" class="form-control" name="user" id="user"
                                        value="<?php echo $username; ?>">
                                </div>

                            </div>
                            <div class='form-row'>
                                <div class='form-group'>
                                    <label class='control-label'>Tanggal Aju</label>
                                    <input class='form-control' id='tglan1' name='tglan1' type='text'>
                                </div>
                            </div>
                            <div class='form-row'>
                                <div class='form-group card required'>
                                    <label class='control-label'>Nomor Daftar</label>
                                    <input class='form-control' id='nopen' name='nopen' type='text'>
                                </div>
                            </div>
                            <div class='form-row'>
                                <div class='form-group'>
                                    <label class='control-label'>Tanggal Daftar</label>
                                    <input class='form-control' id='tglan2' name='tglan2' type='text'>
                                </div>
                                <div class='form-group expiration required'>
                                    <label class='control-label'>Penerima</label>
                                    <input class='form-control' id='penerima' name='penerima' type='text'>
                                </div>
                                <div class='form-group expiration required'>
                                    <label class='control-label'>Negara</label>
                                    <input class='form-control' id='negara' name='negara' type='text'>
                                </div>
                                <div class='form-group expiration required'>
                                    <label class='control-label'>CIF</label>
                                    <input class='form-control' id='cif' name='cif' type='text'>
                                </div>
                            </div>


                            <div class='form-row'>
                                <div class='form-group'>
                                    <label class='control-label'></label>
                                    <div class="col-sm-4">
                                        <button class='form-control btn btn-primary simpan-aju' type='button'> Simpan
                                            →</button>
                                    </div>
                                    <div class="col-sm-4">
                                        <button class='form-control btn btn-warning btn-reset' type='button'>
                                            Reset</button>
                                    </div>
                        </form>

                    </div>
                </div>
                <br><br><br><br>
            </div>

        </div>



    </div>

    <div class="col-sm-6">
        <label class='control-label'></label><!-- spacing -->

        <div class="alert alert-success" id="berhasil" role="alert" style="display:none;"><strong>Tersimpan!</strong>
            Data Berhasil Disimpan.</div>
        <div class="alert alert-warning" id="gagal" role="alert" style="display:none;"><strong>Gagal!</strong> Data
            Gagal Disimpan.</div>
        <div class="alert alert-warning" id="double" role="alert" style="display:none;"><strong>Gagal!</strong> Data Aju
            Sudah Tersimpan Sebelumnya.</div>

        <br>


        <!-- tbl1 -->
        <div class="jumbotron jumbotron-flat">
            <button class='btn btn-info btn-xs' id='btn-ndtl' data-toggle="modal" style="display:none;"
                data-target="#myModal" type='button'> New</button>
            <div class="tampildata1"></div>



        </div>

        <!-- tbl2 -->
        <div class="jumbotron jumbotron-flat">
            <button class='btn btn-info btn-xs' id='btn-nbrg' data-toggle="modal" style="display:none;"
                data-target="#myModal2" type='button'> New</button>
            <div class="tampildata2"></div>


        </div>

        <!-- tbl3 -->
        <div class="jumbotron jumbotron-flat">
            <button class='btn btn-info btn-xs' id='btn-nbrgdtl' data-toggle="modal" style="display:none;"
                data-target="#myModal3" type='button'> New</button>
            <div class="tampildata3"></div>


        </div>
        <br>
        <div class="alert alert-info" id="lucu" role="alert" style="display:none;">Please fill form correctly :). </div>


        <br><br><br>
    </div>



    </div>



    </div>
    </div>

    <?php
    if (!empty($_FILES["filecsv"]["tmp_name"])) {
        include 'db-inc2.php';

        $nama = $_FILES['filecsv']['name'];
        $ext = substr($nama, (strrpos($nama, ".") + 1), (strlen($nama) - strrpos($nama, ".")) - 1);
        if ($ext == "csv") {
            $namafile = $_FILES['filecsv']['tmp_name'];
            $pemisah = ";";
            $datacsv = fopen($namafile, "r");
            $jml = 0;
            while (($data = fgetcsv($datacsv, 10000, $pemisah)) !== FALSE) {
                $jml++;
                $no_aju = $data[0];
                $no_dok = $data[1];
                $batch = $data[2];
                $palet = $data[3];
                $id = $data[4];
                //echo $data[0]." - ".$data[1]." - ".$data[2]." - ".$data[3]." - ".$data[4]."</br>";

                $query = "INSERT INTO report.plb_doc_bc27_brgdtl(
                 no_aju_dok, no_doc_dok, batch_no_dok, pallet_dok, id_brgdtl)
                 VALUES ('$no_aju', '$no_dok', '$batch', '$palet', '$id')";
                $result = pg_query($query);
            }
            fclose($datacsv);
            echo "<SCRIPT>alert('" . $jml . " Data berhasil ditambahkan');</SCRIPT>";
        } else {
            echo "<SCRIPT>alert('Pastikan hanya file berformat csv saja');</SCRIPT>";
        }
    }
    ?>

</body>

</html>
<script type="text/javascript" src="assets/DataTables/media/js/jquerydt.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script type="text/javascript" src="jqueryw.js"></script>
<script type="text/javascript" src="assets/DataTables/media/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
<script type="text/javascript" src="js/bc27.js"></script>
<script>
function Kapital() {
    var x = document.getElementById("cur");
    x.value = x.value.toUpperCase();
}
</script>