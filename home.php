<?php
session_start();
$_SESSION['MENU'] = 'HOME';
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}

?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

    <title>IJS - PLB</title>

    <link rel="shortcut icon" href="images/icons/logoijs.jpg">
    <!-- start:bootstrap v3.2.0 -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <!-- start:font awesome v4.1.0 -->
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <!-- start:bootstrap reset -->
    <link rel="stylesheet" type="text/css" href="css/bootstrap-reset.css">
    <!-- start:style arjuna -->
    <link rel="stylesheet" type="text/css" href="assets/css/arjuna.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-reset.css">
    <link rel="stylesheet" type="text/css" href="assets/css/arjuna.css">
    <script src="assets/js/jquery-3.1.1.min.js"></script>


</head>

<body class="cl-default fixed">
    <!-- start:navbar top -->
    <header class="header">
        <!-- <a href="home.php" class="logo">Indra Jaya Swastika</a> -->
        <header class="header"><?php include("top_nav.php") ?></header>
    </header>
    <!-- end:navbar top -->

    <!-- start:wrapper body -->
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <!-- start:left sidebar -->
        <aside class="left-side sidebar-offcanvas"><?php include("left_menu.php") ?></aside>
        <!-- end:left sidebar -->
        <!-- start:right sidebar -->
        <aside class="right-side">
            <section class="content">
                <!-- start:content -->
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                    <!-- Indicators, Ini adalah Tombol BULET BULET dibawah. item ini dapat dihapus jika tidak diperlukan -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                    </ol>

                    <!-- Wrapper for slides, Ini adalah Tempat Gambar-->
                    <div class="carousel-inner">
                        <div class="item active">
                            <img src="images/images1.jpg" alt="myCarousel">
                            <div class="carousel-caption">

                            </div>
                        </div>

                        <div class="item">
                            <img class="" src="images/images2.jpg" alt="myCarousel">
                            <div class="carousel-caption">
                                <!—Penjelasan -->
                            </div>
                        </div>

                        <div class="item">
                            <img src="images/images3.jpg" alt="myCarousel">
                            <div class="carousel-caption">
                                <!—Penjelasan -->
                            </div>
                        </div>

                    </div>

                    <!-- Controls, Ini adalah Panah Kanan dan Kiri. item ini dapat dihapus jika tidak diperlukan-->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left"></span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right"></span>
                    </a>
                </div>
                <!-- end:content -->

            </section>
        </aside>
        <!-- end:right sidebar -->

    </div>
    <!-- end:wrapper body -->

    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/arjuna.js"></script>
    <!-- end:javascript for all pages-->
</body>

</html>