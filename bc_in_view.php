<?php
if ($_POST['jenis_act']) {

    require_once('db-inc2.php');
    require_once('insert_log_activity.php');

    $jenis = $_POST['jenis_act'];
    $tgl1 = $_POST['tglan1'];
    $tgl2 = $_POST['tglan2'];
    $kode_rel = TRIM($_POST['cust']);
    if ($kode_rel == "ALL") {
        $kode_rel = '50%';
    }

    // insert_log($username,$kategori,$address ,$status,$remark ) 

    $log_remark = "Open REPORT PEMASUKAN BARANG " . $jenis . " Tanggal " . date('d-m-Y', strtotime($tgl1)) . " s/d " . date('d-m-Y', strtotime($tgl2));
    insert_log($username, $kategori, $address, "REPORT", $log_remark);

    if ($jenis == 'BC 1.6') {

        // insert_log($status,$remark );	


        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN " . $jenis . " <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<br>";
        echo "<br>";


        $sqltext = "select A.nomor_daftar,
       A.tanggal_daftar,
       substring(A.nomor_aju,15) as no_aju,
       A.tanggal_aju,
       A.supplier,pack.jumlah4,C.kode_rel,B.nama,
       A.currency,
	   sum_varchar (distinct C.no_coo ||'<br />') as no_coo,
	   sum_varchar (distinct to_char( C.tgl_coo,'dd-MM-YYYY') ||'<br />') as tgl_coo,
	   sum_varchar (distinct A.harga ||'<br />') as harga,
	   sum_varchar (distinct C.no_bl ||'<br />') as no_bl,
	   sum_varchar (distinct to_char( C.tgl_bl,'dd-MM-YYYY') ||'<br />') as tgl_bl,
	   sum_varchar (distinct to_char( G.tgl_awal,'dd-MM-YYYY') ||'<br />') as tgl_masuk,
       sum_varchar (distinct case when A.seri_barang < 10 then '0' else '' end || A.seri_barang ||'. ' || A.kode_barang ||'  |  '||A.uraian_barang ||'<br />' ) as seri_barang,
       sum_varchar (distinct A.jumlah_kemasan ||'<br />') as jumlah_kemasan,
       sum_varchar (distinct A.cif_kemasan ||'<br />') as cif_kemasan,
       sum_varchar (distinct trim(A.nomor_dokumen) ||'<br />') as invoice,
       sum_varchar (distinct to_char( A.tanggal_dokumen,'dd-mm-yyyy')||'<br />') as tgl_invoice,
       --sum_varchar (distinct A.uraian_barang ||'<br />') as uraian_barang,
       --sum_varchar (distinct A.kode_barang ||'<br />') as kode_barang,
       sum_varchar (distinct cast(A.jumlah as integer) ||'<br />') as jumlah,
       sum_varchar (distinct C.keterangan ||'<br />') as keterangan,
	   sum_varchar (distinct C.satuan ||'<br />') as satuan,
       sum_varchar (distinct case when G.kendaraan = 'CONTAINER' THEN  G.no_unit ||' / ' ||G.sizecode else G.nopol end ||'<br>' )as kendaraan,
       sum_varchar (distinct A.hs ||'<br />') as hs,
       sum_varchar (distinct cast(A.tarif as integer) ||'%<br />') as tarif,
       sum_varchar (distinct A.kemasan ||'<br />') as kemasan,
       sum_varchar (distinct split_part(C.id_flowbrg_splitted,'-',1) ||'<br />') as book
       from report.plb_documents_in A
	   left join report.plb_flowbrg C on case when trim(C.batch_no) = trim(substring(A.nomor_dokumen,1,50)) then  trim(substring(A.nomor_dokumen,1,50)) = trim(C.batch_no) else  trim(substring(A.nomor_dokumen,1,25)) = C.exim end
       and A.uraian_dokumen = 'INVOICE'
	   and substring(A.nomor_aju,15)=C.no_aju
       and A.nomor_daftar = C.no_bc_16
       left join v_customer B on C.kode_rel = B.kode_rel
       left join report.plb_flowcont G on C.id_flowbrg_splitted = G.id_flowcont
       left join (select Z.no_aju, sum(Z.jumlah) as jumlah4
			from report.plb_flowbrg Z
			where Z.id_flowbrg_in is null
			and Z.kode_rel like '$kode_rel'
			group by 1) as pack
			on substring(A.nomor_aju,15) = pack.no_aju
       where A.tanggal_daftar between '$tgl1' and '$tgl2'
       and C.id_flowbrg_in is null
       and C.kode_rel like '$kode_rel'
       group by 1,2,3,4,5,6,7,8,9
       order by A.nomor_daftar";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table id='data' class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th rowspan='2'>JENIS DOKUMEN</th>";
        echo            "<th colspan='4'>DOKUMEN PABEAN</th>";
        echo            "<th rowspan='2'>CUSTOMER</th>";
        echo            "<th rowspan='2'>PEMASOK/PENGIRIM</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th rowspan='2'>SERI BARANG | KODE BARANG | NAMA BARANG</th>";
        //echo			"<th rowspan='2'>JUMLAH KEMASAN</th>";
        //echo			"<th rowspan='2'>NILAI KEMASAN</th>";
        echo            "<th colspan='2'>URAIAN BARANG (TPB)</th>";
        echo            "<th colspan='5'>URAIAN BARANG (LPB)</th>";
        echo            "<th colspan='2'>BL/AWB</th>";
        echo            "<th colspan='4'>INVOICE</th>";
        echo            "<th rowspan='2'>TARIF</th>";
        echo            "<th rowspan='2'>KODE HS</th>";
        echo            "<th rowspan='2'>NO COO</th>";
        echo            "<th rowspan='2'>TGL COO</th>";
        echo            "<th rowspan='2'>KETERANGAN</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TGL AJU</th>";
        echo            "<th>JUMLAH KEMASAN</th>";
        echo            "<th>SATUAN KEMASAN</th>";
        echo            "<th>ID BOOK</th>";
        echo            "<th>JUMLAH</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>LOKASI</th>";
        echo            "<th>TANGGAL MASUK</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>CURRENCY</th>";
        echo            "<th>NILAI</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td align=center>" . $number . "</td>";
            echo            "<td>" . $jenis . "</td>";
            echo            "<td>" . $row['nomor_daftar'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_daftar'])) . "</td>";
            echo            "<td>" . $row['no_aju'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_aju'])) . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['supplier'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td class='td-wide'>" . $row['seri_barang'] . "</td>";
            //echo			"<td>".$row['jumlah_kemasan']."</td>";
            //echo			"<td>".$row['cif_kemasan']."</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['kemasan'] . "</td>";
            echo            "<td>" . $row['book'] . "</td>";
            echo            "<td>" . $row['jumlah4'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            if (($rel == '500200') || ($rel == '501500') || ($rel == '500300')) {
                echo            "<td>WH.03</td>";
            } else if (($rel == '501600') || ($rel == '500100')) {
                echo            "<td>WH.04</td>";
            } else {
                echo            "<td>WH.LAP</td>";
            }
            echo            "<td>" . $row['tgl_masuk'] . "</td>";
            echo            "<td>" . $row['no_bl'] . "</td>";
            //		echo			"<td>".date('d-m-Y', strtotime($row['tgl_bl']))."</td>";
            echo            "<td>" . $row['tgl_bl'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . $row['tgl_invoice'] . "</td>";
            echo            "<td>" . $row['currency'] . "</td>";
            echo            "<td>" . number_format($row['harga'], 2) . "</td>";
            echo            "<td>" . $row['tarif'] . "</td>";
            echo            "<td>" . $row['hs'] . "</td>";
            echo            "<td>" . $row['no_coo'] . "</td>";
            echo            "<td>" . $row['tgl_coo'] . "</td>";
            echo            "<td>" . $row['keterangan'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'CY') {


        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN BC 1.6 <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<br>";
        echo "<br>";


        $sqltext = "select A.nomor_daftar, A.tanggal_daftar, substring(A.nomor_aju,15) as no_aju, A.tanggal_aju,
       A.supplier,pack.jumlah4,C.kode_rel,F.nama,
       A.currency,A.harga,B.nomor_dokumen as no_bl, B.tanggal_dokumen as tgl_bl,
       sum_varchar (distinct A.seri_barang ||'<br />') as seri_barang,
       sum_varchar (distinct A.nomor_dokumen ||'<br />') as invoice,
       sum_varchar (distinct to_char( A.tanggal_dokumen,'dd-mm-yyyy')||'<br />') as tgl_invoice,
	   sum_varchar (distinct to_char( G.tgl_in,'dd-mm-yyyy')||'<br />') as tgl_masuk,
       sum_varchar (distinct A.uraian_barang ||'<br />') as uraian_barang,
       sum_varchar (distinct A.kode_barang ||'<br />') as kode_barang,
       sum_varchar (distinct cast(A.jumlah as integer) ||'<br />') as jumlah,
       sum_varchar (distinct C.keterangan ||'<br />') as keterangan,
	   sum_varchar (distinct C.satuan ||'<br />') as satuan,
	   sum_varchar(distinct case when G.kendaraan = 'CONTAINER' THEN  G.no_unit ||' / ' ||G.sizecode end ||'<br />' )as kendaraan,
       sum_varchar (distinct A.hs ||'<br />') as hs,
       sum_varchar (distinct cast(A.tarif as integer) ||'%<br />') as tarif,
       sum_varchar (distinct A.kemasan ||'<br />') as kemasan,
       sum_varchar (distinct split_part(C.id_flowbrg,'-',1) ||'<br />') as book
       from report.plb_documents_in A
       join report.plb_documents_in B on A.nomor_aju = B.nomor_aju
       and A.nomor_daftar = B.nomor_daftar and B.uraian_dokumen = 'B/L'
       left join report.plb_cy C on A.nomor_dokumen = C.batch_no
	   left join v_customer F on C.kode_rel = F.kode_rel
	   left join wh_loket G on C.id_loket = G.id_loket
       left join (select Z.no_aju, sum(Z.jumlah) as jumlah4
			from report.plb_cy Z
			where Z.id_flowbrg_in is null
			and Z.kode_rel like '$kode_rel'
			group by 1) as pack
			on substring(A.nomor_aju,15) = pack.no_aju
       where A.tanggal_daftar between '$tgl1' and '$tgl2'
       and C.id_flowbrg_in is null
       and C.kode_rel like '$kode_rel'
       group by 1,2,3,4,5,6,7,8,9,10,11,12
       order by A.nomor_daftar";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table id='data' class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th rowspan='2'>JENIS DOKUMEN</th>";
        echo            "<th colspan='4'>DOKUMEN PABEAN</th>";
        echo            "<th rowspan='2'>CUSTOMER</th>";
        echo            "<th rowspan='2'>PEMASOK/PENGIRIM</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th rowspan='2'>SERI BARANG | KODE BARANG | NAMA BARANG</th>";
        //echo			"<th rowspan='2'>JUMLAH KEMASAN</th>";
        //echo			"<th rowspan='2'>NILAI KEMASAN</th>";
        echo            "<th colspan='2'>URAIAN BARANG (TPB)</th>";
        echo            "<th colspan='5'>URAIAN BARANG (LPB)</th>";
        echo            "<th colspan='2'>BL/AWB</th>";
        echo            "<th colspan='4'>INVOICE</th>";
        echo            "<th rowspan='2'>TARIF</th>";
        echo            "<th rowspan='2'>KODE HS</th>";
        echo            "<th rowspan='2'>KETERANGAN</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TGL AJU</th>";
        echo            "<th>JUMLAH KEMASAN</th>";
        echo            "<th>SATUAN KEMASAN</th>";
        echo            "<th>ID BOOK</th>";
        echo            "<th>JUMLAH</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>LOKASI</th>";
        echo            "<th>TANGGAL MASUK</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>CURRENCY</th>";
        echo            "<th>NILAI</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td align=center>" . $number . "</td>";
            echo            "<td>" . $jenis . "</td>";
            echo            "<td>" . $row['nomor_daftar'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_daftar'])) . "</td>";
            echo            "<td>" . $row['no_aju'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_aju'])) . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['supplier'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td class='td-wide'>" . $row['seri_barang'] . "</td>";
            //echo			"<td>".$row['jumlah_kemasan']."</td>";
            //echo			"<td>".$row['cif_kemasan']."</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['kemasan'] . "</td>";
            echo            "<td>" . $row['book'] . "</td>";
            echo            "<td>" . $row['jumlah4'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            if (($rel == '500200') || ($rel == '501500') || ($rel == '500300')) {
                echo            "<td>WH.03</td>";
            } else if (($rel == '501600') || ($rel == '500100')) {
                echo            "<td>WH.04</td>";
            } else {
                echo            "<td>WH.LAP</td>";
            }
            echo            "<td>" . $row['tgl_masuk'] . "</td>";
            echo            "<td>" . $row['no_bl'] . "</td>";
            //		echo			"<td>".date('d-m-Y', strtotime($row['tgl_bl']))."</td>";
            echo            "<td>" . $row['tgl_bl'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . $row['tgl_invoice'] . "</td>";
            echo            "<td>" . $row['currency'] . "</td>";
            echo            "<td>" . number_format($row['harga'], 2) . "</td>";
            echo            "<td>" . $row['tarif'] . "</td>";
            echo            "<td>" . $row['hs'] . "</td>";
            echo            "<td>" . $row['keterangan'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'BC 2.7') {


        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN BC 2.7 <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<br>";
        echo "<br>";


        $sqltext = "select A.no_daftar, A.tgl_daftar, A.no_aju, A.tgl_aju,
       case when A.tarif_hs <> '' then A.tarif_hs else J.hs end as hs,
       case when A.currency_cif <> '' then A.currency_cif else J.currency end as curency,
       case when A.satuan is not null then A.satuan else J.satuan end as satuan_dokumen,
       case when A.jumlah is not null then cast(A.jumlah as integer) else cast(pack2.jumlah_brg as integer) end as jumlah_dokumen,
       A.kode_rel,F.nama,pack.jumlahw,
	   sum_varchar (distinct Z.no_doc ||'<br />') as invoiced,
       sum_varchar (distinct B.no_doc ||'<br />') as invoice,
	   sum_varchar (distinct D.satuan ||'<br />') as satuan,
       sum_varchar (distinct to_char( B.tgl_doc,'dd-mm-yyyy')||'<br />') as tgl_invoice,
	   sum_varchar (distinct to_char( Z.tgl_doc,'dd-mm-yyyy')||'<br />') as tgl_docd,
	   sum_varchar (distinct to_char( E.tgl_awal,'dd-mm-yyyy')||'<br />') as tgl_masuk,
       sum_varchar (distinct A.cif ||'<br />') as harga,
       sum_varchar (distinct G.itemname ||'<br />') as nm_barang,
       sum_varchar(distinct case when E.kendaraan = 'CONTAINER' THEN  E.no_unit ||' / ' ||E.sizecode else E.nopol end ||'<br />' )as kendaraan,
       sum_varchar (distinct A.kode_brg ||'<br />') as kode_barang,
       sum_varchar (distinct case when J.seri_barang < 10 then '0' else '' end || J.seri_barang ||'. ' || J.kode_barang ||'  |  '||J.nama_barang ||'<br />') as barang_dok,
       sum_varchar (distinct split_part(D.id_flowbrg,'-',1) ||'<br />') as book
       from report.plb_doc_in27 A
       join report.plb_doc_in27_dtl B on A.no_aju = B.no_aju
       left join report.plb_doc_bc27_brg J on A.no_aju = J.no_aju
       join v_customer F on A.kode_rel = F.kode_rel
       join wh_flowbrg D on B.no_doc = D.exim
       join report.plb_flowcont E on D.id_flowbrg_splitted = E.id_flowcont and substring(A.no_aju,15) = E.no_aju
       join wh_cargo G on D.itemcode = G.itemcode
	   join report.plb_doc_in27_dtl Z on A.no_aju = Z.no_aju and Z.jenis_doc = 'INVOICE'
	   
	   left join (select E.no_aju, sum(F.jumlah0) as jumlahw
                  from report.plb_doc_in27_dtl E, wh_flowbrg F, report.plb_doc_in27 A, wh_flowcont D
                  where E.no_doc = F.exim
                  and A.no_aju = E.no_aju
				  and substring(A.no_aju,15) = D.no_aju
				  and D.id_flowcont = F.id_flowbrg_splitted
                  group by 1) as pack
          on A.no_aju = pack.no_aju

          left join (select E.no_aju, sum(E.jumlah) as jumlah_brg
                  from report.plb_doc_bc27_brg E, report.plb_doc_in27 A
                  where A.no_aju = E.no_aju
                  group by 1) as pack2
          on A.no_aju = pack2.no_aju
	         
       where A.tgl_daftar between '$tgl1' and '$tgl2'
       and A.kode_rel like '$kode_rel'
	   and A.nsurat='BC 2.7'
       and B.jenis_doc in ('INVOICE','LAINNYA','PACKING LIST','SURAT JALAN')
       group by 1,2,3,4,5,6,7,8,9,10,11
       order by A.tgl_daftar";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table id='data' class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th rowspan='2'>JENIS DOKUMEN</th>";
        echo            "<th colspan='4'>DOKUMEN PABEAN</th>";
        echo            "<th rowspan='2'>CUSTOMER</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th rowspan='2'>KODE BARANG</th>";
        echo            "<th rowspan='2'>NAMA BARANG</th>";
        echo            "<th rowspan='2'>SERI BARANG | KODE BARANG | NAMA BARANG</th>";
        echo            "<th colspan='2'>URAIAN BARANG (TPB)</th>";
        echo            "<th colspan='5'>URAIAN BARANG (LPB)</th>";
        echo            "<th colspan='4'>INVOICE</th>";
        echo            "<th colspan='2'>PACKING LIST</th>";
        echo            "<th rowspan='2'>TARIF</th>";
        echo            "<th rowspan='2'>KODE HS</th>";
        echo            "<th rowspan='2'>KETERANGAN</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TGL AJU</th>";
        echo            "<th>JUMLAH KEMASAN</th>";
        echo            "<th>SATUAN KEMASAN</th>";
        echo            "<th>ID BOOK</th>";
        echo            "<th>JUMLAH</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>LOKASI</th>";
        echo            "<th>TANGGAL MASUK</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>CURRENCY</th>";
        echo            "<th>NILAI</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td align=center>" . $number . "</td>";
            echo            "<td>BC 2.7</td>";
            echo            "<td>" . $row['no_daftar'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_daftar'])) . "</td>";
            echo            "<td>" . $row['no_aju'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_aju'])) . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td>" . $row['kode_barang'] . "</td>";
            echo            "<td>" . $row['nm_barang'] . "</td>";
            echo            "<td class='td-wide'>" . $row['barang_dok'] . "</td>";
            echo            "<td>" . $row['jumlah_dokumen'] . "</td>";
            echo            "<td>" . $row['satuan_dokumen'] . "</td>";
            echo            "<td>" . $row['book'] . "</td>";
            echo            "<td>" . $row['jumlahw'] . "</td>";
            //echo			"<td>".$row['satuan']."</td>";
            echo            "<td>BAG</td>";
            if ($rel == '501600') {
                echo            "<td>WH.04</td>";
            } else {
                echo            "<td>WH.LAP</td>";
            }
            echo            "<td>" . $row['tgl_masuk'] . "</td>";
            echo            "<td>" . $row['invoiced'] . "</td>";
            echo            "<td>" . $row['tgl_invoice'] . "</td>";
            echo            "<td>" . $row['curency'] . "</td>";
            echo            "<td>" . number_format($row['harga'], 2) . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . $row['tgl_docd'] . "</td>";
            echo            "<td>" . $row['tarif'] . "</td>";
            echo            "<td>" . $row['hs'] . "</td>";
            echo            "<td>" . $row['keterangan'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'BC 4.0') {


        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN " . $jenis . " <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<br>";
        echo "<br>";


        $sqltext = "select A.nomor_daftar, A.tanggal_daftar, substring(A.nomor_aju,15) as no_aju,
       pack.jumlah4,C.kode_rel,
       A.currency,F.nama,tot_doc.tarif,
	   --A.seri_barang,A.kode_barang,A.uraian_barang,
       sum_varchar (distinct A.nomor_dokumen ||'<br />') as invoice,
	   sum_varchar (distinct c.satuan ||'<br />') as satuan,
	   sum_varchar (distinct to_char( C.tgl_aju,'dd-mm-yyyy')||'<br />') as tgl_aju,
	   sum_varchar (distinct to_char( B.tgl_awal,'dd-mm-yyyy')||'<br />') as tgl_masuk,
	   sum_varchar (distinct to_char( A.tanggal_dokumen,'dd-mm-yyyy')||'<br />') as tanggal_dokumen,
	   sum_varchar (distinct case when A.seri_barang < 10 then '0' else '' end || A.seri_barang ||'. ' || A.kode_barang ||'  |  '||A.uraian_barang ||'<br />' ) as seri_barang,
       --sum_varchar (distinct A.uraian_barang ||'<br />') as uraian_barang,
       --sum_varchar (distinct A.kode_barang ||'<br />') as kode_barang,
       sum_varchar (distinct cast(A.jumlah as integer) ||'<br />') as jumlah,
	   sum_varchar(distinct case when B.kendaraan = 'CONTAINER' THEN  B.no_unit ||' / ' ||B.sizecode else B.nopol end ||'<br />' )as kendaraan,
	   sum_varchar (distinct C.keterangan ||'<br />') as keterangan,
       sum_varchar (distinct A.hs ||'<br />') as hs,
       sum_varchar (distinct A.kemasan ||'<br />') as kemasan,
       sum_varchar (distinct split_part(C.id_flowbrg_splitted,'-',1) ||'<br />') as book
       from report.plb_documents_in40 A
       left join report.plb_flowbrg C on A.nomor_dokumen = C.exim
       and substring(A.nomor_aju,15)=C.no_aju and A.nomor_daftar = C.no_bc_16
	   left join v_customer F on C.kode_rel = F.kode_rel
	   left join report.plb_flowcont B on C.id_flowbrg_splitted = B.id_flowcont
       join (select Z.no_aju, sum(Z.jumlah) as jumlah4
			from report.plb_flowbrg Z
			where Z.id_flowbrg_in is null
			and Z.kode_rel like '501600'
			group by 1) as pack
			on substring(A.nomor_aju,15) = pack.no_aju
	join (select distinct A.nomor_aju, sum(A.tarif) as tarif
	from report.plb_documents_in40 A
	group by 1) as tot_doc on C.no_aju = substring(tot_doc.nomor_aju,15)
       where A.tanggal_daftar between '$tgl1' and '$tgl2'
       and C.id_flowbrg_in is null
	   and C.kode_rel like '501600'
	   and A.uraian_dokumen in ('SURAT JALAN','PACKING LIST','LAINNYA')
       group by 1,2,3,4,5,6,7,8
       order by A.nomor_daftar";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table id='data' class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th rowspan='2'>JENIS DOKUMEN</th>";
        echo            "<th colspan='4'>DOKUMEN PABEAN</th>";
        echo            "<th rowspan='2'>CUSTOMER</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th rowspan='2'>SERI BARANG | KODE BARANG | NAMA BARANG</th>";
        //echo			"<th rowspan='2'>KODE BARANG</th>";
        //echo			"<th rowspan='2'>NAMA BARANG</th>";
        echo            "<th colspan='2'>URAIAN BARANG (TPB)</th>";
        echo            "<th colspan='5'>URAIAN BARANG (LPB)</th>";
        echo            "<th colspan='4'>PACKING LIST</th>";
        echo            "<th rowspan='2'>KETERANGAN</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TGL AJU</th>";
        echo            "<th>JUMLAH KEMASAN</th>";
        echo            "<th>SATUAN KEMASAN</th>";
        echo            "<th>ID BOOK</th>";
        echo            "<th>JUMLAH</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>LOKASI</th>";
        echo            "<th>TANGGAL MASUK</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>VALAS</th>";
        echo            "<th>NILAI</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td align=center>" . $number . "</td>";
            echo            "<td>" . $jenis . "</td>";
            echo            "<td>" . $row['nomor_daftar'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_daftar'])) . "</td>";
            echo            "<td>" . $row['no_aju'] . "</td>";
            echo            "<td>" . $row['tgl_aju'] . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td class='td-wide'>" . $row['seri_barang'] . "</td>";
            //echo			"<td>".$row['kode_barang']."</td>";
            //echo			"<td>".$row['uraian_barang']."</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['kemasan'] . "</td>";
            echo            "<td>" . $row['book'] . "</td>";
            echo            "<td>" . $row['jumlah4'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            if ($rel == '501600') {
                echo            "<td>WH.04</td>";
            }
            echo            "<td>" . $row['tgl_masuk'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_dokumen'])) . "</td>";
            echo            "<td>IDR</td>";
            echo            "<td>" . number_format($row['tarif'], 2) . "</td>";
            echo            "<td>" . $row['keterangan'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    }
}