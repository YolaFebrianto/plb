<?php
session_start();
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}

$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

?>

<?php
if ($_POST['jenisc']) {
?>
<?php
    require_once('db-inc2.php');

    $jenis = $_POST['jenisc'];
    $tgl1 = $_POST['tglc1'];
    $tgl2 = $_POST['tglc2'];
    $kode_rel = TRIM($_POST['custc']);
    if ($kode_rel == "ALL") {
        $kode_rel = '50%';
    }
    $startRec = 0;

    if ($jenis == 'BC 1.6') {
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=LaporanDokumenBC1.6per" . date('d-m-Y', strtotime($tgl1)) . "S/D" . date('d-m-Y', strtotime($tgl2)) . ".xls");

        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> DOKUMEN PABEAN " . $jenis . " <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<hr>";
        echo "<br>";
        echo "<br>";


        $sqltext = "select distinct nomor_daftar,tanggal_daftar,uraian_dokumen,nomor_dokumen,tanggal_dokumen,
            jumlah,kemasan,harga,currency,
            substring(nomor_aju,15) as nomor_aju,tanggal_aju,kode_barang,seri_barang,jumlah_kemasan,cif_kemasan,
            sum_varchar (distinct hs ||'<br />') as hs,
            sum_varchar (distinct uraian_barang ||'<br />') as uraian_barang,
            sum_varchar (distinct tarif ||'<br />') as tarif
            from report.plb_documents_in where tanggal_daftar between '$tgl1' and '$tgl2'
            and uraian_dokumen in ('INVOICE','PACKING LIST')
            group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
            order by nomor_aju,seri_barang";

        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th>NO</th>";
        echo            "<th>URAIAN DOKUMEN</th>";
        echo            "<th>NOMOR DOKUMEN</th>";
        echo            "<th>TANGGAL DOKUMEN</th>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TANGGAL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TANGGAL AJU</th>";
        echo            "<th>SERI BARANG</th>";
        echo            "<th>KODE BARANG</th>";
        echo            "<th>URAIAN BARANG</th>";
        echo            "<th>JUMLAH</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>JUMLAH TOTAL</th>";
        echo            "<th>TARIF KEMASAN</th>";
        echo            "<th>TARIF</th>";
        echo            "<th>CURRENCY</th>";
        echo            "<th>HARGA</th>";
        echo            "<th>KODE HS</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = isset($row['kode_rel']);
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['uraian_dokumen'] . "</td>";
            echo            "<td>" . $row['nomor_dokumen'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_dokumen'])) . "</td>";
            echo            "<td>" . $row['nomor_daftar'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_daftar'])) . "</td>";
            echo            "<td>" . $row['nomor_aju'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_aju'])) . "</td>";
            echo            "<td>" . $row['seri_barang'] . "</td>";
            echo            "<td>" . $row['kode_barang'] . "</td>";
            echo            "<td>" . $row['uraian_barang'] . "</td>";
            echo            "<td>" . $row['jumlah_kemasan'] . "</td>";
            echo            "<td>" . $row['kemasan'] . "</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['cif_kemasan'] . "</td>";
            echo            "<td>" . $row['tarif'] . "</td>";
            echo            "<td>" . $row['currency'] . "</td>";
            echo            "<td>" . $row['harga'] . "</td>";
            echo            "<td>" . $row['hs'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'BC 4.0') {
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=LaporanDokumenBC4.0per" . date('d-m-Y', strtotime($tgl1)) . "S/D" . date('d-m-Y', strtotime($tgl2)) . ".xls");

        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> DOKUMEN PABEAN " . $jenis . " <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<hr>";
        echo "<br>";
        echo "<br>";


        $sqltext = "select distinct nomor_daftar,tanggal_daftar,uraian_dokumen,nomor_dokumen,tanggal_dokumen,
            jumlah,kemasan,
            substring(nomor_aju,15) as nomor_aju,tanggal_aju,kode_barang,seri_barang,
            sum_varchar (distinct uraian_barang ||'<br />') as uraian_barang,
            sum_varchar (distinct tarif ||'<br />') as tarif
            from report.plb_documents_in40 where tanggal_daftar between '$tgl1' and '$tgl2'
            and uraian_dokumen = 'PACKING LIST'
            group by 1,2,3,4,5,6,7,8,9,10,11
            order by nomor_aju,seri_barang";

        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th>NO</th>";
        echo            "<th>URAIAN DOKUMEN</th>";
        echo            "<th>NOMOR DOKUMEN</th>";
        echo            "<th>TANGGAL DOKUMEN</th>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TANGGAL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TANGGAL AJU</th>";
        echo            "<th>SERI BARANG</th>";
        echo            "<th>KODE BARANG</th>";
        echo            "<th>URAIAN BARANG</th>";
        echo            "<th>JUMLAH TOTAL</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>TARIF</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['uraian_dokumen'] . "</td>";
            echo            "<td>" . $row['nomor_dokumen'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_dokumen'])) . "</td>";
            echo            "<td>" . $row['nomor_daftar'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_daftar'])) . "</td>";
            echo            "<td>" . $row['nomor_aju'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_aju'])) . "</td>";
            echo            "<td>" . $row['seri_barang'] . "</td>";
            echo            "<td>" . $row['kode_barang'] . "</td>";
            echo            "<td>" . $row['uraian_barang'] . "</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['kemasan'] . "</td>";
            echo            "<td>" . $row['tarif'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'BC 2.7') {
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=LaporanDokumenBC2.7per" . date('d-m-Y', strtotime($tgl1)) . "S/D" . date('d-m-Y', strtotime($tgl2)) . ".xls");

        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> DOKUMEN PABEAN BC 2.7 <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<hr>";
        echo "<br>";
        echo "<br>";


        $sqltext = "select distinct nomor_daftar,tanggal_daftar,uraian_dokumen,nomor_dokumen,tanggal_dokumen,
            jumlah,SATUAN as kemasan,
            substring(no_aju,15) as nomor_aju,tgl_aju,kode_barang,seri_barang,
            sum_varchar (distinct uraian_barang ||'<br />') as uraian_barang,
            sum_varchar (distinct harga_penyerahan ||'<br />') as tarif
            from report.plb_documents_outbc27 where tanggal_daftar between '$tgl1' and '$tgl2'
            and uraian_dokumen in ('INVOICE','PACKING LIST')
            group by 1,2,3,4,5,6,7,8,9,10,11
            order by nomor_aju,seri_barang";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th>NO</th>";
        echo            "<th>URAIAN DOKUMEN</th>";
        echo            "<th>NOMOR DOKUMEN</th>";
        echo            "<th>TANGGAL DOKUMEN</th>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TANGGAL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TANGGAL AJU</th>";
        echo            "<th>SERI BARANG</th>";
        echo            "<th>KODE BARANG</th>";
        echo            "<th>URAIAN BARANG</th>";
        echo            "<th>JUMLAH TOTAL</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>TARIF</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['uraian_dokumen'] . "</td>";
            echo            "<td>" . $row['nomor_dokumen'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_dokumen'])) . "</td>";
            echo            "<td>" . $row['nomor_daftar'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_daftar'])) . "</td>";
            echo            "<td>" . $row['nomor_aju'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_aju'])) . "</td>";
            echo            "<td>" . $row['seri_barang'] . "</td>";
            echo            "<td>" . $row['kode_barang'] . "</td>";
            echo            "<td>" . $row['uraian_barang'] . "</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['kemasan'] . "</td>";
            echo            "<td>" . $row['tarif'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'BC 2.8') {
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=LaporanDokumenBC2.8per" . date('d-m-Y', strtotime($tgl1)) . "S/D" . date('d-m-Y', strtotime($tgl2)) . ".xls");

        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> DOKUMEN PABEAN BC 2.8 <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<hr>";
        echo "<br>";
        echo "<br>";


        $sqltext = "select distinct nomor_daftar,tanggal_daftar,uraian_dokumen,nomor_dokumen,tanggal_dokumen,
            jumlah,kemasan,cif,cif_rupiah,
            substring(no_aju,15) as nomor_aju,tgl_aju,kode_barang,seri_barang,
            sum_varchar (distinct uraian_barang ||'<br />') as uraian_barang
            from report.plb_documents_out where tanggal_daftar between '$tgl1' and '$tgl2'
            and uraian_dokumen = 'INVOICE'
            group by 1,2,3,4,5,6,7,8,9,10,11,12,13
            order by nomor_aju,seri_barang";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th>NO</th>";
        echo            "<th>URAIAN DOKUMEN</th>";
        echo            "<th>NOMOR DOKUMEN</th>";
        echo            "<th>TANGGAL DOKUMEN</th>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TANGGAL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TANGGAL AJU</th>";
        echo            "<th>SERI BARANG</th>";
        echo            "<th>KODE BARANG</th>";
        echo            "<th>URAIAN BARANG</th>";
        echo            "<th>JUMLAH TOTAL</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>CIF</th>";
        echo            "<th>CIF RUPIAH</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = isset($row['kode_rel']);
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['uraian_dokumen'] . "</td>";
            echo            "<td>" . $row['nomor_dokumen'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_dokumen'])) . "</td>";
            echo            "<td>" . $row['nomor_daftar'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_daftar'])) . "</td>";
            echo            "<td>" . $row['nomor_aju'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime(isset($row['tanggal_aju']))) . "</td>";
            echo            "<td>" . $row['seri_barang'] . "</td>";
            echo            "<td>" . $row['kode_barang'] . "</td>";
            echo            "<td>" . $row['uraian_barang'] . "</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['kemasan'] . "</td>";
            echo            "<td>" . $row['cif'] . "</td>";
            echo            "<td>" . $row['cif_rupiah'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'BC 4.1') {
        header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=LaporanDokumenBC4.1per" . date('d-m-Y', strtotime($tgl1)) . "S/D" . date('d-m-Y', strtotime($tgl2)) . ".xls");

        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> DOKUMEN PABEAN BC 4.1 <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<hr>";
        echo "<br>";
        echo "<br>";


        $sqltext = "select distinct nomor_daftar,tanggal_daftar,uraian_dokumen,
            jumlah,satuan as kemasan,
            substring(no_aju,15) as nomor_aju,tgl_aju,kode_barang,seri_barang,
			sum_varchar (distinct to_char( tanggal_dokumen,'dd-MM-YYYY') ||'<br />') as tanggal_dokumen,
            sum_varchar (distinct nomor_dokumen ||'<br />') as nomor_dokumen,
            sum_varchar (distinct uraian_barang ||'<br />') as uraian_barang
            from report.plb_documents_outbc41 where tanggal_daftar between '$tgl1' and '$tgl2'
            and uraian_dokumen in ('INVOICE','PACKING LIST')
            group by 1,2,3,4,5,6,7,8,9
            order by tanggal_daftar,nomor_aju,seri_barang ASC";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th>NO</th>";
        echo            "<th>URAIAN DOKUMEN</th>";
        echo            "<th>NOMOR DOKUMEN</th>";
        echo            "<th>TANGGAL DOKUMEN</th>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TANGGAL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TANGGAL AJU</th>";
        echo            "<th>SERI BARANG</th>";
        echo            "<th>KODE BARANG</th>";
        echo            "<th>URAIAN BARANG</th>";
        echo            "<th>JUMLAH TOTAL</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>TARIF</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['uraian_dokumen'] . "</td>";
            echo            "<td>" . $row['nomor_dokumen'] . "</td>";
            echo            "<td>" . $row['tanggal_dokumen'] . "</td>";
            echo            "<td>" . $row['nomor_daftar'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_daftar'])) . "</td>";
            echo            "<td>" . $row['nomor_aju'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_aju'])) . "</td>";
            echo            "<td>" . $row['seri_barang'] . "</td>";
            echo            "<td>" . $row['kode_barang'] . "</td>";
            echo            "<td>" . $row['uraian_barang'] . "</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['kemasan'] . "</td>";
            echo            "<td>" . $row['tarif'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    }
}
?>