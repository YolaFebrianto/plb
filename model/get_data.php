<?php
require_once('db-inc2.php');
if(isset($_GET["jenis"]))
{

header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
clearstatcache();


	$jenis = $_GET["jenis"];
	$tgl1 = $_GET["tgl1"];
	$tgl2 = $_GET["tgl2"];
	$kode_rel = trim($_GET["kode_rel"]);
	$logincat = trim($_GET["kat"]);
	$tahun = $_GET["tahun"];
	$data = $_GET["datab"];
	
	if ($kode_rel == "ALL" ){ $kode_rel = '50%';  }
	
	if ( $jenis == "IN-GRUP" ){
		
      $sqltext = "SELECT date_trunc('month', tgl_bc_16) AS Month , count(distinct(B.no_aju)) AS ECCO,
                count(distinct(C.no_aju)) AS BENING, count(distinct(D.no_aju)) AS TANJUNG,
                count(distinct(E.no_aju)) AS CARGILL

                FROM report.plb_flowbrg A

                left join (select distinct no_aju
                from report.plb_flowbrg
                where kode_rel = '500200'
                and id_flowbrg_in is null
                and no_bl is not null
                group by 1) B on A.no_aju = B.no_aju

                left join (select distinct no_aju
                from report.plb_flowbrg
                where kode_rel = '500300'
                and id_flowbrg_in is null
                group by 1) C on A.no_aju = C.no_aju

                left join (select distinct no_aju
                from report.plb_flowbrg
                where kode_rel = '501400'
                and id_flowbrg_in is null
                and no_bl is not null
                group by 1) D on A.no_aju = D.no_aju

                left join (select distinct A.no_aju
                from report.plb_flowbrg A
                --join report.plb_documents_in B on A.no_aju = substring(B.nomor_aju,15)
                --and B.uraian_dokumen = 'INVOICE'
                where A.kode_rel = '501600'
                and A.id_flowbrg_in is null
                and exim like '107%'
                group by 1) E on A.no_aju = E.no_aju
      
                WHERE date_part('year', A.tgl_bc_16) = '$tahun'
                AND A.id_flowbrg_in is null
                GROUP BY Month
                ORDER BY Month";
				
				//echo $sqltext;

				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		
		echo "<br>";
		echo "<div class='container'>";
		echo "<table class='table table-striped'>";
		echo	"<thead>";
		echo		"<tr>";			
		echo			"<th rowspan='2'>NO</th>";
		echo			"<th rowspan='2'>Periode</th>";
		echo			"<th rowspan='2'>ECCO</th>";
		echo			"<th rowspan='2'>BENING</th>";
		echo			"<th rowspan='2'>TANJUNG SARI</th>";
		echo			"<th rowspan='2'>CARGILL</th>";
		echo		"</tr>";
		echo	"</thead>";
		echo	"<tbody>";
		
		while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
		echo		"<tr>";			
		echo			"<td>".$number."</td>";
		echo			"<td>".substr($row['month'],0,7)."</td>";
		echo			"<td>".$row['ecco']."</td>";
		echo			"<td>".$row['bening']."</td>";
		echo			"<td>".$row['tanjung']."</td>";
		echo			"<td>".$row['cargill']."</td>";
		echo		"</tr>";
		}
		echo	"</tbody>";
		echo  "</table>";
		echo  "</div>";
		
		pg_free_result($result);
		 }
		 
		 if ( $jenis == "IN-GRUP" && $data == "ekspor"){
		
		
		
      $sqltext = "SELECT date_trunc('month', tgl_bc_16) AS Month , count(distinct(B.no_aju)) AS ECCO,
                count(distinct(C.no_aju)) AS BENING, count(distinct(D.no_aju)) AS TANJUNG,
                count(distinct(E.no_aju)) AS CARGILL

                FROM report.plb_flowbrg A

                left join (select distinct no_aju
                from report.plb_flowbrg
                where kode_rel = '500200'
                and id_flowbrg_in is null
                and no_bl is not null
                group by 1) B on A.no_aju = B.no_aju

                left join (select distinct no_aju
                from report.plb_flowbrg
                where kode_rel = '500300'
                and id_flowbrg_in is null
                group by 1) C on A.no_aju = C.no_aju

                left join (select distinct no_aju
                from report.plb_flowbrg
                where kode_rel = '501400'
                and id_flowbrg_in is null
                and no_bl is not null
                group by 1) D on A.no_aju = D.no_aju

                left join (select distinct A.no_aju
                from report.plb_flowbrg A
                --join report.plb_documents_in B on A.no_aju = substring(B.nomor_aju,15)
                --and B.uraian_dokumen = 'INVOICE'
                where A.kode_rel = '501600'
                and A.id_flowbrg_in is null
                and exim like '107%'
                group by 1) E on A.no_aju = E.no_aju
      
                WHERE date_part('year', A.tgl_bc_16) = '$tahun'
                AND A.id_flowbrg_in is null
                GROUP BY Month
                ORDER BY Month";
				
				//echo $sqltext;

				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		
		header("Content-type: application/vnd-ms-excel");
        header("Content-Disposition: attachment; filename=LaporanJumlahBC1.6perTahun".$tahun.".xls");
		
		echo "<br>";
		echo "<table class='table table-striped table-bordered data'>";
		echo	"<thead>";
		echo		"<tr>";			
		echo			"<th rowspan='2'>NO</th>";
		echo			"<th rowspan='2'>Periode</th>";
		echo			"<th rowspan='2'>ECCO</th>";
		echo			"<th rowspan='2'>BENING</th>";
		echo			"<th rowspan='2'>TANJUNG SARI</th>";
		echo			"<th rowspan='2'>CARGILL</th>";
		echo		"</tr>";
		echo	"</thead>";
		echo	"<tbody>";
		
		while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
		echo		"<tr>";			
		echo			"<td>".$number."</td>";
		echo			"<td>".substr($row['month'],0,7)."</td>";
		echo			"<td>".$row['ecco']."</td>";
		echo			"<td>".$row['bening']."</td>";
		echo			"<td>".$row['tanjung']."</td>";
		echo			"<td>".$row['cargill']."</td>";
		echo		"</tr>";
		}
		echo	"</tbody>";
		echo  "</table>";
		
		pg_free_result($result);
		 }
		 
		 if ( $jenis == "OUT-GRUP" ){
		
      $sqltext = "SELECT date_trunc('month', tgl_doc_out) AS Month , count(distinct(B.no_aju_out)) AS ECCO,
                count(distinct(C.no_aju_out)) AS BENING, count(distinct(D.no_aju_out)) AS TANJUNG,
                count(distinct(E.no_aju_out)) AS CARGILL

                FROM report.plb_flowbrg A

                left join (select distinct no_aju_out
                from report.plb_flowbrg
                where kode_rel = '500200'
                and id_flowbrg_in is not null
                group by 1) B on A.no_aju_out = B.no_aju_out

                left join (select distinct no_aju_out
                from report.plb_flowbrg
                where kode_rel = '500300'
                and id_flowbrg_in is not null
                group by 1) C on A.no_aju_out = C.no_aju_out

                left join (select distinct no_aju_out
                from report.plb_flowbrg
                where kode_rel = '501400'
                and id_flowbrg_in is not null
                group by 1) D on A.no_aju_out = D.no_aju_out

                left join (select distinct A.no_aju_out
                from report.plb_flowbrg A
                --join report.plb_documents_in B on A.no_aju = substring(B.nomor_aju,15)
                --and B.uraian_dokumen = 'INVOICE'
                where A.kode_rel = '501600'
                and A.id_flowbrg_in is not null
                --and exim like '107%'
                group by 1) E on A.no_aju_out = E.no_aju_out
      
                WHERE date_part('year', A.tgl_doc_out) = '$tahun'
                AND A.id_flowbrg_in is not null
                AND A.doc_out = 'BC 2.7'
                GROUP BY Month
                ORDER BY Month";
				
				//echo $sqltext;

				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		
		echo "<br>";
		echo "<br>";
		echo "<table class='table table-striped'>";
		echo	"<thead>";
		echo		"<tr>";			
		echo			"<th rowspan='2'>NO</th>";
		echo			"<th rowspan='2'>Periode</th>";
		echo			"<th rowspan='2'>ECCO</th>";
		echo			"<th rowspan='2'>BENING</th>";
		echo			"<th rowspan='2'>TANJUNG SARI</th>";
		echo			"<th rowspan='2'>CARGILL</th>";
		echo		"</tr>";
		echo	"</thead>";
		echo	"<tbody>";
		
		while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
		echo		"<tr>";			
		echo			"<td>".$number."</td>";
		echo			"<td>".substr($row['month'],0,7)."</td>";
		echo			"<td>".$row['ecco']."</td>";
		echo			"<td>".$row['bening']."</td>";
		echo			"<td>".$row['tanjung']."</td>";
		echo			"<td>".$row['cargill']."</td>";
		echo		"</tr>";
		}
		echo	"</tbody>";
		echo  "</table>";
		
		pg_free_result($result);
		 }

	
  if ( $jenis == "BC 2.7IN" ) {

	   $sqltext= "select A.no_aju,to_char(A.tgl_aju,'dd/mm/yyyy') as tgl_aju, A.no_daftar,to_char(A.tgl_daftar,'dd/mm/yyyy') as tgl_daftar, A.no_npe, cast(A.jumlah as int), A.satuan,
       to_char(A.tgl_npe,'dd/mm/yyyy') as tgl_npe, B.nama, A.negara,  A.cif, A.tarif_hs,F.satuan as satuan2,pack.jumlah4,
       sum_varchar(distinct case when C.kendaraan = 'CONTAINER' THEN  C.no_unit ||' / ' ||C.sizecode else C.nopol end ||'<br />' )as kendaraan,
       sum_varchar (distinct( split_part(F.id_flowbrg,'-',1)) ||'<br />') as no_book,
       sum_varchar(distinct( split_part(G.itemname,':',1))||'<br />') as cbarangtpb,
	   sum_varchar(distinct( to_char(PL.tgl_doc,'dd/mm/yyyy'))||'<br />') as tgl_pl,
       sum_varchar(distinct( to_char(INV.tgl_doc,'dd/mm/yyyy'))||'<br />') as tgl_inv,
       sum_varchar(distinct( split_part(G.itemname,':',2))||'<br />') as namabarangtpb,
	   sum_varchar(distinct( to_char(C.tgl_awal,'dd/mm/yyyy'))||'<br />') as tgl_awal,
	   sum_varchar(distinct( PL.no_doc)||'<br />') as no_pl,
	   sum_varchar(distinct( INV.no_doc)||'<br />') as no_inv,
	   sum_varchar(distinct( C.destination)||'<br />') as destination
       from report.plb_doc_in27 A
       left join report.plb_doc_in27_brg E on A.no_aju = E.no_aju
       left join wh_flowbrg F on E.id_flowbrg = F.id_flowbrg
       left join (select E.no_aju, sum(F.jumlah0) as jumlah4
                  from report.plb_doc_in27_brg E, wh_flowbrg F, report.plb_doc_in27 A
                  where E.id_flowbrg = F.id_flowbrg
                  and A.no_aju = E.no_aju
                  group by 1) as pack
          on A.no_aju = pack.no_aju
       join report.plb_doc_in27_dtl pl on A.no_aju = pl.no_aju         
       left join wh_flowcont C on F.id_flowbrg_splitted = C.id_flowcont
       left join v_customer B on F.kode_rel = B.kode_rel
       left join wh_cargo G on F.itemcode = G.itemcode
       left join report.plb_doc_in27_dtl inv on A.no_aju = inv.no_aju

       where B.kode_rel LIKE '501600'  and A.tgl_aju between '$tgl1' and '$tgl2'
       and PL.jenis_doc = 'PACKING LIST'
       and INV.jenis_doc = 'INVOICE'
       and C.id_aktifitas IN(1,2,4,16,23) 
       and A.nsurat = 'BC 2.7'
       group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14";
			
			
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td colspan ='4' align='center'><label class='style5'>BC 2.7</label></td>";	
				echo "    <td align='center'><label class='style5'>PENGIRIM</label></td>";
				echo "    <td align='center'><label class='style5'>PENERIMA</label></td>";
				echo "    <td align='center'><label class='style5'>NEGARA</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>KONTAINER</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>INVOICE</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>PACKING LIST</label></td>";
				echo "    <td colspan ='4' align='center'><label class='style5'>URAIAN BARANG (TPB)</label></td>";
				echo "    <td colspan ='5' align='center'><label class='style5'>URAIAN BARANG (LPB)</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'>KETERANGAN</label></td>";
				}
				echo "  </tr>";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No Aju </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Aju</label></td>";
				echo "    <td align='center'><label class='style5'>Nopen </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Nopen</label></td>";		
				//echo "    <td align='center'><label class='style5'>No NPE</label></td>";
				//echo "    <td align='center'><label class='style5'>Tgl NPE</label></td>";
				echo "    <td align='center'><label class='style5'></label></td>";
				echo "    <td align='center'><label class='style5'></label></td>";
				echo "    <td align='center'><label class='style5'>TUJUAN</label></td>";
                echo "    <td align='center'><label class='style5'>20</label></td>";					
				echo "    <td align='center'><label class='style5'>40</label></td>";	
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Nilai</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>HS</label></td>";
				echo "    <td align='center'><label class='style5'>Nama Barang</label></td>";
				echo "    <td align='center'><label class='style5'>Jml</label></td>";
				echo "    <td align='center'><label class='style5'>Satuan</label></td>";
				echo "    <td align='center'><label class='style5'>No Booking</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Aktifitas</label></td>";
				echo "    <td align='center'><label class='style5'>Nama Barang</label></td>";
				echo "    <td align='center'><label class='style5'>Jml</label></td>";
				echo "    <td align='center'><label class='style5'>Sat</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'> </label></td>";
				}
				echo "    <td><label class='style5'> </label></td>";
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
			   $stokA = $row['jumlah2'] - $row['jumlah'];
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>".$row['no_aju']."</label></td>";					
				echo "	<td ><label class='style4'>".$row['tgl_aju']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_daftar']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_daftar']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['no_npe']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['tgl_npe']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></td>";
				echo "	<td ><label class='style4'>PT. INDRA JAYA SWASTIKA</label></td>";
				echo "	<td ><label class='style4'>".$row['negara']."</label></td>";
				echo "	<td colspan='2'><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_inv']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_inv']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cif']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_pl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_pl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tarif_hs']."</label></td>";
				echo "	<td ><label class='style4'>".$row['namabarangtpb']."</label></td>";
				echo "	<td ><label class='style4'>".$row['jumlah']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_book']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_awal']."</label></td>";
				echo "	<td ><label class='style4'>".$row['namabarangtpb']."</label></td>";
				echo "	<td ><label class='style4'>".$row['jumlah4']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan2']."</label></td>";	
				if ($logincat == "PLB-ADMIN"){
				echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
				}
				
		 }
		echo "</table>";
		echo "<br>";
		echo "Total Aju : ".$number;

        pg_free_result($result);
	}
	
 if ( $jenis == "BC 2.7OT" ) {

	$sqltext= "select B.no_aju_out,B.tgl_aju_out,
		B.doc_out as doc_out, E.no_bc_16  as bc16 ,to_char( E.tgl_bc_16 ,'dd/mm/yyyy') as tgl_bc16 , 
		B.no_doc_out as doc_outnum, 
		B.tgl_doc_out as doc_tgl,pack.jumlah4,
		F.cif as cif,
		sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
		sum_varchar (distinct( split_part(B.id_flowbrg,'-',1)) ||'<br />') as id_barang,
		sum_varchar (distinct A.tgl_awal ||'<br />') as tgl_aktifitas,
		sum_varchar (distinct( cast(F.jumlah as int )) ||'<br />') as jml_dokumen,
		sum_varchar (distinct F.satuan ||'<br />') as satuand,
		sum_varchar (distinct E.no_bl ||'<br />') as no_bl,
		sum_varchar (distinct to_char( E.tgl_bl,'dd/mm/yyyy')||'<br />') as tgl_bl,
		sum_varchar (distinct to_char( F.tanggal_dokumen,'dd/mm/yyyy')||'<br />') as tgl_dokumen,
		sum_varchar (distinct B.batch_no ||'<br />') as invoice,
		sum_varchar (distinct to_char( A.tgl_awal,'dd/mm/yyyy')||'<br />') as tgl_awal,
		sum_varchar (distinct to_char( A.jam_awal,'HH24:MI')||'<br />') as jam_awal,
		sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode else A.nopol end ||'<br />' )as kendaraan,
		sum_varchar( distinct(B.satuan) ||'<br />' ) as satuan,
		sum_varchar( distinct(F.uraian_barang) ||'<br />' ) as uraian_barang,
		sum_varchar( distinct(F.hs) ||'<br />' ) as hs,
		sum_varchar( distinct(C.nama) ||'<br />' ) as nama,
		sum_varchar(distinct( split_part(itemname,':',1))||'<br />') as cbarang,
		sum_varchar(distinct( split_part(itemname,':',2))||'<br />') as namabarang,
		sum(E.jumlah) as jumlah2
		from report.plb_doc_out27 F 
		left join report.plb_flowbrg B on B.no_doc_out = F.nomor_daftar and F.uraian_dokumen = 'INVOICE'
		and B.batch_no = F.nomor_dokumen and B.no_aju_out = SUBSTRING(F.no_aju,15)
		left join wh_flowcont A on A.id_flowcont = B.id_flowbrg_splitted   
		join v_customer C on A.kode_rel = C.kode_rel
		join wh_cargo D on B.itemcode = D.itemcode
		join report.plb_flowbrg E on B.id_flowbrg_in = E.id_flowbrg
		join (select Z.no_aju_out, sum(Z.jumlah) as jumlah4
		from report.plb_flowbrg Z
		where Z.kode_rel like '$kode_rel'
		and Z.doc_out = 'BC 2.7'
		group by 1) as pack
		on B.no_aju_out = pack.no_aju_out
		join report.plb_doc_in sup on E.no_bc_16 = sup.nomor_daftar and sup.uraian_dokumen = 'INVOICE'
		and B.batch_no = sup.nomor_dokumen and E.no_aju = SUBSTRING(sup.nomor_aju,15)
		where B.kode_rel LIKE '$kode_rel'  and f.tanggal_daftar  between  '$tgl1' and '$tgl2' and B.doc_out = 'BC 2.7'
		and a.id_aktifitas IN(2,16,24) 
		group by 1,2,3,4,5,6,7,8,9
		union
		select B.no_aju_out,B.tgl_aju_out,
		B.doc_out as doc_out, null as bc16 ,null as tgl_bc16 , 
		B.no_doc_out as doc_outnum, 
		B.tgl_doc_out as doc_tgl,pack.jumlah4,
		F.cif as cif,
		sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
		sum_varchar (distinct( split_part(B.id_flowbrg,'-',1)) ||'<br />') as id_barang,
		sum_varchar (distinct A.tgl_awal ||'<br />') as tgl_aktifitas,
		sum_varchar (distinct( cast(F.jumlah as int )) ||'<br />') as jml_dokumen,
		sum_varchar (distinct F.satuan ||'<br />') as satuand,
		sum_varchar (distinct E.no_bl ||'<br />') as no_bl,
		sum_varchar (distinct to_char( E.tgl_bl,'dd/mm/yyyy')||'<br />') as tgl_bl,
		sum_varchar (distinct to_char( F.tanggal_dokumen,'dd/mm/yyyy')||'<br />') as tgl_dokumen,
		sum_varchar (distinct B.exim ||'<br />') as invoice,
		sum_varchar (distinct to_char( A.tgl_awal,'dd/mm/yyyy')||'<br />') as tgl_awal,
		sum_varchar (distinct to_char( A.jam_awal,'HH24:MI')||'<br />') as jam_awal,
		sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode else A.nopol end ||'<br />' )as kendaraan,
		sum_varchar( distinct(B.satuan) ||'<br />' ) as satuan,
		sum_varchar( distinct(F.uraian_barang) ||'<br />' ) as uraian_barang,
		sum_varchar( distinct(F.hs) ||'<br />' ) as hs,
		sum_varchar( distinct(C.nama) ||'<br />' ) as nama,
		sum_varchar(distinct( split_part(itemname,':',1))||'<br />') as cbarang,
		sum_varchar(distinct( split_part(itemname,':',2))||'<br />') as namabarang,
		--sum(B.jumlah) as jumlah,
		sum(E.jumlah) as jumlah2
		from  report.plb_doc_out27 F 
		join report.plb_flowbrg B on B.no_doc_out = F.nomor_daftar and F.uraian_dokumen = 'PACKING LIST'
		and B.exim = F.nomor_dokumen and B.no_aju_out = SUBSTRING(F.no_aju,15)
		join wh_flowcont A on A.id_flowcont = B.id_flowbrg_splitted   
		join v_customer C on A.kode_rel = C.kode_rel
		join wh_cargo D on B.itemcode = D.itemcode
		join report.plb_flowbrg E on B.id_flowbrg_in = E.id_flowbrg
		join (select Z.no_aju_out, sum(Z.jumlah) as jumlah4
		from report.plb_flowbrg Z
		where Z.kode_rel LIKE '$kode_rel'
		and Z.doc_out = 'BC 2.7'
		--and A.no_aju = E.no_aju
		group by 1) as pack
		on B.no_aju_out = pack.no_aju_out
		
		where B.kode_rel LIKE '$kode_rel'  and f.tanggal_daftar  between  '$tgl1' and '$tgl2' and B.doc_out = 'BC 2.7'
		and a.id_aktifitas IN(2,16,24) 
		group by 1,2,3,4,5,6,7,8,9
		order by no_aju_out";

				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td colspan ='4' align='center'><label class='style5'>BC 2.7</label></td>";
                echo "    <td colspan ='2' align='center'><label class='style5'>BC 1.6</label></td>";				
				echo "    <td align='center'><label class='style5'>IMPORTIR</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>B/L</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>INVOICE</label></td>";
				echo "    <td align='center'><label class='style5'>HS</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>URAIAN BARANG (TPB)</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>KONTAINER</label></td>";
				echo "    <td align='center'><label class='style5'>TGL AKTIFITAS</label></td>";
				echo "    <td colspan ='5' align='center'><label class='style5'>URAIAN BARANG (WH)</label></td>";
				echo "    <td align='center'><label class='style5'>STOCK</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'>KETERANGAN</label></td>";
				}
				echo "  </tr>";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No Aju </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Aju</label></td>";
				echo "    <td align='center'><label class='style5'>Nopen </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Nopen</label></td>";	
                echo "    <td align='center'><label class='style5'>Nopen</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";				
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Nilai</label></td>";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>Jumlah</label></td>";
				echo "    <td align='center'><label class='style5'>Satuan</label></td>";
				echo "    <td align='center'><label class='style5'>20</label></td>";
				echo "    <td align='center'><label class='style5'>40</label></td>";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No Booking</label></td>";
				echo "    <td align='center'><label class='style5'>Kode</label></td>";
				echo "    <td align='center'><label class='style5'>Nama</label></td>";
                echo "    <td align='center'><label class='style5'>Jml</label></td>";					
				echo "    <td align='center'><label class='style5'>Sat</label></td>";	
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'> </label></td>";
				}
				echo "    <td><label class='style5'> </label></td>";
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
			   $stokA = $row['jumlah2'] - $row['jumlah4'];
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>".$row['no_aju_out']."</label></td>";					
				echo "	<td ><label class='style4'>".$row['tgl_aju_out']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_outnum']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_tgl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['invoice']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_dokumen']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cur']." ".number_format($row['cif'],2)."</label></td>";
				echo "	<td ><label class='style4'>".$row['hs']."</label></td>";
				echo "	<td align='center'><label class='style4'>".$row['jml_dokumen']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuand']."</label></td>";
				echo "	<td colspan='2'><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_aktifitas']."</label></td>";
				echo "	<td ><label class='style4'>".$row['id_barang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cbarang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['namabarang']."</label></td>";
				echo "	<td align =right><label class='style4'>".$row['jumlah4']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";
				echo "	<td align='center'><label class='style4'>".$stokA."</label></td>";	
				if ($logincat == "PLB-ADMIN"){
				echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
				}
				
		 }
		echo "</table>";
		echo "<br>";
		echo "Total Aju : ".$number;

        pg_free_result($result);
	}
	/**
	 if ( $jenis == "BC 2.7OT" && $kode_rel == "501600" ) {

				$sqltext= "select B.no_aju_out,B.tgl_aju_out,C.nama,
							B.doc_out as doc_out, 
							B.no_doc_out as doc_outnum, 
							B.tgl_doc_out as doc_tgl,
							F.cif as cif,pack.jumlah4,
							F.tanggal_dokumen as tgl_dokumen,
							sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
							sum_varchar (distinct( split_part(B.id_flowbrg,'-',1)) ||'<br />') as id_barang,
							sum_varchar (distinct A.tgl_awal ||'<br />') as tgl_aktifitas,
							sum_varchar (distinct( cast(F.jumlah as int )) ||'<br />') as jml_dokumen,
							sum_varchar (distinct F.satuan ||'<br />') as satuand,
							sum_varchar (distinct E.no_bl ||'<br />') as no_bl,
							sum_varchar (distinct to_char( E.tgl_bl,'dd/mm/yyyy')||'<br />') as tgl_bl,
							sum_varchar (distinct B.exim ||'<br />') as invoice,
							sum_varchar (distinct to_char( A.tgl_awal,'dd/mm/yyyy')||'<br />') as tgl_awal,
							sum_varchar (distinct to_char( A.jam_awal,'HH24:MI')||'<br />') as jam_awal,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode else A.nopol end ||'<br />' )as kendaraan,
							sum_varchar( distinct(B.satuan) ||'<br />' ) as satuan,
							sum_varchar( distinct(F.uraian_barang) ||'<br />' ) as uraian_barang,
							sum_varchar( distinct(F.hs) ||'<br />' ) as hs,
							sum_varchar(distinct( split_part(itemname,':',1))||'<br />') as cbarang,
							sum_varchar(distinct( split_part(itemname,':',2))||'<br />') as namabarang,
							sum(B.jumlah) as jumlah,
							sum(E.jumlah) as jumlah2
							from report.plb_doc_out27 F 
							left join report.plb_flowbrg B on B.no_doc_out = F.nomor_daftar and F.uraian_dokumen = 'PACKING LIST'
							and B.exim = F.nomor_dokumen and B.no_aju_out = SUBSTRING(F.no_aju,15)
							left join wh_flowcont A on A.id_flowcont = B.id_flowbrg_splitted   
							join v_customer C on A.kode_rel = C.kode_rel
							join wh_cargo D on B.itemcode = D.itemcode
							join report.plb_flowbrg E on B.id_flowbrg_in = E.id_flowbrg
							join (select Z.no_aju_out, sum(Z.jumlah) as jumlah4
							from report.plb_flowbrg Z
							where Z.kode_rel in ('501600')
							and Z.doc_out = 'BC 2.7'
							--and A.no_aju = E.no_aju
							group by 1) as pack
							on B.no_aju_out = pack.no_aju_out
							where B.kode_rel LIKE '501600%'  and B.tgl_doc_out between  '$tgl1' and '$tgl2' and B.doc_out = 'BC 2.7'
							and a.id_aktifitas IN(2,16,24) 
							group by 1,2,3,4,5,6,7,8,9
							order by B.tgl_doc_out ASC";
			

				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td colspan ='4' align='center'><label class='style5'>BC 2.7</label></td>";	
				echo "    <td align='center'><label class='style5'>IMPORTIR</label></td>";
				echo "    <td align='center'><label class='style5'>HS</label></td>";
				echo "    <td colspan ='5' align='center'><label class='style5'>URAIAN BARANG (WH)</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>KONTAINER</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>URAIAN BARANG (TPB)</label></td>";
				echo "    <td align='center'><label class='style5'>TGL AKTIFITAS</label></td>";
				//echo "    <td colspan ='2' align='center'><label class='style5'>B/L</label></td>";
			//	echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>INVOICE</label></td>";
				//echo "    <td colspan ='3' align='center'><label class='style5'>COO/SKA</label></td>";
				//echo "    <td align='center'><label class='style5'>STOCK</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'>KETERANGAN</label></td>";
				}
				echo "  </tr>";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No Aju </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Aju</label></td>";
				echo "    <td align='center'><label class='style5'>Nopen </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Nopen</label></td>";		
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No Booking</label></td>";
				echo "    <td align='center'><label class='style5'>Kode</label></td>";
				echo "    <td align='center'><label class='style5'>Nama</label></td>";
                echo "    <td align='center'><label class='style5'>Jml</label></td>";					
				echo "    <td align='center'><label class='style5'>Sat</label></td>";	
				echo "    <td align='center'><label class='style5'>20</label></td>";
				echo "    <td align='center'><label class='style5'>40</label></td>";
				echo "    <td align='center'><label class='style5'>Jumlah</label></td>";
				echo "    <td align='center'><label class='style5'>Satuan</label></td>";
				echo "    <td align='center'><label class='style5'> </label></td>";
				//echo "    <td align='center'><label class='style5'>No</label></td>";
				//echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				//echo "    <td align='center'><label class='style5'>Nilai</label></td>";
				//echo "    <td align='center'><label class='style5'>No</label></td>";
				//echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				//echo "    <td align='center'><label class='style5'>Jenis</label></td>";
				if ($logincat == "PLB-ADMIN"){
				//echo "    <td align='center'><label class='style5'> </label></td>";
				
				echo "    <td><label class='style5'> </label></td>";
				}
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
			   $stokA = $row['jumlah2'] - $row['jumlah'];
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>".$row['no_aju_out']."</label></td>";					
				echo "	<td ><label class='style4'>".$row['tgl_aju_out']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_outnum']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_tgl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></td>";
				echo "	<td ><label class='style4'>".$row['hs']."</label></td>";
				echo "	<td ><label class='style4'>".$row['id_barang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cbarang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['namabarang']."</label></td>";
				echo "	<td align =right><label class='style4'>".$row['jumlah4']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";
				echo "	<td colspan='2'><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td align='center'><label class='style4'>".$row['jml_dokumen']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuand']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_aktifitas']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['no_bl']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['tgl_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['invoice']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_dokumen']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['cur']." ".number_format($row['cif'],2)."</label></td>";
				//echo "	<td ><label class='style4'> </label></td>";
				//echo "	<td ><label class='style4'> </label></td>";
				//echo "	<td ><label class='style4'> </label></td>";
				//echo "	<td align='center'><label class='style4'>".$row['jml_dokumen']."</label></td>";	
				if ($logincat == "PLB-ADMIN"){
				echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
				}
				
		 }
		echo "</table>";
		echo "<br>";
		echo "Total Aju : ".$number;

        pg_free_result($result);
	} **/
  
 if ( $jenis == "BC 2.8" ) {

				$sqltext= "select B.no_aju_out,C.nama,
							B.doc_out as doc_out, E.no_bc_16  as bc16 , 
							B.no_doc_out as doc_outnum,
							F.cif_rupiah as cif, F.cif as cif_o, F.hs, F.uraian_barang,
							G.bm,G.ppn,G.pph,E.currency,
							sum_varchar (distinct E.tgl_invoice ||'<br />') as tgl_dokumen,
							sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
							sum_varchar (distinct to_char( E.tgl_bc_16,'dd/mm/yyyy')||'<br>') as tgl_bc16,
							sum_varchar (distinct to_char( B.tgl_aju_out,'dd/mm/yyyy')||'<br>') as tgl_aju_out,
							sum_varchar (distinct to_char( B.tgl_doc_out,'dd/mm/yyyy')||'<br>') as doc_tgl,
							sum_varchar (distinct( split_part(B.id_flowbrg,'-',1)) ||'<br />') as id_barang,
							sum_varchar (distinct A.tgl_awal ||'<br />') as tgl_aktifitas,
							sum_varchar (distinct( cast(F.jumlah as int )) ||'<br />') as jml_dokumen,
							sum_varchar (distinct E.no_coo ||'<br>') as no_coo,
							sum_varchar (distinct F.kemasan ||'<br>') as kemasan,
							sum_varchar (distinct to_char( E.tgl_coo,'dd/mm/yyyy')||'<br>') as tgl_coo,
							sum_varchar (distinct E.no_bl ||'<br>') as no_bl,
							sum_varchar (distinct to_char( E.tgl_bl,'dd/mm/yyyy')||'<br>') as tgl_bl,
							sum_varchar (distinct B.batch_no ||'<br>') as invoice,
							sum_varchar (distinct to_char( A.tgl_awal,'dd/mm/yyyy')||'<br>') as tgl_awal,
							sum_varchar (distinct to_char( A.jam_awal,'HH24:MI')||'<br>') as jam_awal,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode else A.nopol end ||'<br>' )as kendaraan,
							sum_varchar( distinct(B.satuan) ||'<br>' ) as satuan,
							sum_varchar(distinct( split_part(itemname,':',1))||'<br>') as cbarang,  sum(B.jumlah) as jumlah, sum(E.jumlah) as jumlah2,
							sum_varchar(distinct( split_part(itemname,':',2))||'<br />') as namabarang

							from report.plb_doc_out28 F 
							left join report.plb_flowbrg B on B.no_aju_out = SUBSTRING(F.no_aju,15) 
							and B.no_doc_out = F.nomor_daftar and F.uraian_dokumen = 'INVOICE' and b.batch_no = F.nomor_dokumen
							left join wh_flowcont A on A.id_flowcont = B.id_flowbrg_splitted 
							left join v_customer C on A.kode_rel = C.kode_rel
							join wh_cargo D on B.itemcode = D.itemcode
							left join report.plb_flowbrg E on B.id_flowbrg_in = E.id_flowbrg
								
							left join report.plb_barang_tarif G on F.no_aju = G.nomor_aju
							
							where B.kode_rel LIKE '$kode_rel'  and F.tanggal_daftar between  '$tgl1' and '$tgl2' and B.doc_out = 'BC 2.8'
							and a.id_aktifitas IN(2,16,24) 
							group by 1,2,3,4,5,6,7,8,9,10,11,12,13
							--order by F.tanggal_daftar ASC";
			

				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td colspan ='4' align='center'><label class='style5'>BC 2.8</label></td>";	
				echo "    <td align='center'><label class='style5'>IMPORTIR</label></td>";
				echo "    <td align='center'><label class='style5'>HS</label></td>";
				echo "    <td colspan ='5' align='center'><label class='style5'>URAIAN BARANG (WH)</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>KONTAINER</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>URAIAN BARANG (TPB)</label></td>";
				echo "    <td align='center'><label class='style5'>TGL AKTIFITAS</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>B/L</label></td>";
			//	echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td colspan ='4' align='center'><label class='style5'>INVOICE</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>PEMBAYARAN</label></td>";
				//echo "    <td colspan ='3' align='center'><label class='style5'>COO/SKA</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>BC 1.6</label></td>";
				echo "    <td align='center'><label class='style5'>STOCK</label></td>";
				echo "    <td align='center'><label class='style5'>KETERANGAN</label></td>";
				echo "  </tr>";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No Aju </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Aju</label></td>";
				echo "    <td align='center'><label class='style5'>Nopen </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Nopen</label></td>";		
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No Booking</label></td>";
				echo "    <td align='center'><label class='style5'>Kode</label></td>";
				echo "    <td align='center'><label class='style5'>Nama</label></td>";
                echo "    <td align='center'><label class='style5'>Jml</label></td>";					
				echo "    <td align='center'><label class='style5'>Sat</label></td>";	
				echo "    <td align='center'><label class='style5'>20</label></td>";
				echo "    <td align='center'><label class='style5'>40</label></td>";
				echo "    <td align='center'><label class='style5'>Jumlah</label></td>";
				echo "    <td align='center'><label class='style5'>Satuan</label></td>";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Nilai (Rp)</label></td>";
				echo "    <td align='center'><label class='style5'>CIF</label></td>";
				echo "    <td align='center'><label class='style5'>BM</label></td>";
				echo "    <td align='center'><label class='style5'>PPN</label></td>";
				echo "    <td align='center'><label class='style5'>PPH</label></td>";
				//echo "    <td align='center'><label class='style5'>No</label></td>";
				//echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				//echo "    <td align='center'><label class='style5'>Jenis</label></td>";
				echo "    <td align='center'><label class='style5'>Nopen</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				//echo "    <td align='center'><label class='style5'>Jml</label></td>";
				echo "    <td align='center'><label class='style5'> </label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td><label class='style5'> </label></td>";
				}
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
			   $stokA = $row['jumlah2'] - $row['jumlah'];
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>".$row['no_aju_out']."</label></td>";					
				echo "	<td ><label class='style4'>".$row['tgl_aju_out']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_outnum']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_tgl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></td>";
				echo "	<td ><label class='style4'>".$row['hs']."</label></td>";
				echo "	<td ><label class='style4'>".$row['id_barang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cbarang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['namabarang']."</label></td>";
				echo "	<td align ='center'><label class='style4'>".$row['jumlah']."</label></td>";
				echo "	<td align><label class='style4'>".$row['satuan']."</label></td>";
				echo "	<td colspan='2'><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td align ='center'><label class='style4'>".$row['jml_dokumen']."</label></td>";
				echo "	<td ><label class='style4'>".$row['kemasan']."</label></td>";
				echo "	<td align='center'><label class='style4'>".$row['tgl_aktifitas']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['invoice']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_dokumen']."</label></td>";
				echo "	<td ><label class='style4'>".number_format($row['cif'],2)."</label></td>";
				echo "	<td ><label class='style4'>".$row['currency']." ".number_format($row['cif_o'],2)."</label></td>";
				echo "	<td ><label class='style4'>".number_format($row['bm'],2)."</label></td>";
				echo "	<td ><label class='style4'>".number_format($row['ppn'],2)."</label></td>";
				echo "	<td ><label class='style4'>".number_format($row['pph'],2)."</label></td>";
				//echo "	<td ><label class='style4'> </label></td>";
				//echo "	<td ><label class='style4'> </label></td>";
				//echo "	<td ><label class='style4'> </label></td>";
				echo "	<td ><label class='style4'>".$row['bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bc16']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['stok_akhir']."</label></td>";
				echo "	<td align='center'><label class='style4'>".$stokA."</label></td>";	
				if ($logincat == "PLB-ADMIN"){
				echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
				}
		 }
		echo "</table>";
		echo "<br>";
		echo "Total Aju : ".$number;
        pg_free_result($result);
	}
	
	 if ( $jenis == "BC 3.0" ) {

	   $sqltext= "select A.no_aju,to_char(A.tgl_aju,'dd/mm/yyyy') as tgl_aju, A.no_daftar,to_char(A.tgl_daftar,'dd/mm/yyyy') as tgl_daftar, A.no_npe, cast(A.jumlah as int), A.satuan,
       to_char(A.tgl_npe,'dd/mm/yyyy') as tgl_npe, B.nama, A.negara,  A.cif, A.tarif_hs,F.satuan as satuan2,A.tgl_daftar,
       to_char(PL.tgl_doc,'dd/mm/yyyy') as tgl_pl,to_char(INV.tgl_doc,'dd/mm/yyyy') as tgl_inv,pack.jumlah4,
       
       sum_varchar(distinct case when C.kendaraan = 'CONTAINER' THEN  C.no_unit ||' / ' ||C.sizecode else C.nopol end ||'<br />' )as kendaraan,
       sum_varchar (distinct( split_part(F.id_flowbrg,'-',1)) ||'<br />') as no_book,
       sum_varchar(distinct( split_part(G.itemname,':',1))||'<br />') as cbarangtpb,
       sum_varchar(distinct( split_part(G.itemname,':',2))||'<br />') as namabarangtpb,
	   sum_varchar(distinct( to_char(C.tgl_awal,'dd/mm/yyyy'))||'<br />') as tgl_awal,
	   sum_varchar(distinct( PL.no_doc)||'<br />') as no_pl,
	   sum_varchar(distinct( INV.no_doc)||'<br />') as no_inv,
	   sum_varchar(distinct( C.destination)||'<br />') as destination

       from report.plb_doc_in27 A
       join report.plb_doc_in27_brg E on A.no_aju = E.no_aju
       join wh_flowbrg F on E.id_flowbrg = F.id_flowbrg
       
       join (select E.no_aju, sum(F.jumlah0) as jumlah4
                  from report.plb_doc_in27_brg E, wh_flowbrg F, report.plb_doc_in27 A
                  where E.id_flowbrg = F.id_flowbrg
                  and A.no_aju = E.no_aju
                  group by 1) as pack
          on A.no_aju = pack.no_aju
          
       join report.plb_doc_in27_dtl pl on A.no_aju = pl.no_aju         
       join wh_flowcont C on F.id_flowbrg_splitted = C.id_flowcont
       join v_customer B on F.kode_rel = B.kode_rel
       join wh_cargo G on F.itemcode = G.itemcode
       join report.plb_doc_in27_dtl inv on A.no_aju = inv.no_aju

       where B.kode_rel LIKE '501600'  and A.tgl_daftar between '$tgl1' and '$tgl2'
       and PL.jenis_doc = 'PACKING LIST'
       and INV.jenis_doc = 'INVOICE'
       and C.id_aktifitas IN(2,16,24) 
       and A.nsurat = 'BC 3.0'
       group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
       order by A.tgl_daftar ASC";
			

				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td colspan ='6' align='center'><label class='style5'>BC 3.0</label></td>";	
				echo "    <td align='center'><label class='style5'>PENGIRIM</label></td>";
				echo "    <td align='center'><label class='style5'>PENERIMA</label></td>";
				echo "    <td align='center'><label class='style5'>NEGARA</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>KONTAINER</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>INVOICE</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>PACKING LIST</label></td>";
				echo "    <td colspan ='4' align='center'><label class='style5'>URAIAN BARANG (TPB)</label></td>";
				echo "    <td colspan ='5' align='center'><label class='style5'>URAIAN BARANG (LPB)</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'>KETERANGAN</label></td>";
				}
				echo "  </tr>";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No Aju </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Aju</label></td>";
				echo "    <td align='center'><label class='style5'>Nopen </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Nopen</label></td>";		
				echo "    <td align='center'><label class='style5'>No NPE</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl NPE</label></td>";
				echo "    <td align='center'><label class='style5'></label></td>";
				echo "    <td align='center'><label class='style5'></label></td>";
				echo "    <td align='center'><label class='style5'>TUJUAN</label></td>";
                echo "    <td align='center'><label class='style5'>20</label></td>";					
				echo "    <td align='center'><label class='style5'>40</label></td>";	
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Nilai</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>HS</label></td>";
				echo "    <td align='center'><label class='style5'>Nama Barang</label></td>";
				echo "    <td align='center'><label class='style5'>Jml</label></td>";
				echo "    <td align='center'><label class='style5'>Satuan</label></td>";
				echo "    <td align='center'><label class='style5'>No Booking</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Aktifitas</label></td>";
				echo "    <td align='center'><label class='style5'>Nama Barang</label></td>";
				echo "    <td align='center'><label class='style5'>Jml</label></td>";
				echo "    <td align='center'><label class='style5'>Sat</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'> </label></td>";
				}
				echo "    <td><label class='style5'> </label></td>";
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
			   $stokA = $row['jumlah2'] - $row['jumlah'];
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>".$row['no_aju']."</label></td>";					
				echo "	<td ><label class='style4'>".$row['tgl_aju']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_daftar']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_daftar']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_npe']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_npe']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></td>";
				echo "	<td ><label class='style4'>".$row['destination']."</label></td>";
				echo "	<td ><label class='style4'>".$row['negara']."</label></td>";
				echo "	<td colspan='2'><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_inv']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_inv']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cif']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_pl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_pl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tarif_hs']."</label></td>";
				echo "	<td ><label class='style4'>".$row['namabarangtpb']."</label></td>";
				echo "	<td ><label class='style4'>".$row['jumlah']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_book']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_awal']."</label></td>";
				echo "	<td ><label class='style4'>".$row['namabarangtpb']."</label></td>";
				echo "	<td ><label class='style4'>".$row['jumlah4']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan2']."</label></td>";	
				if ($logincat == "PLB-ADMIN"){
				echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
				}
				
		 }
		echo "</table>";
		echo "<br>";
		echo "Total Aju : ".$number;

        pg_free_result($result);
	}
	

	if ( $jenis == "BC 4.1" ) {

				$sqltext= "select B.no_aju_out,C.nama,
							B.doc_out as doc_out, B.tgl_doc_out,
							B.no_doc_out as doc_outnum,B.satuan,
							F.cif as cif,cast(F.jumlah as integer) as jumlahd,F.satuan as satuand,
							F.tanggal_dokumen as tgl_dokumen,F.hs,A.tgl_awal as tgl_aktifitas,
							--sup.currency as cur
							sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
							--sum_varchar (distinct( split_part(B.id_flowbrg,'-',1)) ||'<br />') as id_barang,
							--sum_varchar (distinct( cast(F.jumlah as int )) ||'<br />') as jumlahd,
							--sum_varchar (distinct E.no_bl ||'<br />') as no_bl,
							--sum_varchar (distinct to_char( E.tgl_bl,'dd/mm/yyyy')||'<br />') as tgl_bl,
							sum_varchar (distinct F.nomor_dokumen ||'<br />') as invoice,
							sum_varchar (distinct to_char( A.tgl_awal,'dd/mm/yyyy')||'<br />') as tgl_awal,
							sum_varchar (distinct to_char( B.tgl_aju_out,'dd/mm/yyyy')||'<br />') as tgl_aju_out,
							sum_varchar (distinct to_char( B.tgl_doc_out,'dd/mm/yyyy')||'<br />') as doc_tgl,
							sum_varchar (distinct to_char( A.jam_awal,'HH24:MI')||'<br />') as jam_awal,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode else A.nopol end ||'<br />' )as kendaraan,
							sum_varchar( distinct(B.satuan) ||'<br />' ) as satuan,
							sum_varchar(distinct( split_part(F.uraian_barang,' ',2))||'<br />') as cbarang,
                            sum_varchar(distinct(F.uraian_barang)||'<br />') as namabarang,
							sum(B.jumlah) as jumlah
							--sum(E.jumlah) as jumlah2

							from report.plb_doc_out41 F 
							left join report.plb_flowbrg B  on B.no_doc_out = F.nomor_daftar --and F.uraian_dokumen in ('PACKING LIST','LAINNYA')
							and B.no_aju_out = SUBSTRING(F.no_aju,15) and   
							F.nomor_dokumen  = CASE WHEN F.uraian_dokumen = 'LAINNYA'  then B.id_flowbrg_splitted   else  B.exim end
							left join wh_flowcont A on A.id_flowcont = B.id_flowbrg_splitted   
							join v_customer C on A.kode_rel = C.kode_rel
							join wh_cargo D on B.itemcode = D.itemcode
							left join report.plb_flowbrg E on B.id_flowbrg_in = E.id_flowbrg
							
							 
							
							left join report.plb_doc_in sup on E.no_bc_16 = sup.nomor_daftar 
							and sup.uraian_dokumen in ('PACKING LIST', 'LAINNYA', 'SURAT JALAN')
							and B.exim = sup.nomor_dokumen and E.no_aju = SUBSTRING(sup.nomor_aju,15)
							
							where B.kode_rel LIKE '501600%'  and F.tgl_aju between  '$tgl1' and '$tgl2' and B.doc_out = 'BC 4.1'
							and a.id_aktifitas IN(2,16,24) 
							group by 1,2,3,4,5,6,7,8,9,10,11,12
							order by B.tgl_doc_out ASC";
			

				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td colspan ='4' align='center'><label class='style5'>BC 4.1</label></td>";	
				echo "    <td align='center'><label class='style5'>IMPORTIR</label></td>";
				//echo "    <td align='center'><label class='style5'>HS</label></td>";
				echo "    <td colspan ='4' align='center'><label class='style5'>URAIAN BARANG (WH)</label></td>";
				//echo "    <td colspan ='2' align='center'><label class='style5'>KONTAINER</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>URAIAN BARANG (TPB)</label></td>";
				//echo "    <td align='center'><label class='style5'>TGL AKTIFITAS</label></td>";
				//echo "    <td colspan ='2' align='center'><label class='style5'>B/L</label></td>";
			//	echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>PACKING LIST</label></td>";
				//echo "    <td colspan ='3' align='center'><label class='style5'>COO/SKA</label></td>";
				//echo "    <td colspan ='3' align='center'><label class='style5'>BC 4.0</label></td>";
				//echo "    <td align='center'><label class='style5'>STOCK</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'>KETERANGAN</label></td>";
				}
				echo "  </tr>";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No Aju </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Aju</label></td>";
				echo "    <td align='center'><label class='style5'>Nopen </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Nopen</label></td>";		
				echo "    <td align='center'><label class='style5'> </label></td>";
				//echo "    <td align='center'><label class='style5'> </label></td>";
				//echo "    <td align='center'><label class='style5'>No Booking</label></td>";
				echo "    <td align='center'><label class='style5'>Kode</label></td>";
				echo "    <td align='center'><label class='style5'>Nama</label></td>";
                echo "    <td align='center'><label class='style5'>Jml</label></td>";					
				echo "    <td align='center'><label class='style5'>Sat</label></td>";	
				//echo "    <td align='center'><label class='style5'>20</label></td>";
				//echo "    <td align='center'><label class='style5'>40</label></td>";
				echo "    <td align='center'><label class='style5'>Jumlah</label></td>";
				echo "    <td align='center'><label class='style5'>Satuan</label></td>";
				//echo "    <td align='center'><label class='style5'> </label></td>";
				//echo "    <td align='center'><label class='style5'>No</label></td>";
				//echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				//echo "    <td align='center'><label class='style5'>Nilai</label></td>";
				/**
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Jenis</label></td>";
				echo "    <td align='center'><label class='style5'>Nopen</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Jml</label></td>";
				**/
				if ($logincat == "PLB-ADMIN"){
				//echo "    <td align='center'><label class='style5'> </label></td>";
				}
				echo "    <td><label class='style5'> </label></td>";
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
			   $inv = $row['invoice'];
			   //$stokA = $row['jumlah2'] - $row['jumlah'];
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>".$row['no_aju_out']."</label></td>";					
				echo "	<td ><label class='style4'>".$row['tgl_aju_out']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_outnum']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_tgl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['hs']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['id_barang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cbarang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['namabarang']."</label></td>";
				echo "	<td align ='center'><label class='style4'>".$row['jumlah']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";
				//echo "	<td colspan='2'><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td align ='center'><label class='style4'>".$row['jumlahd']."</label></td>";
				echo "	<td align ='center'><label class='style4'>".$row['satuand']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['tgl_aktifitas']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['no_bl']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['tgl_bl']."</label></td>";
				if($inv == " BA-477/WBC.11/KPP.MP.0113/2017<br />"){
				echo "	<td ><label class='style4'>".$row['id_barang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_dokumen']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['cur']." ".number_format($row['cif'],2)."</label></td>";
				/**
				echo "	<td ><label class='style4'> </label></td>";
				echo "	<td ><label class='style4'> </label></td>";
				echo "	<td ><label class='style4'> </label></td>";
				echo "	<td ><label class='style4'>".$row['bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['stok_akhir']."</label></td>";
				**/
				//echo "	<td align='center'><label class='style4'>".$stokA."</label></td>";	
				if ($logincat == "PLB-ADMIN"){
				echo "	<td ><label class='style4'> BA-477/WBC.11/KPP.MP.0113/2017</label></td>";
				}
				}
				else{
					echo "	<td ><label class='style4'>".$inv."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_dokumen']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['cur']." ".number_format($row['cif'],2)."</label></td>";
					
				if ($logincat == "PLB-ADMIN"){
				echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
				}
				}
				
				
		 }
		echo "</table>";
		echo "<br>";
		echo "Total Aju : ".$number;

        pg_free_result($result);
	}
	
	
	
 if ( $jenis == "BC 1.6" ) {
		     $sqltext= "select A.nomor_daftar,
       A.tanggal_daftar,
       A.nomor_aju as no_aju,
       A.tanggal_aju,
       A.supplier,pack.jumlah4,C.kode_rel,B.nama,
       A.currency,A.harga,C.no_bl , C.tgl_bl,
       sum_varchar (distinct A.seri_barang ||'<br />') as seri_barang,
       sum_varchar (distinct A.nomor_dokumen ||'<br />') as invoice,
       sum_varchar (distinct A.tanggal_dokumen ||'<br />') as tgl_invoice,
       sum_varchar (distinct A.uraian_barang ||'<br />') as uraian_barang,
       sum_varchar (distinct A.kode_barang ||'<br />') as kode_barang,
       sum_varchar (distinct cast(A.jumlah as integer) ||'<br />') as jumlah,
       sum_varchar (distinct C.keterangan ||'<br />') as keterangan,
	   sum_varchar (distinct C.satuan ||'<br />') as satuan,
       sum_varchar(distinct case when G.kendaraan = 'CONTAINER' THEN  G.no_unit ||' / ' ||G.sizecode else G.nopol end ||'<br>' )as kendaraan,
       sum_varchar (distinct A.hs ||'<br />') as hs,
       sum_varchar (distinct cast(A.tarif as integer) ||'%<br />') as tarif,
       sum_varchar (distinct A.kemasan ||'<br />') as kemasan,
       sum_varchar (distinct split_part(C.id_flowbrg_splitted,'-',1) ||'<br />') as book
       from report.plb_doc_in A
       left join report.plb_flowbrg C on case when C.batch_no = A.nomor_dokumen then A.nomor_dokumen = C.batch_no else A.nomor_dokumen = C.exim end
       and A.uraian_dokumen = 'INVOICE' and substring(A.nomor_aju,15)=C.no_aju
       and A.nomor_daftar = C.no_bc_16
       left join v_customer B on C.kode_rel = B.kode_rel
       join wh_flowcont G on C.id_flowbrg_splitted = G.id_flowcont
       left join (select Z.no_aju, sum(Z.jumlah) as jumlah4
			from report.plb_flowbrg Z
			where Z.id_flowbrg_in is null
			and Z.kode_rel like '$kode_rel'
			group by 1) as pack
			on substring(A.nomor_aju,15) = pack.no_aju
       where A.tanggal_daftar between '$tgl1' and '$tgl2'
       and C.id_flowbrg_in is null
       and C.kode_rel like '$kode_rel'
       group by 1,2,3,4,5,6,7,8,9,10,11,12
       order by A.nomor_daftar";
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>AJU</label></td>";	
				echo "    <td colspan ='2' align='center'><label class='style5'>NOPEN</label></td>";
				echo "    <td align='center'><label class='style5'>CUSTOMER</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>B/L</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>INVOICE</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>KONTAINER</label></td>";
				echo "    <td align='center'><label class='style5'>HS</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>URAIAN BARANG (TPB)</label></td>";
				echo "    <td colspan ='5' align='center'><label class='style5'>URAIAN BARANG (WH)</label></td>";
				//	echo "    <td><label class='style5'>Shipper</label></td>";
				
				//echo "    <td colspan ='3' align='center'><label class='style5'>COO/SKA</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'>KETERANGAN</label></td>";
				}
				echo "  </tr>";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No. </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>No. </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";		
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Nilai</label></td>";
				echo "    <td align='center'><label class='style5'>20</label></td>";
				echo "    <td align='center'><label class='style5'>40</label></td>";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>Jumlah</label></td>";
				echo "    <td align='center'><label class='style5'>Satuan</label></td>";	
				echo "    <td align='center'><label class='style5'>No Booking</label></td>";
				echo "    <td align='center'><label class='style5'>Kode</label></td>";
				echo "    <td align='center'><label class='style5'>Nama</label></td>";
                echo "    <td align='center'><label class='style5'>Jml</label></td>";					
				echo "    <td align='center'><label class='style5'>Sat</label></td>";		
				//echo "    <td align='center'><label class='style5'>No</label></td>";
				//echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				//echo "    <td align='center'><label class='style5'>Jenis</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td><label class='style5'> </label></td>";
				}


				echo "  </tr>";
				echo "</thead>";
				echo "<tbody>";	
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				 echo "	<td ><label class='style4'>".$row['no_aju']."</label></td>";					
				 echo "	<td ><label class='style4'>".$row['tanggal_aju']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nomor_daftar']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tanggal_daftar']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['invoice']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_invoice']."</label></td>";
				echo "	<td ><label class='style4'> ".$row['currency']." ".number_format($row['harga'],2)." </label></td>";
				echo "	<td colspan='2'><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['hs']."</label></td>";
				echo "	<td align ='center'><label class='style4'>".$row['jumlah']."</label></td>";
				echo "	<td align ='center'><label class='style4'>".$row['kemasan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['book']."</label></td>";
				echo "	<td ><label class='style4'>".$row['kode_barang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['uraian_barang']."</label></td>";
			//	echo "	<td ><label class='style4'>".$row['supplier']."</label></td>";
				echo "	<td align ='center'><label class='style4'>".$row['jumlah4']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";			
				 //echo "	<td ><label class='style4'>".$row['no_coo']."</label></td>";
				 //echo "	<td ><label class='style4'>".$row['tgl_coo']."</label></td>";				
				 //echo "	<td ><label class='style4'>".$row['jenis']."</label></td>";
                 if ($logincat == "PLB-ADMIN"){				 
				 echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
				 }
			
		 }
		echo "</tbody>"; 
		echo "</table>";
		echo "<br>";
		echo "Total Aju : ".$number;
		
        pg_free_result($result);
	}
	
	
	if ( $jenis == "BC 4.0" ) {
		     $sqltext= "select  B.no_bc_16  as bc16 ,to_char( B.tgl_bc_16 ,'dd/mm/yyyy') as tgl_bc16 , B.tgl_bc_16,
						to_char( B.etad ,'dd/mm/yyyy') as etad,to_char( B.tgl_aju,'dd/mm/yyyy') as tgl_aju,
						B.no_aju as nomor_aju,
						pack.jumlah4,
						C.nama,
						sum_varchar (distinct B.harga ||'<br />') as harga,
						sum_varchar (distinct F.hs ||'<br />') as hs,
						sum_varchar (distinct F.uraian_barang ||'<br />') as uraian_barang,
						sum_varchar (distinct F.currency ||'<br />') as currency,
						sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
						sum_varchar (distinct( split_part(B.id_flowbrg,'-',1)) ||'<br />') as kode_barang,
						sum_varchar (distinct A.tgl_awal ||'<br />') as tgl_aktifitas,
						sum_varchar (distinct( cast(F.jumlah as int )) ||'<br />') as jml_dokumen,
						sum_varchar (distinct F.kemasan ||'<br>') as kemasan,
						sum_varchar (distinct B.exim ||'<br>') as invoice,
						sum_varchar (distinct to_char( B.tgl_invoice,'dd/mm/yyyy')||'<br>') as tgl_invoice,
						sum_varchar (distinct A.seal ||'<br>') as seal,
						sum_varchar (distinct to_char( E.tgl_in,'dd/mm/yyyy')||'<br>') as tgl_in,
						sum_varchar (distinct to_char( E.jam_in,'HH24:MI')||'<br>') as jam_in,
						sum_varchar (distinct to_char( A.tgl_awal,'dd/mm/yyyy')||'<br>') as tgl_awal,
						sum_varchar (distinct to_char( A.jam_awal,'HH24:MI')||'<br>') as jam_awal,
						sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode else A.nopol end ||'<br>' )as kendaraan,
						sum_varchar( distinct(satuan) ||'<br>' ) as satuan,
						sum_varchar(distinct( split_part(itemname,':',1))||'<br>') as cbarang,
						sum_varchar(distinct( split_part(itemname,':',2))||'<br />') as namabarang,
						sum(B.jumlah) as jumlah
						from report.plb_doc_in40 F
						left join report.plb_flowbrg B on B.no_aju = substr(F.nomor_aju,15)
						left join wh_flowcont A  on A.id_flowcont = B.id_flowbrg_splitted  
						left join v_customer C on A.kode_rel = C.kode_rel
						left join wh_cargo D on B.itemcode = D.itemcode
						left join (select Z.no_aju, sum(Z.jumlah) as jumlah4
								from report.plb_flowbrg Z
								where Z.id_flowbrg_in is null
								and Z.kode_rel = '501600'
								group by 1) as pack
								on B.no_aju = pack.no_aju
						
						left join wh_loket E on A.id_loket = E.id_loket 
						where B.tgl_bc_16 between '$tgl1' and '$tgl2'
						and	a.kode_rel LIKE '501600' 
                        and A.kode_rel = '501600'	
                        and F.uraian_dokumen in ('PACKING LIST','SURAT JALAN','LAINNYA')
                        and F.nomor_daftar = B.no_bc_16 and F.nomor_dokumen = B.exim						
						and a.id_aktifitas IN(1,14,23) 
						group by 1,2,3,4,5,6,7,8
						order by  B.tgl_bc_16 ASC";
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>AJU</label></td>";	
				echo "    <td colspan ='2' align='center'><label class='style5'>NOPEN</label></td>";
				echo "    <td align='center'><label class='style5'>CUSTOMER</label></td>";
				echo "    <td colspan ='4' align='center'><label class='style5'>URAIAN BARANG</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>KONTAINER</label></td>";
				//echo "    <td colspan ='2' align='center'><label class='style5'>B/L</label></td>";
			//	echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>PACKING LIST</label></td>";
				//echo "    <td colspan ='3' align='center'><label class='style5'>COO/SKA</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'>KETERANGAN</label></td>";
				}
				echo "  </tr>";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No. </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>No. </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";		
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>Kode</label></td>";
				echo "    <td align='center'><label class='style5'>Nama</label></td>";
				echo "    <td align='center'><label class='style5'>Jml</label></td>";		
				echo "    <td align='center'><label class='style5'>Sat</label></td>";	
				echo "    <td align='center'><label class='style5'>20</label></td>";
				echo "    <td align='center'><label class='style5'>40</label></td>";
				//echo "    <td align='center'><label class='style5'>No</label></td>";
				//echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Nilai</label></td>";
				//echo "    <td align='center'><label class='style5'>No</label></td>";
				//echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				//echo "    <td align='center'><label class='style5'>Jenis</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td><label class='style5'> </label></td>";
				}


				echo "  </tr>";
				echo "</thead>";
				echo "<tbody>";	
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				 echo "	<td ><label class='style4'>".$row['nomor_aju']."</label></td>";					
				 echo "	<td ><label class='style4'>".$row['tgl_aju']."</label></td>";
				echo "	<td ><label class='style4'>".$row['bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></label></label></td>";
				echo "	<td ><label class='style4'>".$row['cbarang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['namabarang']."</label></td>";
			//	echo "	<td ><label class='style4'>".$row['supplier']."</label></td>";
				echo "	<td align =right><label class='style4'>".$row['jumlah4']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";
				echo "	<td colspan='2'><label class='style4'>".$row['kendaraan']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['no_bl']."</label></td>";
				//echo "	<td ><label class='style4'>".$row['tgl_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['invoice']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_invoice']."</label></td>";
				echo "	<td ><label class='style4'> ".$row['currency']." ".number_format($row['harga'],2)." </label></td>";
				 //echo "	<td ><label class='style4'>".$row['no_coo']."</label></td>";
				 //echo "	<td ><label class='style4'>".$row['tgl_coo']."</label></td>";				
				 //echo "	<td ><label class='style4'>".$row['jenis']."</label></td>";
                 if ($logincat == "PLB-ADMIN"){				 
				 echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
				 }
			
		 }
		echo "</tbody>"; 
		echo "</table>";
		echo "<br>";
		echo "Total Aju : ".$number;

        pg_free_result($result);
	}
	
	
	
	
	if ( $jenis == "CY-OUT" ) {
		
		$sqltext= "select B.no_aju_out,B.tgl_aju_out,
							B.doc_out as doc_out, E.no_bc_16  as bc16 ,to_char( E.tgl_bc_16 ,'dd/mm/yyyy') as tgl_bc16 , 
							B.no_doc_out as doc_outnum, 
							B.tgl_doc_out as doc_tgl,C.nama,
							B.itemcode as cbarang,F.cif as harga,
							E.tgl_invoice as tgl_dokumen,
							E.currency as cur, B.bm, B.ppn, B.pph,
							sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
							sum_varchar (distinct E.no_bl ||'<br />') as no_bl,
							sum_varchar (distinct to_char( E.tgl_bl,'dd/mm/yyyy')||'<br />') as tgl_bl,
							sum_varchar (distinct B.batch_no ||'<br />') as invoice,
							sum_varchar (distinct to_char( A.tgl_in,'dd/mm/yyyy')||'<br />') as tgl_awal,
							sum_varchar (distinct to_char( A.jam_in,'HH24:MI')||'<br />') as jam_awal,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode end ||'<br />' )as kendaraan,
							sum_varchar( distinct(B.satuan) ||'<br />' ) as satuan,
						    sum(B.jumlah) as jumlah, sum(E.jumlah) as jumlah2
							from wh_loket A 
							join report.plb_cy B on A.id_loket = B.id_loket  
							join v_customer C on A.kode_rel = C.kode_rel 
							join report.plb_cy E on B.id_flowbrg_in = E.id_flowbrg
							join report.plb_doc_out28 F on B.no_doc_out = F.nomor_daftar 
							and uraian_dokumen = 'INVOICE'
							where b.kode_rel LIKE '$kode_rel'  and A.tgl_in between  '$tgl1' and '$tgl2' and B.doc_out = 'BC 2.8'
							group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
							order by no_aju_out";
			

				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td colspan ='4' align='center'><label class='style5'>BC 2.7</label></td>";	
				echo "    <td align='center'><label class='style5'>IMPORTIR</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>URAIAN BARANG</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>KONTAINER</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>B/L</label></td>";
			//	echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>INVOICE</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>PEMBAYARAN</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>COO/SKA</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>BC 1.6</label></td>";
				echo "    <td align='center'><label class='style5'>STOCK</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'>KETERANGAN</label></td>";
				}
				echo "  </tr>";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No Aju </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Aju</label></td>";
				echo "    <td align='center'><label class='style5'>Nopen </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl Nopen</label></td>";		
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>Nama</label></td>";
				echo "    <td align='center'><label class='style5'>Jml</label></td>";		
				echo "    <td align='center'><label class='style5'>Sat</label></td>";	
				echo "    <td align='center'><label class='style5'>20</label></td>";
				echo "    <td align='center'><label class='style5'>40</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Nilai</label></td>";
				echo "    <td align='center'><label class='style5'>BM</label></td>";
				echo "    <td align='center'><label class='style5'>PPN</label></td>";
				echo "    <td align='center'><label class='style5'>PPH</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Jenis</label></td>";
				echo "    <td align='center'><label class='style5'>Nopen</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Jml</label></td>";
				echo "    <td align='center'><label class='style5'> </label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td><label class='style5'> </label></td>";
				}
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
			   $stokA = $row['jumlah2'] - $row['jumlah'];
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>".$row['no_aju_out']."</label></td>";					
				echo "	<td ><label class='style4'>".$row['tgl_aju_out']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_outnum']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_tgl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cbarang']."</label></td>";
				echo "	<td align =right><label class='style4'>".$row['jumlah']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";
				echo "	<td colspan='2'><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['invoice']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_dokumen']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cur']." ".number_format($row['harga'],2)."</label></td>";
				echo "	<td ><label class='style4'>".number_format($row['bm'],2)."</label></td>";
				echo "	<td ><label class='style4'>".number_format($row['ppn'],2)."</label></td>";
				echo "	<td ><label class='style4'>".number_format($row['pph'],2)."</label></td>";
				echo "	<td ><label class='style4'> </label></td>";
				echo "	<td ><label class='style4'> </label></td>";
				echo "	<td ><label class='style4'> </label></td>";
				echo "	<td ><label class='style4'>".$row['bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['stok_akhir']."</label></td>";
				echo "	<td align='center'><label class='style4'>".$stokA."</label></td>";	
				if ($logincat == "PLB-ADMIN"){
				echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
				}
				
		 }
		echo "</table>";

        pg_free_result($result);
		 
	}
	
	if ( $jenis == "CY-IN" ) {
		
		     $sqltext= "select B.no_aju,to_char( B.tgl_aju ,'dd/mm/yyyy') as tgl_aju,
							B.no_bc_16  as bc16 ,to_char( B.tgl_bc_16 ,'dd/mm/yyyy') as tgl_bc16 , 
							B.harga,B.itemcode as cbarang,
							B.tgl_invoice as tgl_dokumen,
							B.currency as cur, C.nama,
							sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
							sum_varchar (distinct B.no_bl ||'<br />') as no_bl,
							sum_varchar (distinct to_char( B.tgl_bl,'dd/mm/yyyy')||'<br />') as tgl_bl,
							sum_varchar (distinct B.batch_no ||'<br />') as invoice,
							sum_varchar (distinct to_char( A.tgl_in,'dd/mm/yyyy')||'<br />') as tgl_awal,
							sum_varchar (distinct to_char( A.jam_in,'HH24:MI')||'<br />') as jam_awal,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode end ||'<br />' )as kendaraan,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.nopol_in end ||'<br />' )as nopol_in,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.seal end ||'<br />' )as seal,
							sum_varchar( distinct(B.satuan) ||'<br />' ) as satuan,
						    sum(B.jumlah) as jumlah
							from wh_loket A 
							join report.plb_cy B on A.id_loket = B.id_loket  
							join v_customer C on A.kode_rel = C.kode_rel 
							where b.kode_rel LIKE '$kode_rel'  and A.tgl_in between  '$tgl1' and '$tgl2' and B.doc_out IS NULL
							group by 1,2,3,4,5,6,7,8,9
							order by no_aju";
		  
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = 0;
		 if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>AJU</label></td>";	
				echo "    <td colspan ='2' align='center'><label class='style5'>NOPEN</label></td>";
				echo "    <td align='center'><label class='style5'>CUSTOMER</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>URAIAN BARANG</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>KONTAINER</label></td>";
				echo "    <td colspan ='2' align='center'><label class='style5'>B/L</label></td>";
			//	echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>INVOICE</label></td>";
				echo "    <td colspan ='3' align='center'><label class='style5'>COO/SKA</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td align='center'><label class='style5'>KETERANGAN</label></td>";
				}
				echo "  </tr>";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>No. </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>No. </label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";		
				echo "    <td align='center'><label class='style5'> </label></td>";
				echo "    <td align='center'><label class='style5'>Nama</label></td>";
				echo "    <td align='center'><label class='style5'>Jml</label></td>";		
				echo "    <td align='center'><label class='style5'>Sat</label></td>";	
				echo "    <td align='center'><label class='style5'>20</label></td>";
				echo "    <td align='center'><label class='style5'>40</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Nilai</label></td>";
				echo "    <td align='center'><label class='style5'>No</label></td>";
				echo "    <td align='center'><label class='style5'>Tgl</label></td>";
				echo "    <td align='center'><label class='style5'>Jenis</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td><label class='style5'> </label></td>";
				}


				echo "  </tr>";
				echo "</thead>";
				echo "<tbody>";	
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				 echo "	<td ><label class='style4'>".$row['no_aju']."</label></td>";					
				 echo "	<td ><label class='style4'>".$row['tgl_aju']."</label></td>";
				echo "	<td ><label class='style4'>".$row['bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></label></label></td>";
				echo "	<td ><label class='style4'>".$row['cbarang']."</label></td>";
			//	echo "	<td ><label class='style4'>".$row['supplier']."</label></td>";
				echo "	<td align =right><label class='style4'>".$row['jumlah']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";
				echo "	<td colspan='2'><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['invoice']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_dokumen']."</label></td>";
				echo "	<td ><label class='style4'> ".$row['currency']." ".number_format($row['harga'],2)." </label></td>";
				 echo "	<td ><label class='style4'>".$row['no_coo']."</label></td>";
				 echo "	<td ><label class='style4'>".$row['tgl_coo']."</label></td>";				
				 echo "	<td ><label class='style4'>".$row['jenis']."</label></td>";		
                 if ($logincat == "PLB-ADMIN"){				 
				 echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
				 }
			
		 }
		echo "</tbody>"; 
		echo "</table>";

        pg_free_result($result);
	}
	
	
 if ( $jenis == "PEMASUKAN" ) {
		   if ($logincat == "PLB-ADMIN"){
		     $sqltext= "select  A.tgl_awal,A.jam_awal,B.batch_no as invoice, B.no_bc_16  as bc16 ,B.tgl_bc_16  as tgl_bc16 , 
						case when A.kendaraan = 'CONTAINER' THEN A.no_unit else A.nopol end as kendaraan,B.etad as etad,
						A.nopol, A.seal,E.tgl_in,E.jam_in,B.no_aju as nomor_aju,B.tgl_aju,B.negara,
						B.no_bl  as no_bl,B.tgl_bl  as tgl_bl, B.supplier,B.no_coo,B.tgl_coo,
						C.nama,B.vessel as vessel ,B.voyage  as voyage,
						sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
						sum_varchar( distinct(satuan) ||'<br>' ) as satuan,
						sum_varchar(distinct( split_part(itemname,':',1))||'<br>') as cbarang,  sum(B.jumlah) as jumlah
						from wh_flowcont A 
						join report.plb_flowbrg B on A.id_flowcont = B.id_flowbrg_splitted  
						join v_customer C on A.kode_rel = C.kode_rel
						join wh_cargo D on B.itemcode = D.itemcode
						left join wh_loket E on A.id_loket = E.id_loket 
						where  A.tgl_awal between '$tgl1' and '$tgl2'
						and	a.kode_rel LIKE '$kode_rel'  
						and a.id_aktifitas IN(1,14) 
						group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22
						order by A.tgl_awal, A.jam_awal";
		   }else{
			$sqltext= "select  A.tgl_awal,A.jam_awal,B.batch_no as invoice, B.no_bc_16  as bc16 , B.supplier, 
						case when A.kendaraan = 'CONTAINER' THEN A.no_unit else A.nopol end as kendaraan,
						B.tgl_bc_16  as tgl_bc16 ,B.no_bl as no_bl,B.tgl_bl  as tgl_bl, C.nama,
						sum_varchar( distinct(satuan) ||'<br>' ) as satuan,
						sum_varchar(distinct(split_part(itemname,':',1)) ||'<br>') as cbarang,   sum(B.jumlah) as jumlah
						from wh_flowcont A 
						join report.plb_flowbrg B on A.id_flowcont = B.id_flowbrg_splitted  
						join v_customer C on A.kode_rel = C.kode_rel
						join wh_cargo D on B.itemcode = D.itemcode
						left join wh_loket E on A.id_loket = E.id_loket	
						where  A.tgl_awal between '$tgl1' and '$tgl2'
						and	a.kode_rel LIKE '$kode_rel' 
						and a.id_aktifitas IN(1,14) 
						group by 1,2,3,4,5,6,7,8,9,10
						order by A.tgl_awal, A.jam_awal";
			  }	
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = 0;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td><label class='style5'>Container</label></td>";
				echo "    <td><label class='style5'>Nopen. BC 1.6</label></td>";
				echo "    <td><label class='style5'>Tgl Nopen BC 1.6 </label></td>";
				echo "    <td align='center'><label class='style5'>BL</label></td>";
				echo "    <td><label class='style5'>Tanggal BL</label></td>";
				echo "    <td><label class='style5'>Tanggal Aktifitas</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td><label class='style5'>Nopol IN</label></td>";
				echo "    <td><label class='style5'>Jam IN</label></td>";	
				echo "    <td align='center'><label class='style5'>Seal</label></td>";
				echo "    <td><label class='style5'>Jam Act</label></td>";		
				echo "    <td><label class='style5'>Negara Asal</label></td>";
				echo "    <td><label class='style5'>Vessel / Voyage </label></td>";
				echo "    <td align='center'><label class='style5'>Etad</label></td>";		
				echo "    <td><label class='style5'>Nomor Aju</label></td>";	
				echo "    <td><label class='style5'>Tgl Aju</label></td>";
				echo "    <td><label class='style5'>Nomor COO</label></td>";	
				echo "    <td><label class='style5'>Tgl COO</label></td>";
				}
				echo "    <td align='center'><label class='style5'>Invoice</label></td>";
				echo "    <td><label class='style5'>Pemilik Barang</label></td>";
				echo "    <td><label class='style5'>Kode Barang</label></td>";
				echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td><label class='style5'>Jumlah</label></td>";
				echo "    <td><label class='style5'>Satuan</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td><label class='style5'>Keterangan</label></td>";
				}
				echo "  </tr>";
				echo "</thead>";
				echo "<tbody>";	
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_awal']."</label></td>";
				if ($logincat == "PLB-ADMIN"){
				 echo "	<td ><label class='style4'>".$row['nopol']."</label></td>";				
				 echo "	<td ><label class='style4'>".$row['jam_in']."</label></td>";	
				 echo "	<td ><label class='style4'>".$row['seal']."</label></td>";						
				 echo "	<td ><label class='style4'>".$row['jam_awal']."</label></td>";	
				 echo "	<td ><label class='style4'>".$row['negara']."</label></td>";				
				 echo "	<td ><label class='style4'>".$row['vessel']." , ".$row['voyage']." </label></td>";				
				 echo "	<td ><label class='style4'>".$row['etad']."</label></td>";	
				 echo "	<td ><label class='style4'>".$row['nomor_aju']."</label></td>";					
				 echo "	<td ><label class='style4'>".$row['tgl_aju']."</label></td>";
				 echo "	<td ><label class='style4'>".$row['no_coo']."</label></td>";
				 echo "	<td ><label class='style4'>".$row['tgl_coo']."</label></td>";					
				}
				echo "	<td ><label class='style4'>".$row['invoice']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></label></label></td>";
				echo "	<td ><label class='style4'>".$row['cbarang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['supplier']."</label></td>";
				echo "	<td align =right><label class='style4'>".$row['jumlah']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
				}
		 }
		echo "</tbody>"; 
		echo "</table>";

        pg_free_result($result);
	}
 if ( $jenis == "PENGELUARAN" ) {
 			if ($logincat == "PLB-ADMIN"){

				$sqltext= "select A.tgl_awal,A.jam_awal,B.batch_no as invoice, 
							B.doc_out as doc_out, 
							B.no_doc_out as doc_outnum, 
							B.tgl_doc_out as doc_tgl,
							case when A.kendaraan = 'CONTAINER' THEN A.no_unit else A.nopol end as kendaraan,
							sup.nomor_daftar as bc16,sup.tanggal_daftar as tgl_bc16, C.nama,  sup.supplier,
                            sum_varchar (distinct B.keterangan ||'<br />') as keterangan,							
							sum_varchar( distinct(B.satuan) ||'<br>' ) as satuan, 
							sum_varchar(distinct(split_part(itemname,':',1)) ||'<br>') as cbarang,sum(B.jumlah) as jumlah
							from wh_flowcont A 
							join report.plb_flowbrg B on A.id_flowcont = B.id_flowbrg_splitted   
							join v_customer C on A.kode_rel = C.kode_rel
							join wh_cargo D on B.itemcode = D.itemcode
							join report.plb_flowbrg E on B.id_flowbrg_in = E.id_flowbrg 
							join report.plb_documents_in sup on E.no_bc_16 = sup.nomor_daftar and sup.uraian_dokumen = 'INVOICE' 
							and trim(E.batch_no) = trim(sup.nomor_dokumen) 
							where B.kode_rel LIKE '$kode_rel'  and A.tgl_awal between  '$tgl1' and '$tgl2'
							and a.id_aktifitas IN(2,16) 
							group by 1,2,3,4,5,6,7,8,9,10,11
							order by A.tgl_awal, A.jam_awal, B.no_doc_out";
			
			}else{
  
				$sqltext= "select A.tgl_awal,A.jam_awal,B.batch_no as invoice, 
							B.doc_out as doc_out, 
							B.no_doc_out as doc_outnum, 
							B.tgl_doc_out as doc_tgl,
							case when A.kendaraan = 'CONTAINER' THEN A.no_unit else A.nopol end as kendaraan,
							sup.nomor_daftar as bc16,sup.tanggal_daftar as tgl_bc16, C.nama,  sup.supplier,
                            sum_varchar (distinct B.keterangan ||'<br />') as keterangan,							
							sum_varchar( distinct(B.satuan) ||'<br>' ) as satuan, 
							sum_varchar(distinct(split_part(itemname,':',1)) ||'<br>') as cbarang,sum(B.jumlah) as jumlah
							from wh_flowcont A 
							join report.plb_flowbrg B on A.id_flowcont = B.id_flowbrg_splitted   
							join v_customer C on A.kode_rel = C.kode_rel
							join wh_cargo D on B.itemcode = D.itemcode
							join report.plb_flowbrg E on B.id_flowbrg_in = E.id_flowbrg 
							join report.plb_documents_in sup on E.no_bc_16 = sup.nomor_daftar and sup.uraian_dokumen = 'INVOICE' 
							and trim(E.batch_no) = trim(sup.nomor_dokumen) 
							where B.kode_rel LIKE '$kode_rel'  and A.tgl_awal between  '$tgl1' and '$tgl2'
							and a.id_aktifitas IN(2,16) 
							group by 1,2,3,4,5,6,7,8,9,10,11
							order by A.tgl_awal, A.jam_awal, B.no_doc_out";
				}
				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = 0;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table width= 100% height=15 border= 1  bgcolor=#0000CC id=data_table >";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td><label class='style5'>Container</label></td>";
				echo "    <td><label class='style5'>Nopen BC 1.6</label></td>";
				echo "    <td><label class='style5'>Tgl Nopen BC 1.6 </label></td>";
				echo "    <td><label class='style5'>Jenis Doc</label></td>";
				echo "    <td><label class='style5'>Nopen BC 2.7/2.8</label></td>";
				echo "    <td><label class='style5'>Tanggal Nopen 2.7/2.8</label></td>";
				echo "    <td><label class='style5'>Tanggal Keluar</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td><label class='style5'>Jam Akhir</label></td>";
				echo "    <td><label class='style5'>Nopol</label></td>";				
				}
				echo "    <td><label class='style5'>Invoice</label></td>";
				echo "    <td><label class='style5'>Pemilik Barang</label></td>";
				echo "    <td><label class='style5'>Nama Barang</label></td>";
				echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td><label class='style5'>Jumlah</label></td>";
				echo "    <td><label class='style5'>Satuan</label></td>";
				echo "    <td><label class='style5'>Keterangan</label></td>";
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bc16']."</label></td>";
				echo "	<td  align ='center' ><label class='style4'>".$row['doc_out']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_outnum']."</label></td>";
				echo "	<td ><label class='style4'>".$row['doc_tgl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_awal']."</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "	<td ><label class='style4'>".$row['jam_akhir']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nopol']."</label></td>";			
				}
				echo "	<td ><label class='style4'>".$row['invoice']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cbarang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['supplier']."</label></td>";
				echo "	<td align =right><label class='style4'>".$row['jumlah']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['keterangan']."</label></td>";
		 }
		echo "</table>";
        pg_free_result($result);
	}
if ( $jenis == "STOCK" ) {
		$sqltext= "select  b.no_bc_16 as bc16,b.tgl_bc_16 as tgl_bc16, B.nama,B.tgl_awal,B.no_bl,B.tgl_bl,
                     sum_varchar (distinct( case when B.kendaraan = 'CONTAINER' THEN B.no_unit else B.nopol end ||'<br />')) as kendaraan,
                     sum_varchar (distinct( split_part(B.itemname,':',1))||'<br />') as cbarang,
                     sum_varchar (distinct( split_part(B.itemname,':',2))||'<br />') as nbarang,
                     sum_varchar (distinct B.itemname||'<br />') as itemname,
                     sum_varchar (distinct B.satuan||'<br />') as satuan,
                     sum_varchar (distinct b.supplier||'<br />') as supplier,    
                     sum_varchar (distinct B.batch_no||'<br />') as invoice,          
		             sum(A.jumlah) as jumlah
					FROM
					(select id_flowbrg_in, sum (qty - qty_out) as jumlah  from v_mutasi
					 where kode_rel  LIKE '$kode_rel'  and tgl_awal <='$tgl1'
					 group by 1)A 
					join
					(select * from report.v_plb_mutasi 
					 where kode_rel LIKE '$kode_rel'   and tgl_awal <='$tgl1'
					 and (id_stok_out is null or id_stok_out ='' )) B
					 on A.id_flowbrg_in = B.id_flowbrg_in
					where A.jumlah>0
					group by 1,2,3,4,5,6" ;
				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = 0;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table width= 100% height=15 border= 1  font=8 bgcolor=#0000CC id=data_table >";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td><label class='style5'>Container</label></td>";
				echo "    <td><label class='style5'>No. BC 1.6</label></td>";
				echo "    <td><label class='style5'>Tanggal BC 1.6 </label></td>";
				echo "    <td><label class='style5'>No. BL</label></td>";
				echo "    <td><label class='style5'>Tanggal BL</label></td>";
				echo "    <td><label class='style5'>Tanggal Masuk</label></td>";
				echo "    <td><label class='style5'>Invoice</label></td>";
				echo "    <td><label class='style5'>Pemilik Barang</label></td>";
				echo "    <td><label class='style5'>Kode Barang</label></td>";
				echo "    <td><label class='style5'>Nama Barang</label></td>";
				echo "    <td><label class='style5'>Jumlah</label></td>";
				echo "    <td><label class='style5'>Satuan</label></td>";
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>".$row['kendaraan']."</label></td>";
				echo "	<td ><label class='style4'>".$row['bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bc16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bl']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_awal']."</label></td>";
				echo "	<td ><label class='style4'>".$row['invoice']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cbarang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['supplier']."</label></td>";
				echo "	<td align =right><label class='style4'>".$row['jumlah']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";

		 }
		echo "</table>";
        pg_free_result($result);
	}



}

if ( $jenis == "MUTASI" ) {
/*	 $sqltext= "select A.id_stok_in,A.id_stok_out,case when kendaraan = 'CONTAINER' then A.no_unit else A.nopol end as jns_kend,
				A.tgl_awal,A.batch_no as invoice,A.nama,A.itemcode,A.doc_out,A.no_doc_out,A.tgl_doc_out, A.supplier,A.no_bl,A.tgl_bl,A.no_bc_16,A.tgl_bc_16,
				trim(split_part(itemname,':',1)) as cbarang, trim(split_part(itemname,':',2)) as nbarang, A.satuan , sum(qty) as jumlah,sum(qty_out) as jumlah_out
				from report.v_plb_mutasi  A 
				join wh_book bo on bo.no_book = split_part(A.id_stok_in, '-',1) 
				where case when tgl_in_asal isnull then tgl_awal between  '$tgl1' and '$tgl2'
				else tgl_in_asal BETWEEN  '$tgl1' and '$tgl2' end
				and A.kode_rel  LIKE '$kode_rel'
				and BO.id_aktifitas IN(1,2,14,16)
				group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
				order by nama,id_stok_in,invoice,A.itemcode,id_stok_out nulls first " ;
*/
	 $sqltext= "select A.id_stok_in,A.id_stok_out,case when kendaraan = 'CONTAINER' then A.no_unit else A.nopol end as jns_kend,
				A.tgl_awal,A.batch_no as invoice,A.nama,A.itemcode,A.doc_out,A.no_doc_out,A.tgl_doc_out, A.supplier,A.no_bl,A.tgl_bl,A.no_bc_16,A.tgl_bc_16,
				trim(split_part(itemname,':',1)) as cbarang, trim(split_part(itemname,':',2)) as nbarang, A.satuan , sum(qty) as jumlah,sum(qty_out) as jumlah_out
				from report.v_plb_mutasi  A 
				where A.id_stok_in || A.itemcode in (
					select distinct A.id_stok_in || A.itemcode
					from report.v_plb_mutasi  A 
					join wh_book bo on bo.no_book = split_part(A.id_stok_in, '-',1) 
					where tgl_awal between  '$tgl1' and '$tgl2'
					and A.kode_rel  LIKE '$kode_rel'
					and BO.id_aktifitas IN(1,2,14,16)
				)
				group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18
				order by nama,id_stok_in,invoice,A.itemcode,id_stok_out nulls first " ;
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = 0;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table width= 100% height=15 border= 1 font=8 bgcolor=#0000CC id=data_table >";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td><label class='style5'>Container</label></td>";
				echo "    <td><label class='style5'>No Bukti Pemasukan</label></td>";
				echo "    <td><label class='style5'>No Bukti Pengeluaran</label></td>";
				echo "    <td><label class='style5'>No. BC 1.6</label></td>";
				echo "    <td><label class='style5'>Tanggal BC 1.6 </label></td>";
				echo "    <td><label class='style5'>No. BC 2.7/2.8</label></td>";
				echo "    <td><label class='style5'>Tanggal </label></td>";
				echo "    <td><label class='style5'>Tanggal Aktifitas</label></td>";
				echo "    <td><label class='style5'>Invoice</label></td>";
				echo "    <td><label class='style5'>Pemilik Barang</label></td>";
				echo "    <td><label class='style5'>Nama Barang</label></td>";
				echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td><label class='style5'>Jumlah IN</label></td>";
				echo "    <td><label class='style5'>Jumlah OUT</label></td>";
				echo "    <td><label class='style5'>Satuan</label></td>";
				echo "  </tr>";
		  }
		  $id0 = "";
		  $satan = "";
		  $jumlahx = 0;
          while ($row = pg_fetch_assoc($result)) {
				if ($id0 != $row['id_stok_in']) {
					if ($id0 != "") {
						echo "  <tr style=background:#E0E0E0; height=30> ";
						echo "	<td colspan=13 align=right><label class='style4'>STOK AKHIR&nbsp;</label></td>";
						echo "	<td colspan=2 align =center ><label class='style4'>$jumlahx</label></td>";
						echo "	<td ><label class='style4'>".$satuan."</label></td>";
						echo "  </tr>";
					}
					$jumlahx = 0;
					$id0 = $row['id_stok_in'];
				}

				$number = $number +1;
				if (($number % 2) == 1) {
					echo "    <tr style=background:#FFFFCC;> ";
				} else {
					echo "    <tr style=background:#99CCFF;> ";
				}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>".$row['jns_kend']."</label></td>";
				echo "	<td ><label class='style4'>".$row['id_stok_in']."</label></td>";
				echo "	<td ><label class='style4'>".$row['id_stok_out']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_bc_16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_bc_16']."</label></td>";
				echo "	<td ><label class='style4'>".$row['no_doc_out']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_doc_out']."</label></td>";
				echo "	<td ><label class='style4'>".$row['tgl_awal']."</label></td>";
				echo "	<td ><label class='style4'>".$row['invoice']."</label></td>";
				echo "	<td ><label class='style4'>".$row['nama']."</label></td>";
				echo "	<td ><label class='style4'>".$row['cbarang']."</label></td>";
				echo "	<td ><label class='style4'>".$row['supplier']."</label></td>";
				echo "	<td align =center ><label class='style4'>".$row['jumlah']."</label></td>";
				echo "	<td align =center><label class='style4'>".$row['jumlah_out']."</label></td>";
				echo "	<td ><label class='style4'>".$row['satuan']."</label></td>";
				echo "  </tr>";
				$jumlahx = $jumlahx + $row['jumlah'] - $row['jumlah_out'];
				$satuan = $row['satuan'];
		  }
		echo "  <tr style=background:#E0E0E0;> ";
		echo "	<td colspan=13 align=right><label class='style4'>STOK AKHIR&nbsp;</label></td>";
		echo "	<td colspan=2 align =center ><label class='style4'>$jumlahx</label></td>";
		echo "	<td ><label class='style4'>".$satuan."</label></td>";
		echo "  </tr>";
		echo "</table>";
        pg_free_result($result);
}

if ( $jenis == "PLAN" ){
      $sqltext="select C.no_unit,A.uraian_dokumen, A.nomor_dokumen as invoice , A.tanggal_dokumen as tgl_invoice, A.nama_pengangkut as vessel, 
				A.nomor_voy_flight as voyage, A.nomor_daftar as bc16 , A.tanggal_daftar as tgl_bc16, A.tanggal_tiba as etad,
				A.uraian_negara, A.supplier,B.nomor_dokumen as no_bl, B.tanggal_dokumen as tgl_bl 
				from  report.plb_documents_in A
				left join report.plb_documents_in B on B.nomor_daftar = A.nomor_daftar and A.tanggal_daftar= B.tanggal_daftar and B.uraian_dokumen = 'B/L' 
				left join report.plb_documents_incont C on  C.tanggal_dokumen = A.tanggal_dokumen and A.nomor_dokumen= C.nomor_dokumen
				where A.uraian_dokumen = 'INVOICE' 
				and ((A.tanggal_dokumen BETWEEN  '$tgl1' and '$tgl2' ) or (B.tanggal_dokumen BETWEEN  '$tgl1' and '$tgl2' )
					or (B.tanggal_aju BETWEEN  '$tgl1' and '$tgl2')
					or (A.tanggal_tiba BETWEEN  '$tgl1' and 
					case when cast('$tgl2' as date) >= current_date then cast('$tgl2' as date) + 90  else cast('$tgl2' as date)  end )
					) " ;

		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td><label class='style5'>Container</label></td>";
				echo "    <td><label class='style5'>No. BC 1.6</label></td>";
				echo "    <td><label class='style5'>Tanggal BC 1.6 </label></td>";
				echo "    <td><label class='style5'>No. BL</label></td>";
				echo "    <td><label class='style5'>Tanggal BL</label></td>";
				echo "    <td><label class='style5'>Invoice</label></td>";
				echo "    <td><label class='style5'>Negara Asal</label></td>";	
				echo "    <td><label class='style5'>Vessel, Voyage</label></td>";			
				echo "    <td><label class='style5'>Tgl Eta</label></td>";			
				echo "    <td><label class='style5'>Shipper</label></td>";
				echo "  </tr>";
				echo "</thead>";
				echo "<tbody>";	
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>$row[no_unit]</label></td>";
				echo "	<td ><label class='style4'>$row[bc16]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_bc16]</label></td>";
				echo "	<td ><label class='style4'>$row[no_bl]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_bl]</label></td>";
				echo "	<td ><label class='style4'>$row[invoice]</label></td>";
				echo "	<td ><label class='style4'>$row[uraian_negara]</label></td>";
				echo "	<td ><label class='style4'>$row[vessel],  $row[voyage]  </label></td>";
				echo "	<td ><label class='style4'>$row[etad]</label></td>";
				echo "	<td ><label class='style4'>$row[supplier]</label></td>";
		 }
		echo "</tbody>"; 
		echo "</table>";

        pg_free_result($result);


}


?>
</p>
