<?php
session_start();
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}

$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

?>

<?php
if ($_POST['tglc1']  && $_POST['tglc2']) {
?>
<?php
    require_once('db-inc2.php');

    $jenis = $_POST['jenisc'];
    $tgl1 = $_POST['tglc1'];
    $tgl2 = $_POST['tglc2'];
    $kode_rel = TRIM($_POST['custc']);
    if ($kode_rel == "ALL") {
        $kode_rel = '50%';
    }

    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=user-session-per" . date('d-m-Y', strtotime($tgl1)) . "S/D" . date('d-m-Y', strtotime($tgl2)) . ".xls");

    echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
    echo "<h5> LAPORAN USER SESSION <br>";
    echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
    echo "<br>";
    echo "<br>";

    $sqltext = "select * from report.log_session where tanggal between '$tgl1' and '$tgl2'";

    $result = pg_query($db2_, $sqltext);
    $baris  = pg_num_rows($result);
    $number = 0;

    echo "<table class='table table-striped table-bordered data'>";
    echo    "<thead>";
    echo        "<tr>";
    echo            "<th>NO</th>";
    echo            "<th>USERNAME</th>";
    echo            "<th>KATEGORI</th>";
    echo            "<th>TANGGAL</th>";
    echo            "<th>WAKTU</th>";
    echo            "<th>STATUS</th>";
    echo            "<th>IP</th>";
    echo        "</tr>";
    echo    "</thead>";
    echo    "<tbody>";

    while ($row = pg_fetch_assoc($result)) {
        $number = $number + 1;
        $rel = isset($row['kode_rel']);
        echo        "<tr>";
        echo            "<td>" . $number . "</td>";
        echo            "<td>" . $row['username'] . "</td>";
        echo            "<td>" . $row['kategori'] . "</td>";
        echo            "<td>" . date('d-m-Y', strtotime($row['tanggal'])) . "</td>";
        echo            "<td>" . $row['waktu'] . "</td>";
        echo            "<td>" . $row['status'] . "</td>";
        echo            "<td>" . $row['ip'] . "</td>";
        echo        "</tr>";
    }
    echo    "</tbody>";
    echo  "</table>";
} else {
    require_once('db-inc2.php');

    header("Content-type: application/vnd-ms-excel");
    header("Content-Disposition: attachment; filename=user-session-per" . date('d-m-Y', strtotime($tgl1)) . "S/D" . date('d-m-Y', strtotime($tgl2)) . ".xls");

    echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
    echo "<h5> LAPORAN USER SESSION <br>";
    echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
    echo "<br>";
    echo "<br>";

    $sqltext = "select * from report.log_session where tanggal = current_date";

    $result = pg_query($db2_, $sqltext);
    $baris  = pg_num_rows($result);
    $number = 0;

    echo "<table class='table table-striped table-bordered data'>";
    echo    "<thead>";
    echo        "<tr>";
    echo            "<th>NO</th>";
    echo            "<th>USERNAME</th>";
    echo            "<th>KATEGORI</th>";
    echo            "<th>TANGGAL</th>";
    echo            "<th>WAKTU</th>";
    echo            "<th>STATUS</th>";
    echo            "<th>IP</th>";
    echo        "</tr>";
    echo    "</thead>";
    echo    "<tbody>";

    while ($row = pg_fetch_assoc($result)) {
        $number = $number + 1;
        $rel = isset($row['kode_rel']);
        echo        "<tr>";
        echo            "<td>" . $number . "</td>";
        echo            "<td>" . $row['username'] . "</td>";
        echo            "<td>" . $row['kategori'] . "</td>";
        echo            "<td>" . date('d-m-Y', strtotime($row['tanggal'])) . "</td>";
        echo            "<td>" . $row['waktu'] . "</td>";
        echo            "<td>" . $row['status'] . "</td>";
        echo            "<td>" . $row['ip'] . "</td>";
        echo        "</tr>";
    }
    echo    "</tbody>";
    echo  "</table>";
}
?>