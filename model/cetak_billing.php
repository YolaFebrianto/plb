<?php
session_start();
if (!isset($_SESSION['username'])) {
	header('Location: index.php');
}

$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

?>
<?php
if ($_POST['tglc1'] && $_POST['tglc2']) {
?>
<?php
	require_once('db-inc2.php');

	$tgl1 = $_POST['tglc1'];
	$tgl2 = $_POST['tglc2'];
	$kode_rel = TRIM($_POST['custc']);
	if ($kode_rel == "ALL") {
		$kode_rel = '50%';
	}
	$startRec = 0;

	header("Content-type: application/vnd-ms-excel");
	header("Content-Disposition: attachment; filename=LaporanBillingBC28per" . date('d-m-Y', strtotime($tgl1)) . "S/D" . date('d-m-Y', strtotime($tgl2)) . ".xls");



	echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
	echo "<h5> LAPORAN BILLING BC 2.8 <br>";
	echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
	echo "<br>";
	echo "<br>";


	$sqltext = "select B.no_aju_out,C.nama,
							B.doc_out as doc_out, E.no_bc_16  as bc16 , 
							B.no_doc_out as doc_outnum,pack.jumlah4,
							F.cif_rupiah as cif, F.cif as cif_o,G.no_billing,G.tgl_billing,G.no_ntpn,G.tgl_ntpn,
							--F.seri_barang,F.kode_barang,F.uraian_barang,
							G.bm,G.ppn,G.pph,F.currency,B.kode_rel,
							sum_varchar (distinct to_char( F.tanggal_dokumen,'dd-mm-yyyy')||'<br>') as tgl_dokumen,
							sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
							sum_varchar (distinct to_char( E.tgl_bc_16,'dd-mm-yyyy')||'<br>') as tgl_bc16,
							sum_varchar (distinct to_char( B.tgl_aju_out,'dd-mm-yyyy')||'<br>') as tgl_aju_out,
							sum_varchar (distinct to_char( B.tgl_doc_out,'dd-mm-yyyy')||'<br>') as doc_tgl,
							sum_varchar (distinct( split_part(B.id_flowbrg,'-',1)) ||'<br />') as book,
							sum_varchar (distinct to_char( A.tgl_awal ,'dd-mm-yyyy') ||'<br />') as tgl_aktifitas,
							sum_varchar (distinct( cast(F.jumlah as int )) ||'<br />') as jumlahd,
							sum_varchar (distinct E.no_coo ||'<br>') as no_coo,
							--sum_varchar (distinct F.uraian_barang ||'<br>') as uraian_barang,
							sum_varchar (distinct F.kemasan ||'<br>') as kemasan,
							sum_varchar (distinct F.hs ||'<br>') as hs,
							sum_varchar (distinct F.seri_barang ||'. ' || F.kode_barang ||'  |  '||F.uraian_barang ||'<br />' ) as seri_barang,
							--sum_varchar (distinct F.kode_barang ||'<br>') as kode_barang,
							sum_varchar (distinct to_char( E.tgl_coo,'dd-mm-yyyy')||'<br>') as tgl_coo,
							sum_varchar (distinct E.no_bl ||'<br>') as no_bl,
							sum_varchar (distinct to_char( E.tgl_bl,'dd-mm-yyyy')||'<br>') as tgl_bl,
							sum_varchar (distinct B.batch_no ||'<br>') as invoice,
							sum_varchar (distinct to_char( A.tgl_awal,'dd-mm-yyyy')||'<br>') as tgl_awal,
							sum_varchar (distinct to_char( A.jam_awal,'HH24:MI')||'<br>') as jam_awal,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode else A.nopol end ||'<br>' )as kendaraan,
							sum_varchar( distinct(B.satuan) ||'<br>' ) as satuan,
							sum_varchar(distinct( split_part(itemname,':',1))||'<br>') as cbarang,
							sum_varchar(distinct( split_part(itemname,':',2))||'<br />') as namabarang

							from report.plb_documents_out F 
							left join report.plb_flowbrg B on B.no_aju_out = SUBSTRING(F.no_aju,15) 
							and B.no_doc_out = F.nomor_daftar and F.uraian_dokumen = 'INVOICE' and trim(B.batch_no) = trim(F.nomor_dokumen)
							left join wh_flowcont A on A.id_flowcont = B.id_flowbrg_splitted 
							left join v_customer C on A.kode_rel = C.kode_rel
							join wh_cargo D on B.itemcode = D.itemcode
							join report.plb_flowbrg E on B.id_flowbrg_in = E.id_flowbrg
							
						        join (select B.no_aju_out, sum(B.jumlah) as jumlah4
                                                        from report.plb_flowbrg B
                                                        where B.kode_rel like '$kode_rel'
                                                        and B.doc_out = 'BC 2.8'
                                                        --and A.no_aju = E.no_aju
                                                        group by 1) as pack
                                                        on substring(F.no_aju,15) = pack.no_aju_out
							left join report.plb_barang_tarif G on F.no_aju = G.nomor_aju
							
							where B.kode_rel LIKE '$kode_rel'  and F.tanggal_daftar between  '$tgl1' and '$tgl2' and B.doc_out = 'BC 2.8'
							and a.id_aktifitas IN(2,16,24) 
							group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
							order by doc_outnum";


	$result = pg_query($db2_, $sqltext);
	$baris  = pg_num_rows($result);
	$number = $startRec;

	echo "<table border='1' class='table table-striped table-bordered data'>";
	echo	"<thead>";
	echo		"<tr>";
	echo			"<th rowspan='2'>NO</th>";
	echo			"<th colspan='4'>DOKUMEN PABEAN BC 2.8</th>";
	echo			"<th rowspan='2'>IMPORTIR</th>";
	echo			"<th colspan='2'>KODE BILLING</th>";
	echo			"<th colspan='2'>NTPN</th>";
	echo			"<th colspan='3'>PEMBAYARAN</th>";
	echo			"<th colspan='2'>DOKUMEN PABEAN BC 1.6</th>";
	echo			"<th rowspan='2'>KETERANGAN</th>";
	echo		"</tr>";
	echo		"<tr>";
	echo			"<th>NOMOR AJU</th>";
	echo			"<th>TGL AJU</th>";
	echo			"<th>NOPEN</th>";
	echo			"<th>TGL NOPEN</th>";
	echo			"<th>NOMOR</th>";
	echo			"<th>TANGGAL</th>";
	echo			"<th>NO</th>";
	echo			"<th>TANGGAL</th>";
	echo			"<th>BM</th>";
	echo			"<th>PPN</th>";
	echo			"<th>PPH</th>";
	echo			"<th>NOPEN</th>";
	echo			"<th>TGL NOPEN</th>";
	echo		"</tr>";
	echo	"</thead>";
	echo	"<tbody>";

	while ($row = pg_fetch_assoc($result)) {
		$number = $number + 1;
		$rel = $row['kode_rel'];
		echo		"<tr>";
		echo			"<td>" . $number . "</td>";
		echo			"<td>" . $row['no_aju_out'] . "</td>";
		echo			"<td>" . $row['tgl_aju_out'] . "</td>";
		echo			"<td>'" . $row['doc_outnum'] . "</td>";
		echo			"<td>" . $row['doc_tgl'] . "</td>";
		echo			"<td>" . $row['nama'] . "</td>";
		echo			"<td>" . $row['no_billing'] . "</td>";
		echo			"<td>" . date('d-m-Y', strtotime($row['tgl_billing'])) . "</td>";
		echo			"<td>" . $row['no_ntpn'] . "</td>";
		echo			"<td>" . date('d-m-Y', strtotime($row['tgl_ntpn'])) . "</td>";
		echo			"<td>" . number_format($row['bm'], 2) . "</td>";
		echo			"<td>" . number_format($row['ppn'], 2) . "</td>";
		echo			"<td>" . number_format($row['pph'], 2) . "</td>";
		echo			"<td>'" . $row['bc16'] . "</td>";
		echo			"<td>" . $row['tgl_bc16'] . "</td>";
		echo			"<td></td>";
		echo		"</tr>";
	}
	echo	"</tbody>";
	echo  "</table>";
}
?>