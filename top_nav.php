<link rel="stylesheet" href="assets/css/sweetalert2.min.css">
<script src="assets/js/sweetalert2.min.js"></script>

<?php

if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}
?>

<a href="home.php" class="logo">IT Inventory PLB</a>
<!-- Header Navbar: style can be found in header.less -->
<nav class="navbar navbar-static-top" role="navigation">
    <!-- Sidebar toggle button-->
    <a href="#" class="navbar-btn sidebar-toggle" data-target="#atas" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
    </a>

    <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
            <a href="#" onclick="confirmOut()">
                <strong><span class="fa fa-sign-out"></span>LOGOUT</strong>
            </a>
            <!-- <a href="logout.php" onclick="return confirm('Apakah anda yakin ingin keluar ?')">[Logout]</a> -->
            <!-- <button class="btn btn-primary" onclick="confirmOut()">Logout</button> -->

        </li>
    </ul>

</nav>

<script>
function confirmOut() {
    Swal.fire({
        title: 'Logout',
        text: 'Do you want to logout this Account?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#DD6B55',
        confirmButtonText: 'Yes!',
        closeOnConfirm: false
    }).then((result) => {
        if (result.isConfirmed) {
            window.location.href = 'logout.php'
        } else {
            // Swal.fire('Back to Application', '', 'info')
        }

    });
}
</script>