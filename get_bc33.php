<?php 

include 'dbmy-inc.php';
require_once('insert_log_activity.php');

$tgl1 = $_POST['tglan1'];
$tgl2 = $_POST['tglan2'];
$jenis_doc =  $_POST['jenis_doc'];
$kategori  =  $_POST['logincat'];
$username = $_POST['username'];
$address = $_POST['address'];


if ($jenis_doc  == 'BC33'){
	
$log_remark = "Open Laporan BC 3.3  Tanggal " . date('d-m-Y', strtotime($tgl1));
insert_log($username, $kategori, $address, "REPORT", $log_remark);	
		
echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";	
echo  "PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA";
echo '<br>';
echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";	
echo  "LAPORAN PER DOKUMEN PABEAN BC 3.3";
echo '<br>';
echo "&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp";	
echo  "PERIODE : " .$tgl1." S.D ".$tgl2 ;
	

	$query =" select jenis_dokumen,nomor_daftar,tanggal_daftar,nomor_aju,tanggal_aju,
			vessel,voy_flight,jumlah_barang,jumlah_kontainer,customer,
			alamat_eksportir,nama_penerima,nama_pembeli,kendaraan,
			group_concat(seri_barang, ' | ',kode_barang,' | ',nama_barang, '\r\n' ) as cargo,
			hs, invoice,tgl_invoice,jumlah_kemasan, sum(nilai_barang) as nilai_brg,currency
			from (
			select  a.nomor_dokumen as invoice, a.tanggal_dokumen as tgl_invoice, c.uraian as jenis_dokumen,
			 b.nomor_daftar,b.tanggal_daftar,  substring(b.nomor_aju,15,15) as nomor_aju, b.tanggal_aju,
			b.nama_pengangkut as vessel,b.nomor_pengangkut as voy_flight,
			b.jumlah_barang,b.jumlah_kontainer,
			b.nama_eksportir as customer, 
			b.alamat_eksportir,b.nama_penerima,
			b.nama_pembeli,  group_concat( distinct j.nomor_kontainer,'/',j.kode_ukuran_kontainer, '\r\n' ) as kendaraan, 
			g.seri_barang, g.kode_barang, g.uraian as nama_barang,
			g.hs,group_concat(distinct  f.jumlah_kemasan ,' ',h.uraian, '\r\n' )  as jumlah_kemasan,
			g.nilai_barang  ,b.kode_valuta as currency
			from td_dokumen a 
			join td_header b on b.id = a.id_header
			join tr_jenis_dokumen c on b.kode_jenis_dokumen = c.kode
			join tr_negara d on d.kode = b.kode_negara_pembeli
			join tr_daerah e on e.kode = b.kode_daerah_asal_barang 
			join td_kemasan f on f.id_header = b.id
			left join td_barang g on b.id = g.id_header
			join tr_jenis_kemasan h on f.kode_jenis_kemasan = h.kode
			join td_barang_tarif i on b.id = i.id_header 
			left join td_kontainer j on b.id = j.id_header
			where  a.kode_jenis_dokumen = 380 and b.tanggal_daftar between '$tgl1' and '$tgl2'
			group by b.id, g.seri_barang,b.nomor_aju
			)aa group by nomor_aju";
			

	if($result = mysqli_query($mydb, $query)){
		if(mysqli_num_rows($result) > 0){
			$number  = 0;
        //$number = $startRec;

				echo "<table id='mytable' class='table table-striped table-bordered data nowrap'>";
				echo    "<thead>";
				echo        "<tr>";
				echo            "<th rowspan='2'>NO</th>";
				echo            "<th rowspan='2'>JENIS DOKUMEN</th>";
				echo            "<th colspan='4'>DOKUMEN PABEAN</th>";
				echo            "<th rowspan='2'>CUSTOMER</th>";
				echo            "<th rowspan='2'>PENERIMA</th>";
				echo            "<th rowspan='2'>PEMBELI</th>";
				echo            "<th rowspan='2'>CONTAINER</th>";
				echo            "<th rowspan='2'>SERI BARANG | KODE BARANG | NAMA BARANG</th>";
				echo            "<th rowspan='2'>JUMLAH KEMASAN</th>";
				echo            "<th colspan='4'>INVOICE</th>";
				echo            "<th rowspan='2'>KODE HS</th>";
				echo            "<th rowspan='2'>KETERANGAN</th>";
				echo        "</tr>";
				echo        "<tr>";
				echo            "<th>NOPEN</th>";
				echo            "<th>TGL NOPEN</th>";
				echo            "<th>NOMOR AJU</th>";
				echo            "<th>TGL AJU</th>";
				echo            "<th>NOMOR</th>";
				echo            "<th>TANGGAL</th>";
				echo            "<th>CURRENCY</th>";
				echo            "<th>NILAI</th>";
				echo        "</tr>";
				echo    "</thead>";
				echo    "<tbody>";

			while($row = mysqli_fetch_array($result)){
				$number = $number + 1;
				$rel = $row['kode_rel'];
				echo        "<tr>";
				echo            "<td align=center>" . $number . "</td>";
				echo            "<td>" . $row['jenis_dokumen'] . "</td>";
				echo            "<td>" . $row['nomor_daftar'] . "</td>";
				echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_daftar'])) . "</td>";
				echo            "<td>" . $row['nomor_aju'] . "</td>";
				echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_aju'])) . "</td>";
				echo            "<td>" . $row['customer'] . "</td>";
				echo            "<td>" . $row['nama_penerima'] . "</td>";
				echo            "<td>" . $row['nama_pembeli'] . "</td>";
				echo            "<td>" . $row['kendaraan'] . "</td>";
				echo            "<td>" . $row['cargo'] . "</td>";
				echo            "<td>" . $row['jumlah_kemasan'] . "</td>";
				echo            "<td>" . $row['invoice'] . "</td>";
				echo            "<td>" . $row['tgl_invoice'] . "</td>";
				echo            "<td>" . $row['currency'] . "</td>";
				echo            "<td>" . $row['nilai_brg'] . "</td>";
				echo            "<td>" . $row['hs'] . "</td>";
				echo            "<td>" . $row['keterangan'] . "</td>";
				echo        "</tr>";				

			}
			echo    "</tbody>";
			echo  "</table>";

		}
	}

}






	





?>