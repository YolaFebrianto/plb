	<?php
    if (isset($_POST['tglan1'])) {
        require_once('db-inc2.php');
        require_once('insert_log_activity.php');

        $cargo = $_POST['cargo'] . "%";
        $tgl1 = $_POST['tglan1'];
        $invpkl = $_POST['invpkl'];
        $kode_rel = TRIM($_POST['cust']);

        // insert_log($username,$kategori,$address ,$status,$remark ) 
        $log_remark = "Open REPORT DETIL LOKASI Tanggal " . date('d-m-Y', strtotime($tgl1));
        if (isset($cargo) != "%") {
            $log_remark .= " Cargo : " . $cargo;
        }
        if (isset($invpkl) != "") {
            $log_remark .= " Invoice / Packing List :" . $invpkl;
        }
        if (isset($kode_rel) != "") {
            $log_remark .= " Customer  :" . $kode_rel;
        }

        insert_log($username, $kategori, $address, "REPORT", $log_remark);

        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> DETIL LOKASI";
        echo "<hr>";
        echo "<br>";
        echo "<br>";

        $sqltext = "select  case when kode_rel = '117900' then split_part(B.itemname,':',3) else split_part(B.itemname,':',1) end as kode_brg,
                    case when kode_rel = '117900'  then split_part(B.itemname,':',1)  else split_part(B.itemname,':',2) end as nbarang, 
                    B.nama, B.itemname,B.itemcode, B.keterangan,B.exim,
                    B.tgl_awal, B.id_stok_in, B.nama_aktifitas, A.id_flowbrg_in, A.jumlah, B.satuan, A.M3, B.kondisi,
                    case when B.kendaraan = 'CONTAINER' THEN B.no_unit else B.nopol end as kendaraan, B.batch_no,B.palet,
                    case when kode_rel ='119200' then round ( cast(A.kg as numeric) ,0) else A.kg end as kg,
                    CASE when Lo.ext_lokasi is null then ltrim(B.id_lokasi,'WH.')  else Lo.ext_lokasi end as id_lokasi,
                    Lo.blok_id as blok 
                    FROM
                    (select id_flowbrg_in, sum (qty - qty_out) as jumlah , sum (m3 - m3_out) as M3, sum (kg - kg_out) as kg  from v_mutasi
                    where kode_rel like '$kode_rel'  and tgl_awal <= '$tgl1'
                    and case when kode_rel = '003612' then active else true end
                    and itemname like '$cargo' 
                    group by 1)A 
                    join
                    (select * from v_mutasi 
                    where kode_rel like '$kode_rel'  and tgl_awal <= '$tgl1'
                    and case when kode_rel = '003612' then active else true end
                    and itemname like '$cargo' 
                    and (id_stok_out is null or id_stok_out ='' )) B
                    on A.id_flowbrg_in = B.id_flowbrg_in
                    left join wh_lokasi Lo  on lo.id_lokasi = B.id_lokasi
                    where A.jumlah>0 AND  B.id_aktifitas <> '23'";
        if ($invpkl != '') {
            $sqltext .= "and case when kode_rel <> '501600' then batch_no = '$invpkl' else exim = '$invpkl' end order by kendaraan,nbarang";
        } else if ($_POST['cargo'] != '') {
            $sqltext .= "and B.itemname like '$cargo' order by kendaraan,nbarang";
        }

        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th>NO</th>";
        echo            "<th>KODE BARANG</th>";
        echo            "<th>LOKASI</th>";
        echo            "<th>TGL MASUK</th>";
        echo            "<th>INVOICE</th>";
        echo            "<th>PACKING LIST</th>";
        echo            "<th>KONDISI</th>";
        echo            "<th>QTY</th>";
        echo            "<th>PACK</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        $nama_brg = "";
        $satuan = "";
        $jumlahx = 0;
        while ($row = pg_fetch_assoc($result)) {

            if ($nama_brg != $row['nbarang']) {
                if ($nama_brg != "") {
                    echo "  <tr style='background:#C0C0C0;'> ";
                    echo "	<td colspan='7' align='right'><b>JUMLAH&nbsp;</b></td>";
                    echo "	<td align ='center'><b>" . $jumlahx . "</b></td>";
                    echo "	<td align ='center'><b>" . $satuan . "</b></td>";
                    echo "  </tr>";
                }
                $jumlahx = 0;
                $nama_brg = $row['nbarang'];
            }

            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['kode_brg'] . "</td>";
            echo            "<td>WH" . $row['id_lokasi'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_awal'])) . "</td>";
            echo            "<td>" . $row['batch_no'] . "</td>";
            echo            "<td>" . $row['exim'] . "</td>";
            echo            "<td>" . $row['kondisi'] . "</td>";
            echo            "<td align ='center'>" . $row['jumlah'] . "</td>";
            echo            "<td align ='center'>" . $row['satuan'] . "</td>";
            echo        "</tr>";
            $jumlahx = $jumlahx + $row['jumlah'];
            $satuan = $row['satuan'];
        }
        echo "  <tr style='background:#C0C0C0;'> ";
        echo "	<td colspan='7' align='right'><b>JUMLAH&nbsp;</b></td>";
        echo "	<td align ='center'><b>" . $jumlahx . "</b></td>";
        echo "	<td align ='center'><b>" . $satuan . "</b></td>";
        echo "  </tr>";
        echo    "</tbody>";
        echo  "</table>";
    }
    ?>