<?php
session_start();
$_SESSION['MENU'] = 'CY';
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}
$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

include 'path.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title><?= TITLE_APP ?></title>
    <link rel="shortcut icon" href="<?= IMAGES_PATH ?>/icons/logoijs.jpg">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap.min.css">

    <!-- start:bootstrap v3.2.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.min.css">
    <!-- start:font awesome v4.1.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/font-awesome.min.css">
    <!-- start:bootstrap reset -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap-reset.css">
    <!-- start:style arjuna -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/arjuna.css">

    <script src="<?= ASSETS_PATH ?>/js/jquery-3.1.1.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/style-view.css">
    <script src="bootcode.js"></script>

    <script src="<?= JS_PATH ?>/jquery.js"></script>
    <script src="<?= JS_PATH ?>/tableToExcel.js"></script>

    <script src="bootcode.js"></script>
    <script language="javascript">
    var ajaxRequest;

    function getAjax() //fungsi untuk mengecek AJAX pada browser
    {
        try {
            ajaxRequest = new XMLHttpRequest();
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {
                    alert("Your browser broke!");
                    return false;
                }
            }
        }
    }

    function showData() {

        var idloket = document.getElementById("loket").value;
        var jumlah = document.getElementById("jumlah").value;
        var select = document.getElementById("stats");
        var status = select.options[select.selectedIndex].value;

        document.getElementById("result").style.display = 'none';
        getAjax();

        ajaxRequest.open("GET", "model/add_cyplb.php?loket=" + idloket + "&jml=" + jumlah + "&stats=" + status);

        ajaxRequest.onreadystatechange = function() {

            document.getElementById("result").style.display = '';
            document.getElementById("result").innerHTML = ajaxRequest.responseText;
            if (ajaxRequest) {
                document.getElementById("form-msg").innerHTML = "";
            }
        }
        ajaxRequest.send(null);

    }

    function post_idloket() {
        document.getElementById("loket").value = document.getElementById("id_loket").value;
    }

    function autoChange() { //fungsi menangkap input search dan menampilkan hasil search
        document.getElementById("id_loket").value = "";
        getAjax();
        var select = document.getElementById("cust");
        var customer = select.options[select.selectedIndex].value;

        ajaxRequest.open("GET", "model/get_loket_cy.php?customer=" + customer);
        ajaxRequest.onreadystatechange = function() {

            if (ajaxRequest.responseText) {
                //document.getElementById("no_book").innerHTML  = ajaxRequest.responseText;
                //var pilih= new Array;


                var pilih = ajaxRequest.responseText;
                var explode = pilih.split('-');

                var myselect = document.getElementById("id_loket");
                baris = explode[0];

                //  hapus select option lama
                for (i = myselect.options.length - 1; i >= 0; i--) {
                    myselect.remove(i);
                }
                // insert Select option
                if (baris > 0)
                    for (var x = 1; x <= baris; x++) {
                        var myselect = document.getElementById("id_loket");
                        myselect.options[x] = new Option(explode[x], explode[x]);
                    }

                document.getElementById("loket").value = document.getElementById("id_loket").value;
            } else {
                document.getElementById("id_loket").value = "";
            }
        }
        ajaxRequest.send(null);
    }

    function status() {

        var select = document.getElementById("stats");
        var status = select.options[select.selectedIndex].value;

        if (status == 'OUT') {
            document.getElementById("tjumlah").style.display = 'none';
        } else {
            document.getElementById("tjumlah").style.display = '';
        }
    }
    </script>

    <style>
    a:hover {
        text-decoration: none;
        background-color: #F2B968;
    }

    .kecil {
        font-size: 9px
    }

    .sedang_kecil {
        font-size: 10px
    }

    .sedang {
        font-size: 11px
    }

    .sedang_besar {
        font-size: 12px
    }

    .style4 {
        font-size: 10px
    }

    .style5 {
        font-size: 11px
    }

    .style6 {
        font-size: 12px
    }

    .style7 {
        font-size: 12px
    }
    </style>
</head>

<body class="cl-default fixed">
    <!-- start:navbar top -->
    <header class="header">
        <!-- <a href="home.php" class="logo">Indra Jaya Swastika</a> -->
        <header class="header"><?php include("top_nav.php") ?></header>
    </header>
    <!-- end:navbar top -->

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas"><?php include("left_menu.php") ?></aside>

        <aside class="right-side">
            <section class="content">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="#"> Laporan</a></li>
                    <li><a href="#"> CY Form</a></li>
                </ol>

                <div>
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalTitle">Modal title</h4>
                                </div>
                                <div class="modal-body" id="myModalBody">
                                    ...
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading hidden-print"><span class="glyphicon glyphicon-tasks"
                                    aria-hidden="true"></span> CY</div>
                            <div class="box">
                                <div class="panel-body" id="pnl-filter">
                                    <form class="form-horizontal" role="form" method="POST"
                                        enctype="multipart/form-data">
                                        <input type="hidden" id="username" name="username"
                                            value="<?php echo $username ?>">
                                        <input type="hidden" id="logincat" name="logincat"
                                            value="<?php echo $logincat ?>">

                                        <div id="dcustomer" class="form-group">
                                            <label class="control-label col-xs-4">Pemilik Barang</label>
                                            <div class="col-md-4 col-xs-8">
                                                <select class="form-control input-sm" name="cust" id="cust"
                                                    onchange="autoChange();">
                                                    <option value="" selected>PILIH</option>

                                                    <option value="501000">CJ FEED JOMBANG</option>
                                                    <option value="501200">CJ. CHEILJEDANG FEED KALIMANTAN</option>";
                                                    <option value="501800">DSI INDONESIA</option>";

                                                </select>
                                            </div>

                                            <p></p>

                                        </div>

                                        <div class="form-group" id="tampil-loket">
                                            <label class="control-label col-xs-4">ID Loket</label>
                                            <div class="col-md-4 col-xs-8">
                                                <select name="id_loket" id="id_loket" class="form-control input-sm"
                                                    onChange="post_idloket();"></select>
                                                <input type="hidden" name="loket" id="loket" size="25" height="14">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Status</label>
                                            <div class="col-md-4 col-xs-8">
                                                <select class="form-control input-sm" style="width:100px;" name="stats"
                                                    id="stats" onchange="status();">
                                                    <option value="IN">IN</option>
                                                    <option value="OUT">OUT</option>";
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group" id='tjumlah'>
                                            <label class="control-label col-xs-4">Jumlah Data</label>
                                            <div class="col-md-4 col-xs-8">
                                                <input type="text" class="form-control" style='width:100px;'
                                                    id="jumlah">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-6 col-xs-offset-4">
                                                <button class="form-control btn-primary" id="submit" type="button"
                                                    onClick="showData();">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-9 col-xs-offset-3" id="form-msg"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-xs-9" id="judul">
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="row">
                                <div class="col-sm"></div>
                            </div>
                            <div class="table-responsive" id="result">
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </aside>
    </div>
    <script src="<?= ASSETS_PATH ?>/js/jquery.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/arjuna.js"></script>

</body>

</html>