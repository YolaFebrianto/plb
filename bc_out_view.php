<?php
if ($_POST['jenis_act']) {

    require_once('db-inc2.php');

    $jenis = $_POST['jenis_act'];
    $tgl1 = $_POST['tglan1'];
    $tgl2 = $_POST['tglan2'];
    $kode_rel = TRIM($_POST['cust']);
    if ($kode_rel == "ALL") {
        $kode_rel = '50%';
    }

    if ($jenis == 'BC27') {


        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN BC 2.7 <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<br>";
        echo "<br>";


        $sqltext = "select no_aju_out,tgl_aju_out,no_doc_out as doc_out, no_bc_16  as bc16 ,to_char( tgl_bc_16 ,'dd-mm-yyyy') as tgl_bc16, 
		kode_rel,tgl_doc_out as doc_tgl, jumlah_wh as jumlah4,
		cif_tpb as cif,
		sum_varchar (distinct currency_tpb ||'<br />') as currency,
		sum_varchar (distinct keterangan ||'<br />') as keterangan,
		sum_varchar (distinct( split_part(id_flowbrg_splitted,'-',1)) ||'<br />') as book,
		sum_varchar (distinct to_char( tgl_awal ,'dd-mm-yyyy') ||'<br />') as tgl_aktifitas,
		sum_varchar (distinct( cast(jumlah_tpb as int )) ||'<br />') as jumlahd,
		sum_varchar (distinct satuan_tpb ||'<br />') as kemasan,
		sum_varchar (distinct case when seri_barang_tpb < 10 then '0' else '' end || seri_barang_tpb ||'. ' || kode_barang_tpb ||'  |  '||uraian_barang_tpb ||'<br />' ) as seri_barang,
		sum_varchar (distinct case when kode_rel <> '501600' then cif_tpb else harga_penyerahan_tpb end ||'%<br />') as harga,
		sum_varchar (distinct no_bl ||'<br />') as no_bl,
		sum_varchar (distinct to_char( tgl_bl,'dd-mm-yyyy')||'<br />') as tgl_bl,
		sum_varchar (distinct to_char( tgl_dokumen_tpb,'dd-mm-yyyy')||'<br />') as tgl_dokumen,
		sum_varchar (distinct batch_no ||'<br />') as invoice,
		sum_varchar (distinct to_char( tgl_awal,'dd-mm-yyyy')||'<br />') as tgl_awal,
		sum_varchar (distinct to_char( jam_awal,'HH24:MI')||'<br />') as jam_awal,
		sum_varchar(distinct case when kendaraan = 'CONTAINER' THEN  no_unit ||' / ' || size_code else nopol end ||'<br />' )as kendaraan,
		sum_varchar( distinct(satuan) ||'<br />' ) as satuan,
		sum_varchar( distinct(hs_tpb) ||'<br />' ) as hs,
		sum_varchar( distinct(nama) ||'<br />' ) as nama
		
		from report.bc27_fix

		where kode_rel like '$kode_rel'
		and tgl_doc_out  between  '$tgl1' and '$tgl2'
		group by 1,2,3,4,5,6,7,8,9";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table  id='data' class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th colspan='4'>DOKUMEN PABEAN BC 2.7</th>";
        echo            "<th colspan='2'>DOKUMEN PABEAN BC 1.6</th>";
        echo            "<th rowspan='2'>CUSTOMER</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th colspan='2'>BL/AWB</th>";
        echo            "<th colspan='4'>INVOICE</th>";
        echo            "<th rowspan='2'>KODE HS</th>";
        echo            "<th rowspan='2'>SERI BARANG | KODE BARANG | NAMA BARANG</th>";
        //echo			"<th rowspan='2'>NAMA BARANG</th>";
        //echo			"<th rowspan='2'>KODE BARANG</th>";
        echo            "<th colspan='2'>URAIAN BARANG (TPB)</th>";
        echo            "<th colspan='5'>URAIAN BARANG (LPB)</th>";
        echo            "<th rowspan='2'>KETERANGAN</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TGL AJU</th>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>VALAS</th>";
        echo            "<th>NILAI</th>";
        echo            "<th>JUMLAH KEMASAN</th>";
        echo            "<th>SATUAN KEMASAN</th>";
        echo            "<th>ID BOOK</th>";
        echo            "<th>JUMLAH</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>LOKASI</th>";
        echo            "<th>TANGGAL KELUAR</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td align=center>" . $number . "</td>";
            echo            "<td>" . $row['doc_out'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['doc_tgl'])) . "</td>";
            echo            "<td>" . $row['no_aju_out'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_aju_out'])) . "</td>";
            echo            "<td>" . $row['bc16'] . "</td>";
            echo            "<td>" . $row['tgl_bc16'] . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td>" . $row['no_bl'] . "</td>";
            echo            "<td>" . $row['tgl_bl'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . $row['tgl_dokumen'] . "</td>";
            echo            "<td>" . $row['currency'] . "</td>";
            echo            "<td>" . number_format($row['harga'], 2) . "</td>";
            echo            "<td>" . $row['hs'] . "</td>";
            echo            "<td class='td-wide'>" . $row['seri_barang'] . "</td>";
            //echo			"<td>".$row['uraian_barang']."</td>";
            //echo			"<td>".$row['kode_barang']."</td>";
            echo            "<td>" . $row['jumlahd'] . "</td>";
            echo            "<td>" . $row['kemasan'] . "</td>";
            echo            "<td>" . $row['book'] . "</td>";
            echo            "<td>" . $row['jumlah4'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            if (($rel == '500200') || ($rel == '500100')) {
                echo            "<td>WH.03</td>";
            } else if ($rel == '501600') {
                echo            "<td>WH.04</td>";
            } else {
                echo            "<td>WH.LAP</td>";
            }
            echo            "<td>" . $row['tgl_aktifitas'] . "</td>";
            echo            "<td>" . $row['keterangan'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'BC28') {


        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN BC 2.8 <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<br>";
        echo "<br>";

        $sqltext = "select B.no_aju_out,C.nama,
							B.doc_out as doc_out, E.no_bc_16  as bc16, 
							B.no_doc_out as doc_outnum,pack.jumlah4,
							F.cif_rupiah as cif, F.cif as cif_o,
							--F.seri_barang,F.kode_barang,F.uraian_barang,
							G.bm,G.ppn,G.pph,F.currency,B.kode_rel,
							sum_varchar (distinct to_char( F.tanggal_dokumen,'dd-mm-yyyy')||'<br>') as tgl_dokumen,
							sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
							sum_varchar (distinct to_char( E.tgl_bc_16,'dd-mm-yyyy')||'<br>') as tgl_bc16,
							sum_varchar (distinct to_char( B.tgl_aju_out,'dd-mm-yyyy')||'<br>') as tgl_aju_out,
							sum_varchar (distinct to_char( B.tgl_doc_out,'dd-mm-yyyy')||'<br>') as doc_tgl,
							sum_varchar (distinct( split_part(B.id_flowbrg,'-',1)) ||'<br />') as book,
							sum_varchar (distinct to_char( A.tgl_awal ,'dd-mm-yyyy') ||'<br />') as tgl_aktifitas,
							sum_varchar (distinct( cast(F.jumlah as int )) ||'<br />') as jumlahd,
							sum_varchar (distinct E.no_coo ||'<br>') as no_coo,
							--sum_varchar (distinct F.uraian_barang ||'<br>') as uraian_barang,
							sum_varchar (distinct F.kemasan ||'<br>') as kemasan,
							sum_varchar (distinct F.hs ||'<br>') as hs,
							sum_varchar (distinct case when F.seri_barang < 10 then '0' else '' end || F.seri_barang ||'. ' || F.kode_barang ||'  |  '||F.uraian_barang ||'<br />' ) as seri_barang,
							--sum_varchar (distinct F.kode_barang ||'<br>') as kode_barang,
							sum_varchar (distinct to_char( E.tgl_coo,'dd-mm-yyyy')||'<br>') as tgl_coo,
							sum_varchar (distinct E.no_bl ||'<br>') as no_bl,
							sum_varchar (distinct to_char( E.tgl_bl,'dd-mm-yyyy')||'<br>') as tgl_bl,
							sum_varchar (distinct B.batch_no ||'<br>') as invoice,
							sum_varchar (distinct to_char( A.tgl_awal,'dd-mm-yyyy')||'<br>') as tgl_awal,
							sum_varchar (distinct to_char( A.jam_awal,'HH24:MI')||'<br>') as jam_awal,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode else A.nopol end ||'<br>' )as kendaraan,
							sum_varchar( distinct(B.satuan) ||'<br>' ) as satuan,
							sum_varchar(distinct( split_part(itemname,':',1))||'<br>') as cbarang,
							sum_varchar(distinct( split_part(itemname,':',2))||'<br />') as namabarang

							from report.plb_documents_out F 
							left join report.plb_flowbrg B on B.no_aju_out = SUBSTRING(F.no_aju,15) 
							and B.no_doc_out = F.nomor_daftar and F.uraian_dokumen = 'INVOICE' and trim(B.batch_no) = trim(F.nomor_dokumen)
							left join report.plb_flowcont A on A.id_flowcont = B.id_flowbrg_splitted 
							left join v_customer C on A.kode_rel = C.kode_rel
							join wh_cargo D on B.itemcode = D.itemcode
							join report.plb_flowbrg E on B.id_flowbrg_in = E.id_flowbrg
							
						        join (select B.no_aju_out, sum(B.jumlah) as jumlah4
                                from report.plb_flowbrg B
								where B.kode_rel like '$kode_rel'
								and B.doc_out = 'BC 2.8'
                                group by 1) as pack
                                on substring(F.no_aju,15) = pack.no_aju_out						
							    left join report.plb_barang_tarif G on F.no_aju = G.nomor_aju
								where F.tanggal_daftar between  '$tgl1' and '$tgl2' and B.doc_out = 'BC 2.8'
								and a.id_aktifitas IN(2,16,24)";

        if ($kode_rel == "50%") {
            $sqltext .=    "and B.kode_rel like '50%'";
            /*
		                        $sqltext .=	"and B.kode_rel in ('500300','501400','501500','500100')";
								*/
        } else {
            $sqltext .=    "and B.kode_rel ='$kode_rel'";
        }
        $sqltext .= "and B.doc_out = 'BC 2.8'
											 group by 1,2,3,4,5,6,7,8,9,10,11,12,13
							                 order by doc_outnum";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table id='data' class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th colspan='4'>DOKUMEN PABEAN BC 2.8</th>";
        echo            "<th colspan='2'>DOKUMEN PABEAN BC 1.6</th>";
        echo            "<th rowspan='2'>CUSTOMER</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th colspan='2'>BL/AWB</th>";
        echo            "<th colspan='4'>INVOICE</th>";
        echo            "<th colspan='3'>PEMBAYARAN</th>";
        echo            "<th rowspan='2'>KODE HS</th>";
        echo            "<th rowspan='2'>SERI BARANG | KODE BARANG | NAMA BARANG</th>";
        //echo			"<th rowspan='2'>KODE BARANG</th>";
        //echo			"<th rowspan='2'>NAMA BARANG</th>";
        echo            "<th colspan='2'>URAIAN BARANG (TPB)</th>";
        echo            "<th colspan='5'>URAIAN BARANG (LPB)</th>";
        echo            "<th rowspan='2'>KETERANGAN</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TGL AJU</th>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>CURRENCY</th>";
        echo            "<th>NILAI</th>";
        echo            "<th>BM</th>";
        echo            "<th>PPN</th>";
        echo            "<th>PPH</th>";
        echo            "<th>JUMLAH KEMASAN</th>";
        echo            "<th>SATUAN KEMASAN</th>";
        echo            "<th>ID BOOK</th>";
        echo            "<th>JUMLAH</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>LOKASI</th>";
        echo            "<th>TANGGAL KELUAR</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td align=center>" . $number . "</td>";
            echo            "<td>" . $row['doc_outnum'] . "</td>";
            echo            "<td>" . $row['doc_tgl'] . "</td>";
            echo            "<td>" . $row['no_aju_out'] . "</td>";
            echo            "<td>" . $row['tgl_aju_out'] . "</td>";
            echo            "<td>" . $row['bc16'] . "</td>";
            echo            "<td>" . $row['tgl_bc16'] . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td>" . $row['no_bl'] . "</td>";
            echo            "<td>" . $row['tgl_bl'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . $row['tgl_dokumen'] . "</td>";
            echo            "<td>" . $row['currency'] . "</td>";
            echo            "<td align=right>" . number_format($row['cif_o'], 2) . "</td>";
            echo            "<td align=right>" . number_format($row['bm'], 2) . "</td>";
            echo            "<td align=right>" . number_format($row['ppn'], 2) . "</td>";
            echo            "<td align=right>" . number_format($row['pph'], 2) . "</td>";
            echo            "<td>" . $row['hs'] . "</td>";
            echo            "<td class='td-wide'>" . $row['seri_barang'] . "</td>";
            //echo			"<td>".$row['kode_barang']."</td>";
            //echo			"<td>".$row['uraian_barang']."</td>";
            echo            "<td align=right>" . $row['jumlahd'] . "</td>";
            echo            "<td>" . $row['kemasan'] . "</td>";
            echo            "<td>" . $row['book'] . "</td>";
            echo            "<td align=right>" . $row['jumlah4'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            if (($rel == '500200') || ($rel == '500100') || ($rel == '501500')) {
                echo            "<td>WH.03</td>";
            } else if ($rel == '501600') {
                echo            "<td>WH.04</td>";
            } else {
                echo            "<td>WH.LAP</td>";
            }
            echo            "<td>" . $row['tgl_aktifitas'] . "</td>";
            echo            "<td>" . $row['keterangan'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'BC30') {
        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN BC 3.0 <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<br>";
        echo "<br>";


        $sqltext = "select A.no_aju,to_char(A.tgl_aju,'dd-mm-yyyy') as tgl_aju, A.no_daftar,to_char(A.tgl_daftar,'dd-mm-yyyy') as tgl_daftar, A.no_npe, cast(A.jumlah as int), A.satuan,
       to_char(A.tgl_npe,'dd-mm-yyyy') as tgl_npe, B.nama, A.negara,  A.cif, A.tarif_hs,F.satuan as satuan2,A.tgl_daftar,
       pack.jumlah4,

       sum_varchar (distinct( to_char(PL.tgl_doc,'dd-mm-yyyy')) ||'<br />') as tgl_pl,
       sum_varchar (distinct( to_char(INV.tgl_doc,'dd-mm-yyyy')) ||'<br />') as tgl_inv,
       sum_varchar(distinct case when C.kendaraan = 'CONTAINER' THEN  C.no_unit ||' / ' ||C.sizecode else C.nopol end ||'<br />' )as kendaraan,
       sum_varchar (distinct( split_part(F.id_flowbrg,'-',1)) ||'<br />') as no_book,
       sum_varchar(distinct( split_part(G.itemname,':',1))||'<br />') as cbarangtpb,
       sum_varchar(distinct( split_part(G.itemname,':',2))||'<br />') as namabarangtpb,
	   sum_varchar(distinct( to_char(C.tgl_awal,'dd-mm-yyyy'))||'<br />') as tgl_awal,
	   sum_varchar(distinct( PL.no_doc)||'<br />') as no_pl,
	   sum_varchar(distinct( INV.no_doc)||'<br />') as no_inv,
	   sum_varchar(distinct( C.destination)||'<br />') as destination

       from report.plb_doc_in27 A
       join report.plb_doc_in27_brg E on A.no_aju = E.no_aju
       join wh_flowbrg F on E.id_flowbrg = F.id_flowbrg
       
       join (select E.no_aju, sum(F.jumlah0) as jumlah4
                  from report.plb_doc_in27_brg E, wh_flowbrg F, report.plb_doc_in27 A
                  where E.id_flowbrg = F.id_flowbrg
                  and A.no_aju = E.no_aju
                  group by 1) as pack
          on A.no_aju = pack.no_aju
          
       join report.plb_doc_in27_dtl pl on A.no_aju = pl.no_aju         
       join report.plb_flowcont C on F.id_flowbrg_splitted = C.id_flowcont
       join v_customer B on F.kode_rel = B.kode_rel
       join wh_cargo G on F.itemcode = G.itemcode
       join report.plb_doc_in27_dtl inv on A.no_aju = inv.no_aju

       where B.kode_rel LIKE '501600'  and A.tgl_daftar between '$tgl1' and '$tgl2'
       and PL.jenis_doc = 'PACKING LIST'
       and INV.jenis_doc = 'INVOICE'
       and C.id_aktifitas IN(2,16,24) 
       and A.nsurat = 'BC 3.0'
       group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15
       order by A.tgl_daftar ASC";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table id='data' class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th colspan='6'>DOKUMEN PABEAN BC 3.0</th>";
        echo            "<th rowspan='2'>PENGIRIM</th>";
        echo            "<th rowspan='2'>PENERIMA</th>";
        echo            "<th rowspan='2'>NEGARA</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th colspan='3'>INVOICE</th>";
        echo            "<th colspan='2'>PACKING LIST</th>";
        echo            "<th colspan='4'>URAIAN BARANG (TPB)</th>";
        echo            "<th colspan='6'>URAIAN BARANG (LPB)</th>";
        echo            "<th rowspan='2'>KETERANGAN</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TGL AJU</th>";
        echo            "<th>NO NPE</th>";
        echo            "<th>TGL NPE</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>NILAI</th>";
        echo            "<th>NO</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>NAMA</th>";
        echo            "<th>JUMLAH KEMASAN</th>";
        echo            "<th>SATUAN KEMASAN</th>";
        echo            "<th>HS</th>";
        echo            "<th>ID BOOKING</th>";
        echo            "<th>NAMA BARANG</th>";
        echo            "<th>JML</th>";
        echo            "<th>SAT</th>";
        echo            "<th>LOKASI</th>";
        echo            "<th>TANGGAL KELUAR</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td align=center>" . $number . "</td>";
            echo            "<td>" . $row['no_daftar'] . "</td>";
            echo            "<td>" . $row['tgl_daftar'] . "</td>";
            echo            "<td>" . $row['no_aju'] . "</td>";
            echo            "<td>" . $row['tgl_aju'] . "</td>";
            echo            "<td>" . $row['no_npe'] . "</td>";
            echo            "<td>" . $row['tgl_npe'] . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['destination'] . "</td>";
            echo            "<td>" . $row['negara'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td>" . $row['no_inv'] . "</td>";
            echo            "<td>" . $row['tgl_inv'] . "</td>";
            echo            "<td>" . number_format($row['cif'], 2) . "</td>";
            echo            "<td>" . $row['no_pl'] . "</td>";
            echo            "<td>" . $row['tgl_pl'] . "</td>";
            echo            "<td>" . $row['namabarangtpb'] . "</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            echo            "<td>" . $row['tarif_hs'] . "</td>";
            echo            "<td>" . $row['no_book'] . "</td>";
            echo            "<td>" . $row['namabarangtpb'] . "</td>";
            echo            "<td>" . $row['jumlah4'] . "</td>";
            //echo			"<td>".$row['satuan2']."</td>";
            echo            "<td>BAG</td>";
            echo            "<td>WH.04</td>";
            echo            "<td>" . $row['tgl_awal'] . "</td>";
            echo            "<td>" . $row['keterangan'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'BC41') {


        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN BC 4.1 <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<br>";
        echo "<br>";


        $sqltext = "select B.no_aju_out,C.nama,
							B.doc_out as doc_out, B.tgl_doc_out,
							B.no_doc_out as doc_outnum,B.satuan as satuand,
							F.cif as cif,cast(F.jumlah as integer) as jumlahd,F.satuan,
							F.tanggal_dokumen as tgl_dokumen,F.hs,A.tgl_awal as tgl_aktifitas,
							F.currency,pack.jumlah4,
							sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
							sum_varchar (distinct case when F.seri_barang < 10 then '0' else '' end || F.seri_barang ||'. ' || F.kode_barang ||'  |  '||F.uraian_barang ||'<br />' ) as seri_barang,
							--sum_varchar (distinct F.kode_barang ||'<br />') as kode_barang,
							--sum_varchar (distinct F.uraian_barang ||'<br />') as uraian_barang,
							sum_varchar (distinct( split_part(B.id_flowbrg,'-',1)) ||'<br />') as book,
							sum_varchar (distinct F.nomor_dokumen ||'<br />') as invoice,
							sum_varchar (distinct to_char( A.tgl_awal,'dd-mm-yyyy')||'<br />') as tgl_awal,
							sum_varchar (distinct to_char( B.tgl_aju_out,'dd-mm-yyyy')||'<br />') as tgl_aju_out,
							sum_varchar (distinct to_char( B.tgl_doc_out,'dd-mm-yyyy')||'<br />') as doc_tgl,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode else A.nopol end ||'<br />' )as kendaraan

							from report.plb_documents_outbc41 F 
							left join report.plb_flowbrg B  on B.no_doc_out = F.nomor_daftar --and F.uraian_dokumen in ('PACKING LIST','LAINNYA')
							and B.no_aju_out = SUBSTRING(F.no_aju,15) and   
							F.nomor_dokumen  = CASE WHEN F.uraian_dokumen = 'LAINNYA'  then B.id_flowbrg_splitted   else  B.exim end
							left join report.plb_flowcont A on A.id_flowcont = B.id_flowbrg_splitted   
							join v_customer C on A.kode_rel = C.kode_rel
							join wh_cargo D on B.itemcode = D.itemcode
							left join report.plb_flowbrg E on B.id_flowbrg_in = E.id_flowbrg
							
							join (select B.no_aju_out, sum(B.jumlah) as jumlah4
                                                        from report.plb_flowbrg B
                                                        where B.kode_rel like '$kode_rel'
                                                        and B.doc_out = 'BC 4.1'
                                                        --and A.no_aju = E.no_aju
                                                        group by 1) as pack
                                                        on substring(F.no_aju,15) = pack.no_aju_out
							 
							
							left join report.plb_documents_in40 sup on E.no_bc_16 = sup.nomor_daftar 
							and sup.uraian_dokumen in ('PACKING LIST', 'LAINNYA', 'SURAT JALAN')
							and B.exim = sup.nomor_dokumen and E.no_aju = SUBSTRING(sup.nomor_aju,15)
							
							where B.kode_rel LIKE '$kode_rel'  and F.tgl_aju between  '$tgl1' and '$tgl2' and B.doc_out = 'BC 4.1'
							and a.id_aktifitas IN(2,16,24) 
							group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14
							order by B.tgl_doc_out ASC";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table id='data' class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th colspan='4'>DOKUMEN PABEAN BC 4.1</th>";
        echo            "<th rowspan='2'>CUSTOMER</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th colspan='2'>INVOICE</th>";
        echo            "<th rowspan='2'>SERI BARANG | KODE BARANG | NAMA BARANG</th>";
        //echo			"<th rowspan='2'>NAMA BARANG</th>";
        //echo			"<th rowspan='2'>KODE BARANG</th>";
        echo            "<th colspan='2'>URAIAN BARANG (TPB)</th>";
        echo            "<th colspan='5'>URAIAN BARANG (LPB)</th>";
        echo            "<th rowspan='2'>KETERANGAN</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TGL AJU</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>JUMLAH KEMASAN</th>";
        echo            "<th>SATUAN KEMASAN</th>";
        echo            "<th>ID BOOK</th>";
        echo            "<th>JUMLAH</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>LOKASI</th>";
        echo            "<th>TANGGAL KELUAR</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td align=center>" . $number . "</td>";
            echo            "<td>" . $row['doc_outnum'] . "</td>";
            echo            "<td>" . $row['doc_tgl'] . "</td>";
            echo            "<td>" . $row['no_aju_out'] . "</td>";
            echo            "<td>" . $row['tgl_aju_out'] . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_dokumen'])) . "</td>";
            echo            "<td class='td-wide'>" . $row['seri_barang'] . "</td>";
            //echo			"<td>".$row['uraian_barang']."</td>";
            //echo			"<td>".$row['kode_barang']."</td>";
            echo            "<td>" . $row['jumlahd'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            echo            "<td>" . $row['book'] . "</td>";
            echo            "<td>" . $row['jumlah4'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            echo            "<td>WH.04</td>";
            echo            "<td>" . $row['tgl_awal'] . "</td>";
            echo            "<td>" . $row['keterangan'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'CY-OUT') {


        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN CY OUT <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<br>";
        echo "<br>";


        $sqltext = "select B.no_aju_out,B.tgl_aju_out,
							B.doc_out as doc_out, E.no_bc_16  as bc16 ,E.tgl_bc_16 as tgl_bc16 , 
							B.no_doc_out as nomor_daftar, F.seri_barang,
							B.tgl_doc_out as tanggal_daftar,C.nama,
							B.itemcode as cbarang,F.cif as harga,
							E.tgl_invoice,pack.jumlah4,
							F.currency, B.bm, B.ppn, B.pph,
							sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
							sum_varchar (distinct E.no_bl ||'<br />') as no_bl,
							sum_varchar (distinct to_char( E.tgl_bl,'dd/mm/yyyy')||'<br />') as tgl_bl,
							sum_varchar (distinct B.batch_no ||'<br />') as invoice,
							sum_varchar (distinct to_char( A.tgl_in,'dd/mm/yyyy')||'<br />') as tgl_awal,
							sum_varchar (distinct split_part(B.id_flowbrg,'-',1) ||'<br />') as book,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode end ||'<br />' )as kendaraan,
							sum_varchar( distinct(B.satuan) ||'<br />' ) as satuan
							from wh_loket A 
							join report.plb_cy B on A.id_loket = B.id_loket  
							join v_customer C on A.kode_rel = C.kode_rel 
							join report.plb_cy E on B.id_flowbrg_in = E.id_flowbrg
							join report.plb_documents_out F on B.no_doc_out = F.nomor_daftar 
							and uraian_dokumen = 'INVOICE'
							left join (select Z.no_aju_out, sum(Z.jumlah) as jumlah4
			                                from report.plb_cy Z
			                                Where Z.id_flowbrg_in is not null
			                                and Z.kode_rel like '$kode_rel'
			                                group by 1) as pack
			                                on substring(F.no_aju,15) = pack.no_aju_out
							where b.kode_rel LIKE '$kode_rel'  and A.tgl_in between  '$tgl1' and '$tgl2' and B.doc_out = 'BC 2.8'
							group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17
							order by no_aju_out";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table id='data' class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th rowspan='2'>JENIS DOKUMEN</th>";
        echo            "<th colspan='4'>DOKUMEN PABEAN</th>";
        echo            "<th rowspan='2'>CUSTOMER</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th rowspan='2'>SERI BARANG | KODE BARANG | NAMA BARANG</th>";
        //echo			"<th rowspan='2'>JUMLAH KEMASAN</th>";
        //echo			"<th rowspan='2'>NILAI KEMASAN</th>";
        echo            "<th colspan='2'>URAIAN BARANG (TPB)</th>";
        echo            "<th colspan='5'>URAIAN BARANG (LPB)</th>";
        echo            "<th colspan='2'>BL/AWB</th>";
        echo            "<th colspan='4'>INVOICE</th>";
        echo            "<th rowspan='2'>TARIF</th>";
        echo            "<th rowspan='2'>KODE HS</th>";
        echo            "<th rowspan='2'>KETERANGAN</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TGL AJU</th>";
        echo            "<th>JUMLAH KEMASAN</th>";
        echo            "<th>SATUAN KEMASAN</th>";
        echo            "<th>ID BOOK</th>";
        echo            "<th>JUMLAH</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>LOKASI</th>";
        echo            "<th>TANGGAL MASUK</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>CURRENCY</th>";
        echo            "<th>NILAI</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td align=center>" . $number . "</td>";
            echo            "<td>" . $jenis . "</td>";
            echo            "<td>" . $row['nomor_daftar'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tanggal_daftar'])) . "</td>";
            echo            "<td>" . $row['no_aju_out'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_aju_out'])) . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td class='td-wide'>" . $row['seri_barang'] . "</td>";
            //echo			"<td>".$row['jumlah_kemasan']."</td>";
            //echo			"<td>".$row['cif_kemasan']."</td>";
            echo            "<td>" . $row['jumlah4'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            echo            "<td>" . $row['book'] . "</td>";
            echo            "<td>" . $row['jumlah4'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            if (($rel == '500200') || ($rel == '501500') || ($rel == '500300')) {
                echo            "<td>WH.03</td>";
            } else if (($rel == '501600') || ($rel == '500100')) {
                echo            "<td>WH.04</td>";
            } else {
                echo            "<td>WH.LAP</td>";
            }
            echo            "<td>" . $row['tgl_awal'] . "</td>";
            echo            "<td>" . $row['no_bl'] . "</td>";
            //		echo			"<td>".date('d-m-Y', strtotime($row['tgl_bl']))."</td>";
            echo            "<td>" . $row['tgl_bl'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . $row['tgl_invoice'] . "</td>";
            echo            "<td>" . $row['currency'] . "</td>";
            echo            "<td>" . number_format($row['harga'], 2) . "</td>";
            echo            "<td>" . $row['tarif'] . "</td>";
            echo            "<td>" . $row['hs'] . "</td>";
            echo            "<td>" . $row['keterangan'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'P3BET') {


        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN PENGELUARAN BARANG PER DOKUMEN PABEAN BC P3BET <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<br>";
        echo "<br>";


        $sqltext = "select B.no_aju_out,C.nama,
							B.doc_out as doc_out, B.tgl_doc_out,
							B.no_doc_out as doc_outnum,B.satuan as satuand,
							F.cif as cif,
							--cast(F.jumlah as integer) as jumlahd,F.satuan,
							F.tanggal_dokumen as tgl_dokumen,F.nomor_daftar,F.tanggal_daftar,
							F.currency,pack.jumlah4,F.penerima,F.kode_negara,
							sum_varchar (distinct cast(F.jumlah as integer) ||'<br />') as jumlahd,
							sum_varchar (distinct F.satuan ||'<br />') as satuan,
							sum_varchar (distinct B.keterangan ||'<br />') as keterangan,
							sum_varchar (distinct case when F.seri_barang < 10 then '0' else '' end || F.seri_barang ||'. ' || F.kode_barang ||'  |  '||F.uraian_barang ||'<br />' ) as seri_barang,
							sum_varchar (distinct( split_part(B.id_flowbrg,'-',1)) ||'<br />') as book,
							sum_varchar (distinct F.nomor_dokumen ||'<br />') as invoice,
							sum_varchar (distinct F.harga_penyerahan ||'<br />') as nilai_inv,
							sum_varchar (distinct to_char( A.tgl_akhir,'dd-mm-yyyy')||'<br />') as tgl_akhir,
							sum_varchar (distinct to_char( B.tgl_aju_out,'dd-mm-yyyy')||'<br />') as tgl_aju_out,
							sum_varchar (distinct to_char( B.tgl_doc_out,'dd-mm-yyyy')||'<br />') as doc_tgl,
							sum_varchar(distinct case when A.kendaraan = 'CONTAINER' THEN  A.no_unit ||' / ' ||A.sizecode else A.nopol end ||'<br />' )as kendaraan

							from report.plb_documents_p3bet F 
							left join report.plb_flowbrg B  on B.no_doc_out = F.nomor_daftar
							and B.no_aju_out = SUBSTRING(F.no_aju,15)
							left join report.plb_flowcont A on A.id_flowcont = B.id_flowbrg_splitted   
							left join v_customer C on A.kode_rel = C.kode_rel
							left join wh_cargo D on B.itemcode = D.itemcode
							left join report.plb_flowbrg E on B.id_flowbrg_in = E.id_flowbrg
							
							left join (select B.no_aju_out, sum(B.jumlah) as jumlah4
                                                        from report.plb_flowbrg B
                                                        where B.kode_rel like '$kode_rel'
                                                        and B.doc_out = 'P3BET'
                                                        group by 1) as pack
                                                        on substring(F.no_aju,15) = pack.no_aju_out
							 
							
							--left join report.plb_documents_in40 sup on E.no_bc_16 = sup.nomor_daftar 
							--and sup.uraian_dokumen in ('PACKING LIST', 'LAINNYA', 'SURAT JALAN')
							--and B.exim = sup.nomor_dokumen and E.no_aju = SUBSTRING(sup.nomor_aju,15)
							
							where B.kode_rel LIKE '$kode_rel'  and F.tanggal_daftar between  '$tgl1' and '$tgl2'
							and B.doc_out = 'P3BET' 
							and F.uraian_dokumen = 'PACKING LIST'
							and a.id_aktifitas IN(1,2,16,24) 
							group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14
							order by B.tgl_doc_out ASC";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table id='data' class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th colspan='4'>DOKUMEN PABEAN P3BET</th>";
        echo            "<th colspan='2'>DOKUMEN PABEAN BC 3.3</th>";
        echo            "<th rowspan='2'>CUSTOMER</th>";
        echo            "<th rowspan='2'>PENERIMA</th>";
        echo            "<th rowspan='2'>NEGARA</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th colspan='4'>INVOICE</th>";
        echo            "<th rowspan='2'>SERI BARANG | KODE BARANG | NAMA BARANG</th>";
        //echo			"<th rowspan='2'>NAMA BARANG</th>";
        //echo			"<th rowspan='2'>KODE BARANG</th>";
        echo            "<th colspan='2'>URAIAN BARANG (TPB)</th>";
        echo            "<th colspan='5'>URAIAN BARANG (LPB)</th>";
        echo            "<th rowspan='2'>KETERANGAN</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR AJU</th>";
        echo            "<th>TGL AJU</th>";
        echo            "<th>NOPEN</th>";
        echo            "<th>TGL NOPEN</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>CURRENCY</th>";
        echo            "<th>NILAI</th>";
        echo            "<th>JUMLAH KEMASAN</th>";
        echo            "<th>SATUAN KEMASAN</th>";
        echo            "<th>ID BOOK</th>";
        echo            "<th>JUMLAH</th>";
        echo            "<th>SATUAN</th>";
        echo            "<th>LOKASI</th>";
        echo            "<th>TANGGAL KELUAR</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td align=center>" . $number . "</td>";
            echo            "<td>" . $row['doc_outnum'] . "</td>";
            echo            "<td>" . $row['doc_tgl'] . "</td>";
            echo            "<td>" . $row['no_aju_out'] . "</td>";
            echo            "<td>" . $row['tgl_aju_out'] . "</td>";
            echo            "<td>" . $row['nomor_daftar'] . "</td>";
            echo            "<td>" . $row['tanggal_daftar'] . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['penerima'] . "</td>";
            echo            "<td>" . $row['kode_negara'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_dokumen'])) . "</td>";
            echo            "<td>USD</td>";
            echo            "<td>" . $row['nilai_inv'] . "</td>";
            echo            "<td class='td-wide'>" . $row['seri_barang'] . "</td>";
            //echo			"<td>".$row['uraian_barang']."</td>";
            //echo			"<td>".$row['kode_barang']."</td>";
            echo            "<td>" . $row['jumlahd'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            echo            "<td>" . $row['book'] . "</td>";
            echo            "<td>" . $row['jumlah4'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            echo            "<td>WH.04</td>";
            echo            "<td>" . $row['tgl_akhir'] . "</td>";
            echo            "<td>" . $row['keterangan'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    }
}