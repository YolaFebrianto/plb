<?php
session_start();

include 'Route.php';

use IJSRoute\Route;

define('BASEPATH', '/plbc/');
define('TITLE_APP', 'IJS - PLB');
define('ASSETS_PATH', '../assets');
define('CSS_PATH', '../css');
define('JS_PATH', '../js');
define('IMAGES_PATH', '../images');

$dataRoute = array(
    ['/', ['get', 'post'], function () {
        if (!isset($_SESSION['username']))
            header('Location: ' . BASEPATH . 'login');
        else header('Location: ' . BASEPATH . 'home');
    }],
    ['/login_proses', ['get', 'post'], function () {
        include 'login_proses.php';
    }],
    ['/home', ['get'], function () {
        include 'home.php';
    }],
    ['/transaction/bc27', ['get', 'post'], function () {
        include 'input_bc27.php';
    }],
    ['/transaction/bc33', ['get', 'post'], function () {
        include 'input_bc33.php';
    }],
    ['/transaction/cek-komoditi', ['get', 'post'], function () {
        include 'cek_komoditi.php';
    }],
    ['/tools/compare', ['get', 'post'], function () {
        include 'compare.php';
    }],
    ['/tools/plb-tools', ['get', 'post'], function () {
        include 'plb_tools.php';
    }],
    ['/tools/get-data-plb', ['get'], function () {
        include 'model/get_data_plb.php';
    }],
    ['/tools/verifikasi', ['get', 'post'], function () {
        include 'verifikasi.php';
    }],
    ['/kunci/data', ['get', 'post'], function () {
        include 'model/kunci_data.php';
    }],
    ['/tools/report-by-month', ['get', 'post'], function () {
        include 'lap_bc_grup.php';
    }],
    ['/tools/model/get-data', ['get'], function () {
        include 'model/get_data.php';
    }],
    ['/tools/report-by-day', ['get', 'post'], function () {
        include 'lap_bc_grup_har.php';
    }],
    ['/report/bc-in', ['get', 'post'], function () {
        include 'bc_in.php';
    }],
    ['/report/bc-out', ['get', 'post'], function () {
        include 'bc_out.php';
    }],
    ['/report/mutasi', ['get', 'post'], function () {
        include 'mutasibc.php';
    }],
    ['/report/stock', ['get', 'post'], function () {
        include 'stokbc.php';
    }],
    ['/report/detil-lokasi', ['get', 'post'], function () {
        include 'dtl_lokasi.php';
    }],

    ['/document/dokumen', ['get', 'post'], function () {
        include 'dokumen.php';
    }],
    ['/document/dokumen-plb', ['get', 'post'], function () {
        include 'plb_document.php';
    }],
    ['/document/get_data_document', ['get', 'post'], function () {
        include 'model/get_data_document.php';
    }],
    ['/document/get_save_document', ['get', 'post'], function () {
        include 'model/get_save_document.php';
    }],
    ['/payment/pembayaran', ['get', 'post'], function () {
        include 'pembayaran.php';
    }],
    ['/cy/cy', ['get', 'post'], function () {
        include 'cy_h.php';
    }],
    ['/cy/add_cyplb', ['get'], function () {
        include 'model/add_cyplb.php';
    }],
    ['/cy/get-loket-cy', ['get'], function () {
        include 'model/get_loket_cy.php';
    }],

    ['/user/user_session', ['get', 'post'], function () {
        include 'plb_user_session.php';
    }],
    ['/user/user-manual', ['get', 'post'], function () {
        include 'userm.php';
    }],


    ['/tools/cetak-compare', ['get', 'post'], function () {
        include 'model/cetak_compare.php';
    }],
    ['/report/cetak-mutasi', ['get', 'post'], function () {
        include 'model/mutasi_print.php';
    }],
    ['/report/cetak-bc-in', ['get', 'post'], function () {
        include 'model/cetak_pemasukan.php';
    }],
    ['/report/cetak-bc-out', ['get', 'post'], function () {
        include 'model/cetak_pengeluaran.php';
    }],
    ['/tools/cetak-report-by-month', ['get', 'post'], function () {
        include 'model/cetak_lap_grup.php';
    }],
    ['/tools/cetak-report-by-day', ['get', 'post'], function () {
        include 'model/cetak_lap_grup_har.php';
    }],
    ['/report/cetak-stock', ['get', 'post'], function () {
        include 'model/cetak_stock.php';
    }],
    ['/report/cetak-detail-lokasi', ['get', 'post'], function () {
        include 'model/cetak_dtllokasi.php';
    }],
    ['/dokumen/cetak-dokumen', ['get', 'post'], function () {
        include 'model/cetak_dokumen.php';
    }],
    ['/payment/cetak-pembayaran', ['get', 'post'], function () {
        include 'model/cetak_billing.php';
    }],
    ['/user/cetak-user-session', ['get', 'post'], function () {
        include 'model/cetak_user_session.php';
    }],


    ['/login', ['get', 'post'], function () {
        include 'login.php';
    }],
    ['/logout', ['get', 'post'], function () {
        include 'logout.php';
    }]
);

Route::addAll($dataRoute);
Route::pathNotFound(function ($path) {
    header('HTTP/1.0 404 Not Found');
});
Route::methodNotAllowed(function ($path, $method) {
    header('HTTP/1.0 405 Method Not Allowed');
});

Route::run(BASEPATH);