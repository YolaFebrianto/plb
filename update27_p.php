<?php 
include 'db-inc2.php';
$aksi = $_POST['aksi'];

//////////////////////////////////////////////////// POST ECCO ///////////////////////////////////
if ($aksi == 'ecco'){

$query1 ="SELECT distinct a.no_aju_out,a.no_doc_out,a.batch_no,b.seri_barang
              FROM report.plb_flowbrg a
              join report.plb_documents_outbc27 b on a.no_aju_out = substring(b.no_aju,15)
              and a.no_doc_out = b.nomor_daftar and a.batch_no = b.nomor_dokumen and b.uraian_dokumen = 'INVOICE'
              where a.kode_rel in ('500200','501500','500100')
              and a.id_flowbrg_in is not null
              and a.tgl_doc_out between current_date - 3 and current_date
              order by 1,4";
	
	$result1 = pg_query($db2_, $query1);
	if ($baris = pg_num_rows($result1) == 0){
			echo "Belum ada data";
		}else{		
		while($data = pg_fetch_array($result1)){
			$no_aju = $data['no_aju_out'];
			$nopen = $data['no_doc_out'];
			$batch_no = $data['batch_no'];
			$seri = $data['seri_barang'];
			
			/**
			echo $no_aju."<br>";
			echo $nopen."<br>";
			echo $batch_no."<br>";
			echo $seri."<br>";
			**/
			
		$query2 ="select distinct *
                  from report.bc27_fix
                  where no_aju_out = '$no_aju'
				  and no_doc_out = '$nopen'
				  and batch_no = '$batch_no'
				  and seri_barang_tpb = '$seri'
                  order by no_aju_out";
		
		$result2 = pg_query($db2_, $query2);
		
		if ($row = pg_num_rows($result2) == 0){
			
			$query3 ="insert into report.bc27_fix(
			no_aju_out,no_doc_out,tgl_aju_out,tgl_doc_out,id_flowbrg_splitted,batch_no,keterangan,kode_rel,
            satuan,hs_tpb,uraian_barang_tpb,satuan_tpb,kode_barang_tpb,currency_tpb,tgl_awal,no_unit,
            size_code,nopol,nama,no_bc_16,tgl_bc_16,no_bl,tgl_bl,jumlah_wh,jam_awal,cif_tpb,seri_barang_tpb,
            harga_penyerahan_tpb,jumlah_tpb,kendaraan,tgl_dokumen_tpb)
			
                  select distinct a.no_aju_out,a.no_doc_out,a.tgl_aju_out,a.tgl_doc_out,a.id_flowbrg_splitted,
                  a.batch_no,a.keterangan,a.kode_rel,a.satuan,
                  b.hs as hs_tpb,b.uraian_barang as uraian_barang_tpb,
                  b.satuan as satuan_tpb,b.kode_barang as kode_barang_tpb,b.currency as currency_tpb,
                  c.tgl_awal,c.no_unit,c.sizecode as size_code,c.nopol,
                  d.nama,
                  e.no_bc_16,e.tgl_bc_16,e.no_bl,e.tgl_bl,
                  pack.jumlah_wh,
                  c.jam_awal,
                  case when b.cif is null then '0.00' else b.cif end as cif_tpb,
                  case when b.seri_barang is null then 1 else b.seri_barang end as seri_barang_tpb,
                  case when b.harga_penyerahan is null then '0.00' else b.harga_penyerahan end as harga_penyerahan_tpb,
                  case when b.jumlah is null then 0 else b.jumlah end as jumlah_tpb,
                  c.kendaraan,
                  case when b.tanggal_dokumen is null then '2017-01-01' else b.tanggal_dokumen end as tgl_dokumen_tpb

                  from report.plb_flowbrg a
                  left join report.plb_documents_outbc27 b on a.no_aju_out = substring(b.no_aju,15)
                  and a.no_doc_out = b.nomor_daftar and a.batch_no = b.nomor_dokumen and b.uraian_dokumen = 'INVOICE'
                  join wh_flowcont c on a.id_flowbrg_splitted = c.id_flowcont
                  join v_customer d on a.kode_rel = d.kode_rel
                  join report.plb_flowbrg e on a.id_flowbrg_in = e.id_flowbrg
                  join (select Z.no_aju_out, sum(Z.jumlah) as jumlah_wh
		          from report.plb_flowbrg Z
		          where Z.kode_rel in ('500200','501500','500100')
		          and Z.doc_out = 'BC 2.7'
		          group by 1) as pack
		          on a.no_aju_out = pack.no_aju_out

                  where a.kode_rel in ('500200','501500','500100') 
                  and a.no_aju_out = '$no_aju'
				  and a.no_doc_out = '$nopen'
				  and a.batch_no = '$batch_no'
				  and b.seri_barang = '$seri'
                  order by no_aju_out";
	        $result3 = pg_query($db2_, $query3);
			
			
			if ($result3) {
				echo "AJU ".$no_aju." Berhasil ditambah <br>";
			}else{
				echo "AJU ".$no_aju." Gagal ditambah <br>";
			}
		}else{
			echo "Data Aju ".$no_aju." Sudah Ada <br>";
		}
	}
		
		}

}

///////////////////////////////////////////////// POST CARGILL ////////////////////////////////////////////////////////
else if ($aksi == 'cargil'){
	
	$query1 ="SELECT distinct a.no_aju_out,a.no_doc_out,a.exim as batch_no,b.seri_barang
              FROM report.plb_flowbrg a
              join report.plb_documents_outbc27 b on a.no_aju_out = substring(b.no_aju,15)
              and a.no_doc_out = b.nomor_daftar and a.exim = b.nomor_dokumen and b.uraian_dokumen = 'PACKING LIST'
              where a.kode_rel = '501600'
              and a.id_flowbrg_in is not null
              and a.tgl_doc_out between current_date - 3 and current_date
              order by 1,4";
	
	$result1 = pg_query($db2_, $query1);
		
		if ($baris = pg_num_rows($result1) == 0){
			echo "Belum ada data";
		}else{
		while($data = pg_fetch_array($result1)){
			$no_aju = $data['no_aju_out'];
			$nopen = $data['no_doc_out'];
			$batch_no = $data['batch_no'];
			$seri = $data['seri_barang'];
			
			/**
			echo $no_aju."<br>";
			echo $nopen."<br>";
			echo $batch_no."<br>";
			echo $seri."<br>";
			**/
			
		$query2 ="select distinct *
                  from report.bc27_fix
                  where no_aju_out = '$no_aju'
				  and no_doc_out = '$nopen'
				  and batch_no = '$batch_no'
				  and seri_barang_tpb = '$seri'
                  order by no_aju_out";
		
		$result2 = pg_query($db2_, $query2);
		
		if ($row = pg_num_rows($result2) == 0){
			
			$query3 ="insert into report.bc27_fix(
			no_aju_out,no_doc_out,tgl_aju_out,tgl_doc_out,id_flowbrg_splitted,batch_no,keterangan,kode_rel,
            satuan,hs_tpb,uraian_barang_tpb,satuan_tpb,kode_barang_tpb,currency_tpb,tgl_awal,no_unit,
            size_code,nopol,nama,jumlah_wh,jam_awal,cif_tpb,seri_barang_tpb,
            harga_penyerahan_tpb,jumlah_tpb,kendaraan,tgl_dokumen_tpb)
			
               select distinct a.no_aju_out,a.no_doc_out,a.tgl_aju_out,a.tgl_doc_out,a.id_flowbrg_splitted,
               a.exim as batch_no,a.keterangan,a.kode_rel,a.satuan,
               b.hs as hs_tpb,b.uraian_barang as uraian_barang_tpb,
               b.satuan as satuan_tpb,b.kode_barang as kode_barang_tpb,b.currency as currency_tpb,
               c.tgl_awal,c.no_unit,c.sizecode as size_code,c.nopol,
               d.nama,
               pack.jumlah_wh,
               c.jam_awal,
               case when b.cif is null then '0.00' else b.cif end as cif_tpb,
               case when b.seri_barang is null then 1 else b.seri_barang end as seri_barang_tpb,
               case when b.harga_penyerahan is null then '0.00' else b.harga_penyerahan end as harga_penyerahan_tpb,
               case when b.jumlah is null then 0 else b.jumlah end as jumlah_tpb,
               c.kendaraan,
               case when b.tanggal_dokumen is null then '2017-01-01' else b.tanggal_dokumen end as tgl_dokumen_tpb
               from report.plb_flowbrg a
               left join report.plb_documents_outbc27 b on a.no_aju_out = substring(b.no_aju,15)
               and a.no_doc_out = b.nomor_daftar and a.exim = b.nomor_dokumen and b.uraian_dokumen = 'PACKING LIST'
               join wh_flowcont c on a.id_flowbrg_splitted = c.id_flowcont
               join v_customer d on a.kode_rel = d.kode_rel
               join report.plb_flowbrg e on a.id_flowbrg_in = e.id_flowbrg
              join (select Z.no_aju_out, sum(Z.jumlah) as jumlah_wh
		      from report.plb_flowbrg Z
		      where Z.kode_rel = '501600'
		      and Z.doc_out = 'BC 2.7'
		      group by 1) as pack
		      on a.no_aju_out = pack.no_aju_out

              where a.kode_rel = '501600'
                  and a.no_aju_out = '$no_aju'
				  and a.no_doc_out = '$nopen'
				  and a.exim = '$batch_no'
				  and b.seri_barang = '$seri'
                  order by no_aju_out";
	        $result3 = pg_query($db2_, $query3);
			
			
			if ($result3) {
				echo "AJU ".$no_aju." Berhasil ditambah <br>";
			}else{
				echo "AJU ".$no_aju." Gagal ditambah <br>";
			}
		}else{
			echo "Data Aju ".$no_aju." Sudah Ada <br>";
		}
	}
		}
		
}



	?>
	