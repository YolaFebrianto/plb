<?php
session_start();

$_SESSION['MENU'] = 'TRANSACTION';
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}

$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];
$address =  $_SERVER['REMOTE_ADDR'];

include 'path.php';
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title><?= TITLE_APP ?></title>
    <link rel="shortcut icon" href="<?= IMAGES_PATH ?>/icons/logoijs.jpg">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha256-k2/8zcNbxVIh5mnQ52A0r3a6jAgMGxFJFE2707UxGCk= sha512-ZV9KawG2Legkwp3nAlxLIVFudTauWuBpC10uEafMHYL0Sarrz5A7G79kXh5+5+woxQ5HM559XX2UZjMJ36Wplg=="
        crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap-datetimepicker.min.css" media="screen">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- start:bootstrap v3.2.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.min.css">
    <!-- start:font awesome v4.1.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/font-awesome.min.css">
    <!-- start:bootstrap reset -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap-reset.css">
    <!-- start:style arjuna -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/arjuna.css">

    <script src="<?= ASSETS_PATH ?>/js/jquery-3.1.1.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?= ASSETS_PATH ?>/DataTables/media/js/jquerydt.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="jqueryw.js"></script>


</head>

<body class="cl-default fixed" style="font-family: 'Open Sans', sans-serif;">
    <!-- start:navbar top -->
    <header class="header">
        <!-- <a href="home.php" class="logo">Indra Jaya Swastika</a> -->
        <header class="header"><?php include("top_nav.php") ?></header>
    </header>
    <!-- end:navbar top -->

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas">
            <?php include("left_menu.php") ?>
        </aside>
        <aside class="right-side">
            <section class="content">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="#"> BC</a></li>
                    <li><a href="#"> BC 3.3</a></li>
                </ol>
                <div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
                            <div class="panel-heading hidden-print">
                                <span class="glyphicon glyphicon-tasks" aria-hidden="true">
                                </span> LAPORAN PENGELUARAN BARANG
                            </div>
                            <div class="box">
                                <hr>
                                <div class="panel-body" id="pnl-filter">
                                    <form class="form-horizontal" role="form" method="POST" action="#" id="param-doc" >
                                        <input type="hidden" id="username" name="username"
                                            value="<?php echo $username ?>">
                                        <input type="hidden" id="logincat" name="logincat"
                                            value="<?php echo $logincat ?>">
										 <input type="hidden" id="address" name="address"
                                            value="<?php echo $address ?>">	

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Jenis Laporan</label>
                                            <div class="col-md-4 col-xs-6">
                                                <select class="form-control input-sm" name="jenis_doc" id="jenis_doc">
                                                    <option value='BC33'>Laporan BC 3.3</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Tanggal</label>
                                            <div class="col-md-4 col-xs-6">
                                                <input type="text" autocomplete="off" class="form-control" id="tglan1"
                                                    name="tglan1" value="<?php echo $tgl1; ?>">
                                            </div>

                                        </div>
                                        <div id="periode2" class="form-group">
                                            <label class="control-label col-xs-4">Sampai Tanggal</label>
                                            <div class="col-md-4 col-xs-6">
                                                <input type="text" autocomplete="off" class="form-control" id="tglan2"
                                                    name="tglan2" value="<?php echo $tgl2; ?>">
                                            </div>
                                        </div>

                                        <div id="dcustomer" class="form-group">
                                            <label class="control-label col-xs-4">Pemilik Barang</label>
                                            <div class="col-md-4 col-xs-6">
                                                <select class="form-control input-sm" name="cust" id="cust">
                                                   <option value="ALL" selected >SEMUA</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-6 col-xs-offset-4">
                                                <button class="form-control btn-primary" id="submit" type="button">Submit</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-6 col-xs-offset-4">
                                                <button class="form-control btn-warning" id="export" type="button">Export</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-9 col-xs-offset-3" id="form-msg"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-xs-9" id="judul">
                                </div>
                            </div>
                        </div>

                    </div>						
						<div class="row">
							<div class="col-md-12">
								<div class="box">
									<div class="row">
										<div class="col-sm"></div>
									</div>
									<div class="table-responsive" id="result">
										<div id="view"></div>
									</div>

								</div>
							</div>
						</div>	

                    </div>

                </div>
            </section>
        </aside>
    </div>

    <script src="<?= ASSETS_PATH ?>/js/jquery.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/arjuna.js"></script>
    <script src="<?= ASSETS_PATH ?>/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="<?= ASSETS_PATH ?>/DataTables/media/js/jquerydt.js"></script>
	<script src="<?= ASSETS_PATH?>/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="<?= JS_PATH ?>/bootstrap-datetimepicker.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/tanggal.js"></script>
    <script type="text/javascript" src="<?= JS_PATH ?>/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="<?= JS_PATH ?>/lapbc33.js"></script>
    <script>
    $(document).ready(function(){
		$('#data').DataTable();
	});
	</script>

</body>

</html>