<?php
session_start();
$_SESSION['MENU'] = 'TRANSACTION';
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}

$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

include 'path.php';
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link rel="shortcut icon" href="images/icons/logoijs.jpg">
    <title><?= TITLE_APP ?></title>
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha256-k2/8zcNbxVIh5mnQ52A0r3a6jAgMGxFJFE2707UxGCk= sha512-ZV9KawG2Legkwp3nAlxLIVFudTauWuBpC10uEafMHYL0Sarrz5A7G79kXh5+5+woxQ5HM559XX2UZjMJ36Wplg=="
        crossorigin="anonymous">
    <link href="css/bootstrap-datetimepicker.min.css" rel="stylesheet" media="screen">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- start:bootstrap v3.2.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.min.css">
    <!-- start:font awesome v4.1.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/font-awesome.min.css">
    <!-- start:bootstrap reset -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap-reset.css">
    <!-- start:style arjuna -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/arjuna.css">
    <link rel="stylesheet" type="text/css" href="css/style-inline2.css">
    <link rel="stylesheet" type="text/css" href="css/check-style-sp.css">

    <script src="<?= ASSETS_PATH ?>/js/jquery-3.1.1.min.js"></script>
    <script src="<?= ASSETS_PATH ?><?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?= ASSETS_PATH ?>/DataTables/media/js/jquerydt.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="jqueryw.js"></script>
    <script type="text/javascript" src="<?= ASSETS_PATH ?>/DataTables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="js/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="js/cek_komo.js"></script>
    <script>
    function Kapital() {
        var x = document.getElementById("cur");
        x.value = x.value.toUpperCase();
    }
    </script>

    <!------ Include the above in your HEAD tag ---------->
</head>

<body class="cl-default fixed" style="font-family: 'Open Sans', sans-serif;">
    <!-- start:navbar top -->
    <header class="header">
        <!-- <a href="home.php" class="logo">Indra Jaya Swastika</a> -->
        <header class="header"><?php include("top_nav.php") ?></header>
    </header>
    <!-- end:navbar top -->

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas">
            <?php include("left_menu.php") ?>
        </aside>
        <aside class="right-side">
            <section class="content">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="#"> BC</a></li>
                    <li><a href="#"> Check Komoditi</a></li>
                </ol>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">
                            <!---------------------------------------------- MODAL PILIH ------------------------------------------------>
                            <div class="modal fade" id="Modal-pilih" tabindex="-1" role="dialog"
                                aria-labelledby="myModalTitle">
                                <div class="modal-dialog " role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalTitle">Pilih Aju</h4>
                                            <form class="form-horizontal" method="post">
                                        </div>

                                        <div class="modal-body" id="myModalBody">
                                            <!-- Isi Modal -->
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table table-striped">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <div class="col-md-8 col-xs-8">
                                                                                <select class="form-control input-sm"
                                                                                    name="laporan-pilih"
                                                                                    id="laporan-pilih">
                                                                                    <option value=''>Pilih Aju</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-info pilih-aju" id="submit"
                                                    name="simpan">Pilih</button>
                                                </form>
                                                <button type="button" class="btn btn-default" id="tutup-modal"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!------------------ END MODAL PILIH --------------------------->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="tab-content">
                                        <div id="stripe" class="tab-pane fade in active">
                                            <form class="form-cek" method="POST">
                                                <div class='form-row'>
                                                    <div class='form-group required'>
                                                        <label class='control-label'>Nomor Aju</label> &nbsp;
                                                        <button class='btn btn-info btn-xs aju-pilih' id='aju-pilih'
                                                            data-toggle="modal" data-target="#Modal-pilih"
                                                            type='button'>Click Me</button>
                                                        <input class='form-control' id='no_aju' name='no_aju'
                                                            type='text'>
                                                        <input type="hidden" class="form-control" name="aksi" id="aksi"
                                                            value="cek-komoditi">
                                                    </div>

                                                </div>
                                                <div class='form-row'>
                                                    <div class='form-group'>
                                                        <select class="form-control input-sm" name="laporan"
                                                            id="laporan">
                                                            <option value=''>Pilih Dokumen</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class='form-row'>
                                                    <div class='form-group'>
                                                        <label class='control-label'></label>
                                                        <div class="col-sm-3">
                                                            <button class='form-control btn-primary komoditas-cek'
                                                                type='button'> Cek →</button>
                                                        </div>
                                                    </div>
                                                </div>

                                            </form>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <label class='control-label'></label><!-- spacing -->
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="hasil-disini">
                            </div>
                        </div>
                    </div>

                </div>
            </section>
        </aside>
    </div>

    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/arjuna.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/arjuna.js"></script>


</body>

</html>