<?php
$startRec = 0;
if (isset($_POST['jenis_act'])) {

    require_once('db-inc2.php');
    require_once('insert_log_activity.php');

    $jenis = $_POST['jenis_act'];
    $tgl1 = $_POST['tglan1'];
    $kode_rel = TRIM($_POST['cust']);
    if ($kode_rel == "ALL") {
        $kode_rel = '50%';
    }

    switch ($jenis) {
        case  "STOKA":
            $jns_rep = 'Laporan Stock BC 1.6';
            break;
        case  "STOKB":
            $jns_rep = 'Laporan Stock BC 2.7';
            break;
        case  "STOKC":
            $jns_rep = 'Laporan Stock BC 4.0';
            break;
    }


    // insert_log($username,$kategori,$address ,$status,$remark ) 
    $log_remark = "Open " . $jns_rep . " Tanggal " . date('d-m-Y', strtotime($tgl1));
    insert_log($username, $kategori, $address, "REPORT", $log_remark);

    if ($jenis == 'STOKA') {

        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN STOCK BARANG PER DOKUMEN PABEAN <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1));
        echo "<hr>";
        echo "<br>";
        echo "<br>";

        $sqltext = "select  b.no_bc_16 as bc16,b.tgl_bc_16 as tgl_bc16, B.nama,B.tgl_awal,B.no_bl,B.tgl_bl,C.kode_barang,C.uraian_barang,B.kode_rel,
                     sum_varchar (distinct( case when B.kendaraan = 'CONTAINER' THEN B.no_unit else B.nopol end ||'<br />')) as kendaraan,
                     sum_varchar (distinct( split_part(B.itemname,':',1))||'<br />') as cbarang,
                     sum_varchar (distinct( split_part(B.itemname,':',2))||'<br />') as nbarang,
                     --sum_varchar (distinct B.uraian_barang||'<br />') as itemname,
                     --sum_varchar (distinct B.kode_barang||'<br />') as itemcode,
                     sum_varchar (distinct B.satuan||'<br />') as satuan,
                     sum_varchar (distinct b.supplier||'<br />') as supplier,    
                     sum_varchar (distinct B.batch_no||'<br />') as invoice,          
		             sum(A.jumlah) as jumlah
					FROM
					(select distinct id_flowbrg_in, sum (qty - qty_out) as jumlah  from v_mutasi
					 where kode_rel  LIKE '$kode_rel'  and tgl_awal <='$tgl1'
					 group by 1)A 
					join
					(select distinct * from report.v_plb_mutasi 
					 where kode_rel LIKE '$kode_rel'   and tgl_awal <='$tgl1'
					 and (id_stok_out is null or id_stok_out ='' )) B
					 on A.id_flowbrg_in = B.id_flowbrg_in
					 join
					 (select distinct nomor_dokumen, nomor_aju,nomor_daftar,
                                        sum_varchar(distinct(kode_barang)||'<br />') as kode_barang,
                                        sum_varchar(distinct(uraian_barang)||'<br />') as uraian_barang
					 from report.plb_documents_in
					 where uraian_dokumen in ('INVOICE','PACKING LIST')
					 group by 1,2,3) C on case when B.kode_rel <> '501600' then B.batch_no = C.nomor_dokumen
					 else B.exim = C.nomor_dokumen end
					 and B.no_aju = substring(C.nomor_aju,15) and B.no_bc_16 = C.nomor_daftar
					where A.jumlah>0
					group by 1,2,3,4,5,6,7,8,9
					order by tgl_bc16";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th colspan='3'>DOKUMEN PABEAN</th>";
        echo            "<th colspan='2'>BL/AWB</th>";
        echo            "<th rowspan='2'>TGL MASUK</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th rowspan='2'>INVOICE</th>";
        echo            "<th rowspan='2'>PEMILIK BARANG</th>";
        echo            "<th rowspan='2'>KODE BARANG</th>";
        echo            "<th rowspan='2'>NAMA BARANG</th>";
        echo            "<th rowspan='2'>JUMLAH</th>";
        echo            "<th rowspan='2'>SATUAN</th>";
        echo            "<th rowspan='2'>LOKASI</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>JENIS</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>BC 1.6</td>";
            echo            "<td>" . $row['bc16'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_bc16'])) . "</td>";
            echo            "<td>" . $row['no_bl'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_bl'])) . "</td>";
            echo            "<td>" . $row['tgl_awal'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['kode_barang'] . "</td>";
            echo            "<td>" . $row['uraian_barang'] . "</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            if (($rel == '500200') || ($rel == '501500') || ($rel == '500300')) {
                echo            "<td>WH.03</td>";
            } else if (($rel == '501600') || ($rel == '500100')) {
                echo            "<td>WH.04</td>";
            } else {
                echo            "<td>WH.LAP</td>";
            }
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'STOKB') {

        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN STOCK BARANG PER DOKUMEN PABEAN <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1));
        echo "<hr>";
        echo "<br>";
        echo "<br>";


        $sqltext = "select  B.nama,B.kode_rel,C.no_aju,C.no_daftar,C.tgl_daftar,C.tgl_aju,
                     sum_varchar (distinct( case when B.kendaraan = 'CONTAINER' THEN B.no_unit else B.nopol end ||'<br />')) as kendaraan,
                     sum_varchar (distinct( split_part(B.itemname,':',1))||'<br />') as cbarang,
                     sum_varchar (distinct( split_part(B.itemname,':',2))||'<br />') as nbarang,
                     sum_varchar (distinct to_char( B.tgl_awal,'dd-MM-YYYY') ||'<br />') as tgl_awal,
                     --sum_varchar (distinct B.uraian_barang||'<br />') as itemname,
                     --sum_varchar (distinct B.kode_barang||'<br />') as itemcode,
                     sum_varchar (distinct B.satuan||'<br />') as satuan,
                     sum_varchar (distinct B.exim||'<br />') as invoice,          
		             sum(A.jumlah) as jumlah
					FROM
					(select distinct id_flowbrg_in, sum (qty - qty_out) as jumlah  from v_mutasi
					 where kode_rel  LIKE '$kode_rel'  and tgl_awal <='$tgl1'
					 group by 1)A 
					left join
					(select distinct * from v_mutasi 
					 where kode_rel LIKE '$kode_rel'   and tgl_awal <='$tgl1'
					 and (id_stok_out is null or id_stok_out ='' )) B
					 on A.id_flowbrg_in = B.id_flowbrg_in
					  join
					 (select distinct B.no_doc, A.no_aju,A.no_daftar,C.id_flowbrg,tgl_daftar,tgl_aju
					 from report.plb_doc_in27 A
					 join report.plb_doc_in27_dtl B on A.no_aju = B.no_aju
					 join report.plb_doc_in27_brg C on A.no_aju = C.no_aju
					 where B.jenis_doc = 'PACKING LIST'
					 group by 1,2,3,4,5,6) C on A.id_flowbrg_in = C.id_flowbrg
					where A.jumlah>0
					group by 1,2,3,4,5,6";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th colspan='3'>DOKUMEN PABEAN</th>";
        echo            "<th rowspan='2'>TGL MASUK</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th rowspan='2'>PACKING LIST</th>";
        echo            "<th rowspan='2'>PEMILIK BARANG</th>";
        echo            "<th rowspan='2'>KODE BARANG</th>";
        echo            "<th rowspan='2'>NAMA BARANG</th>";
        echo            "<th rowspan='2'>JUMLAH</th>";
        echo            "<th rowspan='2'>SATUAN</th>";
        echo            "<th rowspan='2'>LOKASI</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>JENIS</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            if ($rel == '501600') {
                echo            "<td>BC 2.7</td>";
            }
            echo            "<td>" . $row['no_daftar'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_daftar'])) . "</td>";
            echo            "<td>" . $row['tgl_awal'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['cbarang'] . "</td>";
            echo            "<td>" . $row['nbarang'] . "</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            if (($rel == '500200') || ($rel == '501500') || ($rel == '500300')) {
                echo            "<td>WH.03</td>";
            } else if (($rel == '501600') || ($rel == '500100')) {
                echo            "<td>WH.04</td>";
            } else {
                echo            "<td>WH.LAP</td>";
            }
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else if ($jenis == 'STOKC') {

        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN STOCK BARANG PER DOKUMEN PABEAN <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1));
        echo "<hr>";
        echo "<br>";
        echo "<br>";


        $sqltext = "select  b.no_bc_16 as bc16,b.tgl_bc_16 as tgl_bc16, B.nama,B.tgl_awal,B.no_bl,B.tgl_bl,C.kode_barang,C.uraian_barang,B.kode_rel,
                     sum_varchar (distinct( case when B.kendaraan = 'CONTAINER' THEN B.no_unit else B.nopol end ||'<br />')) as kendaraan,
                     sum_varchar (distinct( split_part(B.itemname,':',1))||'<br />') as cbarang,
                     sum_varchar (distinct( split_part(B.itemname,':',2))||'<br />') as nbarang,
                     --sum_varchar (distinct B.uraian_barang||'<br />') as itemname,
                     --sum_varchar (distinct B.kode_barang||'<br />') as itemcode,
                     sum_varchar (distinct B.satuan||'<br />') as satuan,
                     sum_varchar (distinct b.supplier||'<br />') as supplier,    
                     sum_varchar (distinct B.batch_no||'<br />') as invoice,          
		             sum(A.jumlah) as jumlah
					FROM
					(select distinct id_flowbrg_in, sum (qty - qty_out) as jumlah  from v_mutasi
					 where kode_rel  LIKE '$kode_rel'  and tgl_awal <='$tgl1'
					 group by 1)A 
					join
					(select distinct * from report.v_plb_mutasi 
					 where kode_rel LIKE '$kode_rel'   and tgl_awal <='$tgl1'
					 and (id_stok_out is null or id_stok_out ='' )) B
					 on A.id_flowbrg_in = B.id_flowbrg_in
					 join
					 (select distinct nomor_dokumen, nomor_aju,nomor_daftar,
                                        sum_varchar(distinct(kode_barang)||'<br />') as kode_barang,
                                        sum_varchar(distinct(uraian_barang)||'<br />') as uraian_barang
					 from report.plb_documents_in40
					 where uraian_dokumen IN ('PACKING LIST','SURAT JALAN', 'LAINNYA')
					 group by 1,2,3) C on B.exim = C.nomor_dokumen
					 and B.no_aju = substring(C.nomor_aju,15) and B.no_bc_16 = C.nomor_daftar
					where A.jumlah>0
					group by 1,2,3,4,5,6,7,8,9
					order by tgl_bc16";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th colspan='3'>DOKUMEN PABEAN</th>";
        echo            "<th rowspan='2'>TGL MASUK</th>";
        echo            "<th rowspan='2'>CONTAINER</th>";
        echo            "<th rowspan='2'>INVOICE</th>";
        echo            "<th rowspan='2'>PEMILIK BARANG</th>";
        echo            "<th rowspan='2'>KODE BARANG</th>";
        echo            "<th rowspan='2'>NAMA BARANG</th>";
        echo            "<th rowspan='2'>JUMLAH</th>";
        echo            "<th rowspan='2'>SATUAN</th>";
        echo            "<th rowspan='2'>LOKASI</th>";
        echo        "</tr>";
        echo        "<tr>";
        echo            "<th>JENIS</th>";
        echo            "<th>NOMOR</th>";
        echo            "<th>TANGGAL</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = $row['kode_rel'];
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            if ($rel != '501600') {
                echo            "<td>BC 1.6</td>";
            } else {
                echo            "<td>BC 4.0</td>";
            }
            echo            "<td>" . $row['bc16'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_bc16'])) . "</td>";
            echo            "<td>" . $row['tgl_awal'] . "</td>";
            echo            "<td>" . $row['kendaraan'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['kode_barang'] . "</td>";
            echo            "<td>" . $row['uraian_barang'] . "</td>";
            echo            "<td>" . $row['jumlah'] . "</td>";
            echo            "<td>" . $row['satuan'] . "</td>";
            if (($rel == '500200') || ($rel == '501500') || ($rel == '500300')) {
                echo            "<td>WH.03</td>";
            } else if (($rel == '501600') || ($rel == '500100')) {
                echo            "<td>WH.04</td>";
            } else {
                echo            "<td>WH.LAP</td>";
            }
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    }
}