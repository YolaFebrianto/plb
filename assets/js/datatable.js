$(document).ready(function() {
    $(".data").DataTable({
        "responsive": true,
        "ordering": false,
        "searching": true,
        "paging": true,
        "pageLength": 0,
        "lengthMenu": [
            [5, 10, 20, -1],
            [5, 10, 20, "All"]
        ],
        "columnDefs": [{
            "target": 0,
            "searchable": false,
            "visible": true
        }],
        "order": [
            [2, "asc"]
        ]
    });
});