<?php
require_once('db-inc2.php');
if(isset($_GET["jenis_act"]))
{

header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
clearstatcache();

	
$jenis = $_GET['jenis_act'];
$tgl1 = $_GET['tglan1'];
$tgl2 = $_GET['tglan2'];
$kode_rel = TRIM($_GET['cust']);
if ($kode_rel == "ALL" ){ $kode_rel = '50%';  }

if ($jenis == 'BC 1.6'){
	
	
echo "<h5> KAWASAN BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
echo "<h5> LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN ".$jenis." <br>";
echo "<h5> PERIODE : ".$tgl1." S.D ".$tgl2;
echo "<br>";
echo "<br>";


 $sqltext= "select A.nomor_daftar, A.tanggal_daftar, substring(A.nomor_aju,15) as no_aju, A.tanggal_aju,
       A.supplier,pack.jumlah4,C.kode_rel,
       A.currency,A.harga,C.no_bl , C.tgl_bl,
       sum_varchar (distinct A.seri_barang ||'<br />') as seri_barang,
       sum_varchar (distinct A.nomor_dokumen ||'<br />') as invoice,
       sum_varchar (distinct A.tanggal_dokumen ||'<br />') as tgl_invoice,
       sum_varchar (distinct A.uraian_barang ||'<br />') as uraian_barang,
       sum_varchar (distinct A.kode_barang ||'<br />') as kode_barang,
       sum_varchar (distinct cast(A.jumlah as integer) ||'<br />') as jumlah,
	   sum_varchar (distinct C.keterangan ||'<br />') as keterangan,
       sum_varchar (distinct A.hs ||'<br />') as hs,
       sum_varchar (distinct cast(A.tarif as integer) ||'%<br />') as tarif,
       sum_varchar (distinct A.kemasan ||'<br />') as kemasan,
       sum_varchar (distinct split_part(C.id_flowbrg_splitted,'-',1) ||'<br />') as book
       from report.plb_doc_in A
       left join report.plb_flowbrg C on A.nomor_dokumen = C.batch_no
       and A.uraian_dokumen = 'INVOICE' and substring(A.nomor_aju,15)=C.no_aju
       and A.nomor_daftar = C.no_bc_16
       left join (select Z.no_aju, sum(Z.jumlah) as jumlah4
			from report.plb_flowbrg Z
			where Z.id_flowbrg_in is null
			and Z.kode_rel like '$kode_rel'
			group by 1) as pack
			on substring(A.nomor_aju,15) = pack.no_aju
       where A.tanggal_daftar between '$tgl1' and '$tgl2'
       and C.id_flowbrg_in is null
       and C.kode_rel <> '501600'
       and C.kode_rel like '$kode_rel'
       group by 1,2,3,4,5,6,7,8,9,10,11
       order by A.nomor_daftar";
			
			
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		
		echo "<table class='table table-striped table-bordered data'>";
		echo	"<thead>";
		echo		"<tr>";			
		echo			"<th rowspan='2'>NO</th>";
		echo			"<th rowspan='2'>JENIS DOKUMEN</th>";
		echo			"<th colspan='4'>DOKUMEN PABEAN</th>";
		echo			"<th colspan='3'>INVOICE</th>";
		echo			"<th colspan='2'>BL/AWB</th>";
		echo			"<th colspan='3'>LPB</th>";
		echo			"<th rowspan='2'>PEMASOK/PENGIRIM</th>";
		echo			"<th rowspan='2'>NOMOR SERI BARANG</th>";
		echo			"<th rowspan='2'>NAMA BARANG</th>";
		echo			"<th rowspan='2'>KODE HS</th>";
		echo			"<th rowspan='2'>TARIF</th>";
		echo			"<th rowspan='2'>SATUAN</th>";
		echo			"<th rowspan='2'>JUMLAH</th>";
		echo			"<th rowspan='2'>VALAS</th>";
		echo			"<th rowspan='2'>KETERANGAN</th>";
		echo		"</tr>";
		echo		"<tr>";			
		echo			"<th>NOPEN</th>";
		echo			"<th>TGL NOPEN</th>";
		echo			"<th>NOMOR AJU</th>";
		echo			"<th>TGL AJU</th>";
		echo			"<th>NOMOR</th>";
		echo			"<th>TANGGAL</th>";
		echo			"<th>NILAI</th>";
		echo			"<th>NOMOR</th>";
		echo			"<th>TANGGAL</th>";
		echo			"<th>ID BOOK</th>";
		echo			"<th>JUMLAH</th>";
		echo			"<th>LOKASI</th>";
		echo		"</tr>";
		echo	"</thead>";
		echo	"<tbody>";
		
		while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
			   $rel = $row['kode_rel'];
		echo		"<tr>";			
		echo			"<td>".$number."</td>";
		echo			"<td>".$jenis."</td>";
		echo			"<td>".$row['nomor_daftar']."</td>";
		echo			"<td>".$row['tanggal_daftar']."</td>";
		echo			"<td>".$row['no_aju']."</td>";
		echo			"<td>".$row['tanggal_aju']."</td>";
		echo			"<td>".$row['invoice']."</td>";
		echo			"<td>".$row['tgl_invoice']."</td>";
		echo			"<td>".number_format($row['harga'],2)."</td>";
		echo			"<td>".$row['no_bl']."</td>";
		echo			"<td>".$row['tgl_bl']."</td>";
		echo			"<td>".$row['book']."</td>";
		echo			"<td>".$row['jumlah4']."</td>";
		if (($rel == '500200') || ($rel == '500100')){
		echo			"<td>GUDANG 3</td>";
		} else{
		echo			"<td>GUDANG LAPANGAN</td>";
		}
		echo			"<td>".$row['supplier']."</td>";
		echo			"<td>".$row['seri_barang']."</td>";
		echo			"<td>".$row['uraian_barang']."</td>";
		echo			"<td>".$row['hs']."</td>";
		echo			"<td>".$row['tarif']."</td>";
		echo			"<td>".$row['kemasan']."</td>";
		echo			"<td>".$row['jumlah']."</td>";
		echo			"<td>".$row['currency']."</td>";
		echo			"<td>".$row['keterangan']."</td>";
		echo		"</tr>";
		}
		echo	"</tbody>";
		echo  "</table>";
		
     }
	 
	 else if ($jenis == 'CY'){
	
	
echo "<h5> KAWASAN BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
echo "<h5> LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN BC 1.6 <br>";
echo "<h5> PERIODE : ".$tgl1." S.D ".$tgl2;
echo "<br>";
echo "<br>";


 $sqltext= "select A.nomor_daftar, A.tanggal_daftar, substring(A.nomor_aju,15) as no_aju, A.tanggal_aju,
       A.supplier,pack.jumlah4,C.kode_rel,
       A.currency,A.harga,B.nomor_dokumen as no_bl, B.tanggal_dokumen as tgl_bl,
       sum_varchar (distinct A.seri_barang ||'<br />') as seri_barang,
       sum_varchar (distinct A.nomor_dokumen ||'<br />') as invoice,
       sum_varchar (distinct A.tanggal_dokumen ||'<br />') as tgl_invoice,
       sum_varchar (distinct A.uraian_barang ||'<br />') as uraian_barang,
       sum_varchar (distinct A.kode_barang ||'<br />') as kode_barang,
       sum_varchar (distinct cast(A.jumlah as integer) ||'<br />') as jumlah,
       sum_varchar (distinct C.keterangan ||'<br />') as keterangan,
       sum_varchar (distinct A.hs ||'<br />') as hs,
       sum_varchar (distinct cast(A.tarif as integer) ||'%<br />') as tarif,
       sum_varchar (distinct A.kemasan ||'<br />') as kemasan,
       sum_varchar (distinct split_part(C.id_flowbrg,'-',1) ||'<br />') as book
       from report.plb_doc_in A
       join report.plb_doc_in B on A.nomor_aju = B.nomor_aju
       and A.nomor_daftar = B.nomor_daftar and B.uraian_dokumen = 'B/L'
       left join report.plb_cy C on A.nomor_dokumen = C.batch_no
       left join (select Z.no_aju, sum(Z.jumlah) as jumlah4
			from report.plb_cy Z
			where Z.id_flowbrg_in is null
			and Z.kode_rel like '$kode_rel'
			group by 1) as pack
			on substring(A.nomor_aju,15) = pack.no_aju
       where A.tanggal_daftar between '$tgl1' and '$tgl2'
       and C.id_flowbrg_in is null
       and C.kode_rel like '$kode_rel'
       group by 1,2,3,4,5,6,7,8,9,10,11
       order by A.nomor_daftar";
			
			
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		
		echo "<table class='table table-striped table-bordered data'>";
		echo	"<thead>";
		echo		"<tr>";			
		echo			"<th rowspan='2'>NO</th>";
		echo			"<th rowspan='2'>JENIS DOKUMEN</th>";
		echo			"<th colspan='4'>DOKUMEN PABEAN</th>";
		echo			"<th colspan='3'>INVOICE</th>";
		echo			"<th colspan='2'>BL/AWB</th>";
		echo			"<th colspan='3'>LPB</th>";
		echo			"<th rowspan='2'>PEMASOK/PENGIRIM</th>";
		echo			"<th rowspan='2'>NOMOR SERI BARANG</th>";
		echo			"<th rowspan='2'>NAMA BARANG</th>";
		echo			"<th rowspan='2'>KODE HS</th>";
		echo			"<th rowspan='2'>TARIF</th>";
		echo			"<th rowspan='2'>SATUAN</th>";
		echo			"<th rowspan='2'>JUMLAH</th>";
		echo			"<th rowspan='2'>VALAS</th>";
		echo			"<th rowspan='2'>KETERANGAN</th>";
		echo		"</tr>";
		echo		"<tr>";			
		echo			"<th>NOPEN</th>";
		echo			"<th>TGL NOPEN</th>";
		echo			"<th>NOMOR AJU</th>";
		echo			"<th>TGL AJU</th>";
		echo			"<th>NOMOR</th>";
		echo			"<th>TANGGAL</th>";
		echo			"<th>NILAI</th>";
		echo			"<th>NOMOR</th>";
		echo			"<th>TANGGAL</th>";
		echo			"<th>ID BOOK</th>";
		echo			"<th>JUMLAH</th>";
		echo			"<th>LOKASI</th>";
		echo		"</tr>";
		echo	"</thead>";
		echo	"<tbody>";
		
		while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
			   $rel = $row['kode_rel'];
		echo		"<tr>";			
		echo			"<td>".$number."</td>";
		echo			"<td>BC 1.6</td>";
		echo			"<td>".$row['nomor_daftar']."</td>";
		echo			"<td>".$row['tanggal_daftar']."</td>";
		echo			"<td>".$row['no_aju']."</td>";
		echo			"<td>".$row['tanggal_aju']."</td>";
		echo			"<td>".$row['invoice']."</td>";
		echo			"<td>".$row['tgl_invoice']."</td>";
		echo			"<td>".number_format($row['harga'],2)."</td>";
		echo			"<td>".$row['no_bl']."</td>";
		echo			"<td>".$row['tgl_bl']."</td>";
		echo			"<td>".$row['book']."</td>";
		echo			"<td>".$row['jumlah4']."</td>";
		if (($rel == '500200') || ($rel == '500100')){
		echo			"<td>GUDANG 3</td>";
		} else{
		echo			"<td>GUDANG LAPANGAN</td>";
		}
		echo			"<td>".$row['supplier']."</td>";
		echo			"<td>".$row['seri_barang']."</td>";
		echo			"<td>".$row['uraian_barang']."</td>";
		echo			"<td>".$row['hs']."</td>";
		echo			"<td>".$row['tarif']."</td>";
		echo			"<td>".$row['kemasan']."</td>";
		echo			"<td>".$row['jumlah']."</td>";
		echo			"<td>".$row['currency']."</td>";
		echo			"<td>".$row['keterangan']."</td>";
		echo		"</tr>";
		}
		echo	"</tbody>";
		echo  "</table>";
		
     }
	 
	 else if ($jenis == 'BC 2.7'){
	
	
echo "<h5> KAWASAN BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
echo "<h5> LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN BC 2.7 <br>";
echo "<h5> PERIODE : ".$tgl1." S.D ".$tgl2;
echo "<br>";
echo "<br>";


 $sqltext= "select A.no_daftar, A.tgl_daftar, A.no_aju, A.tgl_aju,
       A.currency_cif,A.kode_rel,
       sum_varchar (distinct B.no_doc ||'<br />') as invoice,
       sum_varchar (distinct B.tgl_doc ||'<br />') as tgl_invoice,
	   sum_varchar (distinct A.cif ||'<br />') as harga,
       sum_varchar (distinct A.kode_brg ||'<br />') as kode_barang,
       sum_varchar (distinct cast(A.jumlah as integer) ||'<br />') as jumlah,
       sum_varchar (distinct A.tarif_hs ||'<br />') as hs,
       sum_varchar (distinct A.satuan ||'<br />') as kemasan,
       sum_varchar (distinct split_part(C.id_flowbrg,'-',1) ||'<br />') as book
       from report.plb_doc_in27 A
       left join report.plb_doc_in27_dtl B on A.no_aju = B.no_aju
       left join report.plb_doc_in27_brg C on A.no_aju = C.no_aju
       where A.tgl_daftar between '$tgl1' and '$tgl2'
       and A.kode_rel like '$kode_rel'
	   and A.nsurat='BC 2.7'
       and B.jenis_doc in ('LAINNYA','PACKING LIST','SURAT JALAN')
       group by 1,2,3,4,5,6
       order by A.no_daftar";
			
			
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		
		echo "<table class='table table-striped table-bordered data'>";
		echo	"<thead>";
		echo		"<tr>";			
		echo			"<th rowspan='2'>NO</th>";
		echo			"<th rowspan='2'>JENIS DOKUMEN</th>";
		echo			"<th colspan='4'>DOKUMEN PABEAN</th>";
		echo			"<th colspan='3'>INVOICE</th>";
		echo			"<th colspan='3'>LPB</th>";
		echo			"<th rowspan='2'>KODE BARANG</th>";
		echo			"<th rowspan='2'>KODE HS</th>";
		echo			"<th rowspan='2'>SATUAN</th>";
		echo			"<th rowspan='2'>JUMLAH</th>";
		echo			"<th rowspan='2'>VALAS</th>";
		echo			"<th rowspan='2'>KETERANGAN</th>";
		echo		"</tr>";
		echo		"<tr>";			
		echo			"<th>NOPEN</th>";
		echo			"<th>TGL NOPEN</th>";
		echo			"<th>NOMOR AJU</th>";
		echo			"<th>TGL AJU</th>";
		echo			"<th>NOMOR</th>";
		echo			"<th>TANGGAL</th>";
		echo			"<th>NILAI</th>";
		echo			"<th>ID BOOK</th>";
		echo			"<th>JUMLAH</th>";
		echo			"<th>LOKASI</th>";
		echo		"</tr>";
		echo	"</thead>";
		echo	"<tbody>";
		
		while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
			   $rel = $row['kode_rel'];
		echo		"<tr>";			
		echo			"<td>".$number."</td>";
		echo			"<td>BC 2.7</td>";
		echo			"<td>".$row['no_daftar']."</td>";
		echo			"<td>".$row['tgl_daftar']."</td>";
		echo			"<td>".$row['no_aju']."</td>";
		echo			"<td>".$row['tgl_aju']."</td>";
		echo			"<td>".$row['invoice']."</td>";
		echo			"<td>".$row['tgl_invoice']."</td>";
		echo			"<td>".number_format($row['harga'],2)."</td>";
		echo			"<td>".$row['book']."</td>";
		echo			"<td>".$row['jumlah']."</td>";
		if ($rel == '501600'){
		echo			"<td>GUDANG 4</td>";
		}
		echo			"<td>".$row['kode_barang']."</td>";
		echo			"<td>".$row['hs']."</td>";
		echo			"<td>".$row['kemasan']."</td>";
		echo			"<td>".$row['jumlah']."</td>";
		echo			"<td>".$row['currency_cif']."</td>";
		echo			"<td>".$row['keterangan']."</td>";
		echo		"</tr>";
		}
		echo	"</tbody>";
		echo  "</table>";
		
     }
	 
	 
	 else if ($jenis == 'BC 4.0'){
	
	
echo "<h5> KAWASAN BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
echo "<h5> LAPORAN PEMASUKAN BARANG PER DOKUMEN PABEAN ".$jenis." <br>";
echo "<h5> PERIODE : ".$tgl1." S.D ".$tgl2;
echo "<br>";
echo "<br>";


 $sqltext= "select A.nomor_daftar, A.tanggal_daftar, substring(A.nomor_aju,15) as no_aju, A.tanggal_aju,
       pack.jumlah4,C.kode_rel,
       A.currency,
       sum_varchar (distinct A.nomor_dokumen ||'<br />') as invoice,
       sum_varchar (distinct A.tanggal_dokumen ||'<br />') as tgl_invoice,
       sum_varchar (distinct A.uraian_barang ||'<br />') as uraian_barang,
       sum_varchar (distinct A.kode_barang ||'<br />') as kode_barang,
       sum_varchar (distinct cast(A.jumlah as integer) ||'<br />') as jumlah,
	   sum_varchar (distinct C.keterangan ||'<br />') as keterangan,
       sum_varchar (distinct A.hs ||'<br />') as hs,
       sum_varchar (distinct cast(A.tarif as integer) ||'<br />') as tarif,
       sum_varchar (distinct A.kemasan ||'<br />') as kemasan,
       sum_varchar (distinct split_part(C.id_flowbrg_splitted,'-',1) ||'<br />') as book
       from report.plb_doc_in40 A
       left join report.plb_flowbrg C on A.nomor_dokumen = C.exim
       and substring(A.nomor_aju,15)=C.no_aju and A.nomor_daftar = C.no_bc_16
       left join (select Z.no_aju, sum(Z.jumlah) as jumlah4
			from report.plb_flowbrg Z
			where Z.id_flowbrg_in is null
			and Z.kode_rel like '$kode_rel'
			group by 1) as pack
			on substring(A.nomor_aju,15) = pack.no_aju
       where A.tanggal_daftar between '$tgl1' and '$tgl2'
       and C.id_flowbrg_in is null
	   and C.kode_rel like '$kode_rel'
       group by 1,2,3,4,5,6,7
       order by A.nomor_daftar";
			
			
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		
		echo "<table class='table table-striped table-bordered data'>";
		echo	"<thead>";
		echo		"<tr>";			
		echo			"<th rowspan='2'>NO</th>";
		echo			"<th rowspan='2'>JENIS DOKUMEN</th>";
		echo			"<th colspan='4'>DOKUMEN PABEAN</th>";
		echo			"<th colspan='3'>INVOICE</th>";
		echo			"<th colspan='3'>LPB</th>";
		echo			"<th rowspan='2'>CUSTOMER</th>";
		echo			"<th rowspan='2'>NAMA BARANG</th>";
		echo			"<th rowspan='2'>KODE HS</th>";
		echo			"<th rowspan='2'>SATUAN</th>";
		echo			"<th rowspan='2'>JUMLAH</th>";
		echo			"<th rowspan='2'>VALAS</th>";
		echo			"<th rowspan='2'>KETERANGAN</th>";
		echo		"</tr>";
		echo		"<tr>";			
		echo			"<th>NOPEN</th>";
		echo			"<th>TGL NOPEN</th>";
		echo			"<th>NOMOR AJU</th>";
		echo			"<th>TGL AJU</th>";
		echo			"<th>NOMOR</th>";
		echo			"<th>TANGGAL</th>";
		echo			"<th>NILAI</th>";
		echo			"<th>ID BOOK</th>";
		echo			"<th>JUMLAH</th>";
		echo			"<th>LOKASI</th>";
		echo		"</tr>";
		echo	"</thead>";
		echo	"<tbody>";
		
		while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
			   $rel = $row['kode_rel'];
		echo		"<tr>";			
		echo			"<td>".$number."</td>";
		echo			"<td>".$jenis."</td>";
		echo			"<td>".$row['nomor_daftar']."</td>";
		echo			"<td>".$row['tanggal_daftar']."</td>";
		echo			"<td>".$row['no_aju']."</td>";
		echo			"<td>".$row['tanggal_aju']."</td>";
		echo			"<td>".$row['invoice']."</td>";
		echo			"<td>".$row['tgl_invoice']."</td>";
		echo			"<td>".number_format($row['tarif'],2)."</td>";
		echo			"<td>".$row['book']."</td>";
		echo			"<td>".$row['jumlah4']."</td>";
		if ($rel == '501600'){
		echo			"<td>GUDANG 4</td>";
		} 
		echo			"<td>PT. CARGILL INDONESIA</td>";
		echo			"<td>".$row['uraian_barang']."</td>";
		echo			"<td>".$row['hs']."</td>";
		echo			"<td>".$row['kemasan']."</td>";
		echo			"<td>".$row['jumlah']."</td>";
		echo			"<td>".$row['currency']."</td>";
		echo			"<td>".$row['keterangan']."</td>";
		echo		"</tr>";
		}
		echo	"</tbody>";
		echo  "</table>";
		
     }
	 


}


?>
</p>
