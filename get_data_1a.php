<?php
require_once('db-inc2.php');
if(isset($_GET["jenis"]))
{

header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
clearstatcache();


	$jenis = $_GET["jenis"];
	$tgl1 = $_GET["tgl1"];
	$tgl2 = $_GET["tgl2"];
	$kode_rel = trim($_GET["kode_rel"]);
	$logincat = trim($_GET["kat"]);
	
	if ($kode_rel == "ALL" ){ $kode_rel = '50%';  }
	
	
 if ( $jenis == "PEMASUKAN" ) {
		   if ($logincat == "PLB-ADMIN"){
		     $sqltext= " select  A.tgl_awal,A.jam_awal,B.batch_no as invoice, inv.nomor_daftar as bc16 , inv.supplier,inv.uraian_negara,
						case when A.kendaraan = 'CONTAINER' THEN A.no_unit else A.nopol end as kendaraan,inv.tanggal_tiba as etad,
						A.nopol, A.seal,E.tgl_in,E.jam_in,substring(inv.nomor_aju,15,15) as nomor_aju,
						inv.tanggal_daftar as tgl_bc16 ,bl.nomor_dokumen as no_bl,bl.tanggal_daftar as tgl_bl, 
						C.nama,inv.nama_pengangkut as vessel ,inv.nomor_voy_flight as voyage,
						sum_varchar( distinct(satuan) ||'<br>' ) as satuan,
						sum_varchar(distinct( split_part(itemname,':',1))||'<br>') as cbarang,  sum(B.jumlah) as jumlah
						from wh_flowcont A 
						join report.plb_flowbrg B on A.id_flowcont = B.id_flowbrg_splitted  
						join v_customer C on A.kode_rel = C.kode_rel
						join wh_cargo D on B.itemcode = D.itemcode
						left join wh_loket E on A.id_loket = E.id_loket
						left join report.plb_documents_in inv on trim(B.batch_no) = trim(inv.nomor_dokumen) and inv.uraian_dokumen = 'INVOICE' 
						left join report.plb_documents_in bl on bl.nomor_daftar = inv.nomor_daftar 
						and inv.tanggal_daftar= bl.tanggal_daftar and bl.uraian_dokumen = 'B/L' 
						where  A.tgl_awal between '$tgl1' and '$tgl2'
						and	a.kode_rel LIKE '$kode_rel'  
						and a.id_aktifitas IN(1,14) 
						group by 1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19
						order by A.tgl_awal, A.jam_awal";
		   }else{
			$sqltext= "select  A.tgl_awal,A.jam_awal,B.batch_no as invoice, inv.nomor_daftar as bc16 , inv.supplier, 
						case when A.kendaraan = 'CONTAINER' THEN A.no_unit else A.nopol end as kendaraan,
						inv.tanggal_daftar as tgl_bc16 ,bl.nomor_dokumen as no_bl,bl.tanggal_daftar as tgl_bl, C.nama,
						sum_varchar( distinct(satuan) ||'<br>' ) as satuan,
						sum_varchar(distinct(split_part(itemname,':',1)) ||'<br>') as cbarang,   sum(B.jumlah) as jumlah
						from wh_flowcont A 
						join report.plb_flowbrg B on A.id_flowcont = B.id_flowbrg_splitted   
						join v_customer C on A.kode_rel = C.kode_rel
						join wh_cargo D on B.itemcode = D.itemcode
						left join report.plb_documents_in inv on trim(B.batch_no) = trim(inv.nomor_dokumen) and inv.uraian_dokumen = 'INVOICE' 
						left join report.plb_documents_in bl on bl.nomor_daftar = inv.nomor_daftar and inv.tanggal_daftar= bl.tanggal_daftar and bl.uraian_dokumen = 'B/L' 				
						where  A.tgl_awal between '$tgl1' and '$tgl2'
						and	a.kode_rel LIKE '$kode_rel' 
						and a.id_aktifitas IN(1,14) 
						group by 1,2,3,4,5,6,7,8,9,10
						order by A.tgl_awal, A.jam_awal";
			  }	
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td><label class='style5'>Container</label></td>";
				echo "    <td><label class='style5'>No. BC 1.6</label></td>";
				echo "    <td><label class='style5'>Tanggal BC 1.6 </label></td>";
				echo "    <td><label class='style5'>No. BL</label></td>";
				echo "    <td><label class='style5'>Tanggal BL</label></td>";
				echo "    <td><label class='style5'>Tanggal Aktifitas</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td><label class='style5'>Nopol IN</label></td>";
				echo "    <td><label class='style5'>Jam IN</label></td>";	
				echo "    <td><label class='style5'>Seal</label></td>";
				echo "    <td><label class='style5'>Jam Act</label></td>";		
				echo "    <td><label class='style5'>Negara Asal</label></td>";
				echo "    <td><label class='style5'>Vessel / Voyage </label></td>";
				echo "    <td><label class='style5'>Etad</label></td>";		
				echo "    <td><label class='style5'>Nomor Aju</label></td>";	
				}
				echo "    <td><label class='style5'>Invoice</label></td>";
				echo "    <td><label class='style5'>Pemilik Barang</label></td>";
				echo "    <td><label class='style5'>Nama Barang</label></td>";
				echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td><label class='style5'>Jumlah</label></td>";
				echo "    <td><label class='style5'>Satuan</label></td>";
				echo "  </tr>";
				echo "</thead>";
				echo "<tbody>";	
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>$row[kendaraan]</label></td>";
				echo "	<td ><label class='style4'>$row[bc16]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_bc16]</label></td>";
				echo "	<td ><label class='style4'>$row[no_bl]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_bl]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_awal]</label></td>";
				if ($logincat == "PLB-ADMIN"){
				 echo "	<td ><label class='style4'>$row[nopol]</label></td>";				
				 echo "	<td ><label class='style4'>$row[jam_in]</label></td>";	
				 echo "	<td ><label class='style4'>$row[seal]</label></td>";						
				 echo "	<td ><label class='style4'>$row[jam_awal]</label></td>";	
				 echo "	<td ><label class='style4'>$row[uraian_negara]</label></td>";				
				 echo "	<td ><label class='style4'>$row[vessel] , $row[voyage] </label></td>";				
				 echo "	<td ><label class='style4'>$row[etad]</label></td>";	
				 echo "	<td ><label class='style4'>$row[nomor_aju]</label></td>";					
					
				}
				echo "	<td ><label class='style4'>$row[invoice]</label></td>";
				echo "	<td ><label class='style4'>$row[nama]</label></label></label></td>";
				echo "	<td ><label class='style4'>$row[cbarang]</label></td>";
				echo "	<td ><label class='style4'>$row[supplier]</label></td>";
				echo "	<td align =right><label class='style4'>$row[jumlah]</label></td>";
				echo "	<td ><label class='style4'>$row[satuan]</label></td>";
		 }
		echo "</tbody>"; 
		echo "</table>";

        pg_free_result($result);
	}
 if ( $jenis == "PENGELUARAN" ) {
 			if ($logincat == "PLB-ADMIN"){

				$sqltext= "select A.tgl_awal,A.jam_awal,B.batch_no as invoice, 
							B.doc_out as doc_out, 
							B.no_doc_out as doc_outnum, 
							B.tgl_doc_out as doc_tgl,
							case when A.kendaraan = 'CONTAINER' THEN A.no_unit else A.nopol end as kendaraan,
							A.nopol, A.jam_akhir,
							sup.nomor_daftar as bc16,sup.tanggal_daftar as tgl_bc16, C.nama,  sup.supplier, 
							sum_varchar( distinct(satuan) ||'<br>' ) as satuan, 
							sum_varchar(distinct(split_part(itemname,':',1)) ||'<br>') as cbarang,sum(B.jumlah) as jumlah
							from wh_flowcont A 
							join report.plb_flowbrg B on A.id_flowcont = B.id_flowbrg_splitted   
							join v_customer C on A.kode_rel = C.kode_rel
							join wh_cargo D on B.itemcode = D.itemcode
							left join report.plb_documents_in sup on trim(B.batch_no) = trim(sup.nomor_dokumen) and sup.uraian_dokumen = 'INVOICE' 
							where B.kode_rel LIKE '$kode_rel'  and A.tgl_awal between  '$tgl1' and '$tgl2'
							and a.id_aktifitas IN(2,16) 
							group by 1,2,3,4,5,6,7,8,9,10,11,12,13
							order by A.tgl_awal, A.jam_awal";
			
			}else{
  
				$sqltext= "select A.tgl_awal,A.jam_awal,B.batch_no as invoice, 
							B.doc_out as doc_out, 
							B.no_doc_out as doc_outnum, 
							B.tgl_doc_out as doc_tgl,
							case when A.kendaraan = 'CONTAINER' THEN A.no_unit else A.nopol end as kendaraan,
							sup.nomor_daftar as bc16,sup.tanggal_daftar as tgl_bc16, C.nama,  sup.supplier, 
							sum_varchar( distinct(satuan) ||'<br>' ) as satuan, 
							sum_varchar(distinct(split_part(itemname,':',1)) ||'<br>') as cbarang,sum(B.jumlah) as jumlah
							from wh_flowcont A 
							join report.plb_flowbrg B on A.id_flowcont = B.id_flowbrg_splitted   
							join v_customer C on A.kode_rel = C.kode_rel
							join wh_cargo D on B.itemcode = D.itemcode
							left join report.plb_documents_in sup on trim(B.batch_no) = trim(sup.nomor_dokumen) and sup.uraian_dokumen = 'INVOICE' 
		
							where B.kode_rel LIKE '$kode_rel'  and A.tgl_awal between  '$tgl1' and '$tgl2'
							and a.id_aktifitas IN(2,16) 
							group by 1,2,3,4,5,6,7,8,9,10,11
							order by A.tgl_awal, A.jam_awal";
				}
				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table width= 100% height=15 border= 1  bgcolor=#0000CC id=data_table >";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td><label class='style5'>Container</label></td>";
				echo "    <td><label class='style5'>No. BC 1.6</label></td>";
				echo "    <td><label class='style5'>Tanggal BC 1.6 </label></td>";
				echo "    <td><label class='style5'>Doc. Out</label></td>";
				echo "    <td><label class='style5'>No. Doc.</label></td>";
				echo "    <td><label class='style5'>Tanggal Doc.</label></td>";
				echo "    <td><label class='style5'>Tanggal Keluar</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "    <td><label class='style5'>Jam Akhir</label></td>";
				echo "    <td><label class='style5'>Nopol</label></td>";				
				}
				echo "    <td><label class='style5'>Invoice</label></td>";
				echo "    <td><label class='style5'>Pemilik Barang</label></td>";
				echo "    <td><label class='style5'>Nama Barang</label></td>";
				echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td><label class='style5'>Jumlah</label></td>";
				echo "    <td><label class='style5'>Satuan</label></td>";
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>$row[kendaraan]</label></td>";
				echo "	<td ><label class='style4'>$row[bc16]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_bc16]</label></td>";
				echo "	<td  align ='center' ><label class='style4'>$row[doc_out]</label></td>";
				echo "	<td ><label class='style4'>$row[doc_outnum]</label></td>";
				echo "	<td ><label class='style4'>$row[doc_tgl]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_awal]</label></td>";
				if ($logincat == "PLB-ADMIN"){
				echo "	<td ><label class='style4'>$row[jam_akhir]</label></td>";
				echo "	<td ><label class='style4'>$row[nopol]</label></td>";			
				}
				echo "	<td ><label class='style4'>$row[invoice]</label></td>";
				echo "	<td ><label class='style4'>$row[nama]</label></td>";
				echo "	<td ><label class='style4'>$row[cbarang]</label></td>";
				echo "	<td ><label class='style4'>$row[supplier]</label></td>";
				echo "	<td align =right><label class='style4'>$row[jumlah]</label></td>";
				echo "	<td ><label class='style4'>$row[satuan]</label></td>";
		 }
		echo "</table>";
        pg_free_result($result);
	}
if ( $jenis == "STOCK" ) {
		$sqltext= "select  split_part(B.itemname,':',1)as cbarang, split_part(B.itemname,':',2) as nbarang, B.nama, B.itemname,
					B.tgl_awal, A.jumlah, B.satuan, inv.supplier,
					case when B.kendaraan = 'CONTAINER' THEN B.no_unit else B.nopol end as kendaraan, B.batch_no as invoice, 
					inv.nomor_daftar as bc16,inv.tanggal_daftar as tgl_bc16 ,bl.nomor_dokumen as no_bl,bl.tanggal_daftar as tgl_bl
					FROM
					(select id_flowbrg_in, sum (qty - qty_out) as jumlah  from v_mutasi
					 where kode_rel  LIKE '$kode_rel'  and tgl_awal <='$tgl1'
					 group by 1)A 
					join
					(select * from report.v_plb_mutasi 
					 where kode_rel LIKE '$kode_rel'   and tgl_awal <='$tgl1'
					 and (id_stok_out is null or id_stok_out ='' )) B
					 on A.id_flowbrg_in = B.id_flowbrg_in
					left join report.plb_documents_in inv on trim(B.batch_no) = trim(inv.nomor_dokumen) and inv.uraian_dokumen = 'INVOICE' 
					left join report.plb_documents_in bl on bl.nomor_daftar = inv.nomor_daftar and inv.tanggal_daftar= bl.tanggal_daftar and bl.uraian_dokumen = 'B/L' 				
					where A.jumlah>0" ;
				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table width= 100% height=15 border= 1  font=8 bgcolor=#0000CC id=data_table >";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td><label class='style5'>Container</label></td>";
				echo "    <td><label class='style5'>No. BC 1.6</label></td>";
				echo "    <td><label class='style5'>Tanggal BC 1.6 </label></td>";
				echo "    <td><label class='style5'>No. BL</label></td>";
				echo "    <td><label class='style5'>Tanggal BL</label></td>";
				echo "    <td><label class='style5'>Tanggal Masuk</label></td>";
				echo "    <td><label class='style5'>Invoice</label></td>";
				echo "    <td><label class='style5'>Pemilik Barang</label></td>";
				echo "    <td><label class='style5'>Nama Barang</label></td>";
				echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td><label class='style5'>Jumlah</label></td>";
				echo "    <td><label class='style5'>Satuan</label></td>";
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>$row[kendaraan]</label></td>";
				echo "	<td ><label class='style4'>$row[bc16]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_bc16]</label></td>";
				echo "	<td ><label class='style4'>$row[no_bl]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_bl]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_awal]</label></td>";
				echo "	<td ><label class='style4'>$row[invoice]</label></td>";
				echo "	<td ><label class='style4'>$row[nama]</label></td>";
				echo "	<td ><label class='style4'>$row[cbarang]</label></td>";
				echo "	<td ><label class='style4'>$row[supplier]</label></td>";
				echo "	<td align =right><label class='style4'>$row[jumlah]</label></td>";
				echo "	<td ><label class='style4'>$row[satuan]</label></td>";

		 }
		echo "</table>";
        pg_free_result($result);
	}



}

if ( $jenis == "MUTASI" ) {
	 $sqltext= "select WH.*,inv.nomor_daftar as bc16, inv.tanggal_daftar as tgl_bc16 ,inv.supplier,
				bl.nomor_dokumen as no_bl,bl.tanggal_daftar as tgl_bl, 
				case when WH.id_stok_out isnull then null else inv_out.nomor_daftar end  as bc28 ,
				case when WH.id_stok_out isnull then null else inv_out.tanggal_daftar end as tgl_bc28
				from(
				select A.id_stok_in,A.id_stok_out,case when kendaraan = 'CONTAINER' then A.no_unit else A.nopol end as jns_kend,
				A.tgl_awal,A.batch_no as invoice,A.nama,A.itemcode, 
				trim(split_part(itemname,':',1)) as cbarang, trim(split_part(itemname,':',2)) as nbarang, satuan , sum(qty) as jumlah,sum(qty_out) as jumlah_out
				from report.v_plb_mutasi  A 
				join wh_book bo on bo.no_book = split_part(A.id_stok_in, '-',1) 
				where case when tgl_in_asal isnull then tgl_awal between  '$tgl1' and '$tgl2'
				else tgl_in_asal BETWEEN  '$tgl1' and '$tgl2' end
				and A.kode_rel  LIKE '$kode_rel'
				group by 1,2,3,4,5,6,7,8,9,10  
				order by nama,id_stok_in,invoice,A.itemcode,id_stok_out nulls first
				) WH
				left join report.plb_documents_in inv on trim(WH.invoice) = trim(inv.nomor_dokumen) and inv.uraian_dokumen = 'INVOICE' 
				left join report.plb_documents_in bl on bl.nomor_daftar = inv.nomor_daftar and inv.tanggal_daftar= bl.tanggal_daftar and bl.uraian_dokumen = 'B/L' 	
				left join report.plb_documents_out inv_out on trim(WH.invoice) = trim(inv_out.nomor_dokumen) and inv_out.uraian_dokumen = 'INVOICE' 
				left join report.plb_documents_out bc on bc.nomor_daftar = inv_out.nomor_daftar and inv_out.tanggal_daftar= bc.tanggal_daftar and bc.uraian_dokumen = 'BC 1.6' " ;
				
		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = 0;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table width= 100% height=15 border= 1  font=8 bgcolor=#0000CC id=data_table >";
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td><label class='style5'>Container</label></td>";
				echo "    <td><label class='style5'>No Bukti Pemasukan</label></td>";
				echo "    <td><label class='style5'>No Bukti Pengeluaran</label></td>";
				echo "    <td><label class='style5'>No. BC 1.6</label></td>";
				echo "    <td><label class='style5'>Tanggal BC 1.6 </label></td>";
				echo "    <td><label class='style5'>No. BC 2.8</label></td>";
				echo "    <td><label class='style5'>Tanggal </label></td>";
				echo "    <td><label class='style5'>Tanggal Aktifitas</label></td>";
				echo "    <td><label class='style5'>Invoice</label></td>";
				echo "    <td><label class='style5'>Pemilik Barang</label></td>";
				echo "    <td><label class='style5'>Nama Barang</label></td>";
				echo "    <td><label class='style5'>Shipper</label></td>";
				echo "    <td><label class='style5'>Jumlah IN</label></td>";
				echo "    <td><label class='style5'>Jumlah OUT</label></td>";
				echo "    <td><label class='style5'>Satuan</label></td>";
				echo "  </tr>";
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</td></label>";
				echo "	<td ><label class='style4'>$row[jns_kend]</td></label>";
				echo "	<td ><label class='style4'>$row[id_stok_in]</td></label>";
				echo "	<td ><label class='style4'>$row[id_stok_out]</td></label>";
				echo "	<td ><label class='style4'>$row[bc16]</td></label>";
				echo "	<td ><label class='style4'>$row[tgl_bc16]</td></label>";
				echo "	<td ><label class='style4'>$row[bc28]</td></label>";
				echo "	<td ><label class='style4'>$row[tgl_bc28]</td></label>";
				echo "	<td ><label class='style4'>$row[tgl_awal]</td></label>";
				echo "	<td ><label class='style4'>$row[invoice]</td></label>";
				echo "	<td ><label class='style4'>$row[nama]</td></label>";
				echo "	<td ><label class='style4'>$row[cbarang]</td></label>";
				echo "	<td ><label class='style4'>$row[supplier]</td></label>";
				echo "	<td align =center ><label class='style4'>$row[jumlah]</td></label>";
				echo "	<td align =center><label class='style4'>$row[jumlah_out]</td></label>";
				echo "	<td ><label class='style4'>$row[satuan]</td></label>";

		 }
		echo "</table>";
        pg_free_result($result);
}

if ( $jenis == "PLAN" ){
      $sqltext="select C.no_unit,A.uraian_dokumen, A.nomor_dokumen as invoice , A.tanggal_dokumen as tgl_invoice, A.nama_pengangkut as vessel, 
				A.nomor_voy_flight as voyage, A.nomor_daftar as bc16 , A.tanggal_daftar as tgl_bc16, A.tanggal_tiba as etad,
				A.uraian_negara, A.supplier,B.nomor_dokumen as no_bl, B.tanggal_dokumen as tgl_bl 
				from  report.plb_documents_in A
				left join report.plb_documents_in B on B.nomor_daftar = A.nomor_daftar and A.tanggal_daftar= B.tanggal_daftar and B.uraian_dokumen = 'B/L' 
				left join report.plb_documents_incont C on  C.tanggal_dokumen = A.tanggal_dokumen and A.nomor_dokumen= C.nomor_dokumen
				where A.uraian_dokumen = 'INVOICE' 
				and ((A.tanggal_dokumen BETWEEN  '$tgl1' and '$tgl2' ) or (B.tanggal_dokumen BETWEEN  '$tgl1' and '$tgl2' )
					or (B.tanggal_aju BETWEEN  '$tgl1' and '$tgl2')
					or (A.tanggal_tiba BETWEEN  '$tgl1' and 
					case when cast('$tgl2' as date) >= current_date then cast('$tgl2' as date) + 90  else cast('$tgl2' as date)  end )
					) " ;

		  $result = pg_query($db2_, $sqltext);
		  $baris  = pg_num_rows($result);
		  $number = $startRec;
		  if ($baris > 0 ) {
		     	echo "<a id=dlink  style=display:none;> </a> <br>";
		        echo "<input type=button id=btnexport value='Export to Excel' onclick=exportXLS(); />";
				echo "<table  id='data_table' width= 100% height=15 border=1  bgcolor=#0000CC >";
				echo "<thead>";	
				echo "    <tr style=background:#0099FF;> ";
				echo "    <td><label class='style5'>NO</label></td>";
				echo "    <td><label class='style5'>Container</label></td>";
				echo "    <td><label class='style5'>No. BC 1.6</label></td>";
				echo "    <td><label class='style5'>Tanggal BC 1.6 </label></td>";
				echo "    <td><label class='style5'>No. BL</label></td>";
				echo "    <td><label class='style5'>Tanggal BL</label></td>";
				echo "    <td><label class='style5'>Invoice</label></td>";
				echo "    <td><label class='style5'>Negara Asal</label></td>";	
				echo "    <td><label class='style5'>Vessel, Voyage</label></td>";			
				echo "    <td><label class='style5'>Tgl Eta</label></td>";			
				echo "    <td><label class='style5'>Shipper</label></td>";
				echo "  </tr>";
				echo "</thead>";
				echo "<tbody>";	
		  }
         while ($row = pg_fetch_assoc($result)) {
			   $number = $number +1;
				if (($number % 2) == 1){
					echo "    <tr style=background:#FFFFCC;> ";
					}else{
					echo "    <tr style=background:#99CCFF;> ";
					}
				echo "	<td ><label class='style4'>$number</label></td>";
				echo "	<td ><label class='style4'>$row[no_unit]</label></td>";
				echo "	<td ><label class='style4'>$row[bc16]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_bc16]</label></td>";
				echo "	<td ><label class='style4'>$row[no_bl]</label></td>";
				echo "	<td ><label class='style4'>$row[tgl_bl]</label></td>";
				echo "	<td ><label class='style4'>$row[invoice]</label></td>";
				echo "	<td ><label class='style4'>$row[uraian_negara]</label></td>";
				echo "	<td ><label class='style4'>$row[vessel],  $row[voyage]  </label></td>";
				echo "	<td ><label class='style4'>$row[etad]</label></td>";
				echo "	<td ><label class='style4'>$row[supplier]</label></td>";
		 }
		echo "</tbody>"; 
		echo "</table>";

        pg_free_result($result);


}


?>
</p>
