<?php
session_start();
$_SESSION['MENU'] = 'DOCUMENT';
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}
$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

include 'path.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title><?= TITLE_APP ?></title>
    <link rel="shortcut icon" href="<?= IMAGES_PATH ?>/icons/logoijs.jpg">

    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.css">
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/DataTables/media/css/jquery.dataTables.css">
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/DataTables/media/css/dataTables.bootstrap.css">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap-datetimepicker.min.css" media="screen">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap.min.css">

    <!-- start:bootstrap v3.2.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.min.css">
    <!-- start:font awesome v4.1.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/font-awesome.min.css">
    <!-- start:bootstrap reset -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap-reset.css">
    <!-- start:style arjuna -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/arjuna.css">
    <script src="<?= ASSETS_PATH ?>/js/jquery-3.1.1.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= ASSETS_PATH ?>/DataTables/media/js/jquerydt.js"></script>

    <script src="<?= JS_PATH ?>/jquery.js"></script>
    <script src="<?= JS_PATH ?>/tableToExcel.js"></script>
    <script src="<?= JS_PATH ?>/bootstrap.min.js"></script>
    <script type="text/javascript" src="<?= JS_PATH ?>/jquery-1.8.3.min.js" charset="UTF-8"></script>

    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/style-view.css">
    <script src="bootcode.js"></script>

    <script language="javascript">
    var ajaxRequest;

    function getAjax() //fungsi untuk mengecek AJAX pada browser
    {
        try {
            ajaxRequest = new XMLHttpRequest();
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {
                    alert("Your browser broke!");
                    return false;
                }
            }
        }
    }

    function showData() {
        var no_aju = document.getElementById("no_aju").value;

        var select = document.getElementById("jenis");
        var tipe = select.options[select.selectedIndex].value;

        var select = document.getElementById("data");
        var data = select.options[select.selectedIndex].value;

        logincat = document.getElementById("logincat").value;

        document.getElementById("form-msg").innerHTML = "<img src='progress.gif'>";
        document.getElementById("judul").style.display = 'none';
        document.getElementById("result").style.display = 'none';
        getAjax();

        ajaxRequest.open("GET", "model/get_data_document.php?no_aju=" + no_aju + "&tipe=" + tipe + "&data=" + data);

        ajaxRequest.onreadystatechange = function() {

            document.getElementById("result").style.display = '';
            document.getElementById("result").innerHTML = ajaxRequest.responseText;
            var gagal = ajaxRequest.responseText
            if (ajaxRequest) {
                document.getElementById("form-msg").innerHTML = "";

            }

        }
        ajaxRequest.send(null);
    }

    function jenisLap() {

        var select = document.getElementById("jenis-laporan");
        var status = select.options[select.selectedIndex].value;
        //document.getElementById("jenis-laporan").value = document.getElementById("data-laporan").value ;

        if (status == 'OUT') {
            document.getElementById("pengangkut").style.display = 'none';
            document.getElementById("voy-flight").style.display = 'none';
            document.getElementById("tanggal-tiba").style.display = 'none';
            document.getElementById("negara").style.display = 'none';
            document.getElementById("suplier").style.display = 'none';
            document.getElementById("harga").style.display = 'none';
            document.getElementById("curency").style.display = 'none';
            document.getElementById("kemasan").style.display = '';
        } else if (status == 'OUT27' || status == 'OUT41') {
            document.getElementById("pengangkut").style.display = 'none';
            document.getElementById("voy-flight").style.display = 'none';
            document.getElementById("tanggal-tiba").style.display = 'none';
            document.getElementById("negara").style.display = 'none';
            document.getElementById("suplier").style.display = 'none';
            document.getElementById("harga").style.display = 'none';
            document.getElementById("curency").style.display = 'none';
            document.getElementById("kemasan").style.display = '';
        } else {
            document.getElementById("pengangkut").style.display = '';
            document.getElementById("voy-flight").style.display = '';
            document.getElementById("tanggal-tiba").style.display = '';
            document.getElementById("negara").style.display = '';
            document.getElementById("suplier").style.display = '';
            document.getElementById("harga").style.display = '';
            document.getElementById("curency").style.display = '';
            document.getElementById("kemasan").style.display = '';
            document.getElementById("cif").style.display = 'none';
            document.getElementById("cif-rupiah").style.display = 'none';

        }
    }
    </script>


</head>

<body class="cl-default fixed">
    <!-- start:navbar top -->
    <header class="header">
        <!-- <a href="home.php" class="logo">Indra Jaya Swastika</a> -->
        <header class="header"><?php include("top_nav.php") ?></header>
    </header>
    <!-- end:navbar top -->

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas"><?php include("left_menu.php") ?></aside>

        <aside class="right-side">
            <section class="content">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="#"> Laporan</a></li>
                    <li><a href="#"> PLB Document</a></li>
                </ol>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading hidden-print"><span class="glyphicon glyphicon-tasks"
                                    aria-hidden="true"></span> PLB DOCUMENT</div>
                            <!-- Modal -->
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
                                aria-labelledby="myModalTitle">
                                <div class="modal-dialog " role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalTitle">Tambah Dokumen</h4>
                                            <form class="form-horizontal" role="form" method="post"
                                                action="model/get_save_document.php">
                                                <div class="form-group">
                                                    <div class="col-md-3 col-xs-5">
                                                        <select class="form-control input-sm" name="jenis_laporan"
                                                            id="jenis-laporan" onchange="jenisLap();">
                                                            <option value='IN'>IN 1.6</option>
                                                            <option value='OUT27'>OUT BC 27</option>
                                                            <option value='OUT'>OUT BC 28</option>
                                                            <option value='IN40'>IN BC 40</option>
                                                            <option value='OUT41'>OUT BC 41</option>
                                                        </select>
                                                    </div>
                                                </div>
                                        </div>

                                        <div class="modal-body" id="myModalBody">
                                            <!-- Isi Modal -->
                                            <div class="container">

                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table table-striped">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" value="INVOICE"
                                                                                class="form-control" name="u_dokumen"
                                                                                id="u-dokumen">
                                                                            <input type="hidden" class="form-control"
                                                                                name="data_laporan" id="data-laporan">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Nomor Invoice"
                                                                                id="no-dokumen" name="no_dokumen">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <div class="controls input-append date form_date"
                                                                                data-date=""
                                                                                data-date-format="dd MM yyyy"
                                                                                data-link-field="dtp_input2"
                                                                                data-link-format="yyyy-mm-dd">
                                                                                <input type="text" class="form-control"
                                                                                    id="tanggal-dokumen"
                                                                                    placeholder="Tanggal Invoice"
                                                                                    name="tanggal_dokumen">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="no-daftar" placeholder="Nopen"
                                                                                name="no_daftar">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <div class="controls input-append date form_date"
                                                                                data-date=""
                                                                                data-date-format="dd MM yyyy"
                                                                                data-link-field="dtp_input2"
                                                                                data-link-format="yyyy-mm-dd">
                                                                                <input type="text" class="form-control"
                                                                                    id="tanggal-daftar"
                                                                                    placeholder="Tanggal Nopen"
                                                                                    name="tanggal_daftar">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="hs" value="0" placeholder="HS"
                                                                                name="hs">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="jumlah" placeholder="Jumlah"
                                                                                name="jumlah">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="kemasan" placeholder="Kemasan"
                                                                                name="kemasan">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="no-aju" placeholder="Nomor Aju"
                                                                                name="no_aju">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <div class="controls input-append date form_date"
                                                                                data-date=""
                                                                                data-date-format="dd MM yyyy"
                                                                                data-link-field="dtp_input2"
                                                                                data-link-format="yyyy-mm-dd">
                                                                                <input type="text" class="form-control"
                                                                                    id="tanggal-aju"
                                                                                    placeholder="Tanggal Aju"
                                                                                    name="tanggal_aju">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="cif" value="0" placeholder="CIF"
                                                                                name="cif">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="cif-rupiah" value="0"
                                                                                placeholder="CIF Rupiah"
                                                                                name="cif_rupiah">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <td>
                                                                    <div class="col-xs-12">
                                                                        <input type="text" class="form-control"
                                                                            id="pengangkut"
                                                                            placeholder="Nama Pengangkut"
                                                                            name="pengangkut">
                                                                    </div>
                                                                </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="voy-flight"
                                                                                placeholder="Nomor Voy Flight"
                                                                                name="voy_flight">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <div class="controls input-append date form_date"
                                                                                data-date=""
                                                                                data-date-format="dd MM yyyy"
                                                                                data-link-field="dtp_input2"
                                                                                data-link-format="yyyy-mm-dd">
                                                                                <input type="text" class="form-control"
                                                                                    id="tanggal-tiba"
                                                                                    placeholder="Tanggal Tiba"
                                                                                    name="tanggal_tiba">
                                                                            </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="negara" placeholder="Uraian Negara"
                                                                                name="negara">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="suplier" placeholder="Supplier"
                                                                                name="suplier">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="harga" value="0" placeholder="Harga"
                                                                                name="harga">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                id="curency" placeholder="Currency"
                                                                                name="curency">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="submit" class="btn btn-warning" id="submit"
                                                    name="simpan">Simpan</button>
                                                </form>
                                                <button type="button" class="btn btn-default" id="submit"
                                                    onClick="saveData();" data-dismiss="modal">Cancel</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="box">
                                <div class="panel-body" id="pnl-filter">
                                    <form class="form-horizontal" role="form" method="post"
                                        enctype="multipart/form-data">
                                        <input type="hidden" id="username" name="username"
                                            value="<?php echo $username ?>">
                                        <input type="hidden" id="logincat" name="logincat"
                                            value="<?php echo $logincat ?>">

                                        <div class="form-group">
                                            <label class="control-label col-xs-3">Nomor Aju/Nopen</label>
                                            <div class="col-md-3 col-xs-6">
                                                <input type="text" class="form-control" name="no_aju" value=""
                                                    id="no_aju">

                                            </div>
                                            <div class="col-md-3 col-xs-6">
                                                <select class="form-control input-sm" name="data" id="data">
                                                    <option value='AJU'>AJU</option>
                                                    <option value='NOPEN'>NOPEN</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-xs-3 control-label">Jenis</label>
                                            <div class="col-md-3 col-xs-6">
                                                <select class="form-control input-sm" name="jenis" id="jenis">
                                                    <option value='IN'>IN BC 1.6 / BC 4.0</option>
                                                    <option value='OUTBC27'>OUT BC 2.7</option>
                                                    <option value='OUTBC'>OUT BC 2.8</option>
                                                    <option value='OUTBC41'>OUT BC 4.1</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-6 col-xs-offset-3">
                                                <button class="form-control btn-primary" id="submit" type="button"
                                                    onClick="showData();">Cek</button>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-6 col-xs-offset-3">
                                                <button class="form-control btn-success" id="submit" type="button"
                                                    data-toggle="modal" data-target="#myModal">Tambah</button>
                                            </div>
                                        </div>

                                    </form>


                                </div>
                                <div class="form-group">
                                    <div class="col-xs-9 col-xs-offset-3" id="form-msg"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-xs-9" id="judul">
                                </div>
                            </div>

                        </div>

                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="row">
                                <div class="col-sm"></div>
                            </div>
                            <div class="table-responsive" id="result">
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </aside>
    </div>

    <script src="<?= ASSETS_PATH ?>/js/jquery.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/arjuna.js"></script>
    <script src="<?= ASSETS_PATH ?>/DataTables/media/js/jquery.dataTables.js"></script>
    <script src="<?= JS_PATH ?>/bootstrap-datetimepicker.js"></script>

    <script language="javascript">
    $(function Datepicker() {
        $('#tanggal-dokumen').datetimepicker({
            //language:  'fr',
            format: 'yyyy-mm-dd',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });

        $('#tanggal-daftar').datetimepicker({
            //language:  'fr',
            format: 'yyyy-mm-dd',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });

        $('#tanggal-aju').datetimepicker({
            //language:  'fr',
            format: 'yyyy-mm-dd',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });

        $('#tanggal-tiba').datetimepicker({
            //language:  'fr',
            format: 'yyyy-mm-dd',
            weekStart: 1,
            todayBtn: 1,
            autoclose: 1,
            todayHighlight: 1,
            startView: 2,
            minView: 2,
            forceParse: 0
        });
    });
    </script>


</body>

</html>