<?php
session_start();
$_SESSION['MENU'] = 'USER';
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}
$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

include 'path.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title><?= TITLE_APP ?></title>
    <link rel="shortcut icon" href="<?= IMAGES_PATH ?>/icons/logoijs.jpg">

    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap-datetimepicker.min.css" media="screen">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap.min.css">

    <!-- start:bootstrap v3.2.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.min.css">
    <!-- start:font awesome v4.1.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/font-awesome.min.css">
    <!-- start:bootstrap reset -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap-reset.css">
    <!-- start:style arjuna -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/arjuna.css">

    <script src="<?= ASSETS_PATH ?>/js/jquery-3.1.1.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>


</head>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $jenis = $_POST['jenis_act'];
    $tgl1 = $_POST['tglan1'];
    $tgl2 = $_POST['tglan2'];
    $kode_rel = TRIM($_POST['cust']);
}
?>

<body class="cl-default fixed">
    <!-- start:navbar top -->
    <header class="header">
        <!-- <a href="home.php" class="logo">Indra Jaya Swastika</a> -->
        <header class="header"><?php include("top_nav.php") ?></header>
    </header>
    <!-- end:navbar top -->
    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas">
            <?php include("left_menu.php") ?>
        </aside>
        <aside class="right-side">
            <section class="content">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="#"> User</a></li>
                    <li><a href="#"> User Manual</a></li>
                </ol>
                <!-- Modal 1 -->
                <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
                    <div class="modal-dialog " role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Proses Login</h4>
                            </div>
                            <div class="modal-body">
                                <p>1. Masuk ke web : plb.ijs.co.id</p>
                                <p>2. Tampil gambar dibawah</p>
                                <img src="<?= ASSETS_PATH ?>/image/login.jpg" class="img-rounded" alt="Gambar Login"
                                    width="304" height="236">
                                <p>3. Masukkan Username dan Password</p>
                                <p>4. Tekan Login Button</p>
                                <p>5. Ketika Username dan Password sesuai maka akan masuk ke halaman utama</p>
                                <img src="<?= ASSETS_PATH ?>/image/home.jpg" class="img-rounded" alt="Gambar Login"
                                    width="380" height="250">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Modal footer -->

                <!-- Modal 2 -->
                <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
                    <div class="modal-dialog " role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Laporan Pemasukan</h4>
                            </div>
                            <div class="modal-body">
                                <p>1. Masukkan inputan data filter sesuai yang kita inginkan</p>
                                <img src="<?= ASSETS_PATH ?>/image/home2.jpg" class="img-rounded" alt="Gambar Login"
                                    width="410" height="280">
                                <p>2. Fitur Jenis Laporan</p>
                                <img src="<?= ASSETS_PATH ?>/image/jenis_laporan.jpg" class="img-rounded"
                                    alt="Gambar Login" width="304" height="90">
                                <p>
                                    - CY IN : Pemasukan barang dari import menggunakan BC 1.6 tanpa dilakukan stripping
                                    </br>
                                    - BC 1.6 : Pemasukan barang dari import </br>
                                    - BC 4.0 : Pemasukan barang dari local </br>
                                    - BC 2.7 : Pemasukan barang dari TPB lainnya ( Kawasan Berikat )
                                </p>
                                <p>3. Pilih Tanggal</p>
                                <img src="<?= ASSETS_PATH ?>/image/tanggal.jpg" class="img-rounded" alt="Gambar Login"
                                    width="304" height="90">
                                <p>4. Pilih Pemilik Barang</p>
                                <img src="<?= ASSETS_PATH ?>/image/customer.jpg" class="img-rounded" alt="Gambar Login"
                                    width="410" height="280">
                                <p>5. Tekan Submit maka akan muncul data yang telah difilter</p>
                                <img src="<?= ASSETS_PATH ?>/image/hasil.jpg" class="img-rounded" alt="Gambar Login"
                                    width="530" height="310">
                                <p>6. Fungsi Search : Untuk mencari barang yang di inginkan</p>
                                <p>7. Fungsi show : Untuk menampilkan berapa laporan yang diinginkan</p>
                                <img src="<?= ASSETS_PATH ?>/image/search1.jpg" class="img-rounded" alt="Gambar Login"
                                    width="150" height="70">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->

                <!-- Modal 3 -->
                <div class="modal fade" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
                    <div class="modal-dialog " role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Laporan Pengeluaran</h4>
                            </div>
                            <div class="modal-body">
                                <p>1. Masukkan inputan data filter sesuai yang kita inginkan</p>
                                <img src="<?= ASSETS_PATH ?>/image/pengeluaran.jpg" class="img-rounded"
                                    alt="Gambar Login" width="410" height="280">
                                <p>2. Fitur Jenis Laporan</p>
                                <img src="<?= ASSETS_PATH ?>/image/jenis_laporan2.jpg" class="img-rounded"
                                    alt="Gambar Login" width="304" height="90">
                                <p>
                                    - BC 2.7 : Pengeluaran Barang ke TPB lainnya ( Kawasan Berikat ) <br>
                                    - BC 2.8 : Pengeluaran Barang Import ke umum <br>
                                    - BC 4.1 : Pengeluaran Barang Asal lokal <br>
                                    - BC 3.0 : Pengeluaran Barang untuk ekspor
                                </p>
                                <p>3. Pilih Tanggal</p>
                                <img src="<?= ASSETS_PATH ?>/image/tanggal.jpg" class="img-rounded" alt="Gambar Login"
                                    width="304" height="90">
                                <p>4. Pilih Pemilik Barang</p>
                                <img src="<?= ASSETS_PATH ?>/image/customer.jpg" class="img-rounded" alt="Gambar Login"
                                    width="410" height="280">
                                <p>5. Tekan Submit maka akan muncul data yang telah difilter</p>
                                <img src="<?= ASSETS_PATH ?>/image/hasil2.jpg" class="img-rounded" alt="Gambar Login"
                                    width="530" height="310">
                                <p>6. Fungsi Search : Untuk mencari barang yang di inginkan</p>
                                <p>7. Fungsi show : Untuk menampilkan berapa laporan yang diinginkan</p>
                                <img src="<?= ASSETS_PATH ?>/image/search1.jpg" class="img-rounded" alt="Gambar Login"
                                    width="150" height="70">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->

                <!-- Modal 4 -->
                <div class="modal fade" id="myModal4" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
                    <div class="modal-dialog " role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Laporan Mutasi</h4>
                            </div>
                            <div class="modal-body">
                                <p>1. Masukkan inputan data filter sesuai yang kita inginkan</p>
                                <img src="<?= ASSETS_PATH ?>/image/mutasi.jpg" class="img-rounded" alt="Gambar Login"
                                    width="410" height="280">
                                <p>2. Pilih Tanggal</p>
                                <img src="<?= ASSETS_PATH ?>/image/tanggal.jpg" class="img-rounded" alt="Gambar Login"
                                    width="304" height="90">
                                <p>3. Pilih Pemilik Barang</p>
                                <img src="<?= ASSETS_PATH ?>/image/customer.jpg" class="img-rounded" alt="Gambar Login"
                                    width="410" height="280">
                                <p>4. Tekan Submit maka akan muncul data yang telah difilter</p>
                                <img src="<?= ASSETS_PATH ?>/image/hsl_mutasi.jpg" class="img-rounded"
                                    alt="Gambar Login" width="530" height="310">
                                <p>5. Fungsi ekport to excel : Untuk merubah laporan dalam bentuk excel</p>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->

                <!-- Modal 5 -->
                <div class="modal fade" id="myModal5" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
                    <div class="modal-dialog " role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Laporan Stock</h4>
                            </div>
                            <div class="modal-body">
                                <p>1. Masukkan inputan data filter sesuai yang kita inginkan</p>
                                <img src="<?= ASSETS_PATH ?>/image/stok.jpg" class="img-rounded" alt="Gambar Login"
                                    width="410" height="280">
                                <p>2. Pilih Tanggal</p>
                                <img src="<?= ASSETS_PATH ?>/image/tgl_stok.jpg" class="img-rounded" alt="Gambar Login"
                                    width="304" height="90">
                                <p>4. Pilih Pemilik Barang</p>
                                <img src="<?= ASSETS_PATH ?>/image/customer.jpg" class="img-rounded" alt="Gambar Login"
                                    width="410" height="280">
                                <p>5. Tekan Submit maka akan muncul data yang telah difilter</p>
                                <img src="<?= ASSETS_PATH ?>/image/hsl_stok.jpg" class="img-rounded" alt="Gambar Login"
                                    width="530" height="310">
                                <p>6. Fungsi Search : Untuk mencari barang yang di inginkan</p>
                                <p>7. Fungsi show : Untuk menampilkan berapa laporan yang diinginkan</p>
                                <img src="<?= ASSETS_PATH ?>/image/search1.jpg" class="img-rounded" alt="Gambar Login"
                                    width="150" height="70">
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading hidden-print"><span class="glyphicon glyphicon-tasks"
                                    aria-hidden="true"></span> USER MANUAL IT INVENTORY</div>
                            <div class="box">
                                <hr>
                                <div class="panel-body" id="pnl-filter">
                                    <form class="form-horizontal" role="form">

                                        <div class="form-group">
                                            <p><a data-toggle="modal" href="" data-target="#myModal1"><span
                                                        class="glyphicon glyphicon-ok">
                                                        Login</span></a></p>

                                        </div>

                                        <div class="form-group">
                                            <p><a data-toggle="modal" href="" data-target="#myModal2"><span
                                                        class="glyphicon glyphicon-ok">
                                                        Laporan
                                                        Pemasukan</span></a></p>
                                        </div>

                                        <div class="form-group">
                                            <p><a data-toggle="modal" href="" data-target="#myModal3"><span
                                                        class="glyphicon glyphicon-ok">
                                                        Laporan
                                                        Pengeluaran</span></a></p>
                                        </div>

                                        <div class="form-group">
                                            <p><a data-toggle="modal" href="" data-target="#myModal4"><span
                                                        class="glyphicon glyphicon-ok"> Laporan
                                                        Mutasi</span></a></p>
                                        </div>

                                        <div class="form-group">
                                            <a data-toggle="modal" href="" data-target="#myModal5"><span
                                                    class="glyphicon glyphicon-ok"> Laporan
                                                    Stock</span></a>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-8">
                                                <a href="user_m.pdf"><button class="form-control btn-success" id="userm"
                                                        type="button">
                                                        Download</button></a>
                                            </div>
                                        </div>

                                    </form>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </aside>
    </div>

    <script src="<?= ASSETS_PATH ?>/js/jquery.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/arjuna.js"></script>

</body>

</html>