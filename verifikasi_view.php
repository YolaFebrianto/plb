<?php
if (isset($_POST['tglan1'])  && isset($_POST['tglan2'])) {

    require_once('db-inc2.php');

    $tgl1 = $_POST['tglan1'];
    $tgl2 = $_POST['tglan2'];
    $jenis = $_POST['jenis_act'];
    $kode_rel = TRIM($_POST['cust']);
    if ($kode_rel == "ALL") {
        $kode_rel = '50%';
    }
    $startRec = 0;

    if ($jenis == 'IN') {

        $sqltext = "select A.no_aju,A.tgl_aju,A.no_bc_16,A.tgl_bc_16,
       case when A.verified = true then 'Terkunci' else 'Terbuka' end as verified,B.nama,
       sum_varchar (distinct trim(A.batch_no) ||'<br />') as invoice,
       sum_varchar (distinct trim(A.exim) ||'<br />') as exim
       from report.plb_flowbrg A
	   left join v_customer B on A.kode_rel = B.kode_rel
       where tgl_bc_16 between '$tgl1' and '$tgl2'
       and verified = false
	   and A.kode_rel like '$kode_rel'
       group by 1,2,3,4,5,6";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th rowspan='2'>NOMOR AJU</th>";
        echo            "<th rowspan='2'>TANGGAL AJU</th>";
        echo            "<th rowspan='2'>NOPEN</th>";
        echo            "<th rowspan='2'>TANGGAL NOPEN</th>";
        echo            "<th rowspan='2'>PEMILIK BARANG</th>";
        echo            "<th rowspan='2'>STATUS</th>";
        echo            "<th rowspan='2'>INVOICE</th>";
        echo            "<th rowspan='2'>EXIM</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = isset($row['kode_rel']);
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['no_aju'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_aju'])) . "</td>";
            echo            "<td>" . $row['no_bc_16'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_bc_16'])) . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['verified'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . $row['exim'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    } else {
        $sqltext = "select A.no_aju_out,A.tgl_aju_out,A.no_doc_out,A.tgl_doc_out,A.doc_out,
		  case when A.verified = true then 'Terkunci' else 'Terbuka' end as verified,B.nama,
       sum_varchar (distinct trim(A.batch_no) ||'<br />') as invoice,
       sum_varchar (distinct trim(A.exim) ||'<br />') as exim
       from report.plb_flowbrg A
	   left join v_customer B on A.kode_rel = B.kode_rel
       where tgl_doc_out between '$tgl1' and '$tgl2'
       and verified = false
	   and A.kode_rel like '$kode_rel'
       group by 1,2,3,4,5,6,7";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<table class='table table-striped table-bordered data'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th rowspan='2'>NO</th>";
        echo            "<th rowspan='2'>NOMOR AJU</th>";
        echo            "<th rowspan='2'>TANGGAL AJU</th>";
        echo            "<th rowspan='2'>NOPEN</th>";
        echo            "<th rowspan='2'>TANGGAL NOPEN</th>";
        echo            "<th rowspan='2'>JENIS DOKUMEN</th>";
        echo            "<th rowspan='2'>PEMILIK BARANG</th>";
        echo            "<th rowspan='2'>STATUS</th>";
        echo            "<th rowspan='2'>INVOICE</th>";
        echo            "<th rowspan='2'>EXIM</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            $rel = isset($row['kode_rel']);
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['no_aju_out'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_aju_out'])) . "</td>";
            echo            "<td>" . $row['no_doc_out'] . "</td>";
            echo            "<td>" . date('d-m-Y', strtotime($row['tgl_doc_out'])) . "</td>";
            echo            "<td>BC " . $row['doc_out'] . "</td>";
            echo            "<td>" . $row['nama'] . "</td>";
            echo            "<td>" . $row['verified'] . "</td>";
            echo            "<td>" . $row['invoice'] . "</td>";
            echo            "<td>" . $row['exim'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
    }
}