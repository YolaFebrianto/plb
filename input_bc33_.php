<?php
session_start();
$_SESSION['MENU'] = 'TRANSACTION';
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}

$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

include 'path.php';
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title><?= TITLE_APP ?></title>
    <link rel="shortcut icon" href="<?= IMAGES_PATH ?>/icons/logoijs.jpg">

    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha256-k2/8zcNbxVIh5mnQ52A0r3a6jAgMGxFJFE2707UxGCk= sha512-ZV9KawG2Legkwp3nAlxLIVFudTauWuBpC10uEafMHYL0Sarrz5A7G79kXh5+5+woxQ5HM559XX2UZjMJ36Wplg=="
        crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap-datetimepicker.min.css" media="screen">
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

    <!-- start:bootstrap v3.2.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.min.css">
    <!-- start:font awesome v4.1.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/font-awesome.min.css">
    <!-- start:bootstrap reset -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap-reset.css">
    <!-- start:style arjuna -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/arjuna.css">

    <script src="<?= ASSETS_PATH ?>/js/jquery-3.1.1.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>

    <script type="text/javascript" src="<?= ASSETS_PATH ?>/DataTables/media/js/jquerydt.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript" src="jqueryw.js"></script>


</head>

<body class="cl-default fixed" style="font-family: 'Open Sans', sans-serif;">
    <!-- start:navbar top -->
    <header class="header">
        <!-- <a href="home.php" class="logo">Indra Jaya Swastika</a> -->
        <header class="header"><?php include("top_nav.php") ?></header>
    </header>
    <!-- end:navbar top -->

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas">
            <?php include("left_menu.php") ?>
        </aside>
        <aside class="right-side">
            <section class="content">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="#"> BC</a></li>
                    <li><a href="#"> BC 3.3</a></li>
                </ol>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">
                            <h4>BC 3.3 Form</h4>

                            <a target="_blank" href="cek_komoditi.php"><button type="button"
                                    class="btn btn-success btn-xs">Cek
                                    Komoditi</button></a>
                            <hr class="featurette-divider">

                            <!---------------------------------------------- MODAL DETIL ------------------------------------------------>
                            <div class="modal fade" id="myModal" tabindex="-1" role="dialog"
                                aria-labelledby="myModalTitle">
                                <div class="modal-dialog " role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalTitle">Tambah Dokumen</h4>
                                            <form class="form-horizontal frm-dtl" method="post">
                                        </div>

                                        <div class="modal-body" id="myModalBody">
                                            <!-- Isi Modal -->
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table table-striped">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-xs-8">
                                                                                <select class="form-control input-sm"
                                                                                    name="jenis_laporan"
                                                                                    id="jenis_laporan">
                                                                                    <option value='INVOICE'>INVOICE
                                                                                    </option>
                                                                                    <option value='PACKING LIST'>PACKING
                                                                                        LIST</option>
                                                                                    <option value='SURAT JALAN'>SURAT
                                                                                        JALAN</option>
                                                                                    <option value='LAINNYA'>LAINNYA
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" class="form-control"
                                                                            name="no_ajub" id="no_ajub">
                                                                        <input type="hidden" class="form-control"
                                                                            name="aksi" id="aksi" value="save-dtl">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Nomor Dokumen" id="no_dok"
                                                                                name="no_dok">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <div class="controls input-append date form_date"
                                                                                data-date=""
                                                                                data-date-format="dd MM yyyy"
                                                                                data-link-field="dtp_input2"
                                                                                data-link-format="yyyy-mm-dd">
                                                                                <input type="text" class="form-control"
                                                                                    id="tgl_dok"
                                                                                    placeholder="Tanggal Dokumen"
                                                                                    name="tgl_dok">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-info simpan-dtl" id="submit"
                                                    name="simpan">Simpan</button>
                                                </form>
                                                <button type="button" class="btn btn-default" id="submit"
                                                    data-dismiss="modal">Close</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!------------------ END MODAL DETIL --------------------------->

                            <!---------------------------------------------- MODAL UPD DETIL ------------------------------------------------>
                            <div class="modal fade" id="upd_dtl" tabindex="-1" role="dialog"
                                aria-labelledby="myModalTitle">
                                <div class="modal-dialog " role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalTitle">Ubah Dokumen</h4>
                                            <form class="form-horizontal frm-upd-dtl" method="post">
                                        </div>

                                        <div class="modal-body" id="myModalBody">
                                            <!-- Isi Modal -->
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table table-striped">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-xs-8">
                                                                                <select class="form-control input-sm"
                                                                                    name="jenis_doc_u" id="jenis_doc_u">
                                                                                    <option value='INVOICE'>INVOICE
                                                                                    </option>
                                                                                    <option value='PACKING LIST'>PACKING
                                                                                        LIST</option>
                                                                                    <option value='SURAT JALAN'>SURAT
                                                                                        JALAN</option>
                                                                                    <option value='LAINNYA'>LAINNYA
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" class="form-control"
                                                                            name="no_aju_dtlu" id="no_aju_dtlu">
                                                                        <input type="hidden" class="form-control"
                                                                            name="id_dtl_u" id="id_dtl_u">
                                                                        <input type="hidden" class="form-control"
                                                                            name="aksi" id="aksi" value="upd-dtl">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Nomor Dokumen"
                                                                                id="no_doc_dtlu" name="no_doc_dtlu">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <div class="controls input-append date form_date"
                                                                                data-date=""
                                                                                data-date-format="dd MM yyyy"
                                                                                data-link-field="dtp_input2"
                                                                                data-link-format="yyyy-mm-dd">
                                                                                <input type="text" class="form-control"
                                                                                    id="tgl_doc_u"
                                                                                    placeholder="Tanggal Dokumen"
                                                                                    name="tgl_doc_u">
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-info upd-dtl" id="submit"
                                                    name="simpan">Simpan</button>
                                                </form>
                                                <button type="button" class="btn btn-default" id="submit"
                                                    data-dismiss="modal">Close</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!------------------ END UPD MODAL DETIL --------------------------->

                            <!---------------------------------------------- MODAL BARANG DETIL ------------------------------------------------>
                            <div class="modal fade" id="myModal3" tabindex="-1" role="dialog"
                                aria-labelledby="myModalTitle">
                                <div class="modal-dialog " role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalTitle">Tambah Detil Barang</h4>
                                            <form class="form-horizontal frm-brgdtl" method="post">
                                        </div>

                                        <div class="modal-body" id="myModalBody">
                                            <!-- Isi Modal -->
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table table-striped">
                                                            <tbody>
                                                                <tr>
                                                                    <td>

                                                                        <div class="form-group">
                                                                            <div class="col-md-6 col-xs-8">
                                                                                <select class="form-control input-sm"
                                                                                    name="laporan" id="laporan">
                                                                                    <option value=''>Pilih Dokumen
                                                                                    </option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" class="form-control"
                                                                            name="no_ajud" id="no_ajud">
                                                                        <input type="hidden" class="form-control"
                                                                            name="aksi" id="aksi" value="save-brgdtl">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Pallet" id="pallet"
                                                                                name="pallet">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>

                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="No Batch / Lot"
                                                                                id="batch_no" name="batch_no">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-info simpan-brgdtl" id="submit"
                                                    name="simpan">Simpan</button>
                                                </form>
                                                <button type="button" class="btn btn-default" id="submit"
                                                    data-dismiss="modal">Close</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!------------------ END MODAL BARANG DETIL --------------------------->

                            <!---------------------------------------------- MODAL UPDATE BARANG DETIL ------------------------------------------------>
                            <div class="modal fade" id="upd_brgdtl" tabindex="-1" role="dialog"
                                aria-labelledby="myModalTitle">
                                <div class="modal-dialog " role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalTitle">Ubah Detil Barang</h4>
                                            <form class="form-horizontal frm-upd-brgdtl" method="post">
                                        </div>

                                        <div class="modal-body" id="myModalBody">
                                            <!-- Isi Modal -->
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table table-striped">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="No Dokumen"
                                                                                id="no_doc_dok_u" name="no_doc_dok_u">
                                                                        </div>
                                                                        <input type="hidden" class="form-control"
                                                                            name="no_aju_dok_u" id="no_aju_dok_u">
                                                                        <input type="hidden" class="form-control"
                                                                            name="id_brgdtl_u" id="id_brgdtl_u">
                                                                        <input type="hidden" class="form-control"
                                                                            name="aksi" id="aksi" value="upd-brgdtl">
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Pallet" id="pallet_dok_u"
                                                                                name="pallet_dok_u">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="No Batch / Lot"
                                                                                id="batch_no_dok_u"
                                                                                name="batch_no_dok_u">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-info upd-brgdtl" id="submit"
                                                name="simpan">Simpan</button>
                                            </form>
                                            <button type="button" class="btn btn-default" id="submit"
                                                data-dismiss="modal">Close</button>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <!------------------ END MODAL UPDATE BARANG DETIL --------------------------->

                            <!---------------------------------------------- MODAL BRG ------------------------------------------------>
                            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog"
                                aria-labelledby="myModalTitle">
                                <div class="modal-dialog " role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalTitle">Tambah Barang</h4>
                                            <form class="form-horizontal frm-brg" method="post">
                                        </div>

                                        <div class="modal-body" id="myModalBody">
                                            <!-- Isi Modal -->
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table table-striped">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Seri Barang"
                                                                                id="seri_barang" name="seri_barang">
                                                                            <input type="hidden" class="form-control"
                                                                                name="no_ajuc" id="no_ajuc">
                                                                            <input type="hidden" class="form-control"
                                                                                name="aksi" id="aksi" value="save-brg">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Kode Barang" id="kd_brg"
                                                                                name="kd_brg">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Nama Barang" id="nm_brg"
                                                                                name="nm_brg">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Jumlah" id="jml"
                                                                                name="jml">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Satuan" id="satuan"
                                                                                name="satuan">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="CIF" id="cif" name="cif">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Currency" id="cur"
                                                                                onkeyup="Kapital()" name="cur">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-info simpan-brg" id="submit"
                                                    name="simpan">Simpan</button>
                                                </form>
                                                <button type="button" class="btn btn-default" id="submit"
                                                    data-dismiss="modal">Close</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!------------------ END MODAL BRG --------------------------->

                            <!---------------------------------------------- UPD MODAL BRG ------------------------------------------------>
                            <div class="modal fade" id="upd_brg" tabindex="-1" role="dialog"
                                aria-labelledby="myModalTitle">
                                <div class="modal-dialog " role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalTitle">Update Barang</h4>
                                            <form class="form-horizontal frm-updbrg" method="post">
                                        </div>

                                        <div class="modal-body" id="myModalBody">
                                            <!-- Isi Modal -->
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table table-striped">
                                                            <tbody>
                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Seri Barang"
                                                                                id="seri_barang_ut"
                                                                                name="seri_barang_ut" disabled>
                                                                            <input type="hidden" class="form-control"
                                                                                name="seri_barang_u" id="seri_barang_u">
                                                                            <input type="hidden" class="form-control"
                                                                                name="no_aju_brgu" id="no_aju_brgu">
                                                                            <input type="hidden" class="form-control"
                                                                                name="aksi" id="aksi" value="upd-brg">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Kode Barang"
                                                                                id="kode_barang_u" name="kode_barang_u">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Nama Barang"
                                                                                id="nama_barang_u" name="nama_barang_u">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Jumlah" id="jml_u"
                                                                                name="jml_u">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Satuan" id="sat_u"
                                                                                name="sat_u">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="CIF" id="cif_u"
                                                                                name="cif_u">
                                                                        </div>
                                                                    </td>
                                                                </tr>

                                                                <tr>
                                                                    <td>
                                                                        <div class="col-xs-12">
                                                                            <input type="text" class="form-control"
                                                                                placeholder="Currency" id="cur_u"
                                                                                onkeyup="Kapital()" name="cur_u">
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default upd-brg" id="submit"
                                                    name="update">Update</button>
                                                </form>
                                                <button type="button" class="btn btn-default" id="submit"
                                                    data-dismiss="modal">Close</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!------------------ END UPD MODAL BRG --------------------------->

                            <!---------------------------------------------- MODAL PILIH ------------------------------------------------>
                            <div class="modal fade" id="Modal-pilih" tabindex="-1" role="dialog"
                                aria-labelledby="myModalTitle">
                                <div class="modal-dialog " role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"
                                                aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalTitle">Pilih Aju</h4>
                                            <form class="form-horizontal" method="post">
                                        </div>

                                        <div class="modal-body" id="myModalBody">
                                            <!-- Isi Modal -->
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <table class="table table-striped">
                                                            <tbody>
                                                                <tr>
                                                                    <td>

                                                                        <div class="form-group">
                                                                            <div class="col-md-8 col-xs-8">
                                                                                <select class="form-control input-sm"
                                                                                    name="laporan-pilih"
                                                                                    id="laporan-pilih">
                                                                                    <option value=''>Pilih Aju</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-info pilih-aju" id="submit"
                                                    name="simpan">Pilih</button>
                                                </form>
                                                <button type="button" class="btn btn-default" id="tutup-modal"
                                                    data-dismiss="modal">Close</button>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!------------------ END MODAL PILIH --------------------------->

                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="tab-content">
                                        <div id="stripe" class="tab-pane fade in active">
                                            <form class="form-aju" method="POST">
                                                <div class='form-row'>
                                                    <div class='form-group required'>
                                                        <label class='control-label'>Nomor Aju</label> &nbsp;
                                                        <button class='btn btn-info btn-xs aju-pilih' id='aju-pilih'
                                                            data-toggle="modal" data-target="#Modal-pilih"
                                                            type='button'>Click Me</button>
                                                        <input class='form-control' id='no_aju' name='no_aju'
                                                            type='text'>
                                                        <input type="hidden" class="form-control" name="aksi" id="aksi"
                                                            value="save-aju">
                                                        <input type="hidden" class="form-control" name="user" id="user"
                                                            value="<?php echo $username; ?>">
                                                    </div>

                                                </div>
                                                <div class='form-row'>
                                                    <div class='form-group'>
                                                        <label class='control-label'>Tanggal Aju</label>
                                                        <input class='form-control' id='tglan1' name='tglan1'
                                                            type='text' autocomplete="off">
                                                    </div>
                                                </div>
                                                <div class='form-row'>
                                                    <div class='form-group card required'>
                                                        <label class='control-label'>Nomor Daftar</label>
                                                        <input class='form-control' id='nopen' name='nopen' type='text'>
                                                    </div>
                                                </div>
                                                <div class='form-row'>
                                                    <div class='form-group'>
                                                        <label class='control-label'>Tanggal Daftar</label>
                                                        <input class='form-control' id='tglan2' name='tglan2'
                                                            type='text' autocomplete="off">
                                                    </div>
                                                    <div class='form-group'>
                                                        <label class='control-label'>Nomor NPE</label>
                                                        <input class='form-control' id='no_npe' name='no_npe'
                                                            type='text'>
                                                    </div>
                                                    <div class='form-group'>
                                                        <label class='control-label'>Tanggal NPE</label>
                                                        <input class='form-control' id='tglan3' name='tglan3'
                                                            type='text' autocomplete="off">
                                                    </div>
                                                    <div class='form-group expiration required'>
                                                        <label class='control-label'>Penerima</label>
                                                        <input class='form-control' id='penerima' name='penerima'
                                                            type='text'>
                                                    </div>
                                                    <div class='form-group expiration required'>
                                                        <label class='control-label'>Negara</label>
                                                        <input class='form-control' id='negara' name='negara'
                                                            type='text'>
                                                    </div>
                                                    <div class='form-group expiration required'>
                                                        <label class='control-label'>CIF</label>
                                                        <input class='form-control' id='cif' name='cif' type='text'>
                                                    </div>
                                                    <div class='form-group expiration required'>
                                                        <label class='control-label'>HS</label>
                                                        <input class='form-control' id='hs' name='hs' type='text'>
                                                    </div>
                                                    <div class='form-group expiration required'>
                                                        <label class='control-label'>Currency</label>
                                                        <input class='form-control' id='cur_a' name='cur_a'
                                                            onkeyup="Kapital()" type='text'>
                                                    </div>
                                                </div>


                                                <div class='form-row'>
                                                    <div class='form-group'>
                                                        <label class='control-label'></label>
                                                        <div class="col-sm-4">
                                                            <button class='form-control btn btn-primary simpan-aju'
                                                                type='button'> Simpan
                                                                →</button>
                                                        </div>
                                                        <div class="col-sm-4">
                                                            <button class='form-control btn btn-warning btn-reset'
                                                                type='button'>
                                                                Reset</button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </form>

                                        </div>
                                    </div>

                                </div>

                                <div class="col-sm-6">
                                    <label class='control-label'></label><!-- spacing -->

                                    <div class="alert alert-success" id="berhasil" role="alert" style="display:none;">
                                        <strong>Tersimpan!</strong>
                                        Data Berhasil Disimpan.
                                    </div>
                                    <div class="alert alert-warning" id="gagal" role="alert" style="display:none;">
                                        <strong>Gagal!</strong> Data
                                        Gagal Disimpan.
                                    </div>

                                    <br>


                                    <!-- tbl1 -->
                                    <div class="jumbotron jumbotron-flat">
                                        <button class='btn btn-info btn-xs' id='btn-ndtl' data-toggle="modal"
                                            style="display:none;" data-target="#myModal" type='button'> New</button>
                                        <div class="tampildata1"></div>

                                    </div>

                                    <!-- tbl2 -->
                                    <div class="jumbotron jumbotron-flat">
                                        <button class='btn btn-info btn-xs' id='btn-nbrg' data-toggle="modal"
                                            style="display:none;" data-target="#myModal2" type='button'> New</button>
                                        <div class="tampildata2"></div>

                                    </div>

                                    <!-- tbl3 -->
                                    <div class="jumbotron jumbotron-flat">
                                        <button class='btn btn-info btn-xs' id='btn-nbrgdtl' data-toggle="modal"
                                            style="display:none;" data-target="#myModal3" type='button'> New</button>
                                        <div class="tampildata3"></div>

                                    </div>
                                    <br>
                                    <div class="alert alert-info" id="lucu" role="alert" style="display:none;">
                                        Please fill form correctly :).
                                    </div>


                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </section>
        </aside>
    </div>

    <script src="<?= ASSETS_PATH ?>/js/jquery.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/arjuna.js"></script>

    <script type="text/javascript" src="<?= ASSETS_PATH ?>/DataTables/media/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="<?= JS_PATH ?>/bootstrap-datetimepicker.js" charset="UTF-8"></script>
    <script type="text/javascript" src="<?= JS_PATH ?>/bc33.js"></script>
    <script>
    function Kapital() {
        var x = document.getElementById("cur_u");
        var y = document.getElementById("cur");
        var a = document.getElementById("cur_a");
        x.value = x.value.toUpperCase();
        y.value = y.value.toUpperCase();
        a.value = a.value.toUpperCase();
    }
    </script>

</body>

</html>