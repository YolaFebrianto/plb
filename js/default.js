//browser detection
var strUserAgent = navigator.userAgent.toLowerCase(); 
var isIE = strUserAgent.indexOf("msie") > -1; 
var isNS6 = strUserAgent.indexOf("netscape6") > -1; 
var Firefox = strUserAgent.indexOf("Firefox") > -1; 
var isNS4 = !isIE && !isNS6  && parseFloat(navigator.appVersion) < 5; 

//function auto uppercase
function UppercaseOnChange(objEvent) {
	var objInput;

	if (isIE) {
		objInput = objEvent.srcElement; 
	} else {
		objInput = objEvent.target;
	}
	objInput.value = objInput.value.toUpperCase();
}

//function modulus
function my_mod(int1, int2) {	
	var hasil = int1;
	while (hasil >= int2)
		hasil -= int2;
	return hasil;
}
		
//string trim function
String.prototype.trim = function() {
	a = this.replace(/^\s+/, '');
	return a.replace(/\s+$/, '');
};

//string isFormat function
String.prototype.isFormat = function ( sType ) {
	var undef;
	var aTypes = new Array();
	aTypes['time'] = /^[0-2]?\d:[0-5]{1}\d/;
	aTypes['date'] = /^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])(19|20)\d\d/;
	aTypes['@unique'] = /^\D{4}-[\d|\D]{6}/;
	if ( aTypes[ sType ] == undefined ) {
		return sType.test( this );
	} else {
		return aTypes[ sType ].test( this );
	}
}

// Returns true if the passed value is found in the array
Array.prototype.has = function (value,caseSensitive)
{
	var i;
	for (i=0; i < this.length; i++) {
		// use === to check for Matches. ie., identical (===),
		if(caseSensitive){ //performs match even the string is case sensitive
			if (this[i].toLowerCase() == value.toLowerCase()) {
				return true;
			}
			}else{
				if (this[i] == value) {
					return true;
			}
		}
	}
	return false;
};