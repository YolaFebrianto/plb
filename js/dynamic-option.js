var vendor_name = {
    STARCOOL: ['-', 'SCI|SCU'],
    DAIKIN: ['-', 'LXE', 'LX1'],
    CARRIER: ['-', '69NT40'],
    THERMOKING: ['-', 'MAGNUM', 'CRR40', 'CSR'],
    EVERGREEN: ['-', '69NT40', '69NT40-561', '69NT40-511', '69NT40-551', '69NT40-531', '69NT40-541'],
    HAPAG: ['-', 'SCI', 'SCU', '69NT40', 'MAGNUM', 'LX1', 'LXE'],
    HYUNDAI: ['-', '69NT40', 'LXE', 'LXF', 'MAGNUM', 'CRR', 'SCU']
}

// getting the main and sub menus
var main = document.getElementById('vendor');
var sub = document.getElementById('machine');

// Trigger the Event when main menu change occurs
main.addEventListener('change', function () {
    // getting a selected option
    var selected_option = vendor_name[this.value];
    // removing the sub menu options using while loop
    while (sub.options.length > 0) {
        sub.options.remove(0);
    }

    //conver the selected object into array and create a options for each array elements 
    //using Option constructor  it will create html element with the given value and innerText
    Array.from(selected_option).forEach(function (el) {
        let option = new Option(el, el);
        //append the child option in sub menu
        sub.appendChild(option);
    });

});