// Electrical_supply : 
$(document).ready(function() {
    $('#elect1').bind('keypress', function(e) {
        var value = $('#elect1').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#elect2').bind('keypress', function(e) {
        var value = $('#elect2').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#elect3').bind('keypress', function(e) {
        var value = $('#elect3').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

//Heater_current_ampere :
$(document).ready(function() {
    $('#heat1').bind('keypress', function(e) {
        var value = $('#heat1').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#heat2').bind('keypress', function(e) {
        var value = $('#heat2').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#heat3').bind('keypress', function(e) {
        var value = $('#heat3').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

//Evaporator_motor_high_speed :
$(document).ready(function() {
    $('#evaphi1').bind('keypress', function(e) {
        var value = $('#evaphi1').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#evaphi2').bind('keypress', function(e) {
        var value = $('#evaphi2').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#evaphi3').bind('keypress', function(e) {
        var value = $('#evaphi3').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

//Condenser_fan_motor :
$(document).ready(function() {
    $('#fan1').bind('keypress', function(e) {
        var value = $('#fan1').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#fan2').bind('keypress', function(e) {
        var value = $('#fan2').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#fan3').bind('keypress', function(e) {
        var value = $('#fan3').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

//Temperature_coil :
$(document).ready(function() {
    $('#coil').bind('keypress', function(e) {
        var value = $('#coil').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

//Temperature_partlow :
$(document).ready(function() {
    $('#partlow').bind('keypress', function(e) {
        var value = $('#partlow').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

//Suction_pressure :
$(document).ready(function() {
    $('#suction').bind('keypress', function(e) {
        var value = $('#suction').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

//Discharge_pressure :
$(document).ready(function() {
    $('#disc').bind('keypress', function(e) {
        var value = $('#disc').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});


//Compressor_drive_motor_current 1 :
$(document).ready(function() {
    $('#comp11').bind('keypress', function(e) {
        var value = $('#comp11').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#comp12').bind('keypress', function(e) {
        var value = $('#comp12').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#comp13').bind('keypress', function(e) {
        var value = $('#comp13').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});


//Compressor_drive_motor_current 2:
$(document).ready(function() {
    $('#comp21').bind('keypress', function(e) {
        var value = $('#comp21').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#comp22').bind('keypress', function(e) {
        var value = $('#comp22').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#comp23').bind('keypress', function(e) {
        var value = $('#comp23').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});


//Evaporator_motor_low_speed :
$(document).ready(function() {
    $('#evaplo1').bind('keypress', function(e) {
        var value = $('#evaplo1').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#evaplo2').bind('keypress', function(e) {
        var value = $('#evaplo2').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});

$(document).ready(function() {
    $('#evaplo3').bind('keypress', function(e) {
        var value = $('#evaplo3').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});


//total_qty sparepart_add :
$(document).ready(function() {
    $('#total_qty').bind('keypress', function(e) {
        var value = $('#total_qty').val();
        var characterCode = (e.which) ? e.which : e.keyCode;

        if (characterCode == 46 && value.indexOf('.') != -1)
            return false;

        if (characterCode != 46 && characterCode > 31 && (characterCode < 48 || characterCode > 57))
            return false;

        return true;
    });
});