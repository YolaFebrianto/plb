<?php
session_start();
$_SESSION['MENU'] = 'TOOLS';
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}
$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

include 'path.php';
?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title><?= TITLE_APP ?></title>
    <link rel="shortcut icon" href="<?= IMAGES_PATH ?>/icons/logoijs.jpg">

    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap.min.css">

    <!-- start:bootstrap v3.2.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.min.css">
    <!-- start:font awesome v4.1.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/font-awesome.min.css">
    <!-- start:bootstrap reset -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap-reset.css">
    <!-- start:style arjuna -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/arjuna.css">
    <script src="<?= ASSETS_PATH ?>/js/jquery-3.1.1.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>

    <script src="<?= JS_PATH ?>/jquery.js"></script>
    <script src="<?= JS_PATH ?>/tableToExcel.js"></script>

    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/style-view.css">
    <script src="bootcode.js"></script>
    <script language="javascript">
    var ajaxRequest;

    function getAjax() //fungsi untuk mengecek AJAX pada browser
    {
        try {
            ajaxRequest = new XMLHttpRequest();
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {
                    alert("Your browser broke!");
                    return false;
                }
            }
        }
    }

    function showData() {
        var select = document.getElementById("jenis_act");
        var akitivitas = select.options[select.selectedIndex].value;

        var select = document.getElementById("jenis");
        var tipe = select.options[select.selectedIndex].value;

        var select = document.getElementById("jenisa");
        var datab = select.options[select.selectedIndex].value;

        var data = document.getElementById("data").value;

        logincat = document.getElementById("logincat").value;

        document.getElementById("form-msg").innerHTML = "<img src='progress.gif'>";
        document.getElementById("judul").style.display = 'none';
        document.getElementById("result").style.display = 'none';
        //alert('2');
        getAjax();

        ajaxRequest.open("GET", "model/get_data_plb.php?jenis=" + akitivitas + "&tipe=" + tipe + "&data=" + data +
            "&datab=" +
            datab + "&kat=" + logincat);

        ajaxRequest.onreadystatechange = function() {
            document.getElementById("judul").style.display = '';
            document.getElementById("result").style.display = '';
            document.getElementById("result").innerHTML = ajaxRequest.responseText;
            if (ajaxRequest) {
                document.getElementById("form-msg").innerHTML = "";
            }
        }
        ajaxRequest.send(null);

    }

    function tgl() {

        var select = document.getElementById("jenisa");
        var status = select.options[select.selectedIndex].value;

        if (status == 'TANGGAL') {
            document.getElementById("jenis_act").style.display = 'none';
        } else {
            document.getElementById("jenis_act").style.display = '';
        }
    }
    </script>



</head>

<body class="cl-default fixed">
    <!-- start:navbar top -->
    <header class="header">
        <!-- <a href="home.php" class="logo">Indra Jaya Swastika</a> -->
        <header class="header"><?php include("top_nav.php") ?></header>
    </header>
    <!-- end:navbar top -->

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas"><?php include("left_menu.php") ?></aside>

        <aside class="right-side">
            <section class="content">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="#"> Laporan</a></li>
                    <li><a href="#"> PLB Tools Form</a></li>
                </ol>

                <div>
                    <!-- Modal -->
                    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title" id="myModalTitle">Modal title</h4>
                                </div>
                                <div class="modal-body" id="myModalBody">
                                    ...
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading hidden-print">
                                <span class="glyphicon glyphicon-tasks" aria-hidden="true">
                                </span> PLB Tools Form
                            </div>
                            <div class="box">
                                <hr>
                                <div class="panel-body" id="pnl-filter">
                                    <form class="form-horizontal" role="form" method="post"
                                        enctype="multipart/form-data">
                                        <input type="hidden" id="username" name="username"
                                            value="<?php echo $username ?>">
                                        <input type="hidden" id="logincat" name="logincat"
                                            value="<?php echo $logincat ?>">

                                        <div class="form-group">
                                            <label class="control-label col-lg-4">Jenis Laporan</label>
                                            <div class="col-lg-2">
                                                <select class="form-control input-sm" name="jenis_act" id="jenis_act">
                                                    <?php
                                                    if ($logincat == "PLB-ADMIN") {
                                                        echo "<option value='BC 1.6'>BC 1.6</option>";
                                                        echo "<option value='BC 2.7'>BC 2.7</option>";
                                                        echo "<option value='BC 2.8'>BC 2.8</option>";
                                                        echo "<option value='BC 3.0'>BC 3.0</option>";
                                                        echo "<option value='BC 4.0'>BC 4.0</option>";
                                                        echo "<option value='BC 4.1'>BC 4.1</option>";
                                                        echo "<option value='CY'>CY</option>";
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-lg-4">Jenis</label>
                                            <div class="col-lg-2">
                                                <select class="form-control input-sm" name="jenis" id="jenis">
                                                    <option value='IN'>IN</option>
                                                    <option value='OUT'>OUT</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <select class="form-control input-sm" name="jenisa" id="jenisa"
                                                    onchange="tgl();">
                                                    <option value='INVOICE'>INVOICE</option>
                                                    <option value='INVOICE'>PACKING LIST</option>
                                                    <option value='NOAJU'>NO AJU</option>
                                                    <option value='NOPEN'>NOPEN</option>
                                                    <option value='TANGGAL'>TANGGAL</option>
                                                </select>
                                            </div>
                                            <div class="col-lg-2">
                                                <input type="text" class="form-control" name="data" value="" id="data">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-lg-4"></label>
                                            <div class="col-lg-2">
                                                <button class="form-control btn-primary" id="submit" type="button"
                                                    onClick="showData();">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-9 col-xs-offset-3" id="form-msg"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5 col-xs-9" id="judul">
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="row">
                                <div class="col-sm"></div>
                            </div>
                            <div class="table-responsive" id="result">
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </aside>

    </div>

    <script src="<?= ASSETS_PATH ?>/js/jquery.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/arjuna.js"></script>


</body>

</html>