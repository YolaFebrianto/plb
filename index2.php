<?php
	session_start();
	if (isset($_SESSION['username'])) {
		header('Location: home.php');
	}

	$username = null;
	$password = null;

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		require_once('db-inc.php');
		require_once('db-inc2.php');
		if(!empty($_POST["username"]) && !empty($_POST["password"])) {
			//$SQL = "delete from sessions where substring(session_expires,1,10) < to_char(current_date,'yyyy-mm-dd')";
			//$result = pg_query($db_, $SQL);
			
			$username = addslashes(strtolower($_POST["username"]));
			$password = addslashes($_POST["password"]);
			
			//$SQL  = "SELECT login FROM users_web WHERE login = '$username' and password = CRYPT('$password',password)";
			$SQL  = "SELECT login, kategori FROM users_web WHERE login = '$username' and password = '$password'";
			$SQL .= " AND isactive";
			$result = pg_query($db_, $SQL);
			$fields = pg_fetch_row($result);
			$login = $fields[0];
			$kategori = $fields[1];
			pg_freeresult($result);
			
			if(!empty($login)) {
				session_start();
				$session_key = session_id();
				$session_address = $_SERVER['REMOTE_ADDR'];
				$session_useragent = $_SERVER['HTTP_USER_AGENT'];
				$_SESSION['username'] = $username;
				$_SESSION['kategori'] = $kategori;
				
				echo " ".$username."<br>";
				echo " ".$kategori."<br>";
				echo " ".$session_address."<br>";
				echo " ".$session_useragent."<br>";
	
				$SQL2  = "INSERT INTO report.log_session ( username, kategori, tanggal, waktu, status, ip)
                         VALUES ( 'asdsad', 'asdsadas', current_date, current_time, 'login', '192.140.11.23')";
				
				$result = pg_query($db2_, $SQL2);
				//$fields = pg_fetch_row($result);
				//$id_users = $fields[0];
				//pg_freeresult($result);
	            
				if($result){
				header('Location: home.php');
				}
			}
			else {
				header('Location: index.php');
			}
			
		} else {
			header('Location: index.php');
		}
	} 
	else {
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">

    <title>PLB | PT. INDRA JAYA SWASTIKA</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/signin.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>

    <div class="container">

      <form class="form-signin" role="form" method="POST">
        <h2 class="form-signin-heading">Please sign in</h2>
        <input type="username" id="username" name="username" maxlength=20 class="form-control" placeholder="Username" required autofocus>
        <input type="password" id="password" name="password" maxlength=20 class="form-control" placeholder="Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->

    <script src="js/jquery.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>
<?php 
	} 
?>
