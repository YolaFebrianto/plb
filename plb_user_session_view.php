<?php
$startRec = 0;
if (isset($_POST['tglan1'])  && isset($_POST['tglan2'])) {

    require_once('db-inc2.php');

    $jenis = isset($_POST['jenis_act']);
    $tgl1 = $_POST['tglan1'];
    $tgl2 = $_POST['tglan2'];

    $sqltext = "select *, CAST(waktu as time(0)) as jam 
			from report.log_session where tanggal between '$tgl1' and '$tgl2'  and kategori in('PLB','PLB-ADMIN') ";

    $result = pg_query($db2_, $sqltext);
    $baris  = pg_num_rows($result);
    $number = $startRec;

    echo "<table class='table table-striped table-bordered data'>";
    echo    "<thead>";
    echo        "<tr>";
    echo            "<th>NO</th>";
    echo            "<th>USERNAME</th>";
    echo            "<th>KATEGORI</th>";
    echo            "<th>TANGGAL</th>";
    echo            "<th>WAKTU</th>";
    echo            "<th>STATUS</th>";
    echo            "<th>REMARK</th>";
    echo            "<th>IP</th>";
    echo        "</tr>";
    echo    "</thead>";
    echo    "<tbody>";

    while ($row = pg_fetch_assoc($result)) {
        $number = $number + 1;
        $rel = isset($row['kode_rel']);
        echo        "<tr>";
        echo            "<td>" . $number . "</td>";
        echo            "<td>" . $row['username'] . "</td>";
        echo            "<td>" . $row['kategori'] . "</td>";
        echo            "<td>" . date('d-m-Y', strtotime($row['tanggal'])) . "</td>";
        echo            "<td>" . $row['waktu'] . "</td>";
        echo            "<td>" . $row['status'] . "</td>";
        echo            "<td>" . $row['remark'] . "</td>";
        echo            "<td>" . $row['ip'] . "</td>";
        echo        "</tr>";
    }
    echo    "</tbody>";
    echo  "</table>";
} else {
    require_once('db-inc2.php');

    $sqltext = "select *, CAST(waktu as time(0)) as jam 
					from report.log_session where tanggal = current_date and kategori in('PLB','PLB-ADMIN') ";


    $result = pg_query($db2_, $sqltext);
    $baris  = pg_num_rows($result);
    $number = $startRec;

    echo "<table class='table table-striped table-bordered data'>";
    echo    "<thead>";
    echo        "<tr>";
    echo            "<th>NO</th>";
    echo            "<th>USERNAME</th>";
    echo            "<th>KATEGORI</th>";
    echo            "<th>TANGGAL</th>";
    echo            "<th>WAKTU</th>";
    echo            "<th>STATUS</th>";
    echo            "<th>REMARK</th>";
    echo            "<th>IP</th>";
    echo        "</tr>";
    echo    "</thead>";
    echo    "<tbody>";

    while ($row = pg_fetch_assoc($result)) {
        $number = $number + 1;
        $rel = isset($row['kode_rel']);
        echo        "<tr>";
        echo            "<td>" . $number . "</td>";
        echo            "<td>" . $row['username'] . "</td>";
        echo            "<td>" . $row['kategori'] . "</td>";
        echo            "<td>" . date('d-m-Y', strtotime($row['tanggal'])) . "</td>";
        echo            "<td>" . $row['waktu'] . "</td>";
        echo            "<td>" . $row['status'] . "</td>";
        echo            "<td>" . $row['remark'] . "</td>";
        echo            "<td>" . $row['ip'] . "</td>";
        echo        "</tr>";
    }
    echo    "</tbody>";
    echo  "</table>";
}