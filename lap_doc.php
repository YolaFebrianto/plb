<?php
session_start();
$_SESSION['MENU'] = 'TRANSACTION';
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}

$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];
$address = $_SERVER['REMOTE_ADDR'];

include 'path.php';
?>


<html>

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title><?= TITLE_APP ?></title>
    <link rel="shortcut icon" href="<?= IMAGES_PATH ?>/icons/logoijs.jpg">
    <!-- Bootstrap -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="assets/css/custom.min.css" rel="stylesheet">
	<link href="assets/css/select2.min.css" rel="stylesheet" /> 
	<link href="assets/css/select2-bootstrap.css" rel="stylesheet" /> 
	<link href="assets/css/prettify.css" rel="stylesheet" /> 
	<link href="assets/css/jquery-ui.css" rel="stylesheet" type="text/css">
    <!-- start:style arjuna -->
    <link rel="stylesheet" type="text/css" href="assets/css/arjuna.css">
	<script src="js/jquery-1.8.3.min.js" type="text/javascript"></script>
	<script src="js/jquery.js"></script>
	<script src="js/bootstrap.min.js"></script>
    <script src="js/bootstrap-datetimepicker.js"></script>
	<script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" language="javascript" src="assets/DataTables/media/js/jquery.dataTables.js"></script> 
	<script type="text/javascript" language="javascript" src="assets/DataTables/media/js/dataTables.bootstrap.js"></script>
    <script src="assets/js/arjuna.js"></script>
	<script src="js/tableToExcel.js"></script>
    <script type="text/javascript" src="js/lapbc33.js"></script>
</head>

<body class="cl-default fixed" style="font-family: 'Open Sans', sans-serif;">
    <!-- start:navbar top -->
    <header class="header">
        <!-- <a href="home.php" class="logo">Indra Jaya Swastika</a> -->
        <header class="header"><?php include("top_nav.php") ?></header>
    </header>
    <!-- end:navbar top -->

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas">
            <?php include("left_menu.php") ?>
        </aside>
        <aside class="right-side">
            <section class="content">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="#"> BC</a></li>
                    <li><a href="#"> BC 3.3</a></li>
                </ol>
                <div class="row">
					<div class="col-lg-12">
						<div class="panel panel-primary">
                            <div class="panel-heading hidden-print">
                                <span class="glyphicon glyphicon-tasks" aria-hidden="true">
                                </span> LAPORAN DOKUMEN
                            </div>
                            <div class="box">
                                <hr>
                                <div class="panel-body" id="pnl-filter">
                                    <form class="form-horizontal" role="form" method="POST" action="#" id="param-doc" >
                                        <input type="hidden" id="username" name="username"
                                            value="<?php echo $username ?>">
                                        <input type="hidden" id="logincat" name="logincat"
                                            value="<?php echo $logincat ?>">
                                        <input type="hidden" id="address" name="address"
                                            value="<?php echo $address ?>">

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Jenis Laporan</label>
                                            <div class="col-md-4 col-xs-6">
                                                <select class="form-control input-sm" name="jenis_doc" id="jenis_doc">
                                                    <option value='BC33'>Laporan BC 3.3</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Tanggal</label>
                                            <div class="col-md-4 col-xs-6">
                                                <input type="text" autocomplete="off" class="form-control" id="tglan1"
                                                    name="tglan1" value="<?php echo $tgl1; ?>">
                                            </div>

                                        </div>
                                        <div id="periode2" class="form-group">
                                            <label class="control-label col-xs-4">Sampai Tanggal</label>
                                            <div class="col-md-4 col-xs-6">
                                                <input type="text" autocomplete="off" class="form-control" id="tglan2"
                                                    name="tglan2" value="<?php echo $tgl2; ?>">
                                            </div>
                                        </div>

                                        <div id="dcustomer" class="form-group">
                                            <label class="control-label col-xs-4">Pemilik Barang</label>
                                            <div class="col-md-4 col-xs-6">
                                                <select class="form-control input-sm" name="cust" id="cust">
                                                   <option value="ALL" selected >SEMUA</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-6 col-xs-offset-4">
                                                <button class="form-control btn-primary" id="submit" type="button">Submit</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-6 col-xs-offset-4">
                                                <button class="form-control btn-warning" id="export" onclick ="exportXLS()" type="button">Export</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-9 col-xs-offset-3" id="form-msg"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5 col-xs-9" id="judul">
                                </div>
                            </div>
                        </div>

                    </div>	
					<div class="col-lg-12">					
						<div class="row">
							<div class="col-md-12">
								<div class="box">
									<div class="row">
										<div class="col-sm"></div>
									</div>
									<div class="table-responsive" id="result">
										<div id="view"></div>
									</div>

								</div>
							</div>
						</div>	

                    </div>

                </div>
            </section>
        </aside>
    </div>
</body>



</html>

