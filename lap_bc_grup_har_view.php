<?php
if (isset($_POST['jenis_act'])  && isset($_POST['jenis_bc'])) {
?>
<h4>Data Filter Perhari</h4>
<hr>

<?php

    require_once('db-inc2.php');

    $jenis = $_POST['jenis_act'];
    $tahun = isset($_POST['thn1']);
    $jenisbc = $_POST['jenis_bc'];
    $tglan1 = $_POST['tglan1'];
    $tglan2 = $_POST['tglan2'];

    // echo $jenis."<br>";
    // echo $tahun."<br>";
    // echo $jenisbc."<br>";
    $startRec = 0;

    if ($jenis == "IN-GRUP") {

        $sqltext = "SELECT date_trunc('day', tgl_bc_16) AS Month , count(distinct(B.no_aju)) AS ECCO,
                count(distinct(C.no_aju)) AS BENING, count(distinct(D.no_aju)) AS TANJUNG,
                count(distinct(E.no_aju)) AS CARGILL, count(distinct(F.id_ecco)) AS UNIT_ECCO,
                count(distinct(G.id_bening)) AS UNIT_Bening, count(distinct(H.id_tanjung)) AS UNIT_tanjung,
                count(distinct(I.id_cargill)) AS UNIT_cargill

                FROM report.plb_flowbrg A

                left join (select distinct no_aju
                from report.plb_flowbrg
                where kode_rel = '500200'
                and id_flowbrg_in is null
                and no_bl is not null
                group by 1) B on A.no_aju = B.no_aju

                left join (select distinct no_aju
                from report.plb_flowbrg
                where kode_rel = '500300'
                and id_flowbrg_in is null
                group by 1) C on A.no_aju = C.no_aju

                left join (select distinct no_aju
                from report.plb_flowbrg
                where kode_rel = '501400'
                and id_flowbrg_in is null
                and no_bl is not null
                group by 1) D on A.no_aju = D.no_aju

                left join (select distinct A.no_aju
                from report.plb_flowbrg A
                --join report.plb_documents_in B on A.no_aju = substring(B.nomor_aju,15)
                --and B.uraian_dokumen = 'INVOICE'
                where A.kode_rel = '501600'
                and A.id_flowbrg_in is null
                and exim like '107%'
                group by 1) E on A.no_aju = E.no_aju

                left join (select distinct split_part(A.id_flowbrg,'-',1) as id_ecco, A.no_aju
                from report.plb_flowbrg A
                where A.kode_rel = '500200'
                and A.id_flowbrg_in is null
                and no_bl is not null
                group by 1,2) F on A.no_aju = F.no_aju

                left join (select distinct split_part(A.id_flowbrg,'-',1) as id_bening, A.no_aju
                from report.plb_flowbrg A
                where A.kode_rel = '500300'
                and A.id_flowbrg_in is null
                group by 1,2) G on A.no_aju = G.no_aju

                left join (select distinct split_part(A.id_flowbrg,'-',1) as id_tanjung, A.no_aju
                from report.plb_flowbrg A
                where A.kode_rel = '501400'
                and A.id_flowbrg_in is null
                and no_bl is not null
                group by 1,2) H on A.no_aju = H.no_aju

                left join (select distinct split_part(A.id_flowbrg,'-',1) as id_cargill, A.no_aju
                from report.plb_flowbrg A
                where A.kode_rel = '501600'
                and A.id_flowbrg_in is null
                and exim like '107%'
                group by 1,2) I on A.no_aju = I.no_aju
      
                WHERE A.tgl_bc_16 between '$tglan1' and '$tglan2'
                AND A.id_flowbrg_in is null
                GROUP BY Month
                ORDER BY Month";

        //echo $sqltext;


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<br>";
        echo "<div class='container'>";
        echo "<table border ='1' class='table table-striped'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>No</th>";
        echo "<th>Periode</th>";
        echo "<th>Dokumen Ecco</th>";
        echo "<th>Jumlah Kendaraan Ecco</th>";
        echo "<th>Dokumen Bening</th>";
        echo "<th>Jumlah Kendaraan Bening</th>";
        echo "<th>Dokumen Tanjung Sari</th>";
        echo "<th>Jumlah Kendaraan Tanjung</th>";
        echo "<th>Dokumen Cargill</th>";
        echo "<th>Jumlah Kendaraan Cargill</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . substr($row['month'], 0, 10) . "</td>";
            echo            "<td>" . $row['ecco'] . "</td>";
            echo            "<td>" . $row['unit_ecco'] . "</td>";
            echo            "<td>" . $row['bening'] . "</td>";
            echo            "<td>" . $row['unit_bening'] . "</td>";
            echo            "<td>" . $row['tanjung'] . "</td>";
            echo            "<td>" . $row['unit_tanjung'] . "</td>";
            echo            "<td>" . $row['cargill'] . "</td>";
            echo            "<td>" . $row['unit_cargill'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
        echo  "</div>";
    } else if ($jenis == "OUT-GRUP" && $jenisbc == "27") {

        $sqltext = "SELECT date_trunc('day', tgl_doc_out) AS Month , count(distinct(B.no_aju_out)) AS ECCO,
                count(distinct(C.no_aju_out)) AS BENING, count(distinct(D.no_aju_out)) AS TANJUNG,
                count(distinct(E.no_aju_out)) AS CARGILL, count(distinct(A.id_flowbrg_splitted)) AS UNIT

                FROM report.plb_flowbrg A

                left join (select distinct no_aju_out
                from report.plb_flowbrg
                where kode_rel = '500200'
                and id_flowbrg_in is not null
                group by 1) B on A.no_aju_out = B.no_aju_out

                left join (select distinct no_aju_out
                from report.plb_flowbrg
                where kode_rel = '500300'
                and id_flowbrg_in is not null
                group by 1) C on A.no_aju_out = C.no_aju_out

                left join (select distinct no_aju_out
                from report.plb_flowbrg
                where kode_rel = '501400'
                and id_flowbrg_in is not null
                group by 1) D on A.no_aju_out = D.no_aju_out

                left join (select distinct A.no_aju_out
                from report.plb_flowbrg A
                --join report.plb_documents_in B on A.no_aju = substring(B.nomor_aju,15)
                --and B.uraian_dokumen = 'INVOICE'
                where A.kode_rel = '501600'
                and A.id_flowbrg_in is not null
                --and exim like '107%'
                group by 1) E on A.no_aju_out = E.no_aju_out
      
                WHERE A.tgl_doc_out between '$tglan1' and '$tglan2'
                AND A.id_flowbrg_in is not null
                AND A.doc_out = 'BC 2.7'
                GROUP BY Month
                ORDER BY Month";

        //echo $sqltext;


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<br>";
        echo "<div class='container'>";
        echo "<table class='table table-striped'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>No</th>";
        echo "<th>Periode</th>";
        echo "<th>Dokumen Ecco</th>";
        echo "<th>Bening</th>";
        echo "<th>Dokumen Tanjung Sari</th>";
        echo "<th>Dokumen Cargill</th>";
        echo "<th>Jumlah Kendaraan</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . substr($row['month'], 0, 10) . "</td>";
            echo            "<td>" . $row['ecco'] . "</td>";
            echo            "<td>" . $row['bening'] . "</td>";
            echo            "<td>" . $row['tanjung'] . "</td>";
            echo            "<td>" . $row['cargill'] . "</td>";
            echo            "<td>" . $row['unit'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
        echo  "</div>";
    } else if ($jenis == "OUT-GRUP" && $jenisbc == "28") {

        $sqltext = "SELECT date_trunc('day', tgl_doc_out) AS Month , count(distinct(B.no_aju_out)) AS ECCO,
                count(distinct(C.no_aju_out)) AS BENING, count(distinct(D.no_aju_out)) AS TANJUNG,
                count(distinct(E.no_aju_out)) AS CARGILL, count(distinct(A.id_flowbrg_splitted)) AS UNIT

                FROM report.plb_flowbrg A

                left join (select distinct no_aju_out
                from report.plb_flowbrg
                where kode_rel = '500200'
                and id_flowbrg_in is not null
                group by 1) B on A.no_aju_out = B.no_aju_out

                left join (select distinct no_aju_out
                from report.plb_flowbrg
                where kode_rel = '500300'
                and id_flowbrg_in is not null
                group by 1) C on A.no_aju_out = C.no_aju_out

                left join (select distinct no_aju_out
                from report.plb_flowbrg
                where kode_rel = '501400'
                and id_flowbrg_in is not null
                group by 1) D on A.no_aju_out = D.no_aju_out

                left join (select distinct A.no_aju_out
                from report.plb_flowbrg A
                --join report.plb_documents_in B on A.no_aju = substring(B.nomor_aju,15)
                --and B.uraian_dokumen = 'INVOICE'
                where A.kode_rel = '501600'
                and A.id_flowbrg_in is not null
                --and exim like '107%'
                group by 1) E on A.no_aju_out = E.no_aju_out
      
                WHERE A.tgl_doc_out between '$tglan1' and '$tglan2'
                AND A.id_flowbrg_in is not null
                AND A.doc_out = 'BC 2.8'
                GROUP BY Month
                ORDER BY Month";

        //echo $sqltext;


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<br>";
        echo "<div class='container'>";
        echo "<table class='table table-striped'>";
        echo "<thead>";
        echo "<tr>";
        echo "<th>No</th>";
        echo "<th>Periode</th>";
        echo "<th>Dokumen Ecco</th>";
        echo "<th>Dokumen Bening</th>";
        echo "<th>Dokumen Tanjung Sari</th>";
        echo "<th>Dokumen Cargill</th>";
        echo "<th>Jumlah Kendaraan</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . substr($row['month'], 0, 10) . "</td>";
            echo            "<td>" . $row['ecco'] . "</td>";
            echo            "<td>" . $row['bening'] . "</td>";
            echo            "<td>" . $row['tanjung'] . "</td>";
            echo            "<td>" . $row['cargill'] . "</td>";
            echo            "<td>" . $row['unit'] . "</td>";
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
        echo  "</div>";
    }
}

?>