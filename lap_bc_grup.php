<?php
session_start();
$_SESSION['MENU'] = 'TOOLS';
if (!isset($_SESSION['username'])) {
    header('Location: index.php');
}

$username = $_SESSION['username'];

require_once('db-inc.php');
$query = "select perusahaan,kategori from users_web where login = '$username' ";
$result = pg_query($db_, $query);
$cust = pg_fetch_row($result);
$relasi = $cust[0];
$logincat = $cust[1];

include 'path.php';
?>


<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <title><?= TITLE_APP ?></title>
    <link rel="shortcut icon" href="<?= IMAGES_PATH ?>/icons/logoijs.jpg">

    <script src="<?= JS_PATH ?>/jquery.js"></script>
    <script src="<?= JS_PATH ?>/tableToExcel.js"></script>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" type="text/css" href="<?= CSS_PATH ?>/bootstrap.min.css">
    <!-- start:bootstrap v3.2.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap.min.css">
    <!-- start:font awesome v4.1.0 -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/font-awesome.min.css">
    <!-- start:bootstrap reset -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/bootstrap-reset.css">
    <!-- start:style arjuna -->
    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/arjuna.css">

    <script src="<?= ASSETS_PATH ?>/js/jquery-3.1.1.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>

    <link rel="stylesheet" type="text/css" href="<?= ASSETS_PATH ?>/css/style-view.css">
    <script src="bootcode.js"></script>

    <script language="javascript">
    var ajaxRequest;

    function getAjax() //fungsi untuk mengecek AJAX pada browser
    {
        try {
            ajaxRequest = new XMLHttpRequest();
        } catch (e) {
            try {
                ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
            } catch (e) {
                try {
                    ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
                } catch (e) {
                    alert("Your browser broke!");
                    return false;
                }
            }
        }
    }

    function showData() {

        var select = document.getElementById("jenis_act");
        var akitivitas = select.options[select.selectedIndex].value;
        var select = document.getElementById("thn1");
        var tahun = select.options[select.selectedIndex].value;

        document.getElementById("form-msg").innerHTML = "<img src='progress.gif'>";
        document.getElementById("judul").style.display = 'none';
        document.getElementById("result").style.display = 'none';
        getAjax();

        if (akitivitas != "") {
            ajaxRequest.open("GET", "'model/get_data.php?jenis=" + akitivitas + "&tahun=" + tahun);
            ajaxRequest.onreadystatechange = function() {

                document.getElementById("judul").style.display = '';
                document.getElementById("result").style.display = '';
                document.getElementById("judul").innerHTML = "LAPORAN " + akitivitas + " TAHUN " + tahun;
                document.getElementById("result").innerHTML = ajaxRequest.responseText;
                if (ajaxRequest) {
                    document.getElementById("form-msg").innerHTML = "";
                }
            }
            ajaxRequest.send(null);
        }

    }

    function ekspor() {

        var select = document.getElementById("jenis_act");
        var akitivitas = select.options[select.selectedIndex].value;
        var select = document.getElementById("thn1");
        var tahun = select.options[select.selectedIndex].value;

        getAjax();

        if (akitivitas != "") {
            ajaxRequest.open("GET", "'model/get_data.php?jenis=" + akitivitas + "&datab=ekspor&tahun=" + tahun);
            ajaxRequest.onreadystatechange = function() {

                //document.getElementById("judul").style.display = '';
                //document.getElementById("result").style.display = '';	  
                //document.getElementById("judul").innerHTML  = "LAPORAN "+akitivitas+" TAHUN "+tahun;
                document.getElementById("result").innerHTML = ajaxRequest.responseText;
                if (ajaxRequest) {
                    document.getElementById("form-msg").innerHTML = "";
                }
            }
            ajaxRequest.send(null);
        }

    }

    function filter_doc() {
        var select = document.getElementById("jenis_filter");
        var akitivitas = select.options[select.selectedIndex].value;

        if (aktivitas == "HARIAN") {
            document.getElementById("thn1").style.display = 'none';
        } else {
            document.getElementById("thn1").style.display = '';
        }

    }
    </script>


</head>

<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {

    $jenis = $_POST['jenis_act'];
    $tahuna = $_POST['thn1'];
    $jenisbc = $_POST['jenis_bc'];
    $jenis_filter = isset($_POST['jenis_filter']);
}
?>

<body class="cl-default fixed">
    <!-- start:navbar top -->
    <header class="header">
        <!-- <a href="home.php" class="logo">Indra Jaya Swastika</a> -->
        <header class="header"><?php include("top_nav.php") ?></header>
    </header>
    <!-- end:navbar top -->

    <div class="wrapper row-offcanvas row-offcanvas-left">
        <aside class="left-side sidebar-offcanvas"><?php include("left_menu.php") ?></aside>

        <aside class="right-side">
            <section class="content">
                <ol class="breadcrumb">
                    <li><a href="#"><i class="fa fa-home"></i> Dashboard</a></li>
                    <li><a href="#"> Laporan</a></li>
                    <li><a href="#"> Laporan Jumlah</a></li>
                </ol>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading hidden-print"><span class="glyphicon glyphicon-tasks"
                                    aria-hidden="true"></span> FILTER PER BULAN</div>
                            <div class="box">

                                <hr>
                                <div class="panel-body" id="pnl-filter">
                                    <form class="form-horizontal" role="form" method="POST" action="#">
                                        <input type="hidden" id="username" name="username"
                                            value="<?php echo $username ?>">
                                        <input type="hidden" id="logincat" name="logincat"
                                            value="<?php echo $logincat ?>">

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Jenis Filter</label>
                                            <div class="col-md-4 col-xs-6">
                                                <a href="lap_bc_grup_har.php"><button class="form-control btn-success"
                                                        id="submit" type="button">Harian</button></a>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="control-label col-xs-4">Jenis Laporan</label>
                                            <div class="col-md-4 col-xs-6">
                                                <select class="form-control input-sm" name="jenis_act" id="jenis_act">
                                                    <?php
                                                    if (isset($jenis) && $jenis == "IN-GRUP") {
                                                        echo "<option value='IN-GRUP'>IN</option>";
                                                        echo "<option value='OUT-GRUP'>OUT</option>";
                                                    } else if (isset($jenis) && $jenis == "OUT-GRUP") {
                                                        echo "<option value='OUT-GRUP'>OUT</option>";
                                                        echo "<option value='IN-GRUP'>IN</option>";
                                                    } else {
                                                    ?>
                                                    <option value="IN-GRUP">IN</option>
                                                    <option value="OUT-GRUP">OUT</option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <?php
                                        $day = date("d");
                                        $month = date("m");
                                        $year = date("Y");
                                        $bulan = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
                                        $tahun = array($year - 5, $year - 4, $year - 3, $year - 2, $year - 1, $year);
                                        ?>

                                        <div id="tahun_tok" class="form-group">
                                            <label class="control-label col-xs-4">Tahun</label>
                                            <div class="col-md-4 col-xs-6">

                                                <select name="thn1" id="thn1" class="form-control input-sm">
                                                    <?php

                                                    for ($i = 0; $i <= count($tahun) - 1; $i++) {
                                                        if ($year == $tahun[$i]) {
                                                    ?>
                                                    <option value=<?php echo $tahun[$i] ?> selected>
                                                        <?php echo $tahun[$i] ?>
                                                    </option>
                                                    <?php
                                                        } else {
                                                        ?>
                                                    <option value=<?php echo $tahun[$i] ?> selected>
                                                        <?php echo $tahun[$i] ?>
                                                    </option>
                                                    <?php
                                                        }
                                                    }

                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group" id="bc">
                                            <label class="control-label col-xs-4">Jenis BC</label>
                                            <div class="col-md-4 col-xs-6">
                                                <select class="form-control input-sm" name="jenis_bc" id="jenis_bc">
                                                    <?php
                                                    if (isset($jenisbc) && $jenisbc == "27") {
                                                        echo "<option value='27'>BC 2.7</option>";
                                                        echo "<option value='28'>BC 2.8</option>";
                                                    } else if (isset($jenisbc) && $jenisbc == "28") {
                                                        echo "<option value='28'>BC 2.8</option>";
                                                        echo "<option value='27'>BC 2.7</option>";
                                                    } else {
                                                    ?>
                                                    <option value="27">BC 2.7</option>
                                                    <option value="28">BC 2.8</option>
                                                    <?php
                                                    }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-6 col-xs-offset-4">
                                                <button class="form-control btn-primary" id="submit"
                                                    type="submit">Submit</button>
                                            </div>
                                        </div>

                                    </form>
                                    <?php
                                    if (isset($jenis)) {
                                    ?>
                                    <form class="form-horizontal" role="form" method="POST"
                                        action="model/cetak_lap_grup.php">
                                        <div class="form-group">
                                            <div class="col-md-2 col-xs-6 col-xs-offset-4">
                                                <input type="hidden" class="form-control" id="tahund" name="tahund"
                                                    value="<?php echo $tahuna; ?>">
                                                <input type="hidden" class="form-control" id="jenisc" name="jenisc"
                                                    value="<?php echo $jenis; ?>">
                                                <input type="hidden" class="form-control" id="datab" name="datab"
                                                    value="<?php echo $jenisbc; ?>">
                                                <button class="form-control btn-warning" id="submit"
                                                    type="submit">Export</button>
                                            </div>
                                        </div>

                                    </form>
                                    <?php
                                    } else {
                                    }
                                    ?>
                                </div>

                            </div>
                            <div class="form-group">
                                <div class="col-xs-9 col-xs-offset-3" id="form-msg"></div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-xs-9" id="judul">
                            </div>
                        </div>
                    </div>

                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="box">
                            <div class="row">
                                <div class="col-sm"></div>
                            </div>
                            <div class="table-responsive" id="result">
                                <div id="view"><?php include "lap_bc_grup_view.php"; ?></div>
                            </div>

                        </div>
                    </div>
                </div>
            </section>
        </aside>
    </div>

    <script src="<?= ASSETS_PATH ?>/js/jquery.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/bootstrap.min.js"></script>
    <script src="<?= ASSETS_PATH ?>/js/arjuna.js"></script>


    <script language="javascript">
    function show_change() {
        if (document.getElementById("jenis_act").value == 'STOCK') {
            document.getElementById("periode2").style.display = 'none';

        } else {
            document.getElementById("periode2").style.display = '';
        }

        if (document.getElementById("jenis_act").value == 'PLAN') {
            document.getElementById("dcustomer").style.display = 'none';
        } else {
            document.getElementById("dcustomer").style.display = '';
        }

    }

    function exportXLS() {
        var namafile = "LAPORAN " + document.getElementById("jenis_act").value + ".xls";
        tableToExcel('data_table', document.getElementById("jenis_act").value, namafile);
    }
    </script>

</body>

</html>