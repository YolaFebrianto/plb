<?php
		
	
?>
<script src="bootcode.js"></script>
<script language="javascript" >
var ajaxRequest;
function getAjax() //fungsi untuk mengecek AJAX pada browser
{
	try
	{
		ajaxRequest = new XMLHttpRequest();
	}
	catch (e)
	{
		try
		{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e) 
		{
			try
			{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e)
			{
				alert("Your browser broke!");
				return false;
			}
		}
	}
}


</script>


<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">

    <title>PLB | PT. INDRA JAYA SWASTIKA</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	<style>
		a:hover { text-decoration : none; background-color: #F2B968; }
	.kecil {font-size: 9px}
	.sedang_kecil {font-size: 10px}
	.sedang {font-size: 11px}
	.sedang_besar {font-size: 12px}
	.style4 {font-size: 10px}
	.style5 {font-size: 11px}
	.style6 {font-size: 12px}
	.style7 {font-size: 12px}
-->

    </style>
	
	
	<script type="text/javascript">

    window.onload = function(){
    var button1 = document.getElementById('ecco');
	var button2 = document.getElementById('cargil');
	
	function (Click){
	   button1.click();
	   button2.click();
   }
	
    var myVar = setInterval(Click, 3000);
   };

     </script>
	 
	 
  </head>

  <body>
    <div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<h2 class="text-center">PUSAT LOGISTIK BERIKAT</h2>
				<h3 class="text-center">PT. INDRA JAYA SWASTIKA</h3>
			</div>
	    </div>
		
		<div class="row hidden-print">
			<div class="col-md-8 col-md-offset-2 col-xs-12">
				<div class="panel panel-primary">
					<div class="panel-heading hidden-print"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> FILTER</div>
					<div class="panel-body" id="pnl-filter">
						<form class="form-horizontal" role="form" method="POST">
							
							<div class="form-group">
								<div class="col-md-5 col-xs-6 col-xs-offset-3">
								<input type="submit" class="btn btn-block btn-primary" name="ecco" id="ecco" value="ecco" />
								</div>
								<div class="col-md-5 col-xs-6 col-xs-offset-3">
								<input type="submit" class="btn btn-block btn-primary" name="cargil" id="cargil" value="cargil" />
								</div>
							</div>

						</form>

					</div>
				</div>

			</div>
		</div>
    </div> <!-- /container -->
<div class="col-md-15" id="result">
</div>
<label class="sedang_kecil"> Copy right @ 2017</label>
<br>

<?php
if(isset($_POST['ecco'])) {
	require_once('db-inc2.php');
	
	$query1 ="SELECT distinct a.no_aju_out,a.no_doc_out,a.batch_no,b.seri_barang
              FROM report.plb_flowbrg a
              join report.plb_documents_outbc27 b on a.no_aju_out = b.aju_short
              and a.no_doc_out = b.nomor_daftar and a.batch_no = b.nomor_dokumen and b.uraian_dokumen = 'INVOICE'
              where a.kode_rel in ('500200','501500','500100')
              and a.id_flowbrg_in is not null
              and a.tgl_doc_out between current_date-7 and current_date
              order by 1,4";
	
	$result1 = pg_query($db2_, $query1);
	if ($baris = pg_num_rows($result1) == 0){
			echo "Belum ada data";
		}else{		
		while($data = pg_fetch_array($result1)){
			$no_aju = $data['no_aju_out'];
			$nopen = $data['no_doc_out'];
			$batch_no = $data['batch_no'];
			$seri = $data['seri_barang'];
			
			/**
			echo $no_aju."<br>";
			echo $nopen."<br>";
			echo $batch_no."<br>";
			echo $seri."<br>";
			**/
			
		$query2 ="select distinct *
                  from report.bc27_fix
                  where no_aju_out = '$no_aju'
				  and no_doc_out = '$nopen'
				  and batch_no = '$batch_no'
				  and seri_barang_tpb = '$seri'
                  order by no_aju_out";
		
		$result2 = pg_query($db2_, $query2);
		
		if ($row = pg_num_rows($result2) == 0){
			
			$query3 ="insert into report.bc27_fix(
			no_aju_out,no_doc_out,tgl_aju_out,tgl_doc_out,id_flowbrg_splitted,batch_no,keterangan,kode_rel,
            satuan,hs_tpb,uraian_barang_tpb,satuan_tpb,kode_barang_tpb,currency_tpb,tgl_awal,no_unit,
            size_code,nopol,nama,no_bc_16,tgl_bc_16,no_bl,tgl_bl,jumlah_wh,jam_awal,cif_tpb,seri_barang_tpb,
            harga_penyerahan_tpb,jumlah_tpb,kendaraan,tgl_dokumen_tpb)
			
                  select distinct a.no_aju_out,a.no_doc_out,a.tgl_aju_out,a.tgl_doc_out,a.id_flowbrg_splitted,
                  a.batch_no,a.keterangan,a.kode_rel,a.satuan,
                  b.hs as hs_tpb,b.uraian_barang as uraian_barang_tpb,
                  b.satuan as satuan_tpb,b.kode_barang as kode_barang_tpb,b.currency as currency_tpb,
                  c.tgl_awal,c.no_unit,c.sizecode as size_code,c.nopol,
                  d.nama,
                  e.no_bc_16,e.tgl_bc_16,e.no_bl,e.tgl_bl,
                  pack.jumlah_wh,
                  c.jam_awal,
                  case when b.cif is null then '0.00' else b.cif end as cif_tpb,
                  case when b.seri_barang is null then 1 else b.seri_barang end as seri_barang_tpb,
                  case when b.harga_penyerahan is null then '0.00' else b.harga_penyerahan end as harga_penyerahan_tpb,
                  case when b.jumlah is null then 0 else b.jumlah end as jumlah_tpb,
                  c.kendaraan,
                  case when b.tanggal_dokumen is null then '2017-01-01' else b.tanggal_dokumen end as tgl_dokumen_tpb

                  from report.plb_flowbrg a
                  join report.plb_documents_outbc27 b on a.no_aju_out = b.aju_short
                  and a.no_doc_out = b.nomor_daftar and a.batch_no = b.nomor_dokumen and b.uraian_dokumen = 'INVOICE'
                  join wh_flowcont c on a.id_flowbrg_splitted = c.id_flowcont
                  join v_customer d on a.kode_rel = d.kode_rel
                  join report.plb_flowbrg e on a.id_flowbrg_in = e.id_flowbrg
                  join (select Z.no_aju_out, sum(Z.jumlah) as jumlah_wh
		          from report.plb_flowbrg Z
		          where Z.kode_rel in ('500200','501500','500100')
		          and Z.doc_out = 'BC 2.7'
		          group by 1) as pack
		          on a.no_aju_out = pack.no_aju_out

                  where a.kode_rel in ('500200','501500','500100') 
                  and a.no_aju_out = '$no_aju'
				  and a.no_doc_out = '$nopen'
				  and a.batch_no = '$batch_no'
				  and b.seri_barang = '$seri'
                  order by no_aju_out";
	        $result3 = pg_query($db2_, $query3);
			
			
			if ($result3) {
				echo "AJU ".$no_aju." Berhasil ditambah <br>";
			}else{
				echo "AJU ".$no_aju." Gagal ditambah <br>";
			}
		}else{
			echo "Data Aju ".$no_aju." Sudah Ada <br>";
		}
	}
		
		}
	
}else{
	
	require_once('db-inc2.php');
	
	$query1 ="SELECT distinct a.no_aju_out,a.no_doc_out,a.exim as batch_no,b.seri_barang
              FROM report.plb_flowbrg a
              join report.plb_documents_outbc27 b on a.no_aju_out = substring(b.no_aju,15)
              and a.no_doc_out = b.nomor_daftar and a.exim = b.nomor_dokumen and b.uraian_dokumen in ('PACKING LIST','INVOICE')
              where a.kode_rel = '501600'
              and a.id_flowbrg_in is not null
              and a.tgl_doc_out between current_date-7 and current_date
              order by 1,4";
	
	$result1 = pg_query($db2_, $query1);
		
		if ($baris = pg_num_rows($result1) == 0){
			echo "Belum ada data";
		}else{
		while($data = pg_fetch_array($result1)){
			$no_aju = $data['no_aju_out'];
			$nopen = $data['no_doc_out'];
			$batch_no = $data['batch_no'];
			$seri = $data['seri_barang'];
			
			/**
			echo $no_aju."<br>";
			echo $nopen."<br>";
			echo $batch_no."<br>";
			echo $seri."<br>";
			**/
			
		$query2 ="select distinct *
                  from report.bc27_fix
                  where no_aju_out = '$no_aju'
				  and no_doc_out = '$nopen'
				  and batch_no = '$batch_no'
				  and seri_barang_tpb = '$seri'
                  order by no_aju_out";
		
		$result2 = pg_query($db2_, $query2);
		
		if ($row = pg_num_rows($result2) == 0){
			
			$query3 ="insert into report.bc27_fix(
			no_aju_out,no_doc_out,tgl_aju_out,tgl_doc_out,id_flowbrg_splitted,batch_no,keterangan,kode_rel,
            satuan,hs_tpb,uraian_barang_tpb,satuan_tpb,kode_barang_tpb,currency_tpb,tgl_awal,no_unit,
            size_code,nopol,nama,jumlah_wh,jam_awal,cif_tpb,seri_barang_tpb,
            harga_penyerahan_tpb,jumlah_tpb,kendaraan,tgl_dokumen_tpb)
			
               select distinct a.no_aju_out,a.no_doc_out,a.tgl_aju_out,a.tgl_doc_out,a.id_flowbrg_splitted,
               a.exim as batch_no,a.keterangan,a.kode_rel,a.satuan,
               b.hs as hs_tpb,b.uraian_barang as uraian_barang_tpb,
               b.satuan as satuan_tpb,b.kode_barang as kode_barang_tpb,b.currency as currency_tpb,
               c.tgl_awal,c.no_unit,c.sizecode as size_code,c.nopol,
               d.nama,
               pack.jumlah_wh,
               c.jam_awal,
               case when b.cif is null then '0.00' else b.cif end as cif_tpb,
               case when b.seri_barang is null then 1 else b.seri_barang end as seri_barang_tpb,
               case when b.harga_penyerahan is null then '0.00' else b.harga_penyerahan end as harga_penyerahan_tpb,
               case when b.jumlah is null then 0 else b.jumlah end as jumlah_tpb,
               c.kendaraan,
               case when b.tanggal_dokumen is null then '2017-01-01' else b.tanggal_dokumen end as tgl_dokumen_tpb
               from report.plb_flowbrg a
               join report.plb_documents_outbc27 b on a.no_aju_out = substring(b.no_aju,15)
               and a.no_doc_out = b.nomor_daftar and a.exim = b.nomor_dokumen and b.uraian_dokumen in ('INVOICE','PACKING LIST')
               join wh_flowcont c on a.id_flowbrg_splitted = c.id_flowcont
               join v_customer d on a.kode_rel = d.kode_rel
               join report.plb_flowbrg e on a.id_flowbrg_in = e.id_flowbrg
              join (select Z.no_aju_out, sum(Z.jumlah) as jumlah_wh
		      from report.plb_flowbrg Z
		      where Z.kode_rel = '501600'
		      and Z.doc_out = 'BC 2.7'
		      group by 1) as pack
		      on a.no_aju_out = pack.no_aju_out

              where a.kode_rel = '501600'
                  and a.no_aju_out = '$no_aju'
				  and a.no_doc_out = '$nopen'
				  and a.exim = '$batch_no'
				  and b.seri_barang = '$seri'
                  order by no_aju_out";
	        $result3 = pg_query($db2_, $query3);
			
			
			if ($result3) {
				echo "AJU ".$no_aju." Berhasil ditambah <br>";
			}else{
				echo "AJU ".$no_aju." Gagal ditambah <br>";
			}
		}else{
			echo "Data Aju ".$no_aju." Sudah Ada <br>";
		}
	}
		}
}

?>

  </body>
</html>
<script src="js/jquery.js"></script>


