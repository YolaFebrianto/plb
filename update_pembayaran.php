<?php
	
	require_once('db-inc.php');
	$query ="select perusahaan,kategori from users_web where login = '$username' ";
	$result = pg_query($db_, $query);
	$cust = pg_fetch_row($result);
	$relasi = $cust[0];	
	$logincat = $cust[1];	
	
?>
<script src="bootcode.js"></script>
<script language="javascript" >
var ajaxRequest;
function getAjax() //fungsi untuk mengecek AJAX pada browser
{
	try
	{
		ajaxRequest = new XMLHttpRequest();
	}
	catch (e)
	{
		try
		{
			ajaxRequest = new ActiveXObject("Msxml2.XMLHTTP");
		}
		catch (e) 
		{
			try
			{
				ajaxRequest = new ActiveXObject("Microsoft.XMLHTTP");
			}
			catch (e)
			{
				alert("Your browser broke!");
				return false;
			}
		}
	}
}


</script>


<!DOCTYPE html>
<html lang="en">
  <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="favicon.ico">

    <title>PLB | PT. INDRA JAYA SWASTIKA</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="js/html5shiv.js"></script>
      <script src="js/respond.min.js"></script>
    <![endif]-->
	<style>
		a:hover { text-decoration : none; background-color: #F2B968; }
	.kecil {font-size: 9px}
	.sedang_kecil {font-size: 10px}
	.sedang {font-size: 11px}
	.sedang_besar {font-size: 12px}
	.style4 {font-size: 10px}
	.style5 {font-size: 11px}
	.style6 {font-size: 12px}
	.style7 {font-size: 12px}
-->

    </style>
  </head>

  <body>
    <div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<h2 class="text-center">PUSAT LOGISTIK BERIKAT</h2>
				<h3 class="text-center">PT. INDRA JAYA SWASTIKA</h3>
			</div>
	    </div>

		<!-- Modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalTitle">
		  <div class="modal-dialog" role="document">
			<div class="modal-content">
			  <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalTitle">Modal title</h4>
			  </div>
			  <div class="modal-body" id="myModalBody">
				...
			  </div>
			  <div class="modal-footer">
				<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			  </div>
			</div>
		  </div>
		</div>
		
		<div class="row hidden-print">
			<div class="col-md-8 col-md-offset-2 col-xs-12">
				<div class="panel panel-primary">
					<div class="panel-heading hidden-print"><span class="glyphicon glyphicon-tasks" aria-hidden="true"></span> FILTER</div>
					<div class="panel-body" id="pnl-filter">
						<form class="form-horizontal" role="form" method="POST">
							
							<div class="form-group">
								<div class="col-md-5 col-xs-6 col-xs-offset-3">
								<input type="submit" class="btn btn-block btn-primary" name="update" value="Update" />
								</div>
							</div>

						</form>

					</div>
				</div>

			</div>
		</div>
    </div> <!-- /container -->
<div class="col-md-15" id="result">
</div>
<label class="sedang_kecil"> Copy right @ 2017</label>
<br>

<?php
if(isset($_POST['update'])) {
	require_once('db-inc2.php');
	
	$query1 ="select distinct no_aju_out, batch_no from report.plb_flowbrg
			  order by no_aju_out";
	
	$result1 = pg_query($db2_, $query1);
		
		while($data = pg_fetch_array($result1)){
			$batch_no = $data['batch_no'];
			$no_aju = $data['no_aju_out'];
			
		$query2 ="select * from report.plb_doc_in_out where no_aju = '$no_aju' and nomor_dokumen ='$batch_no' and uraian_dokumen='INVOICE'";
		
		$result2 = pg_query($db2_, $query2);
		
		if (pg_num_rows($result2) < 1){
			
			$query3 ="insert into report.plb_doc_in_out 
             (uraian_dokumen,nomor_dokumen,tanggal_dokumen_in,nama_pengangkut,nomor_voy_flight,
              nomor_daftar_in,tanggal_daftar,tanggal_tiba,uraian_negara,supplier,nomor_aju,
              tanggal_aju,jumlah,kemasan,harga,currency,nomor_aju_out,cif,cif_rupiah,no_aju,tanggal_dokumen_out,tanggal_aju_out,nomor_daftar_out)
              select distinct A.*,B.no_aju,B.cif,B.cif_rupiah,SUBSTRING(B.no_aju,15),B.tanggal_dokumen,B.tgl_aju,B.nomor_daftar
              from report.plb_documents_in A, report.plb_documents_out B
              where A.nomor_dokumen = B.nomor_dokumen
			  and A.uraian_dokumen = 'INVOICE'
			  and A.nomor_dokumen = '$batch_no'
			  and B.nomor_dokumen = '$batch_no'";
	        $result3 = pg_query($db2_, $query3);
			
			
			if ($result3) {
				echo "INVOICE ".$batch_no." Berhasil ditambah <br>";
			}else{
				echo "INVOICE ".$batch_no." Gagal ditambah <br>";
			}
		}else{
			echo "Invoice ".$batch_no." Sudah Ada <br>";
		}
	}
		
	
	/**
	$query5 ="truncate table report.plb_barang_tarif";
	$result5 = pg_query($db2_, $query5);
	
	$query ="INSERT INTO report.plb_barang_tarif (nomor_aju,bm,ppn,pph)
             select distinct nomor_aju,
             sum(case when jenis_tarif like 'BM%' then nilai_bayar else 0 end),
             sum(case when jenis_tarif like 'PPN%' then nilai_bayar else 0 end),
             sum(case when jenis_tarif like 'PPH%' then nilai_bayar else 0 end)
             from report.plb_barang_tarif_detail
             group by nomor_aju order by nomor_aju";
	$result = pg_query($db2_, $query);	
	
	$query2 ="truncate table report.plb_doc_in_out";
	$result2 = pg_query($db2_, $query2);
	
	$query3 ="insert into report.plb_doc_in_out 
             (uraian_dokumen,nomor_dokumen,tanggal_dokumen_in,nama_pengangkut,nomor_voy_flight,
              nomor_daftar,tanggal_daftar,tanggal_tiba,uraian_negara,supplier,nomor_aju,
              tanggal_aju,jumlah,kemasan,harga,currency,nomor_aju_out,cif,cif_rupiah,no_aju,tanggal_dokumen_out)
              select distinct A.*,B.no_aju,B.cif,B.cif_rupiah,SUBSTRING(B.no_aju,15),B.tanggal_dokumen
              from report.plb_documents_in A, report.plb_documents_out B
              where A.nomor_dokumen = B.nomor_dokumen";
	$result3 = pg_query($db2_, $query3);
	
	
	
	if ($result && $result3){
		
		echo "<script>
              alert('Update Berhasil!');
             </script>";
			 
	} **/
}

?>

  </body>
</html>
<script src="js/jquery.js"></script>


