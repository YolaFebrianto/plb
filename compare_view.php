<?php
if (isset($_POST['jenis_act'])) {
?>
<h4>Data Compare</h4>
<hr>

<?php
    require_once('db-inc2.php');

    $jenis = $_POST['jenis_act'];
    $tgl1 = $_POST['tglan1'];
    $tgl2 = $_POST['tglan2'];
    if ($kode_rel == "ALL") {
        $kode_rel = '50%';
    }
    $startRec = 0;

    if ($jenis == 'BC 1.6') {
        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN COMPARE DATA " . $jenis . " <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<hr>";
        echo "<br>";
        echo "<br>";

        $sqltext = "select substring(A.nomor_aju,15) as aju_tpb, B.no_aju as aju_wh
                from report.plb_documents_in A
                left join report.plb_flowbrg B on substring(A.nomor_aju,15) = B.no_aju
                where A.tanggal_daftar between '$tgl1' and '$tgl2'
                group by 1,2
                order by aju_tpb ASC";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<div class='table-responsive'>";
        echo "<table id='table' class='table table-striped table-bordered data' style='width:100%'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th>NO</th>";
        echo            "<th>AJU TPB</th>";
        echo            "<th>AJU IJS</th>";
        echo            "<th>STATUS</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['aju_tpb'] . "</td>";
            echo            "<td>" . $row['aju_wh'] . "</td>";
            if ($row['aju_wh'] == null) {
                echo            "<td>TIDAK KELUAR</td>";
            } else {
                echo            "<td>OK</td>";
            }
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
        echo  "</div>";
    } else if ($jenis == 'BC 2.7') {
        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN COMPARE DATA " . $jenis . " <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<hr>";
        echo "<br>";
        echo "<br>";


        $sqltext = "select substring(A.no_aju,15) as aju_tpb, B.no_aju_out as aju_wh
            from report.plb_documents_outbc27 A
            left join report.bc27_fix B on substring(A.no_aju,15) = B.no_aju_out
            and A.nomor_daftar = B.no_doc_out
            where A.tanggal_daftar between '$tgl1' and '$tgl2'	
            group by 1,2
            order by aju_tpb ASC";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<div class='table-responsive'>";
        echo "<table id='table' class='table table-striped table-bordered data' style='width:100%'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th>NO</th>";
        echo            "<th>AJU TPB</th>";
        echo            "<th>AJU IJS</th>";
        echo            "<th>STATUS</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['aju_tpb'] . "</td>";
            echo            "<td>" . $row['aju_wh'] . "</td>";
            if ($row['aju_wh'] == null) {
                echo            "<td>TIDAK KELUAR</td>";
            } else {
                echo            "<td>OK</td>";
            }
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
        echo  "</div>";
    } else if ($jenis == 'BC 2.8') {
        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN COMPARE DATA " . $jenis . " <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<hr>";
        echo "<br>";
        echo "<br>";

        $sqltext = "select substring(A.no_aju,15) as aju_tpb, B.no_aju_out as aju_wh
            from report.plb_documents_out A
            left join report.plb_flowbrg B on substring(A.no_aju,15) = B.no_aju_out
            and A.nomor_daftar = B.no_doc_out
			and B.doc_out = 'BC 2.8'
            where A.tanggal_daftar between '$tgl1' and '$tgl2'	
            group by 1,2
            order by aju_tpb ASC";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<div class='table-responsive'>";
        echo "<table id='table' class='table table-striped table-bordered data' style='width:100%'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th>NO</th>";
        echo            "<th>AJU TPB</th>";
        echo            "<th>AJU IJS</th>";
        echo            "<th>STATUS</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['aju_tpb'] . "</td>";
            echo            "<td>" . $row['aju_wh'] . "</td>";
            if ($row['aju_wh'] == null) {
                echo            "<td>TIDAK KELUAR</td>";
            } else {
                echo            "<td>OK</td>";
            }
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
        echo  "</div>";
    } else if ($jenis == 'BC P3BET') {
        echo "<h5> PUSAT LOGISTIK BERIKAT PT. INDRA JAYA SWASTIKA </h5>";
        echo "<h5> LAPORAN COMPARE DATA " . $jenis . " <br>";
        echo "<h5> PERIODE : " . date('d-m-Y', strtotime($tgl1)) . " S.D " . date('d-m-Y', strtotime($tgl2));
        echo "<hr>";
        echo "<br>";
        echo "<br>";


        $sqltext = "select substring(A.no_aju,15) as aju_tpb, B.no_aju_out as aju_wh
            from report.plb_documents_P3BET A
            left join report.plb_flowbrg B on substring(A.no_aju,15) = B.no_aju_out
            and B.doc_out = 'P3BET'
	        left join report.referensi_status C on A.kode_pabean = C.kode_dokumen
	        and A.kd_status = C.kode_status
            where A.tanggal_daftar between '$tgl1' and '$tgl2'	
            group by 1,2
            order by aju_tpb ASC";


        $result = pg_query($db2_, $sqltext);
        $baris  = pg_num_rows($result);
        $number = $startRec;

        echo "<div class='table-responsive'>";
        echo "<table id='table' class='table table-striped table-bordered data' style='width:100%'>";
        echo    "<thead>";
        echo        "<tr>";
        echo            "<th>NO</th>";
        echo            "<th>AJU TPB</th>";
        echo            "<th>AJU IJS</th>";
        echo            "<th>STATUS</th>";
        echo        "</tr>";
        echo    "</thead>";
        echo    "<tbody>";

        while ($row = pg_fetch_assoc($result)) {
            $number = $number + 1;
            echo        "<tr>";
            echo            "<td>" . $number . "</td>";
            echo            "<td>" . $row['aju_tpb'] . "</td>";
            echo            "<td>" . $row['aju_wh'] . "</td>";
            if ($row['aju_wh'] == null) {
                echo            "<td>TIDAK KELUAR</td>";
            } else {
                echo            "<td>OK</td>";
            }
            echo        "</tr>";
        }
        echo    "</tbody>";
        echo  "</table>";
        echo  "</div>";
    }
}