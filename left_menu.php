<section class="sidebar">
    <ul class="sidebar-menu">
        <li <?php if ($_SESSION['MENU'] == 'HOME') echo 'class="active"'; ?>>
            <a href="home.php">
                <span>Dashboard</span>
            </a>
        </li>

        <li class="treeview <?php if ($_SESSION['MENU'] == 'TRANSACTION') echo 'active';
                            if ($_SESSION['username'] == 'bc') echo 'hidden'; ?>">
            <a href="#">
                <i class="fa fa-cog"></i>
                <span>Transaction</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="input_bc27.php"><i class="fa fa-circle-o"></i> BC 2.7</a></li>
                <li><a href="input_bc33.php"><i class="fa fa-circle-o"></i> BC 3.3</a></li>
            </ul>
        </li>
        <li class="treeview <?php if ($_SESSION['MENU'] == 'TOOLS') echo 'active';
                            if ($_SESSION['username'] == 'bc') echo 'hidden'; ?>">
            <a href="#">
                <i class="fa fa-cog"></i>
                <span>Tools</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="compare.php"><i class="fa fa-circle-o"></i> Compare</a></li>
                <li><a href="plb_tools.php"><i class="fa fa-circle-o"></i> PLB Tools</a></li>
                <li><a href="verifikasi.php"><i class="fa fa-circle-o"></i> Verifikasi</a></li>
                <li><a href="lap_bc_grup.php"><i class="fa fa-circle-o"></i> Laporan Jumlah</a></li>
                <li><a href="lap_bc_grup_har.php"></a></li>
            </ul>
        </li>
        <li class="treeview <?php if ($_SESSION['MENU'] == 'REPORT') echo 'active'; ?>">
            <a href="#">
                <i class="fa fa-cog"></i>
                <span>Report</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="bc_in.php"><i class="fa fa-circle-o"></i> Laporan Pemasukan</a></li>
                <li><a href="bc_out.php"><i class="fa fa-circle-o"></i> Laporan Pengeluaran</a></li>
                <li><a href="lap_doc.php"><i class="fa fa-circle-o"></i> Laporan Dokumen</a></li>
                <li><a href="mutasibc.php"><i class="fa fa-circle-o"></i> Mutasi</a></li>
                <li><a href="stokbc.php"><i class="fa fa-circle-o"></i> Stock</a></li>
                <li><a href="dtl_lokasi.php"><i class="fa fa-circle-o"></i> Detil Lokasi</a></li>
            </ul>
        </li>
        <li class="treeview <?php if ($_SESSION['MENU'] == 'DOCUMENT') echo 'active';
                            if ($_SESSION['username'] == 'bc') echo 'hidden'; ?>">
            <a href="#">
                <i class="fa fa-cog"></i>
                <span>Dokumen</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="dokumen.php"><i class="fa fa-circle-o"></i> Dokumen</a></li>
                <li><a href="plb_document.php"><i class="fa fa-circle-o"></i> PLB Document</a></li>
            </ul>
        </li>

        <li class=" <?php if ($_SESSION['MENU'] == 'PAYMENT') echo 'active'; ?> ">
            <a href="pembayaran.php">
                <i class="fa fa-cog"></i>
                <span>Pembayaran</span>
            </a>
        </li>

        <li class=" <?php if ($_SESSION['MENU'] == 'CY') echo 'active';
                    if ($_SESSION['username'] == 'bc') echo 'hidden'; ?>">
            <a href="cy_h.php">
                <i class="fa fa-cog"></i>
                <span>CY</span>
            </a>

        </li>

        <li class="treeview <?php if ($_SESSION['MENU'] == 'USER') echo 'active'; ?>">
            <a href="#">
                <i class="fa fa-cog"></i>
                <span>User</span>
                <i class="fa fa-angle-left pull-right"></i>
            </a>
            <ul class="treeview-menu">
                <li><a href="plb_user_session.php"><i class="fa fa-circle-o"></i> User Session</a></li>
                <li><a href="userm.php"><i class="fa fa-circle-o"></i> User Manual</a></li>
            </ul>
        </li>



    </ul>
</section>